# Code Samples#

*auction_site*

White label auction site developed in OO PHP 5. 
* Bid queue
* Environment aware

*surveyvista*

Personal project. Built to assist my wife in collecting autism research data
as part of her MSc. 

Displays short videos on the screen and asks the user to provide a rating.

*Integrates Flowplayer to display videos. 
*Designed to allow for multiple projects.


*fridayreads-gather*

Personal project. Collected Twitter data from the #FridayReads meme for analysis to 
discover the most popular books. More info in the README.md file with the code.