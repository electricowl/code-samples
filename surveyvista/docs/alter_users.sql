USE surveyvista;


/************ Update: Tables ***************/

/* Table Items: users */
ALTER TABLE users DROP PRIMARY KEY;

/* Alter Columns */
ALTER TABLE users CHANGE COLUMN username username VARCHAR(60) NOT NULL;
ALTER TABLE users ADD user_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE users ADD real_name VARCHAR(100) NOT NULL;