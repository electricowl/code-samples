<?php
/**
 * File: constants.php
 * @author: Ian Lewis <electricowl@bookmeme.net>
 * Date: 21-Jun-2010
 */
define('SQL_DATE', 'YYYY-MM-DD HH:mm:ss');

define('FORM_PROJECT_INTRO',         'project_intro');
define('FORM_PROJECT_DEMOGRAPHIC',   'project_demographic');
define('FORM_PROJECT_START_DETAILS', 'project_start_details');
define('FORM_PROJECT_TEST_PAGE',     'project_test_page');
define('FORM_PROJECT_QUESTIONNAIRE', 'project_questionnaire');
define('FORM_PROJECT_THANKS',        'project_thanks');

