<?php
/**
 * Description of Abstract
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Controller_Abstract extends Zend_Controller_Action
{
    private $oStorage;

    public function getProjectId()
    {
        if ($this->getRequest()->has('project_id')) {
            return intval($this->_request->project_id);
        }
        if ($this->getRequest()->has('p')) {
            return intval($this->_request->p);
        }
        return null;
    }
}

