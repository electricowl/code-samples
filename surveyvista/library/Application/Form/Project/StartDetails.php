<?php
/**
 * @filesource StartInfo.php
 * Date: 21-Jun-2010
 * Description of StartInfo
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Project_StartDetails extends Zend_Form{

    public function init()
    {
        $this->setName(FORM_PROJECT_START_DETAILS);
        $this->setAction('/project/edit-start-details/');

        $this->addElement(
            'hidden',
            'project_id'
        );

        $this->addElement(
            'textarea',
            'details_text',
            array(
                'cols'        => 40,
                'rows'        => 25,
                'id'          => 'editable',
                'label'       => 'Start Details',
                'description' => 'Provide the start details on this form.'
            )
        );

        $this->addElement(
            'submit',
            'submit',
            array(
                'label' => 'Save'
            )
        );
    }
}

