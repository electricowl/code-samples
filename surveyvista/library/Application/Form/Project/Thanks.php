<?php
/**
 * @filesource Thanks.php
 * Date: 21-Jun-2010
 * Description of Thanks
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Project_Thanks extends Zend_Form {

    public function init()
    {
        $this->setName(FORM_PROJECT_THANKS);
        $this->setAction('/project/edit-thanks-page/');

        $this->addElement(
            'hidden',
            'project_id'
        );

        $this->addElement(
            'textarea',
            'thanks_text',
            array(
                'cols' => 40,
                'rows' => 25,
                'id'          => 'editable',
                'label'       => 'Edit The Thankyou Page',
                'description' => 'Use this to say thanks.'
            )
        );

        $this->addElement(
            'submit',
            'submit',
            array(
                'label' => 'Save'
            )
        );
    }
}

