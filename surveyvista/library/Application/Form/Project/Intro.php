<?php
/**
 * @filesource Intro.php
 * Date: 21-Jun-2010
 * Description of Intro
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Project_Intro extends Zend_Form {

    public function init()
    {
        $this->setName(FORM_PROJECT_INTRO);
        $this->setAction('/project/edit-intro/');

        $this->addElement(
            'hidden',
            'project_id'
        );

        $this->addElement(
            'textarea',
            'intro_text',
            array(
                'cols' => 40,
                'rows' => 25,
                'id'   => 'editable',
                'label'       => 'Introduction',
                'description' => 'This is the introduction to the tests.'
            )
        );

        $this->addElement(
            'submit',
            'submit',
            array(
                'label' => 'Save'
            )
        );
    }
}

