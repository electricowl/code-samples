<?php
/**
 * @filesource TestPageGuidance.php
 * Date: 21-Jun-2010
 * Description of TestPageGuidance
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Project_TestPageGuidance extends Zend_Form {

    public function init()
    {
        $this->setName(FORM_PROJECT_TEST_PAGE);
        $this->setAction('/project/edit-test-page-guidance/');

        $this->addElement(
            'hidden',
            'project_id'
        );

        $this->addElement(
            'textarea',
            'guidance_text',
            array(
                'cols'        => 40,
                'rows'        => 25,
                'id'          => 'editable',
                'label'       => 'Edit The Test Page Guidance',
                'description' => 'Use this form to provide the guidance notes for the test subject.'
            )
        );

        $this->addElement(
            'submit',
            'submit',
            array(
                'label' => 'Save'
            )
        );
    }
}

