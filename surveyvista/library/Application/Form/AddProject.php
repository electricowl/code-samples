<?php
/**
 * @filesource AddProject.php
 * Date: 20-Jun-2010
 * Description of AddProject
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_AddProject extends Zend_Form {

    public function init() {
        $this->addElement(
            'hidden',
            'project_id'
        );

        $this->addElement(
            'hidden',
            'user_id'
        );

        $this->addElement(
                'text',
                'project_name',
                array(
                    'label' => 'Project Name *',
                    'required' => true,
                    'attribs'  => array(
                        'size' => 30
                    )
                )
            );

        $this->addElement(
                'text',
                'organization',
                array(
                    'label' => 'Organization Name *',
                    'required' => true,
                    'attribs'  => array(
                        'size' => 30
                    )
                )
            );

        $this->addElement(
                'text',
                'project_description',
                array (
                    'label'    => 'Description *',
                    'required' => true,
                    'attribs'  => array(
                        'size' => 30
                    )
                )
            );

        $this->addElement(
                'textarea',
                'project_summary',
                array(
                    'label' => 'Project Summary',
                    'rows' => 5,
                    'cols' => 40
                )
            );

        $this->addElement(
                'select',
                'project_type',
                array(
                    'label' => 'Project Type',
                    'width' => 30
                )
            );
        $this->project_type->addMultiOptions(
                array(
                    1 => 'Multiple Videos',
                    2 => 'Multiple Questions'
                )
            );

        $this->addElement(
                'submit',
                'submit',
                array(
                    'label' => 'Save'
                )
            );
    }
}

