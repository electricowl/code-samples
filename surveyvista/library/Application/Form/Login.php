<?php
/**
 * Description of Login
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Login extends Zend_Form {
    //put your code here
    public function init()
    {
        $this->setMethod('post');
        $this->setName('form_login');

        $this->addElement(
            'text',
            'username',
            array(
                'label' => 'Username'
            )
        );

        $this->addElement(
            'password',
            'password',
            array(
                'label' => 'Password'
            )
        );

        $this->addElement('submit', 'Login');
    }
}

