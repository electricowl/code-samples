<?php
/**
 * @filesource Demographic.php
 * Date: 19-Jun-2010
 * Description of Demographic
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Demographic extends Zend_Form {

    public function init()
    {
        $this->addElement(
            'select',
            'example',
            array(
                'label' => 'Do you like Cheesy Peas?'
            )
        );
        $this->example->addMultiOptions(array(
                    'yes' => 'Yes',
                    'no'  => 'No'
                )
            );
    }
}

