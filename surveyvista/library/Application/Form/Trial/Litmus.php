<?php
/**
 * @filesource Litmus.php
 * Date: 22-Jun-2010
 * Description of Litmus
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_Form_Trial_Litmus extends Zend_Form {

    public function init()
    {
        $this->setAction('/trial/litmus/');

        $this->addElement(
            'hidden',
            'project_id'
        );

        // Tracker is loaded - first time - with the list of files
        $this->addElement(
                'hidden',
                'tracker'
            );
    }
}

