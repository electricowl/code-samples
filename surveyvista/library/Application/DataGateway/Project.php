<?php
/**
 * @filesource Project.php
 * Date: 20-Jun-2010
 * Description of Project
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Application_DataGateway_Project extends Zend_Db_Table_Abstract {

    const PROJECT_TABLE = 'project';
    const PROJECT_DATA_TABLE = 'project_data';
    
    public function add($iUserId, $sOrganization, $sName, $sSummary, $sDescription, $iProjectType)
    {
        $db = $this->getDefaultAdapter();
        $iProjectId = $db->insert(self::PROJECT_TABLE, array(
            'user_id'      => $iUserId,
            'organization' => $sOrganization,
            'project_name' => $sName,
            'project_summary' => $sSummary,
            'project_description' => $sDescription,
            'project_type'        => $iProjectType
        ));
        return $iProjectId;
    }

    public function listProjects($iUserId, $bIsAdmin = null) {
        $db = $this->getDefaultAdapter();
        $sSql = <<<EOS
SELECT p.project_id,
        p.organization,
        p.project_name,
        p.project_type,
        t.description
FROM project p
JOIN project_type_lu t
ON p.project_type = t.project_type_id
WHERE p.user_id = :iUserId
EOS;

        $oStatement = $db->prepare($sSql);
        $oStatement->execute(array('iUserId' => $iUserId));
        return $oStatement->fetchAll();
    }

    public function fetchProjectDetails($iProjectId)
    {
        $db = $this->getDefaultAdapter();
        $sSql = <<<EOS
SELECT p.project_id,
        p.organization,
        p.project_name,
        p.project_type,
        p.project_description,
        p.project_summary,
        t.description
FROM project p
JOIN project_type_lu t
ON p.project_type = t.project_type_id
WHERE project_id = :iProjectId
EOS;
        $oStatement = $db->prepare($sSql);
        $oStatement->execute(array('iProjectId' => $iProjectId));
        return $oStatement->fetchAll();
    }

    public function saveFormData($iProjectId, $sFormId, $sContent)
    {
        $db = $this->getDefaultAdapter();
        $sSql = <<<EOS
REPLACE INTO project_pages (project_id, form_name, content)
    VALUES (
        :iProjectId,
        :sFormId,
        :sContent
)
EOS;
        $oStatement = $db->prepare($sSql);
        $oStatement->execute(
            array(
                'iProjectId' => $iProjectId,
                'sFormId'    => $sFormId,
                'sContent'   => $sContent
            )
        );
    }

    public function fetchFormData($iProjectId, $sFormId)
    {
        $db = $this->getDefaultAdapter();
        $sSql = <<<EOS
SELECT content
    FROM project_pages
    WHERE project_id = :iProjectId
    AND form_name = :sFormId
EOS;
        $oStatement = $db->prepare($sSql);
        $oStatement->execute(
            array(
                'iProjectId' => $iProjectId,
                'sFormId'    => $sFormId
            )
        );
        return $oStatement->fetchColumn(0);
    }

    public function saveResultData($iProjectId, $iParticipantId, $sFormName, $sData, $sMetaData, $iLastResultId = null)
    {
        $oDate = new Zend_Date();
        $sDateTime = $oDate->toString(SQL_DATE);
        $db = $this->getDefaultAdapter();
        $iResultId = $db->insert(self::PROJECT_DATA_TABLE, array(
            'project_id'      => $iProjectId,
            'participant_id'  => $iParticipantId,
            'form_name'       => $sFormName,
            'data'            => $sData,
            'metadata'        => $sMetaData,
            'date_updated'    => $sDateTime
        ));
        return $iResultId;
    }
}

