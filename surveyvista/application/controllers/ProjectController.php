<?php
/**
 * @filesource ProjectController.php
 * Date: 19-Jun-2010
 * Description of ProjectController
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class ProjectController extends Application_Controller_Abstract
{
    public function init()
    {
        $this->oStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->_helper->layout->setLayout('default');
    }

    public function indexAction()
    {
        // show all projects
    }

    public function addAction()
    {
        // create form.
        $oForm = new Application_Form_AddProject();
        $oForm->setMethod('post');
        $oForm->setAction('/project/add');

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                // submit data to DB
                $oDb = new Application_DataGateway_Project();
                $oDb->add(
                    $this->oStorage->user_id,
                    $oForm->getValue('organization'),
                    $oForm->getValue('project_name'),
                    $oForm->getValue('project_summary'),
                    $oForm->getValue('project_description'),
                    $oForm->getValue('project_type')
                );
                $this->_redirect->gotoUrl('/');
            }
        }

        $this->view->oForm = $oForm;
    }

    public function editAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $aDetails = $oDb->fetchProjectDetails($iProjectId);
        $this->view->aDetails = reset($aDetails);
    }

    public function listProjectsAction()
    {
        // ajaxify
    }

    // This section is where the forms are rendered for each section
    public function editIntroAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_INTRO
        );
        $oForm = new Application_Form_Project_Intro();
        $oForm->project_id->setValue($iProjectId);
        $oForm->intro_text->setValue($sText);

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                // submit data to DB
                $oDb->saveFormData(
                        $oForm->getValue('project_id'),
                        FORM_PROJECT_INTRO,
                        $oForm->getValue('intro_text')
                    );
                
                $this->_redirect("/project/edit/p/$iProjectId");
            }
        }
        $this->view->oForm = $oForm;
    }

    public function editStartDetailsAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_START_DETAILS
        );

        $oForm = new Application_Form_Project_StartDetails();
        $oForm->project_id->setValue($iProjectId);
        $oForm->details_text->setValue($sText);

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                // submit data to DB
                $oDb->saveFormData(
                        $oForm->getValue('project_id'),
                        FORM_PROJECT_START_DETAILS,
                        $oForm->getValue('details_text')
                    );
                $this->_redirect('/project/edit/p/' . $iProjectId);
            }
        }
        $this->view->oForm = $oForm;
    }

    public function editTestPageGuidanceAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_TEST_PAGE
        );

        $oForm = new Application_Form_Project_TestPageGuidance();
        $oForm->project_id->setValue($iProjectId);
        $oForm->guidance_text->setValue($sText);
        
        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                // submit data to DB
                $oDb->saveFormData(
                        $oForm->getValue('project_id'),
                        FORM_PROJECT_TEST_PAGE,
                        $oForm->getValue('guidance_text')
                    );
                $this->_redirect('/project/edit/p/' . $iProjectId);
            }
        }
        $this->view->oForm = $oForm;
    }

    public function editThanksPageAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_THANKS
        );

        $oForm = new Application_Form_Project_Thanks();
        $oForm->project_id->setValue($iProjectId);
        $oForm->thanks_text->setValue($sText);

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                // submit data to DB
                $oDb->saveFormData(
                        $oForm->getValue('project_id'),
                        FORM_PROJECT_THANKS,
                        $oForm->getValue('thanks_text')
                    );
                $this->_redirect('/project/edit/p/' . $iProjectId);
            }
        }
        $this->view->oForm = $oForm;
    }

    public function preDispatch()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->redirector('index', 'login');
        }
    }
}

