<?php
/**
 * Description of IndexController
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class AdminController extends Application_Controller_Abstract {
    //put your code here
    public function init()
    {
        $this->oStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->_helper->layout->setLayout('admin');
    }

    public function indexAction()
    {
        $this->view->oRequest = $this->getRequest();
    }

    public function preDispatch()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->redirector('index', 'login');
        }
    }
}

