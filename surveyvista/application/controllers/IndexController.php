<?php
/**
 * Default index controller
 */
class IndexController extends Application_Controller_Abstract
{

    public function init()
    {
        /* Initialize action controller here */
        $this->oStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->_helper->layout->setLayout('default');
    }

    public function indexAction()
    {
        if ($this->oStorage->user_id) {
            $iUserId   = $this->oStorage->user_id;
            $oDb       = new Application_DataGateway_Project();
            $aProjects = $oDb->listProjects($iUserId);
            $this->view->aProjects = $aProjects;
        }
    }

    public function appleAction()
    {
        die(__FUNCTION__);
    }

    public function preDispatch()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->redirector('index', 'login');
        }
    }
}

