<?php
/**
 * Description of TrialController
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class TrialController extends Application_Controller_Abstract {

    public function init()
    {
        $this->oStorage = Zend_Auth::getInstance()->getStorage()->read();
        $this->_helper->layout->setLayout('trial');
    }

    public function indexAction()
    {
        $this->_helper->redirector('intro');
    }

    public function introAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_INTRO
        );

        if ($sText) {
            $this->view->sDescription = $sText;
        }
        else {
            $this->view->sMessage = "Error: Could not find project.";
        }
        $this->view->iProjectId = $iProjectId;
    }
    
    public function demographicAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_DEMOGRAPHIC
        );

        if ($sText) {
            $this->view->sDescription = $sText;
        }
        $oForm = new Application_Form_Demographic();
        $oForm->user_id->setValue($this->oStorage->user_id);
        $oForm->project_id->setValue($iProjectId);

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->_request->getParams())) {
                
            }
        }

        $this->view->oForm = $oForm;

        $this->view->iProjectId = $iProjectId;
    }
    
    public function startAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_START_DETAILS
        );

        if ($sText) {
            $this->view->sDescription = $sText;
        }
        
        $this->view->iProjectId = $iProjectId;
    }

    public function litmusAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_TEST_PAGE
        );

        if ($sText) {
            $this->view->sDescription = $sText;
        }
        // get project data from db
        // record the data from the latest submit and do the next action
        // rinse and repeat
        // when complete go to the final action.
        $this->view->headScript()->appendFile('/js/flowplayer/flowplayer-3.2.2.min.js');

        $this->view->iProjectId = $iProjectId;
    }

    public function finalAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_FINAL
        );

        if ($sText) {
            $this->view->sContent = $sText;
        }

        $this->view->iProjectId = $iProjectId;
    }

    public function thanksAction()
    {
        $iProjectId = $this->getProjectId();
        $oDb = new Application_DataGateway_Project();
        $sText = $oDb->fetchFormData(
            $iProjectId,
            FORM_PROJECT_THANKS
        );

        if ($sText) {
            $this->view->sContent = $sText;
        }

        $this->view->iProjectId = $iProjectId;
    }

    public function preDispatch()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->redirector('index', 'login');
        }
    }
}

