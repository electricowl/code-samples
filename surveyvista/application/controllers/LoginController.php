<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginController extends Application_Controller_Abstract
{
    public function init()
    {
        $this->_helper->layout->setLayout('default');
    }

    public function indexAction()
    {
        $this->view->oForm = new Application_Form_Login(array(
            'action' => '/login/process/'
        ));
        
    }

    public function testAction()
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $oStatement = $db->prepare("SELECT * FROM users");
        $oStatement->execute();
        $this->view->aData = $oStatement->fetchAll();

    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector->gotoUrl('/login'); // back to login page
    }

    public function processAction()
    {
        $request = $this->getRequest();
        // Check if we have a POST request
        if (!$request->isPost()) {
            return $this->_helper->redirector('index');
        }

        // Get our form and validate it
        $form = new Application_Form_Login(array(
            'action' => '/login/process/'
        ));
        if (!$form->isValid($request->getPost())) {
            // Invalid entries
            $this->view->oForm = $form;
            return $this->render('index'); // re-render the login form
        }
        
        // Get our authentication adapter and check credentials
        $adapter = $this->getAuthAdapter($form->getValues());
        $auth    = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        
        if (!$result->isValid()) {
            // Invalid credentials
            $form->setDescription('Invalid credentials provided');
            $this->view->oForm = $form;
            return $this->render('login'); // re-render the login form
        }
        $storage = $auth->getStorage();
        $storage->write($adapter->getResultRowObject(array(
            'username',
            'real_name',
        )));
        // store the identity as an object where the password column has
        // been omitted
        $storage->write($adapter->getResultRowObject(
            null,
            'password'
        ));
        // We're authenticated! Redirect to the home page
        $this->_helper->redirector('index', 'index');
    }

    public function getAuthAdapter(array $params)
    {
        $sUsername = $params['username'];
        $sPassword = $params['password'];

        $oDb = Zend_Db_Table_Abstract::getDefaultAdapter();
        $oAuthAdapter = new Zend_Auth_Adapter_DbTable($oDb);
        $oAuthAdapter->setTableName('users')
            ->setIdentityColumn('username')
            ->setCredentialColumn('password');
        $oAuthAdapter->setIdentity($sUsername)
            ->setCredential(md5($sPassword));
        //var_dump($params, md5($sPassword), $oAuthAdapter->getResultRowObject(array('username', 'password')));
        return $oAuthAdapter;
    }

    public function preDispatch()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            // If the user is logged in, we don't want to show the login form;
            // however, the logout action should still be available
            if ('logout' != $this->getRequest()->getActionName()) {
                $this->_helper->redirector('index', 'index');
            }
        } else {
            // If they aren't, they can't logout, so that action should
            // redirect to the login form
            if ('login' == $this->getRequest()->getControllerName() &&
                    'index' == $this->getRequest()->getActionName()) {
                    return;
            }
            if ('login' == $this->getRequest()->getControllerName() &&
                    'process' == $this->getRequest()->getActionName()) {
                    return;
            }
            //if ('logout' == $this->getRequest()->getActionName()) {
                $this->_helper->redirector->gotoUrl('/login');
            //}
        }
    }
}