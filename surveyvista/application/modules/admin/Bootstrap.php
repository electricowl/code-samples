<?php
/**
 * Description of Bootstrap
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Admin_Bootstrap  extends Zend_Application_Module_Bootstrap
{
    //put your code here
    protected function _initAppAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Application',
            'basePath' => dirname(__FILE__),
        ));
        return $autoloader;
    }
}

