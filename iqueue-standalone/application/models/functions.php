<?php

/**
 * Description of functions (functions.php)
 * @package core-staging-branch
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

// Found at: http://dev.kanngard.net/Permalinks/ID_20050507183447.html

function selfURL() {
	$s = empty($_SERVER["HTTPS"]) ? ''
		: ($_SERVER["HTTPS"] == "on") ? "s"
		: "";
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? ""
		: (":".$_SERVER["SERVER_PORT"]);
	return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
}

function strleft($s1, $s2) {
	return substr($s1, 0, strpos($s1, $s2));
}

// Quick and easy 
function useCurl($url) {

    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output   = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get the code of request
    curl_close($ch);

    if($httpCode == 400) {
        return '400: Failed.' . $output;
    }


    if($httpCode == 200) {
        //is ok?
        return $output;
    }
}