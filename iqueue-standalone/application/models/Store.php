<?php
namespace iqueue;

use iqueue\Mongo;

/**
 * Stores
 *
 * Store management for IQueue
 *
 */
class Store {

    /** @var \Mongo **/
    private $mongo;
    /** @var \MongoDB **/
    private $mongoDb;
    /** @var \MongoCollection */
    private $mongoCollection;

    public function __construct(Mongo $mongo, $options = array())
    {
        $this->mongo           = $mongo->mongoConnection();
        $this->mongoDb         = $mongo->mongoDb();
        $this->mongoCollection = $mongo->collection();
    }

    public function findAll() {
        return $this->mongoCollection->find(array('store_id' => array('$ne' => 'default')))->sort(array('store_id' => 1));
    }

    public function findByMongoId($mongoId) {
        return $this->mongoCollection->findOne(array('_id' => new \MongoId($mongoId)));
    }

    public function findByStoreId($storeId) {
        return $this->mongoCollection->findOne(array('store_id' => (int) $storeId));
    }
    
    public function findDefaultStore() {
        return $this->mongoCollection->findOne(array('store_id' => 'default'));
    }

    public function findActiveQueueStores() {
        $return = array();
        $activeQueues = $this->mongoCollection->find(
            array(
                'status' => array(
                    '$exists' => true, 
                    '$ne' => 'inactive'
                 ),
                 'store_id' => array(
                     '$ne' => 'default'
                 ),
            ),
            array(
                'store_id' => true,
                'block_size' => true,
                'check_interval' => true,
                'threshold' => true,
                'status' => true,
                'is_automated' => true,
            )
        );
        if (!empty($activeQueues)) {
            foreach($activeQueues as $queue) {
                $return[$queue['store_id']] = $queue;
            }
        }
        return $return;
    }

    public function findAutomatedStores() {
        return $this->mongoCollection->find(
            array(
                'is_automated' => 1,
            ),
            array(
                'store_id' => true,
                'threshold' => true,
                'status' => true,
            )
        );
    }

    public function insert($data) {
       try {
            $this->mongoCollection->insert($data, array("w" => 1));
            return true;
        } catch(\MongoCursorException $e) {
            return false;
        }
    }

    public function buildNewStoreConfig($options) {
        $defaultStore = $this->findDefaultStore();
        if (empty($defaultStore)) {
            $defaultStore = $this->createDefaultStore();
        }
        $newStore = array(
            'store_id' => (int) $options['store_id'],
            'band' => $defaultStore['band'],
            'text' => $defaultStore['text'],
            'title' => $defaultStore['title'],
            'css' => $defaultStore['css'],
            'display_pos' => (int) $defaultStore['display_pos'],
            'is_social_queue' => $defaultStore['is_social_queue'],
            'target_url' => $defaultStore['target_url'],
            'is_automated' => (int) $defaultStore['is_automated'],
            'status' => $options['status'],
            'threshold' => (float) $defaultStore['threshold'],
            'check_interval' => (int) $defaultStore['check_interval'],
            'block_size' => (int) $defaultStore['block_size'],
            'last_changed' => time(),
            'loading_gif' => $defaultStore['loading_gif'],
        );
        return $this->insert($newStore);
    }
    
    public function createDefaultStore() {
        $defaultStore = array(
            'store_id' => 'default',
            'event_id' => '',
            'band' => '*',
            'text' => '',
            'title' => 'You are currently in a queue for tickets',
            'css' => '',
            'display_pos' => 0,
            'is_social_queue' => 0,
            'target_url' => 'http://crowdsurge.com/store',
            'is_automated' => (int) false,
            'status' => 'inactive',
            'threshold' => 4,
            'check_interval' => 20,
            'block_size' => 200,
            'last_changed' => time(),
            'loading_gif' => 'framelys',
        );
        $this->insert($defaultStore);
        return $this->findDefaultStore();
        
    }

    public function updateByMongoId($mongoId, $data) {
        return $this->mongoCollection->update(
            array('_id' => new \MongoId($mongoId)),
            array('$set' =>$data),
            array('w' => 1)
        );
    }

    public function delete($mongoId) {
        return $this->mongoCollection->remove(array('_id' => new \MongoId($mongoId)), array("justOne" => true));
    }

    public function ensureUniqueIndexes() {
        $this->mongoCollection->ensureIndex(array('store_id' => 1), array('unique' => true));
    }
    
}

