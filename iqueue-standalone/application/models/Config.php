<?php
namespace iqueue;
/**
 * Config
 *
 * Parses Ini Config file
 *
 */
class Config {

    private $ini_config = array();
    private $active_ini_config = array();

    public function buildParameters($pathToConfig, $environment = null) {

        if (! file_exists(APP_PATH . $pathToConfig)) {
            throw new \Exception('Unable to load config file');
        }

        $ini_config = parse_ini_file(APP_PATH . $pathToConfig, true);
        $this->ini_config = $ini_config;

        if (! array_key_exists('production', $ini_config)) {
            throw new \Exception('Unable to parse config settings');
        }

        if (! empty($ini_config[$environment])) {
            $this->active_ini_config = $ini_config[$environment];
            return $ini_config[$environment];
        }

        return $ini_config['production'];

    }

    public function getConfig()
    {
        return $this->ini_config;
    }

    public function getActiveConfig()
    {
        return $this->active_ini_config;
    }

}
