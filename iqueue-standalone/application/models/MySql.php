<?php

/**
 * Description of MySql (MySql.php)
 * @package core-staging-branch
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class MySql {

    private $pdo;

    public function __construct($options) {

        $this->pdo = new PDO(
                $dsn,
                $username,
                $passwd,
                $options
            );
    }

}

