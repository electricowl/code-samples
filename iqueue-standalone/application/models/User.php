<?php
namespace iqueue;
/**
 * User
 *
 * Simple authentication for user to access queue admin
 *
 */
class User {

    /** @var \Mongo **/
    private $mongo;
    /** @var \MongoDB **/
    private $mongoDb;
    /** @var \MongoCollection */
    private $mongoCollection;

    private $salt = '~&)T^>;-=)}J,UDJ*>-aX2ttMb)Ret]c';

    public function __construct(Mongo $mongo, $options = array())
    {
        $this->mongo           = $mongo->mongoConnection();
        $this->mongoDb         = $mongo->mongoDb();
        $this->mongoCollection = $mongo->collection();
    }


    public function getSessionFromCookie() {
        return $_COOKIE['crowdsurge_q_user'];
    }


    public function userCookieExists() {
       return (isset($_COOKIE['crowdsurge_q_user']));
    }


    public function setCookie($value) {
        return setcookie(
            "crowdsurge_q_user",
            $value,
            time() + 60 * 60 * 8,
            '/',
            $_SERVER['HTTP_HOST']
        );
    }


    public function login($data) {
        $user = $this->findUser($data);
        if ($user) {
            $sessionId = md5(time() . $this->salt);
            if ($this->setCookie($sessionId) && $this->addSessionToUserSessions($sessionId)) {
                return true;
            }
        }
        return false;
    }


    public function logout() {
        $user = $this->findUserByToken($this->getSessionFromCookie());
         if ($user) {
           $sessionId = md5(time() . $this->salt);
           $this->addSessionToUser($user['_id'], $sessionId);
        }
        setcookie ('crowdsurge_q_user', '', time() - 3600, '/', $_SERVER['HTTP_HOST']);
    }


    public function findUser($data) {
        return $this->mongoDb->selectCollection('users')->findOne(array(
            'username' => $data['username'],
            'password' => md5($data['password'] . $this->salt),
            'ip' => $_SERVER['REMOTE_ADDR'],
        ));
    }


    public function findUserByToken($data) {
        return $this->mongoDb->selectCollection('users')->findOne(array(
            'token' => $data['username'],
        ));
    }


    public function addSessionToUserSessions($sessionId) {
        return $this->mongoDb->selectCollection('user_sessions')->insert(
            array('token' => $sessionId)
        );
    }


    public function authenticate() {
        if ($this->userCookieExists()) {
            $userSession = $this->mongoDb->selectCollection('user_sessions')->findOne(array(
                'token' => $this->getSessionFromCookie(),
            ));
            return (!empty($userSession));
        }
        return false;
    }

}

