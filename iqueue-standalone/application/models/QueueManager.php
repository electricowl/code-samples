<?php
namespace iqueue;
ini_set('date.timezone', 'Europe/London');

use iqueue\Mongo;
/**
 * QueueManager
 * Class to manage a mongodb queue
 * Currently hardcoded to Crowdstore
 */
class QueueManager {

    const REFRESH_INTERVAL_SECS = 60;
    const MAXIMUM_QUEUE_SECS    = 120;

    /** @var \Mongo **/
    private $mongo;
    /** @var \MongoDB **/
    private $mongoDb;
    /** @var \MongoCollection */
    private $mongoCollection;

    private static $instance = null;

    private $collectionStatus = true;
    private $queueManagementMethod;

    /** @var iqueue\Counter **/
    private $queueCounter = null;

    public function __construct(Mongo $mongo, $options = array())
    {
        self::$instance = $this;

        $this->queueManagementMethod =
                isset($options['queue_management']) && !empty($options['queue_management'])
                ? $options['queue_management'] : 'unlock';

        //Connect to MDB
        $this->mongo           = $mongo->mongoConnection();
        $this->mongoDb         = $mongo->mongoDb();
        $this->mongoCollection = $mongo->collection();
    }

    /**
     *
     * @param \iqueue\Counter $counter
     */
    public function setCounter(Counter $counter)
    {
        $this->queueCounter = $counter;
    }

    private function getPositionFromCounter($identifier)
    {
        // TODO Checking for type would be more robust
        if (null !== $this->queueCounter) {
            return $this->queueCounter->incrementCounter($identifier);
        }
        else {
            throw \Exception("Counter not initialised or not available to Queue Manager");
        }
    }

    /**
     * Capture the Request uri from the client and store in session
     * The returned MongoId can be used in page as the identifier to check
     * whether locked or not
     * @return string
     */
    public static function loadQueue($storeid, $eventid, $is_social = false, $social_options = array())
    {
        $instance = self::getInstance();

        // gather our data here
        $identifier = $instance->getQueueId($storeid);
        $counter = $instance->getPositionFromCounter($identifier);

        $position = $counter['value']['counter'];

        $data = array(
            'eventid'        => $eventid,
            'storeid'        => $storeid,
            'remote_addr'    => $_SERVER['REMOTE_ADDR'],
            'request_uri'    => $_SERVER['REQUEST_URI'],
            'query_string'   => $_SERVER['QUERY_STRING'],
            'locked'         => (bool) true,
            'queue'          => $identifier,
            'queue_position' => $position,
            'is_social'      => $is_social,
            //'debug_counter'  => $counter,
        );

        if ($is_social) {
            foreach ($social_options as $key => $value) {
                $data[$key] = $value;
            }
        }

        /**
         * On storage we get the counter object (if debugging set)
         * $data['_id'] is the MongoId
         */
        $stored = $instance->storeData($data);
        return $stored;
    }

    /**
     *
     * @param array $data
     * @return \MongoId
     * @TODO refactor to remove queue time
     */
    private function storeData($data, $queueTime = self::MAXIMUM_QUEUE_SECS)
    {
        $data["queued_at"] = new \MongoDate();

        // Set the expire time (2 minutes hence)
        $time = new \DateTime();
        $time->add(
            \DateInterval::createFromDateString($queueTime . ' seconds')
        );

        //$data["expires"] = new \MongoDate(strtotime($time->format('Y-m-d H:i:s')));

        // need to return the key
        $this->setCollection($data['queue'])->insert(
            $data
        );

        return $data;
    }
    /**
     *
     * @param string $mongoId
     * @return null|array
     */
    public function check($mongoId, $storeid)
    {
        $coll = $this->setCollection($this->getQueueId($storeid));
        $result = $coll->findOne(
                array("_id" => new \MongoId($mongoId)
            )
        );

        return $result;
    }
    /**
     * Process the queue according to set rules
     * Maybe flush by time
     */
    private function processQueue($method = 'unlock')
    {
        $method = strtolower($this->queueManagementMethod) . 'Queue';

        $this->$method();

    }

    private function clearExpiredQueueEntries($queueid)
    {
        $this->setCollection($queueid)->remove(
            array(
                'expires' => array(
                    '$lt' => new \MongoDate()
                )
            )
        );
    }

    public function batcholdestQueue($batchSize = 100, $storeid = null, $eventid = null)
    {
        $query = array(
            'locked' => true
        );

        if (null != $storeid) {
            $query['storeid'] = $storeid;
        }

        if (null != $eventid) {
            $query['eventid'] = $eventid;
        }
        $coll = $this->setCollection($this->getQueueId($storeid));
        $cursor = $coll
                ->find($query)
                ->sort(array('_id' => 1))
                ->limit($batchSize);

        $update = array('$set' => array("locked" => false));

        if ($cursor->hasNext()) {
            foreach ($cursor as $id => $doc) {
                $coll->update(
                    array(
                        '_id' => new \MongoId($id)
                    ),
                    $update,
                    array(
                        'multiple' => false
                    )
                );
            }
        }
    }
    // use this
    public function batchSocialQueue($batchSize = 100, $storeid = null, $eventid = null)
    {
        $query = array(
            'locked' => true,
            'is_social' => true,
        );

        if (null != $storeid) {
            $query['storeid'] = $storeid;
        }

        if (null != $eventid) {
            $query['eventid'] = $eventid;
        }
        $coll = $this->setCollection($this->getQueueId($storeid));
        $cursor = $coll
                ->find($query)
                ->sort(array(
                    'factor' => -1,
                    'queued_at' => -1)
                )
                ->limit($batchSize);

        $update = array('$set' => array("locked" => false));

        if ($cursor->hasNext()) {
            foreach ($cursor as $id => $doc) {
                $coll->update(
                    array(
                        '_id' => new \MongoId($id)
                    ),
                    $update,
                    array(
                        'multiple' => false
                    )
                );
            }
        }
    }
    /**
     *
     * @param type $storeid
     * @param type $batchSize
     */
    public function releaseBlockFromQueue($storeid, $batchSize)
    {
        $coll = $this->setCollection($this->getQueueId($storeid));

        $lowerBound = $this->getCurrentPointerValue($storeid);

        $upperBound = (int) $lowerBound + (int) $batchSize;

        // Let's update the pointer first then carry out the task
        $this->updatePointer($storeid, $batchSize);

        $time = new \DateTime();
        $mongoDateNow = new \MongoDate(strtotime($time->format('Y-m-d H:i:s')));

        $coll->update(
            array(
                'queue_position' => array(
                    '$gte' => (int) $lowerBound, '$lt' => $upperBound
                )
            ),
            array(
                '$set' => array(
                        "locked" => false,
                        "released" => $mongoDateNow
                    )
                ),
            array(
                'multiple' => true
            )
        );

    }

    /**
     *
     * @param type $storeid
     * @param type $batchSize
     */
    public function releaseBlockFromSocialQueue($storeid, $batchSize)
    {
        $coll = $this->setCollection($this->getQueueId($storeid));

        $lowerBound = $this->getCurrentPointerValue($storeid);
        //var_dump("Lower bound value", $lowerBound);

        $upperBound = (int) $lowerBound + (int) $batchSize;
        //var_dump("Upper bound value", $upperBound);

        // Let's update the pointer first then carry out the task
        $this->updatePointer($storeid, $batchSize);

        $time = new \DateTime();
        $mongoDateNow = new \MongoDate(strtotime($time->format('Y-m-d H:i:s')));

        $coll->update(
            array(
                'queue_position' => array(
                    '$gte' => (int) $lowerBound, '$lt' => $upperBound
                )
            ),
            array(
                '$set' => array(
                        "locked" => false,
                        "released" => $mongoDateNow
                    )
                ),
            array(
                'multiple' => true
            )
        );

    }

    private function unlockQueue()
    {
        $this->mongoCollection->update(
            array( // search bit
                'expires' => array(
                    '$lt' => new \MongoDate()
                )
            ),
            array( // This is the update bit
                '$set' => array(
                        "locked" => false
                    )
                ),
            array( // Apply to multiple records
                'multiple' => true
            )
        );
    }

    /**
     * ::run() simply starts off the process which will continue for as
     * long as the object persists.
     */
    public static function run()
    {
        $instance = self::getInstance();
        $instance->main();
    }

    private function main()
    {
        while ($this->collectionStatus) {
            $this->processQueue();

            sleep(10);
        }
    }

    public function start()
    {
        // start the collection process
        $this->collectionStatus = true;
    }

    public static function stop()
    {
        // stop building the queue
        self::getInstance()->collectionStatus = false;
        self::getInstance()->flush();
    }

    public function removeQueue($queueid)
    {
        // remove the queue
        $this->setCollection($queueid)->remove();
    }

    /**
     *
     * @return QueueManager
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * For testing the run() functionality
     */
    public function generateTestData()
    {
        $data = array(
            'request_uri'  => 'http://test.uri',
            'query_string' => '?storeid=888&menu=detail&eventid=28188',
            'locked'       => true
        );
        for ($i=2; $i<100; $i+=2) {
            $this->storeData($data, $i*30);
        }
    }

    public function useDb($db)
    {
        $this->mongo->mongoConnection()->selectDb($db);
    }

    public function setCollection($collection)
    {
        return $this->mongoDb->selectCollection($collection);
    }

    /**
     * TODO This needs to be get queues rather than get stores in queue
     * @param type $queueName
     * @return type
     */
    public function getStoresInQueue($queueName)
    {
        throw new Exception("This method needs to be reimplemented as something like get_store_queues");
        return $this->setCollection($queueName)->distinct('storeid');
    }

    /**
     * Get the length of the queue dependent on several criteria
     * @param type $storeid
     * @param type $locked
     * @return type
     */
    private function countQueueLength($storeid, $locked = null)
    {
        $identifier = $this->getQueueId($storeid);

        $criteria = array();
        if (null !== $locked) {
            $criteria['locked'] = $locked;
        }

        if (null !== $storeid) {
            $criteria['storeid'] = $storeid;
        }

        return $this->setCollection($identifier)->count($criteria);
    }

    public function totalQueueSize($storeid = null)
    {
        return $this->countQueueLength($storeid);
    }

    public function totalLocked($storeid = null)
    {
        return $this->countQueueLength($storeid, true);
    }
    /**
     * How many items have been released from the queue?
     * @param type $storeid
     * @return int
     */
    public function totalReleased($storeid)
    {
        return $this->countQueueLength($storeid, false);
    }

    public function truncateQueue($storeid)
    {
        $identifier = $this->getQueueId($storeid);
        $coll = $this->setCollection($identifier);
        // Truncate collection
        $coll->remove();

        if (null !== $this->queueCounter) {
            $this->queueCounter->resetCounter($identifier);
        }

    }

    public function getQueue($queueid)
    {
        return $this->setCollection($queueid)->find();
    }

    /**
     * TODO currently in a debugging state
     * @param \MongoCursor $cursor
     */
    public function iterateCursor(\MongoCursor $cursor)
    {
        while($record = $cursor->getNext()) {
            var_dump($record);
        }
    }

    private function updatePointer($storeid, $quantity)
    {
        // TODO check for type instead
        if (null != $this->queueCounter) {
            $identifier = $this->getQueueId($storeid);
            return $this->queueCounter->incrementPointer($identifier, $quantity);
        }
    }

    public function getCurrentPointerValue($storeid)
    {
        if (null !== $this->queueCounter) {
            $identifier = $this->getQueueId($storeid);
            return $this->queueCounter->getPointerValue($identifier);
        }
    }

    public function getCounter($storeid)
    {
        if (null !== $this->queueCounter) {
            $identifier = $this->getQueueId($storeid);
            return $this->queueCounter->getCounter($identifier);
        }
    }

    /**
     *
     * @param mixed $id
     * @param string $prefix store_ default
     * @return string
     */
    public function getQueueId($id, $prefix = 'store_')
    {
        return $prefix . $id;
    }
}

/**
 * Usage
 * QueueManager::run();
 *
 * Inside a web page
 * include 'queue_manager.php';
 * $manager = QueueManager::getInstance();
 * $manager
 *
 */


