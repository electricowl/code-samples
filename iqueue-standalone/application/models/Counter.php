<?php
namespace iqueue;

include_once 'Mongo.php';

/**
 * Counter
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Counter extends Mongo {

    private $counterCache = array();
    private $isSocial     = null;

    /**
     * Facade for findAndModify()
     *
     * @param type $collection
     * @param type $identifier
     */
    public function incrementCounter($identifier)
    {
        if (! array_key_exists($identifier, $this->counterCache)) {

            if (! $this->collection()->findOne(array("_id" => $identifier))) {
                $this->initialiseCounter($identifier);
                //$this->counterCache[$identifier] = true;
            }
        }

        return $this->mongoDb->command(
            array(
              "findandmodify" => $this->collection,
              "query" => array("_id"=> $identifier),
              "update" => array('$inc' => array("counter" => 1)),
            )
         );
    }

    public function incrementPointer($identifier, $released)
    {
        if (! array_key_exists($identifier, $this->counterCache)) {

            if (! $this->collection()->findOne(array("_id" => $identifier))) {
                $this->initialiseCounter($identifier);
                //$this->counterCache[$identifier] = true;
            }
        }

        return $this->mongoDb->command(
            array(
              "findandmodify" => $this->collection,
              "query" => array("_id"=> $identifier),
              "update" => array('$inc' => array("pointer" => (int) $released)),
            )
         );
    }

    public function getPointerValue($identifier)
    {
        $counter = $this->collection()->findOne(array("_id" => $identifier));
        return $counter['pointer'];
    }

    /**
     * initialiseCounter - check counter exists and set it up if required
     * The keys are retained in a cache to cut the load on the db server.
     * @param string $identifier
     * @param int $startvalue
     */
    private function initialiseCounter($identifier, $startvalue = 0)
    {
        if (null == $this->collection()->findOne(array("_id" => $identifier))) {
            // Load the counter into the counter collection
            $config = array(
                            "_id" => $identifier,
                            "counter" => $startvalue,
                            "pointer" => 0
                        );
            if ($this->isSocial) {
                $config['is_social'] = true;
            }

            $this->collection()->insert(
                    $config
                    );
        }
        $this->counterCache[$identifier] = true;
    }

    public function setIsSocial()
    {
        $this->isSocial = true;
    }

    public function  isSocial()
    {
        return $this->isSocial;
    }

    public function resetCounter($identifier)
    {
        $this->collection()->update(
                array('_id' => $identifier),
                array( '$set' => array(
                        "counter" => 0,
                        "pointer" => 0
                    )
                )
            );
    }

    public function getCounter($identifier)
    {
        return $this->collection()->findOne(array("_id" => $identifier));
    }
}

