<?php
namespace iqueue;
/**
 * Mongo
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Mongo {
    
    /** @var MongoCollection */
    protected $mongoCollection;
    
    protected $mongo = null;
    protected $mongoDb = null;
    
    protected $host;
    protected $db;
    protected $collection;
    protected $port;

    public function __construct($options = array()) {
                
        $this->host       = isset($options['host']) ? $options['host'] : null;
        $this->db         = isset($options['db']) ? $options['db'] : null;
        $this->collection = isset($options['collection']) ? $options['collection'] : null;
        $this->port       = isset($options['port']) ? $options['port'] : '27017';
        
        //Connect to MDB
        $this->mongo = new \Mongo("mongodb://" . $this->host . ":" . $this->port);
        $this->mongoDb  = $this->mongo->selectDB($this->db);
        if (null !== $this->collection) {
            $this->mongoCollection = $this->mongoDb->selectCollection($this->collection);
        }
    }
    
    public function mongoConnection()
    {
        return $this->mongo;
    }
    
    public function mongoDb()
    {
        return $this->mongoDb;
    }
    /**
     * 
     * @return \MongoCollection
     */
    public function collection()
    {
        return $this->mongoCollection;
    }
}

