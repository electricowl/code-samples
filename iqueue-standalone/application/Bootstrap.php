<?php
namespace iqueue;

$environment = getenv('APPLICATION_ENV');

if ('production' === $environment) {
    ini_set('display_errors', 0);

} else {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

if (null !== $environment) {
    define('ENVIRONMENT',  $environment);
}

if (!defined('ENVIRONMENT')) {
    define('ENVIRONMENT',  'production');
}

// Define file path constants
define('APP_PATH',  dirname(__FILE__) . '/');
define('ROOT',  dirname(dirname(__FILE__)) . '/');

// Include our classes
include_once APP_PATH . 'models/Mongo.php';
include_once APP_PATH . 'models/QueueManager.php';
include_once APP_PATH . 'models/Counter.php';
include_once APP_PATH . 'models/Store.php';
include_once APP_PATH . 'models/User.php';
include_once APP_PATH . 'models/Config.php';

// load config based on environment
$configModel = new Config();
$config = $configModel->buildParameters('configs/config.ini', ENVIRONMENT);

// mongo configs
$queueConfig = array(
    'host'       => $config['mongoHost'],
    'db'         => $config['mongoDB'],
    'collection' => null
);

$qCounterConfig = array(
    'host'       => $config['mongoHost'],
    'db'         => $config['mongoDB'],
    'collection' => 'qcounter'
);

$storeConfig = array(
    'host'       => $config['mongoHost'],
    'db'         => $config['mongoDB'],
    'collection' => 'store_configs'
);

$userConfig = array(
    'host'       => $config['mongoHost'],
    'db'         => $config['mongoDB'],
    'collection' => null
);

