<?php
namespace iqueue;

session_save_path($config['sessionSavePath']);
/**
 * Description of social_config (social_config.php)
 * @package core-staging-branch
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

//P3P Session solution | The iframes flux capacitor
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

$environment = getenv('APPLICATION_ENV');

if ('development' == $environment) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

include_once APP_PATH . 'models/Mongo.php';

$socialConfig = array(
    'host'       => 'localhost',
    'db'         => 'social_data',
    'collection' => null
);

if ('development' !== $environment) {
    $socialConfig['host']    = 'db1.certechhq.com';
    //$qCounterConfig['host'] = 'db1.certechhq.com';
}

use iqueue\Mongo;


