<?php

/**
 * Description of facebook_ (facebook_.php)
 * @package core-staging-branch
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

// Facebook social App

$app_name   = "ILCSDEV";
$app_id     = "429474120465641";
$app_secret = "793ff0ceab58eb35d06602d54ce7bc00";

$app_id_phoenix = "272020439592524";
$app_secret_phoenix = "b385a91f5eadc1c98e8559e6149130b8";

// How invasive
$scope = array(
    'user_birthday',
    'read_stream',
    // extras
    'user_location',
    'user_status',
    'user_activities',
    'user_interests',
    'user_events',
    'user_groups',
    'user_photos',
    'user_subscriptions',
);

$scope_phoenix = array(
    'email',
    'read_stream',
    'user_likes',
    'user_birthday',
    'user_location',
    'user_actions.music',
);

$fb_graph_endpoint = "https://graph.facebook.com/";
$fb_graph_query = $fb_graph_endpoint . "%s?fields=id,name,location,bio,checkins,statuses,photos,likes,interests,activities,posts,locations,inspirational_people";

$user_query = urlencode("SELECT email, name, locale FROM standard_user_info WHERE uid =");

$fb_graph_queries = array(
    // %s %s is userid access_token in that order!
    'posts'      => "https://graph.facebook.com/%s/posts?limit=5000&offset=0&access_token=%s",
    'likes'      => "https://graph.facebook.com/%s/likes?limit=5000&offset=0&access_token=%s",
    'interests'  => "https://graph.facebook.com/%s/interests?limit=5000&offset=0&access_token=%s",
    'activities' => "https://graph.facebook.com/%s/activities?limit=5000&offset=0&access_token=%s",
    'checkins'   => "https://graph.facebook.com/%s/checkins?limit=5000&offset=0&access_token=%s",
    'statuses'   => "https://graph.facebook.com/%s/statuses?limit=5000&offset=0&access_token=%s",
    'locations'  => "https://graph.facebook.com/%s/locations?limit=5000&offset=0&access_token=%s",
    // NB tjhe access token here is an App Access Token
    'user'   => "https://graph.facebook.com/fql?q=" . $user_query . "{fbid}&access_token={token}",
);

$fb_graph_queries_phoenix = array(
    'read_stream' => "https://graph.facebook.com/%s/home?limit=5000&offset=0&access_token=%s",
    'likes'       => "https://graph.facebook.com/%s/likes?limit=5000&offset=0&access_token=%s",
    'locations'   => "https://graph.facebook.com/%s/locations?limit=5000&offset=0&access_token=%s",
    'friends'     => "https://graph.facebook.com/%s/friends?limit=5000&offset=0&access_token=%s",
    'person'     => "https://graph.facebook.com/%s/?fields=birthday,email,location,quotes,age_range,friends,posts,statuses&access_token=%s",

);





