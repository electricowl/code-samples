<?php
/**
 * queue_monitor
 * Follow the activity on the database server and release queue members based on the load of the server.
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once dirname(dirname(__FILE__)).'/application/Bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);


declare(ticks = 1);

pcntl_signal(SIGTERM, "signal_handler");
pcntl_signal(SIGINT, "signal_handler");

function signal_handler($signal = null) {

    switch($signal) {
        case SIGTERM:
            print "Caught SIGTERM\n";
            exit(0);
        case SIGKILL:
            print "Caught SIGKILL\n";
            exit(0);
        case SIGINT:
            print "Caught SIGINT\n";
            exit(0);
    }
}

function getDBLoad($queueServerLoadLogFile) {
   if ('production' === ENVIRONMENT ) {
       $serverLoad = file_get_contents('http://www.crowdsurge.com/db--mon/', null, null, -1, 200);
   } else {
       $serverLoad = file_get_contents('http://176.74.173.208/db--mon/', null, null, -1, 200);
   }
   if (file_put_contents($queueServerLoadLogFile, $serverLoad)) {
      return unserialize($serverLoad);
   }
   return array('1min' => '204');
}

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Counter;
use iqueue\Store;

$activeStores = array();
// holds list of activated stores sent to WebStore
$notifyWebStore = array(
    'is_queue_startup' => true,
    'data_has_changed' => false,
    'active_stores' => array(),
    'inactive_stores' => array(),
    'started_stores' => array(),
    'url' => 'http://www.crowdsurge.com/store/api/iqueue/queue_control.php',
);
if ('production' !== ENVIRONMENT ) {
   $notifyWebStore['url'] = 'http://www.crowdsurge.com/store_dev/api/iqueue/queue_control.php';
}

$mainLoopInterval = 5;
$offset = 0.2; // % offset to turn automatic queue on or off
$pid = getmypid();
file_put_contents($config['queueLastRunLogFile'], 'pid: ' . $pid . ' -- last loop: ');

$dBLoad = getDBLoad($config['queueServerLoadLogFile']);

$storeMongoModel = new Mongo($storeConfig);
$storeModel = new Store($storeMongoModel);

$mongo = new Mongo($queueConfig); // Get Mongo instance (using our own Mongo Class)
$qm = new QueueManager($mongo); // Create a QueueManager instance
$mongoCounter = new Counter($qCounterConfig);
$qm->setCounter($mongoCounter);

print "Starting Queue Monitor\n";

while (1) {

    // file to log last loop time
    $time = new DateTime();
    $date = $time->format("Y-m-d H:i:s");
    file_put_contents($config['queueLastRunLogFile'], 'pid: ' . $pid . ' -- last loop: ' . $date);

    $dBLoad = getDBLoad($config['queueServerLoadLogFile']); // should check on every iteration

    // Auto switch on and off
    $automatedStores = $storeModel->findAutomatedStores();
    foreach ($automatedStores as $automatedStore) {
        if (floatval($dBLoad['1min']) > floatval($automatedStore['threshold'] * (1-$offset))) {
            if ((string) $automatedStore['status'] === 'inactive') {
                print "Update QueueMonitor for Store: {$automatedStore['store_id']} to starting\n";
                $storeModel->updateByMongoId($automatedStore['_id'], array('status' => 'starting', 'last_changed' => time()));
            }
            if ((string) $automatedStore['status'] === 'shutting down') {
                print "Update QueueMonitor for Store: {$automatedStore['store_id']} to running\n";
                $storeModel->updateByMongoId($automatedStore['_id'], array('status' => 'running', 'last_changed' => time()));
            }
        } else {
            if ((string) $automatedStore['status'] === 'running') {
                print "Sending shutdown for Store: {$automatedStore['store_id']}\n";
                $storeModel->updateByMongoId($automatedStore['_id'], array('status' => 'shutdown sent', 'last_changed' => time()));
            }
        }
    }

    // get any changes from admin
    $storesLookup = $storeModel->findActiveQueueStores();
    if (!empty($storesLookup)) {
        foreach ($storesLookup as $lookupId => $lookup) {

            if (array_key_exists($lookup['store_id'], $activeStores)) {
                // Update any changes to load values, threshold, etc
                $activeStores[$lookupId]['block_size'] = $lookup['block_size'];
                $activeStores[$lookupId]['threshold'] = $lookup['threshold'];
                $activeStores[$lookupId]['check_interval'] = $lookup['check_interval'];
                $activeStores[$lookupId]['status'] = $lookup['status'];
                $activeStores[$lookupId]['is_automated'] = $lookup['is_automated'];

                // has the check interval been updated
                if ($activeStores[$lookupId]['base_check_interval'] !== $lookup['check_interval']) {
                    $activeStores[$lookupId]['time_until_check'] = $lookup['check_interval']-$activeStores[$lookupId]['time_until_check'];
                    $activeStores[$lookupId]['base_check_interval'] = $lookup['check_interval'];
                }
            } else {
                // process is starting - add it to the active stores
                $activeStores[$lookupId] = $lookup;

                $now = new DateTime();
                $date = $now->format("Y-m-d-H-i-s");
                // start up data for store log
                $logfile = $config['queuesPath'] . "/queue_monitor_{$date}_{$lookup['store_id']}_{$lookup['check_interval']}_{$lookup['block_size']}.log";
                file_put_contents(
                    $logfile,
                    "Monitoring: {$lookup['store_id']}, {$lookup['block_size']}, {$lookup['check_interval']}, {$lookup['threshold']}\n",
                    FILE_APPEND
                );

                // Create lock file
                $lockfile = $config['lockPath'] . '/queue_monitor_' . (int) $lookup['store_id'] . '.lock';
                if (file_exists($lockfile)) {
                    // process not terminated correctly
                    print "QueueMonitor was still running for Store: {$lookup['store_id']} but had not completely shutdown\n";
                }
                else {
                    print "Starting QueueMonitor for Store: {$lookup['store_id']}\n";
                    // create lockfile
                    file_put_contents($lockfile, $pid);
                }

                // update our store array
                $activeStores[$lookupId]['status'] = 'running';
                $activeStores[$lookupId]['logfile'] = $logfile;
                $activeStores[$lookupId]['lockfile'] = $lockfile;

                $activeStores[$lookupId]['base_check_interval'] = $lookup['check_interval'];
                $activeStores[$lookupId]['time_until_check'] = $lookup['check_interval'];

                // tell Webstore a new store is added
                $notifyWebStore['started_stores'][] = $lookupId;

                // tell admin process has started
                $storeModel->updateByMongoId($lookup['_id'], array('status' => 'running', 'last_changed' => time()));
            }

            if ($lookup['status'] === 'shutdown sent') {
                print "Shutting down QueueMonitor for Store: {$lookupId}\n";
                $activeStores[$lookupId]['status'] = 'shutting down';
                $storeModel->updateByMongoId($lookup['_id'], array('status' => 'shutting down', 'last_changed' => time()));
            }

        }
    }

    // Main process to update queues
    if (!empty($activeStores)) {

        $now = new DateTime();
        $time = $now->format(DATE_ISO8601);

        foreach ($activeStores as $store) {

            $activeStores[$store['store_id']]['time_until_check'] -= (int) $mainLoopInterval;

            if ($store['time_until_check'] <= 0) {

                // write current db load at time of check
                file_put_contents(
                    $store['logfile'],
                    "$time [DB Load Average] {$dBLoad['1min']} \n",
                    FILE_APPEND
                );

                if (floatval($dBLoad['1min']) < floatval($store['threshold'])) {

                    $counterData = $qm->getCounter($store['store_id']);

                    if (empty($counterData)) {
                        // TODO handle this better
                        $counterData = array(
                            'counter' => 0,
                            'pointer' => 0,
                        );
                    }

                    // Social queue parameters
                    $is_social_queue = false;
                    if (isset($counterData['is_social'])) {
                        $is_social_queue = (bool) $counterData['is_social'];
                    }

                    // Only process if counter is more than the pointer value
                    if ((int) $counterData['counter'] > (int) $counterData['pointer']) {
                        $difference = (int) $counterData['counter'] - (int) $counterData['pointer'];
                        // Keep block size under control
                        $control_block_size = $store['block_size'];
                        if ($store['block_size'] > $difference) {
                           $control_block_size = $difference;
                        }

                        if ($is_social_queue) {
                            $qm->batchSocialQueue($control_block_size, $store['store_id']);
                        }
                        else {
                            // This does the donkeywork of releasing the data from the queue
                            $qm->releaseBlockFromQueue($store['store_id'], $control_block_size);
                        }

                        $ptr = $qm->getCurrentPointerValue($store['store_id']);

                        file_put_contents(
                            $store['logfile'],
                            "$time Released: $control_block_size entries from queue, current position $ptr\n",
                            FILE_APPEND
                        );
                    }
                    else {
                        file_put_contents(
                            $store['logfile'],
                            "$time None Released: " . implode("|", $counterData) . "\n",
                            FILE_APPEND
                        );

                    }
                } else {
                    file_put_contents(
                        $store['logfile'],
                        "None Released: DB load higher than store threshold " . $store['threshold'] . "\n",
                        FILE_APPEND
                    );
                }

                $activeStores[$store['store_id']]['time_until_check'] = $store['check_interval'];


                if ((int) $store['is_automated'] === 1) {
                    // auto switch off
                    if (
                       (floatval($dBLoad['1min']) < floatval($store['threshold'] * (1-$offset)))
                       &&
                       $store['status'] === 'shutting down'
                       )
                    {
                        $counterData = $qm->getCounter($store['store_id']);
                        if ((int) $counterData['counter'] === (int) $counterData['pointer']) {
                            print "Service stopped for store " . $store['store_id'] . "\n";
                            unlink($store['lockfile']);
                            $storeModel->updateByMongoId($store['_id'], array('status' => 'inactive', 'last_changed' => time()));
                            $notifyWebStore['inactive_stores'][] = $store['store_id'];
                            unset($activeStores[$store['store_id']]);
                        }
                    }
                } else {
                    // manual switch off
                    if ($store['status'] === 'shutting down') {
                        $counterData = $qm->getCounter($store['store_id']);
                        if ((int) $counterData['counter'] === (int) $counterData['pointer']) {
                            print "Service stopped for store " . $store['store_id'] . "\n";
                            unlink($store['lockfile']);
                            $storeModel->updateByMongoId($store['_id'], array('status' => 'inactive', 'last_changed' => time()));
                            $notifyWebStore['inactive_stores'][] = $store['store_id'];
                            unset($activeStores[$store['store_id']]);
                        }
                    }

                }

            }

        }
    }

    // notify store for changes in active stores
    if ($notifyWebStore['is_queue_startup'] && !empty($activeStores)) {
        foreach ($activeStores as $alreadyRunning) {
            $notifyWebStore['active_stores'][$alreadyRunning['store_id']] = $alreadyRunning['store_id'];
        }
        $notifyWebStore['is_queue_startup'] = false;
        $notifyWebStore['data_has_changed'] = true;
    }

    if (!empty($notifyWebStore['inactive_stores'])) {
        foreach ($notifyWebStore['inactive_stores'] as $toStop) {
            unset($notifyWebStore['active_stores'][$toStop]);
        }
        $notifyWebStore['inactive_stores'] = array();
        $notifyWebStore['data_has_changed'] = true;
    }

    if (!empty($notifyWebStore['started_stores'])) {
        foreach ($notifyWebStore['started_stores'] as $toStart) {
            $notifyWebStore['active_stores'][$toStart] = $toStart;
        }
        $notifyWebStore['started_stores'] = array();
        $notifyWebStore['data_has_changed'] = true;
    }

    if ($notifyWebStore['data_has_changed']) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$notifyWebStore['url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(array(
                'data_has_changed' => 1,
                'stores' => $notifyWebStore['active_stores'],
                'time' => time()
            ))
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        if (!empty($notifyWebStore['active_stores'])) {
            $storesStringForLog = implode(',', $notifyWebStore['active_stores']);
        }
        else {
            $storesStringForLog = 'no stores active';
        }

        print "Notification sent to WebStore at " . date('H:i:s') . " with active stores: " . $storesStringForLog . "\n";
        print "Server responded with " . htmlentities($server_output, ENT_QUOTES, 'utf-8') . "\n";
        $notifyWebStore['data_has_changed'] = false;
    }

    //print memory_get_usage() . " (current memory)\n";
    //print memory_get_peak_usage() . " (peak memory)\n";
    signal_handler();
    sleep((int) $mainLoopInterval);
}
