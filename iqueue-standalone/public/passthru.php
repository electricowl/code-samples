<?php
include_once '../application/Bootstrap.php';

$storeid = '';
$eventid = '';
$fb_uid = '';
$mongoid = '';
$ids = array();

if (isset($_GET['storeid'])) {
    $storeid = trim($_GET['storeid']);
    $ids[] = "storeid={$storeid}";
}

if (isset($_GET['eventid'])) {
    $eventid = trim($_GET['eventid']);
    $ids[] = "eventid={$eventid}";
}

// Facebook id - We assume the social queue is active if this is present

if (isset($_GET['fb_uid'])) {
    $fb_uid = intval($_GET['fb_uid']);
    $ids[] = "fb_uid={$fb_uid}";
}

if (isset($_GET['z'])) {
    $mongoid = trim($_GET['z']);
    $ids[] = "z={$mongoid}";
}

$query_string = "?" . implode('&', $ids);
unset($ids);

$location = "/queue_wait.php" . $query_string;


//P3P Session solution | The iframes flux capacitor
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . $location);
exit;