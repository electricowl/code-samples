<?php

//P3P Session solution | The iframes flux capacitor
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

register_shutdown_function('handleShutdown');
$info_qw = '';

// TODO This is a hack but prevents errors where missing Mongo class break the page.
function handleError($info_qw, $store_config, $log_file)
{

    $user_log_message = '[' . date('D M d H:i:s Y') . '][' . $_SERVER['REMOTE_ADDR'] . ']' . $info_qw . 'referer ' . $_SERVER['HTTP_REFERER'] . "\n";
    error_log($user_log_message, 3, $log_file);

    ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>You are currently being held in a queue for tickets...</title>
                <meta http-equiv="refresh" content="30">
            </head>
            <body>
                <?php
                if (isset($store_config) && isset($store_config['text'])) {
                    echo $store_config['text'];
                }
                else {
                ?>
                Click to return to <a href="http://www.crowdsurge.com">Crowdsurge.com</a>
                <?php } ?>

            </body>
        </html>
    <?php

}

function handleShutdown() {
    global $config;
    global $store_config;
    $error = error_get_last();

    if($error !== NULL){
        $info_qw = "[SHUTDOWN] type: " . $error['type'] . "  file:".$error['file'] . " | line:" . $error['line'] . " | msg:" . $error['message'] . PHP_EOL;
        handleError($info_qw, $store_config, $config['queueUserErrorLogFile']);
    }
}


// Loading type
define('LAZY_LOAD', 1);
define('EAGER_LOAD', 2);
define('LOADING_SCHEME', EAGER_LOAD);

include_once '../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Store;
use iqueue\Counter;

$storeid = '';
$eventid = '';
$mongoid = '';
$ids = array();

if (isset($_GET['storeid'])) {
    $storeid = (int) trim($_GET['storeid']);
    $ids[] = "storeid={$storeid}";
}

if (isset($_GET['eventid'])) {
    $eventid = (int) trim($_GET['eventid']);
    $ids[] = "eventid={$eventid}";
}
$query_string = "?" . implode('&', $ids);
unset($ids);

try {
    // Get Mongo instance (using our own Mongo Class)
    // TODO Change the name of this class
    $mongo = new Mongo($queueConfig);
    // Create a QueueManager instance
    $qm = new QueueManager($mongo);

    $mongoStore = new Mongo($storeConfig);
    $store = new Store($mongoStore);

    $store_config = $store->findByStoreId($storeid);

    if (empty($store_config)) {
        // TODO Something.. Redirect somewhere useful...
        echo "This page is misconfigured.";
        exit;
    }

    $mongoCounter = new Counter($qCounterConfig);

    if (isset($store_config['is_social_queue']) && $store_config['is_social_queue']) {
        $mongoCounter->setIsSocial();
    }
    // Here we will assign the.. counter
    $qm->setCounter($mongoCounter);


    // is there a mongo id in the url
    if (isset($_GET['z'])) {
        $mongoid = $_GET['z'];
    }
    else {
        // we cannot find the id so lets create a new record
        $queueData = $qm->loadQueue(
            $storeid,
            $eventid
        );
        $query_string .= '&z=' . (string) $queueData['_id'];
        $location = "/passthru.php" . $query_string;

        header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . $location);
        exit;
    }

    // Get the users queue entry
    $record = $qm->check($mongoid, $storeid);

    if (empty($record['_id'])) {
        // we cannot find the id so lets create a new record
        $queueData = $qm->loadQueue(
            $storeid,
            $eventid
        );
        $query_string .= '&z=' . (string) $queueData['_id'];
        $location = "/passthru.php" . $query_string;

        header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . $location);
        exit;
    }

    $release_store_section = '&menu=listings';
    if ((int) $eventid > 0) {
        $release_store_section = '&menu=detail&eventid=' . (int) $eventid;
    }

    $release_url = $store_config['target_url'] . '/index.php?v=t&storeid=' . (int) $storeid . $release_store_section;

    if (LOADING_SCHEME === EAGER_LOAD) {
        // SCHEME 1: Eager Loading : This scheme explicitly checks for locked:false
        // and the queue user is forwarded to the start of the buying process
        if (false === (bool) $record['locked']) {
            header("Location: $release_url");
            exit;
        }
    }

    if (LOADING_SCHEME === LAZY_LOAD) {
        // SCHEME 2: Lazy Load: This scheme only checks the value of the pointer.
        // If the pointer >= own position value then we fetch and release
        $pointer = $qm->getCurrentPointerValue($storeid);
        if ((int) $record['queue_position'] <= (int) $pointer) {
            header("Location: " . $release_url);
            exit;
        }
    }

}
catch (Exception $e) {
    // No action
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>You are currently being held in a queue for tickets</title>
        <meta http-equiv="refresh" content="30">
        <style>
            /* General Layout */
            body {background: #fff; font-family: arial; padding:0; margin: 0; line-height: 20px; font-size: 14px;}
            h2 {font-size: 14px; font-weight: 700; margin: 0 0 8px 0; padding: 0;}
            ul {margin: 0; padding: 0; margin-bottom: 10px; margin-left: 20px; font-size: 12px;}
            ul li { margin-bottom: 2px;}

            .feed_msg { text-align: center; margin-bottom: 20px; font-size: 12px; display: none; clear:both; }

            <?php   if (empty($_GET['eventid'])) { ?>
                /* Store Layout */
                #event_stats {width:100%; clear:both;}
                #event_stats tr td {line-height: 20px; padding: 11px 20px; border: none; background: #f2f2f2; vertical-align: middle; font-size: 12px; color: #888;}
                #event_stats tr:nth-child(even) td {background: #f9f9f9; text-align: left;}
                #event_stats tr.table_head td {color: #fff; background: #080808; border: none; font-weight: 700; text-transform: uppercase; padding: 14px 20px;}
                #event_stats .event_6163 td {color: #111; font-weight: bold;}

                #event_stats tr td.green {color:#007236;}
                #event_stats tr td.amber {color:#f7941d;}
                #event_stats tr td.red {color:#d02142;}

                #wrap {padding: 0;}

                p {padding: 0 20px 8px 20px; margin: 0; margin-left:60px;}
                img {float:left; display: block; margin-top:22px; margin-left:20px;}
                h1 {background: #080808; font-weight: bold; margin: 0; padding: 14px 20px; color: #fff; text-transform: uppercase; font-size: 100%; margin-bottom: 14px;}

            <?php } else { ?>

                /* Event Layout */
                #loading {padding: 20px 20px; text-align: center; background: #fff;}
                #loading img {max-height: 40px; }
                #loading table {padding: 0; margin:0px; width:100%;}
                #loading table tr td    {font-size: 12px; vertical-align: middle; padding-right: 20px;}
                #loading table tr td.status {text-align: right;}

                #loading table tr td.green {color:#007236;  font-weight: bold;}
                #loading table tr td.amber {color:#f7941d;}
                #loading table tr td.red {color:#d02142;}

                #wrap {padding: 20px 20px 12px; background: #f9f9f9;}

                p {padding: 0 0 8px 0; margin: 0; font-size: 12px;}
                h1 {background: #080808; font-weight: bold; margin: 0; padding: 14px 20px; color: #fff; text-transform: uppercase; font-size: 100%; margin-bottom: 0px;}

            <?php } ?>
        </style>
        <?php if (!empty($store_config['css'])) { ?>
                <style><?php echo $store_config['css']; ?></style>
        <?php } ?>
    </head>
    <body>
        <h1>
            <?php if (!empty($store_config['title'])) {
                echo $store_config['title'];
            }else{
                echo 'You are currently in a queue for tickets';
            } ?>
        </h1>

        <?php   if (empty($_GET['eventid'])) { ?>
            <div id="wrap">
                <img style = "max-height:40px" src="img/<?php echo $store_config['loading_gif']?>.gif" />
                <p>Due to extraordinary demand, you have been placed in a queue of fans in search of tickets.</p>
                <p>You will enter the sale in the order you arrived. By leaving or reloading this page, you will forfeit your place.</p>

            <?php   if (!empty($store_config['text']))
                        echo $store_config['text'];
            ?>

                <p>Thank you for your patience.</p>
                <div class="feed_msg"></div>

            <?php if (!empty($store_config['display_pos']) && (int)$store_config['display_pos'] === 1 ){ ?>

                <table cellspacing="0" cellpadding="3" border="0" id="event_stats">
                    <thead>
                        <tr class="table_head">
                            <td>Artist</td>
                            <td>Date</td>
                            <td>Venue</td>
                            <td>City</td>
                            <td>Country</td>
                            <td>Availability</td>
                        </tr>
                   <thead>
                   <tbody></tbody>
                </table>

            <?php }
                }

            if (!empty($_GET['eventid'])) { ?>

                <?php if (!empty($store_config['display_pos']) && (int)$store_config['display_pos'] === 1 ){ ?>
                <div id="loading">

                        <table id="event_stats">
                           <tbody></tbody>
                        </table>

                </div><!-- loading -->
            <?php } ?>

            <div id="wrap">
                <h2>Due to extraordinary demand, you have been placed in a queue of fans in search of tickets.</h2>
                <ul>
                    <li>This page will automatically refresh every 30 seconds, by leaving or reloading this page, you will forfeit your place.</li>
                    <li>Information on the approximate number of tickets left will be updated above.</li>
                    <li>You will enter the presale in the order you arrived.</li>
                </ul>

                <?php   if (!empty($store_config['text']))
                        echo $store_config['text'];
                ?>

                <p>Thank you for your patience.</p>

                <?php if (empty($store_config['display_pos']) || (int)$store_config['display_pos'] === 0 ){ ?>
                    <img style = "max-height:40px; margin-left:48%" src="img/<?php echo $store_config['loading_gif']?>.gif" />
                <?php } ?>

            <?php } ?>

            </div> <!-- wrap -->
            <div class="feed_msg"></div>

<?php if (!empty($store_config['display_pos']) && (int)$store_config['display_pos'] === 1 ){ ?>

<script src="./js/jquery.js"></script>
<script type="text/javascript">

    function ajaxCall (type, url, data) {
        var a_type = type;
        var a_url = url;
        var dataString = data;
        return $.ajax({
            type: a_type, url: a_url, async: false, data: dataString, dataType: 'jsonp',
            success: function(json) {
                var gif = <?php if(!empty($_GET['eventid'])){ echo '\'<td><img src="img/'.$store_config['loading_gif'].'.gif" /></td>\''; } else { echo "''";} ?>;
                var tableData = '';
                $.each(json.events, function(i, obj) {
                    tableData = tableData +  '<tr class="table_row event_'+obj.eventid+'">'+gif+'<td><strong>'+obj.artist+'</strong></td><td>'+obj.eventdate+'</td><td>'+obj.venue+'</td><td>'+obj.towncity+'</td><td>'+obj.countryname+'</td><td class="'+obj.trafficlight+'">'+obj.stock_msg+'</td></tr>';
                });
                $('#event_stats tbody').append(tableData);
                $('.table_head').show();
            }
        });
    }

    <?php if (!empty($_GET['eventid'])) { ?>

        $(document).ready(function() {
            ajaxCall('GET', "http://crowdsurge.com/presale/feeds/listings.php", 'storeid=<?php echo $store_config['store_id']; ?>&eventid=<?php echo $_GET['eventid']; ?>');

        });

    <?php } else { ?>

        $(document).ready(function() {
            ajaxCall('GET', "http://crowdsurge.com/presale/feeds/listings.php", 'storeid=<?php echo $store_config['store_id']; ?>');
        });

    <?php } ?>

</script>
<script type="text/javascript"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-29179676-1']); _gaq.push(['_setDomainName', 'certechhq.com']); _gaq.push(['_trackPageview']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); </script>

<?php } ?>
    </body>
</html>