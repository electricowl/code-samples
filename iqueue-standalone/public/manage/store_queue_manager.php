<?php
/**
 * store_queue_manager
 * Manage stores
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;
use iqueue\User;
use iqueue\QueueManager;
use iqueue\Counter;

$mongo = new Mongo($queueConfig);
$counter = new Counter($qCounterConfig);
$qm = new QueueManager($mongo);
$qm->setCounter($counter);


$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

if (isset($_GET['storeid']) && $_GET['storeid'] != 0) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_queue_edit.php?storeid=' . $_GET['storeid']);
}

$stores = $store->findAll();

$storesDropdown = array();
$storesList = array();
$numberStores = 0;

// build our view data
if (!empty($stores)) {
    foreach ($stores as $store) {

       $storesDropdown[(string) $store['_id']] = $store['store_id'] . ' ' . $store['band'];

       $storeCounter = new Counter($qCounterConfig);
       // build manual stores
       if (
           ((int) $store['is_automated'] === 0)
           &&
           (
            (!empty($store['status']) && $store['status'] != 'inactive')
            ||
            (!empty($store['last_changed']) && (int) $store['last_changed'] > (time()-60*60))
           )
       ) {

          $store['counter_data'] = $storeCounter->getCounter('store_' . $store['store_id']);
          $storesList[] = $store;
          $numberStores ++;
       }

       // build automated
       if ((int) $store['is_automated'] === 1) {
          $store['counter_data'] = $storeCounter->getCounter('store_' . $store['store_id']);
          $automatedStoresList[] = $store;
       }
    }
}

// Get the current Queue status
$queueMonitorLogFile = $config['queueLastRunLogFile'];
if (file_exists($queueMonitorLogFile)) {
    $queueStatus = file_get_contents($queueMonitorLogFile);
} else {
    $queueStatus = 'stopped';
}

$pid = 0;
$split = explode(' -- ', $queueStatus);
$pidSplit = explode(': ', $split[0]);
if (!empty($pidSplit[1])) {
    $pid = (int) trim($pidSplit[1]);
}

$lastRun = substr($queueStatus, -19);
if (strtotime($lastRun) < time()-20) {
    $queueStatus = 'stopped';
}


$dbLoadLogFile = $config['queueServerLoadLogFile'];
if (file_exists($dbLoadLogFile) &&  $queueStatus != 'stopped') {
    $dbLoad = unserialize(trim(file_get_contents($config['queueServerLoadLogFile'])));
} else  {
    $dbLoad['1min'] = '';
}



$script = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];


$title = 'Store Queue Manager';
?>

<?php include_once 'header.phtml'; ?>

<div>
    <div id="loading"></div>
    <a href="#" id="update_data" />Update</a>
    <a href="#" id="auto_update" />Turn Auto Update Off</a>
    <p><b>Server load:</b> <span id="db_load"><?php echo htmlentities($dbLoad['1min'], ENT_QUOTES, 'utf-8'); ?></span></p>
    <p>
        <b>Queue Health:</b> <span id="queue_health"><?php echo htmlentities($queueStatus, ENT_QUOTES, 'utf-8'); ?></span>

    <a href="#" id="start_process" />Start</a>
    <a href="#" id="stop_process" />Stop</a>

    <span id="update"></span>
    <div style="display:none;" id="id"><?php echo $pid; ?></div>
    </p>
</div>
<hr>

<form action="<?php echo $script; ?>" method="get" name="store_select" class="inline">
    <label for="store">Stores</label>
    <select name="storeid" id="store">
        <option value="0"></option>
        <?php foreach ($storesDropdown as $value => $option) { ?>
            <option value="<?php echo $value; ?>"><?php echo $option; ?></option>
        <?php } ?>
    </select>
    <input type="submit" name="s" value="Go!" />
 </form>

<hr>

<div>
    <h4>Manual Queue</h4>
    <div id="manual_queue">
    <?php if ($numberStores > 0) { ?>
        <table>
            <thead>
                <tr>
                   <td>Store</td>
                   <td>Threshold</td>
                   <td>Block size</td>
                   <td>Check interval</td>
                   <td>Last changed</td>
                   <td>Queue</td>
                   <td>Status</td>
                   <td>&nbsp;</td>
                </tr>
             </thead>
             <tbody>
            <?php foreach ($storesList as $store) { ?>
                <tr>
                    <td>
                        #<?php echo (int) $store['store_id']; ?>
                        <?php echo htmlentities($store['band'], ENT_QUOTES, 'utf-8'); ?>
                    </td>
                    <td><?php echo (float) $store['threshold']; ?></td>
                    <td><?php echo (int) $store['block_size']; ?></td>
                    <td><?php echo (int) $store['check_interval']; ?></td>
                    <td><?php echo date('H:i:s', $store['last_changed']); ?></td>
                    <td>
                    Queue Length: <?php echo $store['counter_data']['counter'] ?><br>
                    Processed To: <?php echo $store['counter_data']['pointer'] ?><br>
                    Still in Queue: <?php echo $store['counter_data']['counter'] - $store['counter_data']['pointer'] ?>
                    </td>
                    <td><?php echo htmlentities($store['status'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td>
                       <a href="store_queue_edit.php?storeid=<?php echo htmlentities($store['_id'], ENT_QUOTES, 'UTF-8'); ?>">Edit</a>
                       |
                       <a href="queue_stats.php?store=store_<?php echo (int) $store['store_id']; ?>" target="_blank">Stats</a>
                    </td>
                </tr>
            <?php } ?>
                </tbody>
         </table>
    <?php } else { ?>
        <p>No Stores recently activated</p>
    <?php } ?>
    </div>
</div>
<div>
    <h4>Automated Queue</h4>
    <div id="automated_queue">
    <?php if (!empty($automatedStoresList)) { ?>
        <table>
            <thead>
                <tr>
                   <td>Store</td>
                   <td>Threshold</td>
                   <td>Block size</td>
                   <td>Check interval</td>
                   <td>Last changed</td>
                   <td>Queue</td>
                   <td>Status</td>
                   <td>&nbsp;</td>
                </tr>
             </thead>
             <tbody>
            <?php foreach ($automatedStoresList as $automated) { ?>
                <tr>
                    <td>
                        #<?php echo (int) $automated['store_id']; ?>
                        <?php echo htmlentities($automated['band'], ENT_QUOTES, 'utf-8'); ?>
                    </td>
                    <td><?php echo (float) $automated['threshold']; ?></td>
                    <td><?php echo (int) $automated['block_size']; ?></td>
                    <td><?php echo (int) $automated['check_interval']; ?></td>
                    <td><?php echo date('H:i:s', $automated['last_changed']); ?></td>
                    <td>
                    Queue Length: <?php echo $automated['counter_data']['counter'] ?><br>
                    Processed To: <?php echo $automated['counter_data']['pointer'] ?><br>
                    Still in Queue: <?php echo $automated['counter_data']['counter'] - $automated['counter_data']['pointer'] ?>
                    </td>
                    <td><?php echo htmlentities($automated['status'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td>
                       <a href="store_queue_edit.php?storeid=<?php echo htmlentities($automated['_id'], ENT_QUOTES, 'UTF-8'); ?>">Edit</a>
                       |
                       <a href="queue_stats.php?store=store_<?php echo (int) $automated['store_id']; ?>" target="_blank">Stats</a>
                    </td>
                </tr>
            <?php } ?>
                </tbody>
         </table>
    <?php } else { ?>
        <p>No stores are automated</p>
    <?php } ?>
    </div>
</div>

<script src="../js/jquery.js"></script>
<script type="text/javascript">

    function updateInterval() {
       return  setInterval(function() {
			 doStoresAjax();
       }, 5000);
    }

    var autoUpdate = updateInterval();
    $('#auto_update').click(function(e){
        if ($('#auto_update').html() === 'Turn Auto Update On') {
            autoUpdate = updateInterval();
            $('#auto_update').html('Turn Auto Update Off');
        }
        else {
            $('#auto_update').html('Turn Auto Update On');
            window.clearInterval(autoUpdate);
        }
    });

    $('#start_process').click(function(e){
        doQueueProcessAjax('start');
        setTimeout(function() {
			doQueueProcessAjax('update_last_run');
		}, 1000);

    });

    $('#stop_process').click(function(e){
         doQueueProcessAjax('stop');
    });

    $('#update_data').click(function(e){
         doStoresAjax();
    });

    function doQueueProcessAjax(action) {
        $.ajax({
            url : 'ajax_start_stop_queue.php',
            type : 'POST',
            data: {'action' : action, 'id' : $('#id').html()},
        }).done(
            function(e) {
                if (action === 'update_last_run') {
                    $('#queue_health').html(e.message);
                    $('#id').html(e.id);
                } else {
                    $('#update').html(e.message);
                }
            }
        );
    }
    function doStoresAjax() {
        $.ajax({
            url : 'ajax_stores.php',
            type : 'GET',
            beforeSend: function() {
                $('#loading').html('<img src="/img/load-px.gif" />');
            }
        }).done(
            function(e) {
                $('#manual_queue').html(e.manual_queue);
                $('#automated_queue').html(e.automated_queue);
                $('#queue_health').html(e.queueStatus);
                $('#db_load').html(e.dBLoad);
                $('#id').html(e.id);
                $('#update').html('');
                $('#loading').html('');
            }
        );
    }
</script>
<?php include_once 'footer.phtml'; ?>
