<?php
/**
 * store_queue_manager
 * Manage stores
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;
use iqueue\User;
use iqueue\QueueManager;
use iqueue\Counter;

$mongo = new Mongo($queueConfig);
$counter = new Counter($qCounterConfig);
$qm = new QueueManager($mongo);
$qm->setCounter($counter);


$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

if(isset($_GET['storeid'])){
    $storeid = (int)$_GET['storeid'];
    $exist = $store->findByStoreId($storeid);

    if (empty($exist) || $exist == FALSE || $exist = '') {
        unset($storeid);
        $msg = "Sorry, this store id doesn't exist";
    }
}


if(!empty($_POST['NumberToRelease']) && $_POST['NumberToRelease'] > 0) {

    $singleStore = $store->findByStoreId($storeid);
    $storeCounter = new Counter($qCounterConfig);
    $singleStore['counter_data'] = $storeCounter->getCounter('store_' . $singleStore['store_id']);

    $numberToRelease = (int) $_POST['NumberToRelease'];
    if ($singleStore['counter_data']['counter'] - $singleStore['counter_data']['pointer'] <= (int) $_POST['NumberToRelease']){
        $numberToRelease = $singleStore['counter_data']['counter'] - $singleStore['counter_data']['pointer'];
    } 
    $mongoCounter = new Counter($qCounterConfig);
    $qm->setCounter($mongoCounter);
    $qm->releaseBlockFromQueue($storeid, $numberToRelease);

}

if (isset($storeid) && $storeid != 0) { 
    // We have to re-call getCounter to have an up to date counter of the store after releasing people for the queue
    $singleStore = $store->findByStoreId($storeid);
    $storeCounter = new Counter($qCounterConfig);
    $singleStore['counter_data'] = $storeCounter->getCounter('store_' . $singleStore['store_id']);

}

$stores = $store->findAll();
$storesDropdown = array();
$numberStores = 0;

// build our view data
if (!empty($stores)) {
    foreach ($stores as $store) {
        $storesDropdown[(int) $store['store_id']] = $store['store_id'] . ' ' . $store['band'];
        $numberStores ++;
    
    }
}

$script = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];


$title = 'Store Queue Manual Release';
?>

<?php include_once 'header.phtml'; ?>

<form action="<?php echo $script; ?>" method="get" name="store_select" class="inline">
    <label for="store">Stores</label>
    <select name="storeid" id="store">
        <option value="0"></option>
        <?php if ($numberStores > 0) { ?>
            <?php foreach ($storesDropdown as $value => $option) {
                if ($value == $storeid) { ?>
                     <option value="<?php echo $value; ?>" selected><?php echo $option; ?></option>
                <?php }else{ ?>
                    <option value="<?php echo $value; ?>"><?php echo $option; ?></option>
            <?php } 
            }
        } ?>
    </select>
    <input type="submit" name="s" value="Go!" /> 
 </form>
<hr>

    <?php if (isset($singleStore)) { ?>
        <div class="head_form">
            <div>
                Queue Length: <?php echo $singleStore['counter_data']['counter'] ?><br>
                Processed To: <?php echo $singleStore['counter_data']['pointer'] ?><br>
                Still in Queue: <?php echo $singleStore['counter_data']['counter'] - $singleStore['counter_data']['pointer'] ?>
            </div>
            <form action="<?php echo $script."?storeid=".$storeid; ?>" method="POST" name="store_select" class="inline" id="form_release">
                <div>
                    <label for="NumberToRelease">Number Of People To Release</label>
                    <input 
                        type="number" 
                        name="NumberToRelease" 
                        value="0"
                    />
                </div>
                <div>
                    <input
                        type="submit" 
                        name="submit" 
                        value="Release" 
                    />
                </div>
                    
            </form>
        </div>
    <?php }
    if (isset($msg)) { ?>
        <div class="msg error" style="break:both">
            <?php echo $msg; ?>
        </div>
    <?php } ?>
<?php include_once 'footer.phtml'; ?>
