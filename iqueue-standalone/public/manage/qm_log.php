<?php
/**
 * queue_stats
 * Display queue statistics including length of queue and released count
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
$selectedStore = null;
$counterData = null;
$storeid = '';

if (isset($_GET['store'])) {
     $selectedStore = $_GET['store'];
     list(,$storeid) = explode('_', $selectedStore);
}

// display available queues
// examine queue
// start/stop queue
// We'll do this in Raw Mongo
include_once '../../application/Bootstrap.php';
use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Counter;
use iqueue\Store;
use iqueue\User;


$mongoUser = new Mongo($userConfig);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

exec('cat ' . $config['queueLogFile'], $response, $code);

?>

<?php include_once 'header.phtml'; ?>
    <div>
       <pre> <?php print_r($response); ?></pre>
    </div>
<?php include_once 'footer.phtml'; ?>