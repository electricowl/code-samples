<?php
/**
 * store_add
 * Add a store to the store_configs collection
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;

$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$errors = '';

if (!empty($_POST['delete'])) {
    if (!empty($_POST['_id'])) {
        $mongoId = $_POST['_id'];
        if ($store->delete($mongoId)) {
            header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_index.php');
            exit();
        } else {
            $errors .= 'Unable to delete';
        }
    }
}



if (isset($_GET['storeid']) && !empty($_GET['storeid'])) {
    $formData = $store->findByMongoId($_GET['storeid']);
} else {
    echo 'no id passed';
    exit();
}

$title = 'Store Delete';
?>

<?php include_once 'header.phtml'; ?>

<h2>Delete Store</h2>
<div class="errors"><?php echo $errors; ?></div>
<div class="head_form">
    <form action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . '?storeid=' . $formData['_id']; ?>" method="post" name="store_delete" id="store_add">
    <p>Confirm delete store: #<?php echo (int) $formData['store_id'] . ' ' . htmlentities($formData['band'], ENT_QUOTES, 'utf-8'); ?></p>
    <input type="hidden" name="_id" value="<?php echo $formData['_id']; ?>" />
    <input type="submit" name="delete" value="Delete" />
    </form>
</div>

<?php include_once 'footer.phtml'; ?>