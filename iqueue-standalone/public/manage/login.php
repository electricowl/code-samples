<?php
/**
 * store_add
 * Add a store to the store_configs collection
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\User;

$mongo = new Mongo($userConfig);
$user = new iQueue\User($mongo);

if (!empty($_POST)) {
    $data['username'] = $_POST['username'];
    $data['password'] = $_POST['password'];
    
    if ($user->login($data)) {
        header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_queue_manager.php');
        exit();
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crowdsurge iQueue || Stores</title>
        <style type="text/css">
            body {font-family: Verdana, Trebuchet, Sans-serif;}
            .form {margin: 0 auto; width: 400px; margin-top: 160px;}
            form div {width: 100%; height: 30px;}
            input { display:block; width: 65%; float: left;}
            label { display:block; width: 33%; float: left;}
            input[type=submit] { width: 20%; float: right;}
        </style>
    </head>
    <body>
        <div class="form">
            <form action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']; ?>" method="post" name="login" id="login">    
            <div>
                <label for="username">Username</label>
                <input type="text" name="username" value="" />
            </div>
            <div>
                <label for="password">Password</label>
                <input type="password" name="password" value="" />
            </div>
            <div>
                <input type="submit" name="login" value="log in" />
            </div>
            </form>
        </div>
    </body>
</html>
