<?php
/**
 * Ajax Endpoint
 *
 */

include_once '../../application/Bootstrap.php';


use iqueue\Mongo;
use iqueue\User;

$mongoUser = new Mongo($userConfig);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    exit;
}

$results = array();

if (!empty($_POST['action'])) {

   $currentPid = (!empty($_POST['id'])) ? (int) $_POST['id'] : 0;

   $results['action'] = $_POST['action'];

   if ('start' === $_POST['action']) {

       $results['message'] = 'process is already running';

       if (! file_exists($config['queueLockFile'])) {
           $environment = 'APPLICATION_ENV=' . ENVIRONMENT .' ; export APPLICATION_ENV ; ';
           $script =  $environment . ' ' . $config['phpPath'] . ' ' . ROOT . $config['queueScriptFile'] . ' >' . $config['queueLogFile'] . ' 2>&1 &';
           exec($script);
           file_put_contents($config['queueLockFile'], 'running');
           $results['message'] = 'process started';
       }
   }

   if ('stop' === $_POST['action']) {

       $results['message'] = 'process is not running';

       if (file_exists($config['queueLockFile'])) {
           $stopScript = '/bin/kill -s SIGTERM ' . $currentPid;
           exec($stopScript);
           unlink($config['queueLockFile']);
           $results['message'] = 'process killed';
       }
   }

   if ('update_last_run' === $_POST['action']) {
        $results['message'] =  'Unable to open file';
        if (file_exists($config['queueLastRunLogFile'])) {
            $queueStatus = file_get_contents($config['queueLastRunLogFile']);
            $results['message'] = $queueStatus;
            $results['id'] = 0;
            $split = explode(' -- ', $queueStatus);
            $pidSplit = explode(': ', $split[0]);
            if (!empty($pidSplit[1])) {
                $results['id'] = (int) trim($pidSplit[1]);
            }
        }
   }
}
header('Content-type: text/json');
//header('Content-type: text/plain');
echo json_encode($results);


