<?php
/**
 * store_add
 * Add a store to the store_configs collection
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;
use iqueue\User;


$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$stores = $store->findAll();

$defaultStore = $store->findDefaultStore();

$title = 'Store Index';
?>

<?php include_once 'header.phtml'; ?>

<h2>
    Stores
    <span class="pull_right size12">
        <a href="store_add.php">Add store</a>
    </span>
</h2>

<div class="stores_list">
    <div class="store">
            <div class="list_content">
             Default Store
            </div>
            <div class="actions">
                <a href="store_edit.php?storeid=<?php echo htmlentities($defaultStore['_id'], ENT_QUOTES, 'UTF-8'); ?>">edit</a>
                |
                <a href="../store_preview.php?storeid=<?php echo htmlentities($defaultStore['store_id'], ENT_QUOTES, 'UTF-8'); ?>" target="_blank">preview</a>
            </div>
        </div>

    <?php  foreach ($stores as $store) { ?>
        <div class="store">
            <div class="list_content">
             Store #<?php echo (int) $store['store_id']; ?>
             <?php echo htmlentities($store['band'], ENT_QUOTES, 'utf-8'); ?>
            </div>
            <div class="actions">
                <a href="store_edit.php?storeid=<?php echo htmlentities($store['_id'], ENT_QUOTES, 'UTF-8'); ?>">edit</a>
                |
                <a href="../store_preview.php?storeid=<?php echo (int) $store['store_id']; ?>" target="_blank">preview</a>
                |
                <a href="store_delete.php?storeid=<?php echo htmlentities($store['_id'], ENT_QUOTES, 'UTF-8'); ?>">delete</a>
            </div>
        </div>

    <?php } ?>
</div>

<?php include_once 'footer.phtml'; ?>

