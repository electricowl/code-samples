<?php
/**
 * store_add
 * Add a store to the store_configs collection
 * @author Jamie Wright <jamie.wright@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;

$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$errors = '';

if (!empty($_POST)) {

    if (
        empty($_POST['_id'])
        ||
        empty($_POST['store_id'])
        ||
        empty($_POST['block_size'])
        ||
        empty($_POST['check_interval'])
        ||
        empty($_POST['threshold'])
    ) {
        $errors .= 'Please complete all the fields';
    }
    else {
        $mongoId = $_POST['_id'];

        $status = $_POST['status'];
        $changeTime = $_POST['last_changed'];

        if (!empty($_POST['start_queue']) && (int) $_POST['start_queue'] === 1) {
            $status = 'starting';
            $changeTime = time();
        }

        if (!empty($_POST['stop_queue']) && (int) $_POST['stop_queue'] === 1) {
            $status = 'shutdown sent';
            $changeTime = time();
        }

        $data = array(
            'store_id' => (int) $_POST['store_id'],
            'band' => $_POST['band'],
            'block_size' => $_POST['block_size'],
            'check_interval' => $_POST['check_interval'],
            'threshold' => $_POST['threshold'],
            'is_automated' => (!empty($_POST['is_automated'])) ? (int) $_POST['is_automated'] : 0,
            'status' => $status,
            'last_changed' => $changeTime,
        );

        if ($store->updateByMongoId($mongoId, $data)) {
            header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_queue_manager.php');
            exit();
        } else {
            $errors .= 'Unable to save data';
        }
    }

}

if (!empty($_POST)) {
    $formData = $_POST;
}
else if (isset($_GET['storeid']) && !empty($_GET['storeid'])) {
    $formData = $store->findByMongoId($_GET['storeid']);
} else {
    echo 'no id passed';
    exit();
}

$queueStatus = (!empty($formData['status'])) ? $formData['status'] : 'inactive';
$queueChangeTime = (!empty($formData['last_changed'])) ? $formData['last_changed'] : 0;


$title = 'Store Queue Edit';
?>

<?php include_once 'header.phtml'; ?>

<p><b>Store Queue:</b> <?php echo (int) $formData['store_id']; ?> <?php echo htmlentities($formData['band'], ENT_QUOTES, 'utf-8'); ?></p>

<div class="errors"><?php echo $errors; ?></div>
<div class="head_form">

    <form
        action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . '?storeid=' . $formData['_id']; ?>"
        method="post"
        name="store_edit"
        id="store_edit"
    >
    <input type="hidden" name="_id" value="<?php echo $formData['_id']; ?>" />
    <input type="hidden" name="store_id" value="<?php echo $formData['store_id']; ?>" />
    <input type="hidden" name="band" value="<?php echo $formData['band']; ?>" />
    <input type="hidden" name="status" value="<?php echo $queueStatus; ?>" />
    <input type="hidden" name="last_changed" value="<?php echo $queueChangeTime; ?>" />
    <div>
        Queue Status: <b><?php echo $queueStatus; ?></b>
    </div>
    <div>
        <label for="is_automated">Automate store queue?</label>
        <input
            type="checkbox"
            name="is_automated"
            value="1"
            <?php echo (!empty($formData['is_automated']) && (int) $formData['is_automated'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
        <p class="info">
            Checking this will allow the Queue process to automatically start/stop the queue for this store.
        </p>
    </div>
    <?php if ((int) $formData['is_automated'] == 0) { ?>
        <?php if ($queueStatus === 'inactive') { ?>
            <div>
                <label for="start_queue">Start queue</label>
                <input
                    type="checkbox"
                    name="start_queue"
                    value="1"
                />
                <p class="info">
                    Starting the queue will free queued users depending on parameters below
                </p>
            </div>
        <?php } else if ($queueStatus === 'running') { ?>
        <div>
            <label for="stop_queue">Stop queue</label>
            <input
                type="checkbox"
                name="stop_queue"
                value="1"
            />
            <p class="info">
                Stopping the queue will send a shutdown to the queue process
                - users in queue will continue to clear until queue is empty
            </p>
        </div>
        <?php } ?>
    <?php } ?>
    <div>
        <label for="block_size">Block Size</label>
        <input
            type="number"
            name="block_size"
            value="<?php echo (!empty($formData['block_size'])) ? $formData['block_size'] : ''; ?>"
        />
    </div>
    <div>
        <label for="check_interval">Check Interval(s)</label>
        <input
            type="number"
            name="check_interval"
            value="<?php echo (!empty($formData['check_interval'])) ? $formData['check_interval'] : ''; ?>"
        />
    </div>
    <div>
        <label for="threshold">Threshold</label>
        <input
            type="number"
            name="threshold"
            value="<?php echo (!empty($formData['threshold'])) ? $formData['threshold'] : ''; ?>"
            step="any"
        />
    </div>
    <div>
        <input
            type="submit"
            name="submit"
            value="save"
        />
    </div>
    </form>
</div>

 <?php include_once 'footer.phtml'; ?>
