<?php
/**
 * store_add
 * Add a store to the store_configs collection
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;

$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$errors = '';

// default values
$formData = array(
    'store_id' => '',
    'band' => '',
    'title' => '',
    'text' => '',
    'css' => '',
    'display_pos' => '',
    'is_social_queue' => '',
    'target_url' => '',
    'is_live' => '',
    'loading_gif' => '',
);

if (!empty($_POST)) {

    $formData = array(
        'store_id' => (!empty($_POST['store_id'])) ? (int) $_POST['store_id'] : '',
        'band' => (!empty($_POST['band'])) ? $_POST['band'] : '',
        'text' => (!empty($_POST['text'])) ? $_POST['text'] : '',
        'title' => (!empty($_POST['title'])) ? $_POST['title'] : '',
        'css' => (!empty($_POST['css'])) ? $_POST['css'] : '',
        'display_pos' => (!empty($_POST['display_pos'])) ? (int) $_POST['display_pos'] : 0,
        'is_social_queue' => (!empty($_POST['is_social_queue'])) ? (int) $_POST['is_social_queue'] : 0,
        'target_url' => (!empty($_POST['target_url'])) ? $_POST['target_url'] : '',
        'is_automated' => (!empty($_POST['is_automated'])) ? (int) $_POST['is_automated'] : 0,
        'status' => 'inactive',
        'threshold' => 5,
        'check_interval' => 30,
        'block_size' => 100,
        'loading_gif' => (!empty($_POST['loading_gif'])) ? $_POST['loading_gif'] : '',
    );

    if (empty($_POST['store_id']) || empty($_POST['band']) || empty($_POST['target_url'])) {
        $errors .= 'Please enter the store id, band title, and target URL';
    } else {

            if ($store->insert($formData)) {
                header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_index.php');
                exit();
            } else {
                $errors .= 'Unable to save data. Check the store ID has not already been used.';
            }
    }

}

$title = 'Store Add';
?>

<?php include_once 'header.phtml'; ?>

<script>
    $(document).ready(function() {

        function appendGif(fileName){
            fileName = typeof fileName !== 'undefined' ? fileName : 'framelys' ;
            $("#gifContainer").append(fileName ? "<img src='../img/" + fileName + ".gif' class='gif' style='width:40px'>" : "");
            return "TRUE";
        };

        appendGif();

        $('[name="loading_gif"]').change(function () {
            var src = $(this).val();
            if($('.gif').attr('src') != ''){
                $('.gif').hide();
            };
            appendGif(src);
        });
    });
</script>

<h2>Add Store</h2>
<div class="errors"><?php echo $errors; ?></div>
<div class="head_form">
    <form
        action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']; ?>"
        method="post"
        name="store_add"
        id="store_add"
        enctype="multipart/form-data"
    >
    <div>
        <label for="is_automated">Automate store queue?</label>
        <input
            type="checkbox"
            name="is_automated"
            value="1"
            <?php echo (!empty($formData['is_automated']) && (int) $formData['is_automated'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
        <p class="info">
            Checking this will allow the Queue process to automatically start/stop the queue for this store.
        </p>
    </div>
    <div>
        <label for="store_id">Store Id</label>
        <input
            type="text"
            name="store_id"
            value="<?php echo $formData['store_id']; ?>"
        />
    </div>
    <div>
        <label for="band">Band</label>
        <input
            type="text"
            name="band"
            value="<?php echo $formData['band']; ?>"
        />

    </div>
    <div>
        <label for="target_url">Target Url</label>
        <input
            type="text"
            name="target_url"
            value="<?php echo $formData['target_url']; ?>"
        />
    </div>
    <div class="text">
        <label for="title">Title</label>
        <input name="title"><?php echo $formData['title']; ?></input>
    </div>
    <div class="textarea">
        <label for="text">Body HTML (Text)</label>
        <p class="info">
            Simply leave it blank if you want the default text
        </p>
        <textarea name="text"><?php echo $formData['text']; ?></textarea>
    </div>
    <div class="textarea">
        <label for="css">CSS</label>
        <textarea name="css"><?php echo $formData['css']; ?></textarea>
    </div>
    <div>
        <label for="display_pos">Display POS & TTW</label>
        <input
            type="checkbox"
            name="display_pow"
            value="1"
            <?php echo ((int) $formData['display_pos'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
    </div>
    <div>
        <label for="is_social_queue">Is Social?</label>
        <input
            type="checkbox"
            name="is_social_queue"
            value="1"
            <?php echo ((int) $formData['is_social_queue'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
    </div>
    <div> <!-- HANDLE UPLOAD LOADING GIF -->
        <label for="loading_gif">Gif Loading Image</label>
         <select name="loading_gif"/>
            <option value="framelys">Circle ColorFull</option>
            <option value="load-ak">Red Circle</option>
            <option value="load-px">Grey Bar</option>
            <option value="load">Grey Stars</option>
            <option value="loading0">Grey Thin Circle</option>
            <option value="loading1">Colorful Thin Circle</option>
            <option value="loading2">Blue Circle</option>
            <option value="loading3">Grey "Apple" Circle</option>
        </select>
    </div>
        <div id="gifContainer" style="text-align:center;">
    </div>
    <div>
        <input
            type="submit"
            name="submit"
            value="save"
        />
    </div>

    </form>
</div>

<?php include_once 'footer.phtml'; ?>