<?php

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\User;

$mongoUser = new Mongo($userConfig);
$user = new iQueue\User($mongoUser);

$user->logout();
header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_index.php');
exit();

