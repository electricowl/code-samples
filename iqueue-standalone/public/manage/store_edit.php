<?php
/**
 * store_add
 * Add a store to the store_configs collection
 * @copyright (c) 2012, Crowdsurge Ltd
 */

include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\Store;

$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$errors = '';

if (!empty($_POST)) {

    if (empty($_POST['store_id']) || empty($_POST['band']) || empty($_POST['target_url'])) {
        $errors .= 'Please enter the store id, band title, text, and target URL';
    } else {
        $mongoId = $_POST['_id'];
        $data = array(
            'band' => $_POST['band'],
            'text' => (!empty($_POST['text'])) ? $_POST['text'] : '',
            'title' => (!empty($_POST['title'])) ? $_POST['title'] : '',
            'css' => (!empty($_POST['css'])) ? $_POST['css'] : '',
            'store_id' => ($_POST['store_id'] === 'default') ? $_POST['store_id'] : (int) $_POST['store_id'],
            'display_pos' => (!empty($_POST['display_pos'])) ? (int) $_POST['display_pos'] : 0,
            'is_social_queue' => (!empty($_POST['is_social_queue'])) ? (int) $_POST['is_social_queue'] : 0,
            'target_url' => $_POST['target_url'],
            'is_live' => (!empty($_POST['is_live'])) ? (int) $_POST['is_live'] : 0,
            'loading_gif' => (!empty($_POST['loading_gif'])) ? $_POST['loading_gif'] : '',
        );
        if ($store->updateByMongoId($mongoId, $data)) {
            header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/store_index.php');
            exit();
        } else {
            $errors .= 'Unable to save data';
        }
    }

}

if (!empty($_POST)) {
    $formData = $_POST;
}
else if (isset($_GET['storeid']) && !empty($_GET['storeid'])) {
    $formData = $store->findByMongoId($_GET['storeid']);
} else {
    echo 'no id passed';
    exit();
}

$title = 'Store Edit';
?>

<?php include_once 'header.phtml'; ?>

<script>
    $(document).ready(function() {

        function appendGif(fileName){
            fileName = typeof fileName !== 'undefined' ? fileName : 'framelys' ;
            $("#gifContainer").append(fileName ? "<img src='../img/" + fileName + ".gif' class='gif' style='width:40px'>" : "");
            return "TRUE";
        };

        appendGif();

        $('[name="loading_gif"]').change(function () {
            var src = $(this).val();
            if($('.gif').attr('src') != ''){
                $('.gif').hide();
            };
            appendGif(src);
        });
    });
</script>

<h2>Edit Store</h2>
<div class="errors"><?php echo $errors; ?></div>
<div class="head_form">
    <form
        action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . '?storeid=' . $formData['_id']; ?>"
        method="post"
        name="store_edit"
        id="store_edit"
        enctype="multipart/form-data"
    >
    <input type="hidden" name="_id" value="<?php echo $formData['_id']; ?>" />
    <div>
        <label for="is_automated">Automate store queue?</label>
        <input
            type="checkbox"
            name="is_automated"
            value="1"
            <?php echo (!empty($formData['is_automated']) && (int) $formData['is_automated'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
        <p class="info">
            Checking this will allow the Queue process to automatically start/stop the queue for this store.
        </p>
    </div>
    <div>
        <label for="store_id">Store Id</label>
        <?php if ($formData['store_id'] === 'default') { ?>
        <input
            type="text"
            name="store_id"
            value="<?php echo $formData['store_id']; ?>"
            READONLY
        />
        <?php } else { ?>
        <input
            type="text"
            name="store_id"
            value="<?php echo $formData['store_id']; ?>"
        />
        <?php } ?>
    </div>
    <div>
        <label for="band">Band</label>
        <input
            type="text"
            name="band"
            value="<?php echo $formData['band']; ?>"
        />
    </div>
    <div>
        <label for="target_url">Target Url</label>
        <input
            type="text"
            name="target_url"
            value="<?php echo $formData['target_url']; ?>"
        />
    </div>
    <div class="text">
        <label for="title">Title</label>
        <input name="title" value="<?php if (!empty($formData['title'])) echo $formData['title'] ?>"></input>
    </div>
    <div class="textarea">
        <label for="text">Body HTML (Text)</label>
        <textarea name="text"><?php if (!empty($formData['text'])) echo $formData['text'] ?></textarea>
    </div>
    <div class="textarea">
        <label for="css">CSS</label>
        <textarea name="css"><?php if (!empty($formData['css'])) echo $formData['css'] ?></textarea>
    </div>
    <div>
        <label for="display_pos">Display POS & TTW</label>
        <input
            type="checkbox"
            name="display_pos"
            value="1"
            <?php echo (!empty($formData['display_pos']) && (int) $formData['display_pos'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
    </div>
    <div>
        <label for="is_social_queue">Is Social?</label>
        <input
            type="checkbox"
            name="is_social_queue"
            value="1"
            <?php echo ((int) $formData['is_social_queue'] == 1) ? 'CHECKED=CHECKED': ''; ?>
        />
    </div>
    <div> <!-- HANDLE UPLOAD LOADING GIF -->
        <label for="loading_gif">Gif Loading Image</label>
         <select name="loading_gif"/>
            <option value="framelys">Circle ColorFull</option>
            <option value="load-ak">Red Circle</option>
            <option value="load-px">Grey Bar</option>
            <option value="load">Grey Stars</option>
            <option value="loading0">Grey Thin Circle</option>
            <option value="loading1">Colorful Thin Circle</option>
            <option value="loading2">Blue Circle</option>
            <option value="loading3">Grey "Apple" Circle</option>
        </select>
    </div>
    <div id="gifContainer" style="text-align:center;">
    </div>
    <div>
        <input
            type="submit"
            name="submit"
            value="save"
        />
    </div>
    </form>
</div>

 <?php include_once 'footer.phtml'; ?>
