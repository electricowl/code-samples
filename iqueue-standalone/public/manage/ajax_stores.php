<?php
/**
 * Ajax Endpoint
 */

include_once '../../application/Bootstrap.php';


function buildQueueTable($storesList) {
    $row = '';
    foreach ($storesList as $store) {
       $stillInQueue = $store['counter_data']['counter'] - $store['counter_data']['pointer'];
       $row .= '
            <tr>
                <td>
                    #' .  (int) $store['store_id'] .'
                    ' . htmlentities($store['band'], ENT_QUOTES, 'utf-8') . '
                </td>
                <td>' . (float) $store['threshold'] . '</td>
                <td>' .  (int) $store['block_size'] . '</td>
                <td>' .  (int) $store['check_interval'] . '</td>
                <td>' .  date('H:i:s', $store['last_changed']) . '</td>
                <td>
                Queue Length: ' . $store['counter_data']['counter'] . '<br>
                Processed To: ' . $store['counter_data']['pointer'] . '<br>
                Still in Queue: ' . $stillInQueue . '
                </td>
                <td>' .  htmlentities($store['status'], ENT_QUOTES, 'UTF-8') . '</td>
                <td>
                   <a href="store_queue_edit.php?storeid=' . htmlentities($store['_id'], ENT_QUOTES, 'UTF-8') . '">Edit</a>
                   |
                   <a href="queue_stats.php?store=store_' . (int) $store['store_id'] . '" target="_blank">Stats</a> 
                </td>
            </tr>
         ';
     }
    
    return '
        <table>
            <thead>
                <tr>
                   <td>Store</td>
                   <td>Threshold</td>
                   <td>Block size</td>
                   <td>Check interval</td>
                   <td>Last changed</td>
                   <td>Queue</td>
                   <td>Status</td>
                   <td>&nbsp;</td>
                </tr>
             </thead>
             <tbody>
             ' . $row . '
             </tbody>
         </table>
     ';
}

use iqueue\Mongo;
use iqueue\Store;
use iqueue\User;
use iqueue\QueueManager;
use iqueue\Counter;

$mongo = new Mongo($queueConfig);
$counter = new Counter($qCounterConfig);
$qm = new QueueManager($mongo);
$qm->setCounter($counter);


$mongoStore = new Mongo($storeConfig);
$mongoUser = new Mongo($userConfig);

$store = new iqueue\Store($mongoStore);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    exit;
}

$output = array();


$stores = $store->findAll();

$storesDropdown = array();
$storesList = array();
$numberStores = 0;
$isLiveDBLoad = false; // whether the DBLoad is changing by the queue


// build our view data
if (!empty($stores)) {
    foreach ($stores as $store) {

       $storesDropdown[(string) $store['_id']] = $store['store_id'] . ' ' . $store['band'];
    
       $storeCounter = new Counter($qCounterConfig);
       // build manual stores
       if (
           ((int) $store['is_automated'] === 0)
           &&
           (
            (!empty($store['status']) && $store['status'] != 'inactive')
            ||
            (!empty($store['last_changed']) && (int) $store['last_changed'] > (time()-60*60))
           )
       ) {
          $store['counter_data'] = $storeCounter->getCounter('store_' . $store['store_id']);
          $storesList[] = $store;
          $numberStores ++;
       }
       
       // build automated
       if ((int) $store['is_automated'] === 1) { 
          $store['counter_data'] = $storeCounter->getCounter('store_' . $store['store_id']);
          $automatedStoresList[] = $store;
       }
       
       // is the DB load in action
       if ($store['status'] !== 'inactive') {
            $isLiveDBLoad = true;          
       }
    }
}


// manual queue html
if ($numberStores > 0) {
    $output['manual_queue'] = buildQueueTable($storesList);
 } else {
    $output['manual_queue'] = '<p>No Stores recently activated</p>';
 }

// automated queue html
if (!empty($automatedStoresList)) {
    $output['automated_queue'] = buildQueueTable($automatedStoresList);
 } else {
    $output['automated_queue'] = '<p>No stores are automated</p>';
 }

// Get the current Queue status
$queueMonitorLogFile = $config['queueLastRunLogFile'];
if (file_exists($queueMonitorLogFile)) {
    $output['queueStatus'] = file_get_contents($queueMonitorLogFile);
} else {
    $output['queueStatus'] = 'stopped';
}

$output['id'] = 0;
$split = explode(' -- ', $output['queueStatus']);
$pidSplit = explode(': ', $split[0]);
if (!empty($pidSplit[1])) {
    $output['id'] = (int) trim($pidSplit[1]);
}
$lastRun = substr($output['queueStatus'], -19);
if (strtotime($lastRun) < time()-20) {
    $output['queueStatus'] = 'stopped';
}

$dbLoadLogFile = $config['queueServerLoadLogFile'];
if (file_exists($dbLoadLogFile) &&  $output['queueStatus'] != 'stopped') {
    $logFileContents = unserialize(trim(file_get_contents($config['queueServerLoadLogFile'])));
    $output['dBLoad'] = $logFileContents['1min'];
} else {
    $output['dBLoad'] = '';
}

header('Content-type: text/json');
//header('Content-type: text/plain');
echo json_encode($output);


