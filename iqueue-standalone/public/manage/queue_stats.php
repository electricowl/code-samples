<?php
/**
 * queue_stats
 * Display queue statistics including length of queue and released count
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
$selectedStore = null;
$counterData = null;
$storeid = '';

if (isset($_GET['store'])) {
     $selectedStore = $_GET['store'];
     list(,$storeid) = explode('_', $selectedStore);
}

// display available queues
// examine queue
// start/stop queue
// We'll do this in Raw Mongo
include_once '../../application/Bootstrap.php';
use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Counter;
use iqueue\Store;
use iqueue\User;


$mongoUser = new Mongo($userConfig);
$user = new iqueue\User($mongoUser);

if (!$user->authenticate()) {
    header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . '/manage/login.php');
    exit();
}

$mongo = new Mongo($queueConfig);
$db = $mongo->mongoDb();

$qm = new iqueue\QueueManager($mongo);

$count = new iqueue\Counter($qCounterConfig);
$qm->setCounter($count);

$collectionsList = $db->listCollections();

$stores = array();
foreach ($collectionsList as $coll) {
    list($database, $cn) = explode('.', $coll);
    if (preg_match('/store_/', $cn)) {
        $stores[] = $cn;
    }
}

if (isset($_GET['store'])) {
     $counterData = $count->getCounter($selectedStore);
}


// We need to grab a log file if one is available
$ts = new \DateTime();

$date = $ts->format("Y-m-d");

// Glob the log files directory and build a list of log files to view.


$lockfile = $config['lockPath'] . "/queue_monitor_{$storeid}.lock";

$current_pid = null;
if (file_exists($lockfile)) {
    $current_pid = file_get_contents($lockfile);
}

$logfile_pattern = "/queue_monitor_.*_{$storeid}_.*\.log/";
 
$logfiles = array();
$showfile = null;
$showlog  = null;

foreach (glob($config['queuesPath'] . "/queue_monitor_*") as $file) {

    if (preg_match($logfile_pattern, $file)) {
        $logfiles[] = array(
            'name' => basename($file),
            'path' => $file
        );
        
        if (isset($_GET['showlog'])) {
            $showlog = $_GET['showlog'];
        }
        if (basename($file) == $showlog) {
            $showfile = $file;
        }
    
    }
    
}
$log = "No Log data for Shop $storeid yet";

// Get the log file we want to display
$success = false;
if (file_exists($showfile)) {
    $logfile = file($showfile);
    $logfile = implode("", array_reverse($logfile));
    $success = true;
}
// This script - Duh!
$script = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
?>

<?php include_once 'header.phtml'; ?>

        <div class="head_form">
            <form action="<?php echo $script ?>" method="get" name="store_select" id="store_select" class="inline">
            <label for="store">Stores</label>
            <select name="store" id="store">
                <option value=""></option>
                <?php foreach ($stores as $s) : ?>
                <?php 
                $selected = '';
                if (isset($_GET['store']) && $_GET['store'] == $s) {
                    $selected = ' selected="selected" ';
                }
                ?>
                <option value="<?php echo $s ?>" <?php echo $selected ?> ><?php echo $s ?></option>
                <?php endforeach ?>
            </select>
            
            <input type="submit" name="s" value="Go!" />
            </form>
        </div>
        <div>
        <?php if (null !== $counterData) : ?>
        
            <h3>Queue Stats</h3>
            <ul>
                <li>Queue Length: <?php echo $counterData['counter'] ?></li>
                <li>Processed To: <?php echo $counterData['pointer'] ?></li>
                <li>Still in Queue: <?php echo $counterData['counter'] - $counterData['pointer'] ?></li>
            </ul>
        
        <?php else : ?>
            <span>No counter data was found for this queue.</span>
        <?php endif ?>
        </div>
        <div>
            <?php if (null == $current_pid) : ?>
            Cannot find a running QueueMonitor for this queue.
            <?php else : ?>
            The QueueMonitor for this queue reports a process id of <?php echo $current_pid ?>
            <?php endif ?>
        </div>
        <div>
            <ul>
                <?php foreach ($logfiles as $l) : ?>
                <li><a href="<?php echo $script ?>?store=<?php echo $selectedStore ?>&showlog=<?php echo $l['name'] ?>"><?php echo $l['name'] ?></a></li>
                <?php endforeach ?>
            </ul>
        </div>
        <?php if ($success) : ?>
        <div style="maxwidth: 350px; max-height: 300px; overflow-y: auto;">
            <?php echo nl2br($logfile); ?>
        </div>
        <?php endif ?>

<?php include_once 'footer.phtml'; ?>