<?php
// We need to receive the tokens and ids from the other server
//
$pat = null;
$fbid = null;
$storeid = null;
$liked_page_id = null;

if (isset($_GET['phoenix_access_token'])) {
    $pat = $_GET['phoenix_access_token'];
}

if (isset($_GET['fbid'])) {
    $fbid = $_GET['fbid'];
}

if (isset($_GET['storeid'])) {
    $storeid = $_GET['storeid'];
}

if (isset($_GET['liked_pageid'])) {
    $liked_page_id = $_GET['liked_pageid'];
}

$analysis_database = 'phoenix_test';

namespace iqueue;

session_save_path("/data/shared_config/sessions/social");
/**
 * Description of social_config (social_config.php)
 * @package core-staging-branch
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

//P3P Session solution | The iframes flux capacitor
//header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

$environment = 'production' ;// getenv('APPLICATION_ENV');

if ('development' == $environment) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}
include_once '../classes/functions.php';
include_once '../classes/Mongo.php';

$socialConfig = array(
    'host'       => 'localhost',
    'db'         => 'social_data',
    'collection' => null
);

if ('development' !== $environment) {
    $socialConfig['host']    = 'db1.certechhq.com';
    //$qCounterConfig['host'] = 'db1.certechhq.com';
}

use iqueue\Mongo;
/**
 * Description of load_facebook_data (load_facebook_data.php)
 * @package core-staging-branch
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

$app_id_phoenix = "272020439592524";
$app_secret_phoenix = "b385a91f5eadc1c98e8559e6149130b8";

$scope_phoenix = array(
    'email',
    'read_stream',
    'user_likes',
    'user_birthday',
    'user_location',
    'user_actions.music',
);

$fb_graph_queries_phoenix = array(
    'read_stream' => "https://graph.facebook.com/%s/home?limit=5000&offset=0&access_token=%s",
    'likes'       => "https://graph.facebook.com/%s/likes?limit=5000&offset=0&access_token=%s",
    'locations'   => "https://graph.facebook.com/%s/locations?limit=5000&offset=0&access_token=%s",
    'friends'     => "https://graph.facebook.com/%s/friends?limit=5000&offset=0&access_token=%s",
    'person'      => "https://graph.facebook.com/%s/?fields=birthday,email,location,quotes,age_range,friends,posts,statuses&access_token=%s",

);

$page_liked_query = "SELECT page_id FROM page_fan WHERE uid=%s AND page_id=%s&access_token=%s";


$resultset = array();

foreach ($fb_graph_queries_phoenix as $section => $query) {
    $result = useCurl(
            sprintf($query, $fbid, $pat)
        );
    $resultset[$section] = json_decode($result, true);
}

$page_liked_result = useCurl(
        sprintf($page_liked_query, $fbid, $liked_page_id, $access_token)
        );

$resultset['page_liked_result'] = $page_liked_result;

if (count($page_liked_result['data'])) {
    $resultset['has_liked_page'] = true;
}
else {
    $resultset['has_liked_page'] = false;
}

// Grab a quick value for friends
$friends_count = count($resultset['person']['friends']['data']);
$resultset['friends_count'] = $friends_count;
$resultset['facebook_id']   = $fbid;

$resultset['email'] = $resultset['person']['email'];

// Now we store in the MongoDb instance
$mongo = new iqueue\Mongo($socialConfig);

if (count($resultset)) {
    $socialConfig['collection'] = $analysis_database;

    $mongo = new iqueue\Mongo($socialConfig);
    $resultset['_id'] = new \MongoId();

    $mongo->collection()->save(
        $resultset
    );
}

$return_results = array(
    'fbid'    => $fbid,
    'storeid' => $store_id,
    'email'   => $resultset['email'],
    'friends_count' => $friends_count,
    'has_liked_page' => $resultset['has_liked_page'],
    'page_id'        => $liked_page_id,
);

header('Content-type: text/json');
echo json_encode($return_results);












