<?php
include_once '../../application/Bootstrap.php';
include_once '../../application/configs_social/social_config.php';
include_once '../../application/models/functions.php';

/**
 * Description of index (index.php)
 * @package core-staging-branch
 * @category iQueue
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
// We will need some data here - probably config information
$storeid = isset($_GET['storeid']) ? intval(trim($_GET['storeid'])) : null;
$eventid = isset($_GET['eventid']) ? intval(trim($_GET['eventid'])) : null;

// Fetch the config file
if (file_exists(APP_PATH . "configs_stores/store_{$storeid}.php")) {
    include_once APP_PATH . "configs_stores/store_{$storeid}.php";
    include_once APP_PATH . "configs_social/facebook_.php";
    session_start();
}
else {
    // TODO Something.. Redirect somewhere useful...
    echo "This page is misconfigured.";
    //echo "<br />";
    //$path = session_save_path();
    // echo "Session path: " . $path;
    //var_dump(is_readable($path), is_writable($path), $_SERVER['SERVER_ADDR']);
    exit;
}

$code = isset($_REQUEST['code']) ? $_REQUEST['code'] : null;

if (! isset($_SESSION['script_url'])) {

    $_SESSION['script_url'] = selfURL();
}
$script_url = $_SESSION['script_url'];

$params       = null;
$dialog_url   = null;
$user         = null;
$graph_result = null;
$resultset    = null;
$app_params   = null;

if (empty($code)) {
    $_SESSION['state'] = md5(uniqid(rand(), true)); // provides CSRF protection

    // script url
    $dialog_url = "https://www.facebook.com" .
            "/dialog/oauth?client_id=" . $app_id
            . "&redirect_uri=" . urlencode($script_url)
            . "&state=" . $_SESSION['state']
            . "&scope=" . implode(',', $scope);

    // echo out as JS
    //echo("<script> top.location.href='" . $dialog_url . "'</script>");
}
else {
    // We can now continue
    if(isset($_SESSION['state']) && ($_SESSION['state'] === $_REQUEST['state'])) {
        $token_url = "https://graph.facebook.com/"
               . "oauth/access_token?"
               . "client_id=" . $app_id
               . "&redirect_uri=" . urlencode($script_url)
               . "&client_secret=" . $app_secret
               . "&code=" . $code;

        //$response = file_get_contents($token_url);
        $response = useCurl($token_url);
        parse_str($response, $params);
        $access_token = isset($params['access_token']) ? $params['access_token'] : null;
        unset($response);


        // Now we need an app token
        // These last a while..
        if (! isset($_SESSION['app_token'])) {
            $app_token_url = "https://graph.facebook.com/oauth/access_token?"
                . "client_id=" . $app_id
                . "&client_secret=" . $app_secret
                . "&grant_type=client_credentials";

                $response = useCurl($app_token_url);

            parse_str($response, $app_params);

            $_SESSION['app_token'] = $app_params['access_token'];
        }
        $app_token = $_SESSION['app_token'];

        // If we have an access token then make use of it here.
        if (null !== $access_token) {
            $_SESSION['access_token'] = $access_token;

            $graph_url = "https://graph.facebook.com/me?access_token=" . $access_token;

            $user = json_decode(useCurl($graph_url), true);

            $fb_user_id = $user['id'];

            //$fb_graph_query = str_replace('%s', $fb_user_id, $fb_graph_query);
            //$fb_graph_query .= "&access_token=" . $params['access_token'];

            //$graph_result = json_decode(useCurl($fb_graph_endpoint . $fb_graph_query), true);
            $resultset = array();
            foreach($fb_graph_queries as $query_tag => $fb_graph_query) {
                if ($query_tag == 'user') {
                    $graph_result = useCurl(str_replace(
                            array('{fbid}', '{token}'),
                            array($fb_user_id, $app_token),
                            $fb_graph_query)
                        );
                }
                else {
                    $graph_result = useCurl(sprintf($fb_graph_query, $fb_user_id, $access_token));
                }
                $json_data = json_decode($graph_result, true);


                if ($query_tag == 'user') {
                    $email = $json_data['data'][0]['email'];
                    $name  = $json_data['data'][0]['name'];
                }
                $resultset[$query_tag] = $json_data;
                unset($graph_result);
            }

            if (count($resultset)) {
                $socialConfig['collection'] = $store_config['analysis_database'];

                $mongo = new iqueue\Mongo($socialConfig);

                $resultset['_id'] = new \MongoId();
                $resultset['email'] = $email;
                $resultset['name']  = $name;

                $mongo->collection()->save(
                    $resultset
                );
            }
        }
   }
   else {
     echo("The state does not match. You may be a victim of CSRF.");
     exit;
   }
}

// Set a title for page if not in config to prevent notice
if (empty($store_config['title'])) {
    $store_config['title'] = 'Social data';
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $store_config['title']; ?></title>
    </head>
    <h1><?php echo $store_config['title']; ?></h1>
    <body>
        <?php if (null !== $dialog_url) : ?>
        <a href="<?php echo $dialog_url ?>">Go to Facebook.</a>
        <?php endif ?>

        <pre>
            <?php
                if (null !== $params) {
                    var_dump($params, $app_params);
                }

                if (null !== $user) {
                    //var_dump($user);
                }

                echo $fb_graph_query . "<br />";

                $index_sections = array(
                    'checkins', 'statuses', 'photos',
                    'likes', 'interests', 'activities', 'posts',
                    'locations',
                );

                if (! empty($graph_result) && null !== $graph_result) {
                    foreach ($index_sections as $section) {
                        echo "<strong>$section</strong><br />";
                        echo "Data Length: " . count($graph_result[$section]['data']) . "<br />";

                        if (isset($graph_result[$section]['paging']['next'])) {
                            echo "Page Next: " . $graph_result[$section]['paging']['next'] . "<br />";
                        }

                        if (isset($graph_result[$section]['paging']['previous'])) {
                            echo "Page Previous: " . $graph_result[$section]['paging']['previous'] . "<br />";
                        }

                        echo "<br />";
                    }

                }

                if (null !== $resultset) {
                    var_dump($resultset);



                }

                ?>
        </pre>
        _version:1
    </body>
</html>
