<?php
namespace iqueue;

include_once '../application/Bootstrap.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Queue</title>
    </head>
    <body>
        <pre>
        <?php if ($_SERVER['REMOTE_ADDR'] == '86.188.247.250') : ?>
        <h1>Admin Debug View</h1>
        <h2>Constants</h2>
        <?php var_dump('APP_PATH', APP_PATH, 'ROOT', ROOT) ?>
        <?php endif ?>

        <h2>Current Active Config</h2>
        <?php var_dump($configModel->getActiveConfig()) ?>
        <h2>Full Config</h2>
        <?php var_dump($configModel->getConfig()) ?>
        </pre>

    </body>
</html>
