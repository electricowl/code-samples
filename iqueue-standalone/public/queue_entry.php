<?php
/**
 *  ### iQueue entry point ###
 * Functions performed here:
 * Assign entry in queue - via mongoDb
 * Passing in relevant data to the queue
 * Set a mango id to be passed in the URI
 * Forward to the Intermediary page which is itself a forwarder.
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd, UK
 *
 */

$time = microtime(true);

include_once '../application/Bootstrap.php';

$locked = true;
$storeid = '';
$eventid = '';
$ids = array();

if (isset($_GET['storeid'])) {
    $storeid = trim($_GET['storeid']);
    $ids[] = "storeid={$storeid}";
} else {
    // we need a store id to proceed
    echo 'This page is misconfigured';
    exit();
}

if (isset($_GET['eventid'])) {
    $eventid = trim($_GET['eventid']);
    $ids[] = "eventid={$eventid}";
}

// Facebook id - We assume the social queue is active if this is present
$fb_uid = null;
if (isset($_GET['fb_uid'])) {
    $fb_uid = intval($_GET['fb_uid']);
    $ids[] = "fb_uid={$fb_uid}";
}

$query_string = "?" . implode('&', $ids);
unset($ids);

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Counter;
use iqueue\Store;

$is_social_queue = false;

$mongoStore = new Mongo($storeConfig);
$store = new Store($mongoStore);

$store_config = $store->findByStoreId($storeid);

if (empty($store_config)) {
    if ($store->buildNewStoreConfig(array('store_id' => $storeid, 'status' => 'starting'))) {
        $store_config = $store->findByStoreId($storeid);
    }
    else {
        // we need a store id to proceed
        echo 'This page is misconfigured';
        exit;
    }
}

// start queue if necessary
if ($store_config['status'] === 'inactive') {
    $store->updateByMongoId($store_config['_id'], array('status' => 'starting', 'last_changed' => time()));
}

$position_factor = 1;

if (isset($store_config['is_social_queue']) && (int) $store_config['is_social_queue'] === 1) {
    $is_social_queue = $store_config['is_social_queue'];

    // Query queue data endpoint
    // TODO this relies on the other server.
    $location = "https://api.crowdsurge.com/_social/feed.php?storeid={$storeid}&fb_uid={$fb_uid}";
    $json = file_get_contents($location);
    $social_options = array();
    $decoded = json_decode($json, true);

    if ($decoded) {

        $social_options = $decoded[$fb_uid];
        // This is our regression value
        $position_factor = $social_options['factor'];
    }
}

/**
 * From here we are setting up the Queue
 */
// Get Mongo instance (using our own Mongo Class)
$mongo = new Mongo($queueConfig);
// Create a QueueManager instance
$qm = new QueueManager($mongo);
// Create an iqueue Counter instance
$mongoCounter = new Counter($qCounterConfig);

if ($is_social_queue) {
    $mongoCounter->setIsSocial();
}
// Here we will assign the.. counter
$qm->setCounter($mongoCounter);

if ($is_social_queue) {
    // Load Social Queue data...
    $queueData = $qm->loadQueue(
        $storeid,
        $eventid,
        $is_social_queue,
        $social_options
    );
}
else {
    $queueData = $qm->loadQueue(
        $storeid,
        $eventid
    );
}

// Add our mongoID to the URI
$query_string .= '&z=' . (string) $queueData['_id'];
$location = "/passthru.php" . $query_string;

//P3P Session solution | The iframes flux capacitor
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

header("Location: " . "http://" . $_SERVER['HTTP_HOST'] . $location);
exit;
