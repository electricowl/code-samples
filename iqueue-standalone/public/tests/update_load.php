<?php
include_once '../../application/Bootstrap.php';

if ('production' !== $environment) {
    if (!empty($_POST)) {
        $serverLoadLogFile = $config['queueServerLoadLogFile'];
        $dbLoadArray = unserialize(trim(file_get_contents($serverLoadLogFile)));
        $dbLoadArray['1min'] = (float) $_POST['dbload'];
        file_put_contents($serverLoadLogFile, serialize($dbLoadArray));
    }
}

