<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>iQueue - test script</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    </head>
    <body>
         <?php if ('production' != getenv('APPLICATION_ENV')) { ?>
        <h1>iQueue - Tests</h1>
        <hr>
        <h4>Set DB load</h4>
        


    <p>
      <label for="load">Test load:</label>
      <input type="text" id="load" style="border: 0; color: #f6931f; font-weight: bold;" />

    </p>
    <div style="width:500px;">
        <div id="slider-range-max"></div>
    </div>
    <div>
        <h4>Add to queue</h4>
    <form 
            action="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']; ?>" 
            method="post" 
            name="test" 
            id="test"
            class="inline"
        >    

        <div>
            <label for="numbers_to_load">Add users to queues:</label>
            <input 
                type="number" 
                name="numbers_to_load" 
                value="1000" 
            />
        </div>

        <div>
            <input
                type="submit" 
                name="submit" 
                value="Add" 
            />
        </div>
    </form>
    </div>
   
         <?php include_once '../../tests/test_multipleQueues.php'; ?>
         <iframe src="multiple_queues_releases.php" width="60%" height="250"></iframe>
    <?php } ?>
         
         <script>
          $(function() {
          $( "#slider-range-max" ).slider({
            min: 0,
            max: 20,
            value: 0,
            step: 0.2,
            slide: function( event, ui ) {
              $( "#load" ).val( ui.value );
            },
            stop: function( event, ui ) {
              var data = 'dbload='+ui.value;
              $.ajax({ 
                  type: 'POST', 
                  url: 'update_load.php', 
                  timeout: 20000,
                  data: data,
                  error: function() {},
                  success: function(data) { 
                   // assuming okay
                  }
              });
            }
          });
          $( "#load" ).val( $( "#slider-range-max" ).slider( "value" ) );
        });
        </script>
    </body>
</html>
