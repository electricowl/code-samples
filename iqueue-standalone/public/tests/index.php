<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>iQueue - test script</title>
    </head>
    <body>
        <h1>iQueue - Tests</h1>
        <?php
            if ('production' != getenv('APPLICATION_ENV')) {
                include_once '../../tests/test_counter.php';
                include_once '../../tests/test_loadQueue.php';
            }
        ?>
    </body>
</html>
