<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="10">
        <title>iQueue - test script</title>
    </head>
    <body>
        <?php
            if ('production' != getenv('APPLICATION_ENV')) {
                include_once '../../tests/test_multipleQueuesReleased.php';
            }
        ?>
    </body>
</html>
