<?php
include_once '../../application/Bootstrap.php';

$db_load = unserialize(trim(file_get_contents($config['queueServerLoadLogFile'])));

echo '<p>DB load: ' . $db_load['1min']  . '</p>';

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Store;
use iqueue\Counter;

$mongo = new Mongo($queueConfig);
$qm = new QueueManager($mongo);

$mongoCounter = new Counter($qCounterConfig);
$qm->setCounter($mongoCounter);

$storeMongoModel = new Mongo($storeConfig);
$storeModel = new Store($storeMongoModel);

$stores = array(
    0 => array(
        'storeId' => 300,
        'eventId' => 1,
    ),
    1 => array(
        'storeId' => 947,
        'eventId' => 1,
    ),
);

foreach ($stores as $store) {
    
    $storeLookup = $storeModel->findByStoreId($store['storeId']);
    $queueSize = $qm->totalQueueSize($store['storeId']);
    $totalReleased = $qm->totalReleased($store['storeId']);

    if ($queueSize === 0) {
        echo "Queue: " . $store['storeId'] . " is empty<br />";
    }
    else if ($queueSize === $totalReleased) {
        $qm->truncateQueue($store['storeId']);
        echo "Queue: " . $store['storeId'] . " has been truncated<br />";
    }
    else {
        echo "Queue: {$store['storeId']} Entries: " . $queueSize . " Released: " . $totalReleased . "<br />";
    }
    
    echo "Threshold: " . (float) $storeLookup['threshold'];
    echo " Block size: " . (int) $storeLookup['block_size'];
    echo " Status: " . htmlentities($storeLookup['status'], ENT_QUOTES, 'UTF-8');
    echo "<br>";
    echo " Last changed: " . date('d-m-Y H:i:s', $storeLookup['last_changed']);
    echo "<br>";
    echo "<br>";
}