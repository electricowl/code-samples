<?php
include_once '../../application/Bootstrap.php';

// Get the current Queue status
$queueMonitorLogFile = $config['queueLastRunLogFile'];
if (file_exists($queueMonitorLogFile)) {
    $queueStatus = file_get_contents($queueMonitorLogFile);
} else {
    $queueStatus = 'No Process found';
}
echo '<p>Queue health: ' . $queueStatus . '</p>';

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Store;

$mongo = new Mongo($queueConfig);
$qm = new QueueManager($mongo);

$mongoCounter = new \iqueue\Counter($qCounterConfig);
$qm->setCounter($mongoCounter);


if (!empty($_POST['numbers_to_load'])) {
    $quanitityPerStorePerLoad = (int) $_POST['numbers_to_load'];

    $numberLoops = 1;

    $stores = array(
        0 => array(
            'storeId' => 300,
            'eventId' => 1,
        ),
        1 => array(
            'storeId' => 947,
            'eventId' => 1,
        ),
    );

    for ($i = 0; $i < $numberLoops; $i++) {
        foreach ($stores as $store) {
            $targetUrl = "http://api.crowdsurge.com/crowdstore/?v=t&storeid={$store['storeId']}&menu=detail&eventid={$store['storeId']}";
            for ($q = 0; $q < $quanitityPerStorePerLoad; $q++) {
               $qm->loadQueue($targetUrl, $store['storeId'], $store['eventId']);
            }
            echo "Queue: {$store['storeId']} has " . $qm->totalQueueSize($store['storeId']) . " entries<br />";
        }
    }

}
/*
// clear all
foreach ($stores as $store) {
    $qm->truncateQueue($store['storeId']);
    echo "Queue: {$store['storeId']} has " . $qm->totalQueueSize($store['storeId']) . " entries<br />";
}
*/
