<?php
include_once '../../application/Bootstrap.php';

use iqueue\Mongo;
use iqueue\QueueManager;
use iqueue\Counter;

/**
 * CustomerEntersQueueTest
 * 
 */
class CustomerEntersQueueTest extends \PHPUnit_Extensions_Selenium2TestCase
{
    public $storeId;
    public $eventId;
    public $website;
    public $mongoHost;
    public $mongoDB;
    private $queueConfig;
    private $qCounterConfig;
    private $mongoCounter;
    private $mongo;
    private $qm;
    
    protected function setUp()
    {
        $environment = 'jamie'; // TODO implement this better
        $iniArray = parse_ini_file('../../application/configs/config.ini', true);
        $config = $iniArray[$environment];
        
        $this->storeId = 300;
        $this->eventId = 23;
        $this->mongoHost = $config['mongoHost'];
        $this->mongoDB = $config['mongoDB'];
        $this->website = $config['website'];
        
        
        $this->queueConfig = array(
            'host'       => $this->mongoHost,
            'db'         => $this->mongoDB,
            'collection' => null,
        );
        $this->qCounterConfig = array(
            'host'       => $config['mongoHost'],
            'db'         => $config['mongoDB'],
            'collection' => 'qcounter'
        );

        
        $this->mongo = new Mongo($this->queueConfig);
        $this->qm = new QueueManager($this->mongo);

        $this->mongoCounter = new \iqueue\Counter($this->qCounterConfig);
        $this->qm->setCounter($this->mongoCounter);
        
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://' . $this->website . '/queue_entry.php?storeid=' . $this->storeId . '&eventid=' . $this->eventId);
        
    }
 
    public function testEntersAndLeavesQueue()
    {       
        $this->assertEquals('You are currently being held in a queue for tickets', $this->title());
        $this->refresh();
        $this->assertEquals('You are currently being held in a queue for tickets', $this->title());
        $this->qm->releaseBlockFromQueue($this->storeId, 1);
        $this->refresh();
        $this->assertEquals('Dev Store Detail', $this->title());
    }
    
    public function tearDown() {
       $this->qm->truncateQueue($this->storeId);
       // delete pointer record
       $this->qm->setCollection('qcounter')->remove(array('_id' => 'store_300'), array("justOne" => true));
    }
 
}

