<?php
include_once '../../application/Bootstrap.php';

echo "<h4>" . __FILE__ . "</h4>";

use iqueue\Counter;
// Initialise
$counterMongo = new Counter($qCounterConfig);

$data = array('a','b','c','d',
                'A','B','C','D',
                '1','2','3','4'
                );
shuffle($data);

$counters = array(
    'test_01', 'test_02', 'test_03',
    implode('', $data)
);

echo "<pre>";

var_dump($counters);

foreach ($counters as $identifier) {
    $output = $counterMongo->incrementCounter($identifier);
    var_dump($output, $output['value']['counter']);
}