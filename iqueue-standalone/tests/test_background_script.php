<?php
    declare(ticks = 1);

    pcntl_signal(SIGTERM, "signal_handler");
    pcntl_signal(SIGINT, "signal_handler");

    function signal_handler($signal = null) {
        switch($signal) {
            case SIGTERM:
                print "Caught SIGTERM\n";
                exit;
            case SIGKILL:
                print "Caught SIGKILL\n";
                exit;
            case SIGINT:
                print "Caught SIGINT\n";
                exit;
        }
    }
    
    $loop = 1;
    while(1) {
        echo "Looping: count = " . $loop++ . "\n";
        sleep(2);
        signal_handler();
    }
