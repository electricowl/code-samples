<?php

include_once '../../application/Bootstrap.php';

echo "START;";

echo "<h4>" . __FILE__ . "</h4>";

use iqueue\Mongo;
use iqueue\QueueManager;

$mongo = new Mongo($queueConfig);
$qm = new QueueManager($mongo);

$mongoCounter = new \iqueue\Counter($qCounterConfig);
$qm->setCounter($mongoCounter);

echo "<h5>Build a queue</h5>";

$storeid = '441';
$eventid = '616919';
$identifier = "store_{$storeid}";

$targetUrl = "http://api.crowdsurge.com/crowdstore/?v=t&storeid={$storeid}&menu=detail&eventid={$eventid}";

for ($q=1; $q<=10; $q++) {
    $qm->loadQueue($targetUrl, $storeid, $eventid);
    echo "Added item $q to queue.<br />";
}
echo "Queue: $identifier has " . $qm->totalQueueSize($storeid) . " entries<br />";

echo "<h5>Show the queue</h5>";

$cursor = $qm->getQueue($identifier);
$qm->iterateCursor($cursor);

echo "<h5>Release items from queue</h5>";

// Let's release the next 5 items from the queue and
// update the queue pointer.

$qm->releaseBlockFromQueue($storeid, 5);

$released = $qm->totalReleased($storeid);
echo "Released Queue Items: $released";


// Clear queue
echo "<h5>Truncate queue</h5>";

$qm->truncateQueue($storeid);

echo "Queue: $identifier has " . $qm->totalQueueSize($storeid) . " entries<br />";

echo "END;";
