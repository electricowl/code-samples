Automated Intelligent Queue
===========================

Event driven process for managing user ticket purchase volumes. 


The Queue Manager will put users into a queue (MongoDb collection) until 
resources are freed on the DB server to process the request.


