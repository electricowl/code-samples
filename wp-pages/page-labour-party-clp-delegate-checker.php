<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Fictive
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>
                    <div id="clp-form-area">

<p>Simply enter the number of members in your CLP and answer two questions.</p>
<form action="#" name="clp-calc">Total CLP Membership: <input id="clp-member-total" name="clp-member-total" size="20" onsubmit="javascript: return false;" type="text"><p></p>
<table>
<tbody><tr>
<td><strong>CLP Delegate Calculator</strong></td>
</tr>
<tr>
<td><input id="clp-young-members" name="clp-young-members" value="1" type="checkbox"> Tick if there are 30 or more young members (u-25).</td>
</tr>
<tr>
<td><input id="clp-female-members" name="clp-female-members" value="1" type="checkbox"> Tick if there are 100 or more female members.</td>
</tr>
<tr>
<td><button onclick="javascript: calc_member_delegates(); return false;">Click to Calculate</button></td>
</tr>
</tbody></table>
</form>
<div>
<p>Your CLP can send <span id="delegate-count"></span> delegates to conference.</p>
</div>
<p>Widget created by: Ian Lewis ianl@electricowl.co.uk</p>


                    </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>