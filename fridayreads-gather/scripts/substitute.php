<?php
//require './substitute_phrases.php';
/**
 * Substitute data from one file with that from another
 */
 
$target = "/home/ianl/Dropbox/fridayreads_0204_FINAL.txt";
 $authorsource = "/home/ianl/Dropbox/authorlist_sorted2.txt";
 $titlesource = "/home/ianl/Dropbox/all_titles.csv";
 $outputfile = "/home/ianl/Dropbox/substituted_text_0204.csv";
 
 $sFileContents = file_get_contents($target);
 //$sFileContents = tidyPhrases($sFileContents);
 //file_put_contents($outputfile . "-tidied.txt", html_entity_decode($sFileContents));
 
 // Titles
 $oHandle = fopen($titlesource, "r");
 while (($line = fgets($oHandle, 256)) !== false) {
 $sTitle = trim($line);
 echo "$sTitle :\n";
 // then we substitute these cases
 // strictly a few options for single words 
 $sFileContents = str_ireplace("'$sTitle'", "<span class='title'>$sTitle</span>", $sFileContents);
 $sFileContents = str_ireplace('"' . $sTitle .'"', "<span class='title'>$sTitle</span>", $sFileContents);
 
 if (str_word_count($sTitle) < 2) {
     continue;
 }
 $sFileContents = str_ireplace(" $sTitle, ", "<span class='title'>$sTitle</span>", $sFileContents);
 $sFileContents = str_ireplace(" $sTitle by", "<span class='title'>$sTitle</span> by", $sFileContents);
 $sFileContents = str_ireplace("'s $sTitle ", " <span class='title'>$sTitle</span> ", $sFileContents);
 $sFileContents = str_ireplace("#fridayreads $sTitle ", "#fridayreads <span class='title'>$sTitle</span>", $sFileContents);
 $sFileContents = str_ireplace(" $sTitle #fridayreads", " <span class='title'>$sTitle</span> #fridayreads", $sFileContents);
 $sFileContents = str_ireplace(" $sTitle ", "#fridayreads <span class='title'>$sTitle</span>", $sFileContents);
 }
 fclose($oHandle);
 
 // Authors
 
 $oHandle = fopen($authorsource, "r");
 while (($line = fgets($oHandle, 256)) !== false) {
 $sAuthor = trim($line);
 echo $sAuthor . " : " . strlen($sFileContents) . " : ";
 if ($sAuthor == '') {
     continue;
 }
 $sSubstitute = null;
 if (preg_match("/^\@/", $sAuthor)) {
    $sSubstitute = "http://twitter.com/" . str_replace("@", "", $sAuthor); 
 } 
 if (preg_match("/^\#/", $sAuthor)) {
    $sSubstitute = "http://search.twitter.com/search?q=%23" . str_replace("#", "", $sAuthor); 
 } 
 if (null == $sSubstitute) {
    $sSubstitute = "<span class='author'>{$sAuthor}</span>";
 }
 $sFileContents = str_ireplace(" $sAuthor", " $sSubstitute ", $sFileContents);
 echo "$sAuthor $sSubstitute \n";
 
 }
 fclose($oHandle);
 
 file_put_contents($outputfile, html_entity_decode($sFileContents));
 
 
