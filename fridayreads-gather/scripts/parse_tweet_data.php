<?php 
/**
 * Ian Lewis <ian.lewis@electricowl.co.uk>
 */
define('IGNORE_RT', true);
define('IGNORE_CORE', true);
$aFridayReadsCore = array ('thebookmaven', 'erinfaye', 'bookmeme', 'ibc4', 'littlefluffycat', 
	'traceyjoa', 'mardixon', 'shelfmedia', 'bookladysblog',
	'lm_preston', 'victoriatwead', 'randommagic', 'randommagictour' // not core but ignore
);


$aFound = array();
$path = '/home/ianl/Dropbox';
//$path = "D:\Users\Naomi Hillman\Documents\My Dropbox";
$inputdata = $path . '/fridayreads_20110804_FINAL.csv';
$outputdata = preg_replace("/FINAL/", "FINAL-parsed", $inputdata);
//$outputdata = preg_replace("/\.csv/", "-parsed.csv", $inputdata);
file_put_contents($outputdata, "id|user|retweet|phrase\n");
$oHandle = fopen($inputdata, "r");
$iLineNumber = 0;
//while (($line = fgetcsv($oHandle, 0, "\t")) !== false) { // TAB separated
while (($line = fgetcsv($oHandle, 0, "|")) !== false) { // PIPE separated - from book-meme
 //while (($line = fgetcsv($oHandle, 0, ",")) !== false) {
    // Skip first
    if ($iLineNumber == 0) {
        $iLineNumber++;
        continue;
    }
    // If there are fewer than 5 fields of data we skip
    // probably a defective line
    if (count($line) < 5) {
        continue;
    }
    // Grab the fields into the relevant variables.
    list($sId, $sDate, $sTime, $sUser, $sTweet) = $line; // dl from book-meme
    //list($sId, $sUser, $sDate, $sTime, $sTweet) = $line;
    // alternate
    //list($sId, $sType, $sDate, $sTime, $sUser, $sTweet) = $line;
    // Skip empty tweet data
    if (!$sTweet) {
        continue;
    }
    // Start
    // Ignore data
    if (IGNORE_CORE && in_array(strtolower($sUser), $aFridayReadsCore)) {
        continue;
    }
    // Is this a retweet
    $sReTweet = "";
    if (preg_match("#(^RT\s|\sRT\s)#", $sTweet)) {
        $sReTweet = "Y";
    }
    // Retweet 
    if ($sReTweet == "Y" && IGNORE_RT) {
        continue;
    }

    // Pull out _title_
    preg_match("/_(.*)_/iU", $sTweet, $aResult);
    if ($aResult) {
        $sPhrase = $aResult[1];
        file_put_contents($outputdata, "$sId|$sUser|$sReTweet|" . trim($sPhrase) . "\n", FILE_APPEND);
        unset($sPhrase);
        continue;
    }
    // search - CAPITALS
    $sTweetCleaned = preg_replace("/[^A-Za-z0-9\s,]/", "", $sTweet);
    preg_match_all("/\s([A-Z\s,]+)/", $sTweetCleaned, $aMatched);
    if ($aMatched) {
        $aMatched = reset($aMatched);
        foreach ($aMatched as $sPhrase) {
            if (preg_match("#\s[A-Z]$#", $sPhrase)) {
                $sPhrase = substr($sPhrase, 0, -2);            
            }
            if (strlen(trim($sPhrase)) <= 2) {
                continue;
            }
            $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $sPhrase);
            $aFound[$sId] = null;
            file_put_contents($outputdata, "$sId|$sUser|$sReTweet|" . trim($sPhrase) . "\n", FILE_APPEND);
        }
    }
    // Next lot - Quoted
    if (array_key_exists($sId, $aFound)) {
        continue;
    }
    $sTweetCleaned = str_replace(array("'s", "'m", "'t", '."', ',"'), array("{s}","{m}", "{t}", '"', '"') , $sTweet);
    preg_match_all("#[\"'_](.*)[_\"']#U", $sTweetCleaned, $aMatched);
    if ($aMatched) {
        reset($aMatched);
        $aMatched = next($aMatched);
        foreach ($aMatched as $sPhrase) {
            if (strlen(trim($sPhrase)) <= 2) {
                continue;
            }
            $sPhrase = str_replace(array("{s}","{m}", "{t}"), array("'s", "'m", "'t"), $sPhrase);
            $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $sPhrase);
            $aFound[$sId] = null;
            file_put_contents($outputdata, "$sId|$sUser|$sReTweet|" . trim($sPhrase) . "\n", FILE_APPEND);
        }
    }
    
    if (array_key_exists($sId, $aFound)) {
        continue;
    }
    // Bracketed by
    switch (true) {

        case preg_match("/#fridayreads\sis\"(.*)\"\sby/iU", $sTweet, $aResult) :
            break;
        case preg_match("/#fridayreads\sis'(.*)'\sby/iU", $sTweet, $aResult) :
            break;
        case preg_match("/#fridayreads\sis(.*)\sby/iU", $sTweet, $aResult) :
            break;
	case preg_match("/#fridayreads,\s?(.*)\sby/iU", $sTweet, $aResult) :
            break;
        case preg_match("/reading\s(.*)\sby/iU", $sTweet, $aResult) :
            break;
	case preg_match("/read\s(.*)\sby/iU", $sTweet, $aResult) :
            break;
	case preg_match("/started\s(.*)\sby/iU", $sTweet, $aResult) :
            break;
	case preg_match("/finished\s(.*)\sby/iU", $sTweet, $aResult) :
            break;
        //case preg_match("/'s\s(.*)\s+#fridayreads/iU", $sTweet, $aResult) :
        //    break;
    }
    if ($aResult) {
    $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $aResult[1]);
    $aFound[$sId] = null;
	file_put_contents($outputdata, "$sId|$sUser|$sReTweet|" . trim($sPhrase) . "\n", FILE_APPEND);  
    }
 }
