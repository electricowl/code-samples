<?php 

$input = "/home/ianl/Dropbox/fridayreads/book-data-bestsellers/book_list_unique.csv";

function index($sLine) {
    $stopwords = array(' to ',' a ',',','-',' on ',' as ', "'s ", ' the ', ' and ', ' if ', ' of ', '#fridayreads', '#fridaylistens', ' loved ', "\t", "?");
    $stopped = str_replace($stopwords, " ", strtolower($sLine));
    $tokens    = explode(" ", $stopped);
    return $tokens;
}

$handle = fopen($input, 'r');
$count = 0;
$index = array();

while (($line = fgetcsv($handle, 4096,  ",")) !== false) {
    if ($count == 0) {
        $count++;
        continue;
    }
    @list($title, $author) = $line;
    $index['line'][$count] = "$title|$author";
    $tok = index($title);
    foreach ($tok as $token) {
        if ($token == '') {
            continue;
        }
        $index[trim($token)][] = $count;
    }
    $count++;
}

var_dump($index);
