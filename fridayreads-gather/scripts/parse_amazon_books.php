<?php
/**
 * Parse amazon books fromm Thunderbird Feed.
 * Basically the Html is parsed
 */
$target_dir = "/home/ianl/Dropbox/fridayreads/book-data-bestsellers";
$source = $target_dir . "/amazon-co_uk_bestsellers_list_sep2011.txt";
$destination = $target_dir . "/amazon_co_uk_bestsellers_captured4.csv";
$authordest  = $target_dir . "/amazon_co_uk_bestsellers_authors_captured4.csv";
//file_put_contents($outputdata, "id|user|retweet|phrase\n");
// file_put_contents($outputdata, "$sId|$sUser|$sReTweet|" . trim($sPhrase) . "\n", FILE_APPEND);

$preg_subject     = "/^Subject:(.*)/";
$preg_strip_rank  = "/.\d\d?:/";
$preg_author_line = "/riRssContributor/";

$oHandle = fopen($source, "r");
$aMatches = array(
    'counter' => 0,
    'data'    => array()
);

while (($line = fgets($oHandle, 4096)) !== false) {

    if (preg_match($preg_subject, $line, $matches)) {
        $aBookData = array();
        //echo "\n---------\n" . $matches[1] . "\n";
        $sTitle = preg_replace($preg_strip_rank, "", $matches[1]);

        @list($sTitle, $sComment) = explode("(", $sTitle);
        @list($sTitle, $sSubTitle) = explode(":", $sTitle);
        $aMatches['data'][$aMatches['counter']] = array(
            'title' => trim($sTitle),
            'subtitle' => $sSubTitle,
            'comment'  => str_replace(")", "", $sComment)
        );
        // So we now have the first set of book data
        // need to grab authors next
    }
    // Simple
    // <span class="riRssContributor">Timothy Ferriss <span class="byLinePipe">(Author)</span></span>

    // Complex
    // <span class="riRssContributor">Stieg Larsson <span class="byLinePipe">(Author)</span>, Reg Keeland <span class="byLinePipe">(Translator)</span></span>
    //
    // 

    //
    // Grabbing authors
    if (preg_match($preg_author_line, $line)) {
        //echo  "\n-----------\n";
        preg_match("/dp\/(.+)\//U", $line, $asin);
        $asin = $asin[1];
        preg_match("/riRssContributor\"\>(.*)\<\/span\>(\<\/span\>|\<\/div\>)/", $line, $match_auth);
        //echo $match_auth[1] . "\n";
        if (isset($match_auth[1])) {
            $sMultiContri = str_replace(array("<span class=\"byLinePipe\">("), "|", $match_auth[1]);
            $sMultiContri = str_replace(array(")</span>", ")"), "^", $sMultiContri);
            $sMultiContri = str_replace(array(", ", ")"), "", $sMultiContri);
            $aMultiContrib = explode("^", $sMultiContri);

            $aBookData['asin']         = $asin;

            $aContributors = array();
            foreach ($aMultiContrib as $sContri) {
                if (preg_match("/|/", $sContri)) {
                    @list($sName, $sRole) = explode("|", $sContri);
                    $sName = trim(str_replace(".", "", $sName));
                    if (preg_match("/script/", $sName)) {
                        continue;
                    }
                    if (preg_match("/contributorNameTrigger/", $sName)) {
                        preg_match("/h3.*\>(.+)\<\/b\>$/U", $sName, $parsed_name);
                        @list(,$sName) = $parsed_name;
                    }
                    $sRole = strtolower(trim($sRole));
                    if ($sRole) {
                        $aContributors[$sRole][] = $sName;
                    }
                }
            }

            $aBookData['contributors'] = $aContributors;
            $aMatches['data'][$aMatches['counter']] = $aMatches['data'][$aMatches['counter']] + $aBookData;

            $aMatches['counter']++;
        }
    }

}
file_put_contents($authordest, "\"title\"|\"author\"" . "\n");
foreach ($aMatches['data'] as $aBook) {
    $sAuthors = implode(" & ", $aBook['contributors']['author']);
    $sBookTitle = trim($aBook['title']);
    file_put_contents($authordest, "\"{$sBookTitle}\"|\"{$sAuthors}\"" . "\n", FILE_APPEND);
}

file_put_contents($destination, json_encode($aMatches) . "\n", FILE_APPEND);
var_dump($aMatches);
