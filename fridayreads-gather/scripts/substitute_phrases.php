<?php 

function tidyPhrases($sInputData) {
$aSubs = array(
"Another #fridayreads|{AFR}",
"Finished reading|{FR}",
"Finishing up|{FU}",
"#fridayreads:|{FR}",
"#FridayReads is|{FRI}",
"#fridayreads Reading|{FRR}",
"I just finished|{IJF}",
"I'm reading|{IR}",
"Just finished|{JF}",
"Just started reading|{JSR}",
"Love reading|{LR}",
"My first #fridayreads is|{MFR}",
"My #FridayReads is|{MFI}",
"My second #fridayreads is|{MSF}",
"now reading|{NR}",
"Starting to read|{STR}",
"Still finishing|{STF}",
"Still on|{ST}",
"I #amreading|{IAR}",
"This weekend I'm reading|{TWR}",
"this week's #fridayreads|{TWF}",
"Today I'm reading|{TR}",
"#fridayreads|{FR}"
);

foreach ($aSubs as $sSubstituteLine) {
list($sSearchPhrase, $sReplaceGlyph) =  explode("|", $sSubstituteLine);
    $sInputData = str_ireplace($sSearchPhrase, $sReplaceGlyph, $sInputData);
}

return $sInputData;
}


