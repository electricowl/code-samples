FridayReads Gather
==================

Not the greatest name for a project.

FridayReads is a global Twitter hashtag (topic/meme) that I became involved in
as a personal project to teach myself about social media.

'''Inspiration''' Made to Stick by Chip and Dan Heath

'''Goal''' to build a social media presence, from scratch. Facebook was rejected
because it was/is 'old hat' and is a closed network. Twitter won the selection 
evaluation because it is an open network.

'''Why #FridayReads?''' 
I have always loved books and reading so after some research I discovered the 
FridayReads hashtag and from some analysis of collected data I determined that 
the weekly engagement was around 300.

'''Methodology - Growing the meme'''
I followed the principles laid out in ''Made to Stick'', in essence getting people 
to connect emotionally with the concept of sharing what they happened to be 
reading on a Friday. This caught on very quickly and an informal team built up
with me leading the fray, teaching others what to do.

'''Methodology - Data collection and stats'''
Using CouchDB and a cron script I collected the JSON data containing the tweets
and then used the FetchParseTweetData.php script to reduce the data down to a 
'Top 20' of books for that week.


