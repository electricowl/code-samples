<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Summarize Page | @bookmeme | #FridayReads </title>
    </head>
    <body>
        <p>This page is all about getting the Most Read books for any given FridayReads event.</p>
        <p>The data is drawn from Twitter.</p>
        <p>
            Please enter the start date for the summary (it will be a Thursday to allow for the Southern Hemisphere).
        </p>
        <form name="date_form" action="/fridayreads/" method="post">
            <fieldset>
                <legend>Select Date:</legend>
            <select name="year">
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
            </select>
            <select name="month">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04" >April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08" selected>August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
            <select name="day">
                <?php for ($m=1; $m<32; $m++) : ?>
                <option value="<?php echo $m ?>"><?php echo $m ?></option>
                <?php endfor; ?>
            </select>
            </fieldset>
            <br />
            <fieldset>
                <legend>Identity</legend>
                Identify yourself with your email address.
                <input type="text" size="60" name="email_address" />
            </fieldset>


            <input type="submit" name="submit" value="Tell Me!" />
        </form>

        <?php
        $sMessage = "";
        if (isset($_POST['submit'])) {
            $aPermitted = array(
                'ian.lewis.65@gmail.com',
                'erinfaye@gmail.com'
            );
            $sEmail = trim($_POST['email_address']);
            if (in_array($sEmail, $aPermitted)) {
                require_once '/var/www/fridayreads/FetchParseTweetData.php';
                $oTask = new FetchParseTweetData();
                $oTask->run();

                if (!$oTask->emailCsv($sEmail)) {
                    $sMessage = "Tried to send the data to you but apparently it failed. #:-/";
                }
                else {
                    $sMessage = "The summary file has been emailed to you... see you next week #:-)";
                }
            }
            else {
                $sMessage = "Sorry. You're not on the list to receive the summary file. :(";
            }
        }
        ?>
        <p>
            <?php echo $sMessage ?>
            
        </p>
    </body>
</html>
