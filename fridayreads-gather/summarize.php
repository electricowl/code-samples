<?php
$dsn = 'mysql:host=localhost;dbname=fridayreads';
$username = 'root';
$password = 'Finial5$';
$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);

$db = new PDO($dsn, $username, $password, $options);

// This will be 20110812 format
$dateIdentifier = '';



// This will be replaced by the data held in memory or loaded
// direct into the DB
$loadData = <<<EOS
LOAD DATA LOCAL INFILE '/home/ianl/Dropbox/fridayreads_$dateIdentifier_FINAL-parsed.csv'
INTO TABLE fridayreads.found_$dateIdentifier
FIELDS TERMINATED by '|' optionally ENCLOSED BY '"'
IGNORE 1 LINES;
EOS;

$cleanData = <<<EOS
DELETE FROM fridayreads.found_$dateIdentifier where found_text in ('arc', 'viii', 'nyt','hbo','mjr','r r','random magic',
'tbr','distrib @patrickj','faith','dfw','your','nyc','the',
'and','love','loving','lol','not','great','tv','omg','evar','you','wip','fridayreads',
'iii','loved','tgif','ggbc','obl','still', 'j r r', 'jrr', 'r r', ' ', '', 'bea', 'i love', 'npr', 'grrm',
'wlt');
EOS;

// This generates the list of books
$summarizeBooks = <<<EOS
SELECT count(*) total, f.found_text, b.title, b.author
FROM fridayreads.found_$dateIdentifier f
LEFT JOIN fridayreads.book_unique b ON trim(f.found_text) = trim(b.title)
GROUP BY f.found_text
order by total DESC;
EOS;
