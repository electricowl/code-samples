<?php
/**
 * Ian Lewis <ian.lewis@electricowl.co.uk>
 * 07584 673095
 */
error_reporting(E_ALL);
ini_set('display_errors','On');

class FetchParseTweetData {

    private $aFridayReadsCore = array ('thebookmaven', 'erinfaye', 'bookmeme', 'ibc4', 'littlefluffycat',
            'traceyjoa', 'mardixon', 'shelfmedia', 'bookladysblog',
            'lm_preston', 'victoriatwead', 'randommagic', 'randommagictour' // not core but ignore
    );

    private $outputdata = "fridayreads_%s_FINAL-parsed.csv";
    private $oDb = null;

    private $sDate = null;
    private $aFound = array();
    
    public function __construct()
    {
        if (!defined('IGNORE_RT')) {
            define('IGNORE_RT', true);
        }
        if (!defined('IGNORE_CORE')) {
            define('IGNORE_CORE', true);
        }
    }

    public function run()
    {
        
        $query = $this->createQuery($_POST['year'], $_POST['month'], $_POST['day']);
        
        $data  = $this->executeQuery($query);
        
        if (null !== $data) {
            $this->createFoundTable();
            $this->processData($data);
            $this->cleanBookData();
        }
        else {
            throw Exception("No data found");
        }
    }
    /**
     * Assumes this is being called after checking that the form has been submitted
     * @param <type> $year
     * @param <type> $month
     * @param <type> $day
     */
    public function createQuery($year, $month, $day)
    {
        $ymd = '%year%%month%%day%';
        $json_query = 'startkey=%22|ymd|%22&_include_docs=true';
        
        $ymd = str_replace('%year%', trim($year), $ymd);
        $ymd = str_replace('%month%', trim($month), $ymd);
        $day = str_pad(trim($day), 2, '0', STR_PAD_LEFT);
        $ymd = str_replace('%day%', $day, $ymd);
        $this->sDate = $ymd;
        $json_query = str_replace('|ymd|', $ymd, $json_query);

        //$query = " http://localhost:5984/my_db/_design/fridayreads/_view/time_indexed?";
        $query = " http://localhost:5984/fridayreads/_view/fridayreads/time_indexed?" . $json_query;

        return $query;
    }

    public function executeQuery($query)
    {
        $curl = "curl -XGET $query";
        exec($curl, $result);
        if (is_array($result)) {
            $result = implode('', $result);
        }
        $results = json_decode($result, true);

        if ($results) {
            return $results;
        }
        return null;
    }

    public function processData($data)
    {
        if (isset($data['rows'])) {
            $aHits = $data['rows'];
            foreach ($aHits as $hit) {
                $aMessage = $hit['value'];
                $id = $aMessage['id_str'];
                $from_user = $aMessage['from_user'];
                $tweet = html_entity_decode($aMessage['text'], ENT_QUOTES);
                $tweet = str_replace('&apos;', "'", $tweet);
                // We have the tweet value now it should be processed
                $this->parseBook($id, $from_user, $tweet);
            }
        }
    }

    /**
     * Called per-row
     * @param string $sId
     * @param string $sUser
     * @param <type> $sTweet
     */
    public function parseBook($sId, $sUser, $sTweet)
    {
        // Ignore data
        if (IGNORE_CORE && in_array(strtolower($sUser), $this->aFridayReadsCore)) {
            return null;
        }
        // Is this a retweet
        $sReTweet = "";
        if (preg_match("#(^RT\s|\sRT\s)#", $sTweet)) {
            $sReTweet = "Y";
        }
        // Retweet
        if ($sReTweet == "Y" && IGNORE_RT) {
            return null;
        }
        // Pull out _title_
        preg_match("/_(.*)_/iU", $sTweet, $aResult);
        if ($aResult) {
            $sPhrase = $aResult[1];
            $this->loadFoundBook($sId, $sUser, $sReTweet, trim($sPhrase));
            return;
        }
        // search - CAPITALS
        $sTweetCleaned = preg_replace("/[^A-Za-z0-9\s,]/", "", $sTweet);
        preg_match_all("/\s([A-Z\s,]+)/", $sTweetCleaned, $aMatched);
        if ($aMatched) {
            $aMatched = reset($aMatched);
            foreach ($aMatched as $sPhrase) {
                if (preg_match("#\s[A-Z]$#", $sPhrase)) {
                    $sPhrase = substr($sPhrase, 0, -2);
                }
                if (strlen(trim($sPhrase)) <= 2) {
                    continue;
                }
                $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $sPhrase);
                $this->aFound[$sId] = null;
                $this->loadFoundBook($sId, $sUser, $sReTweet, trim($sPhrase));
            }
        }
        // Next lot - Quoted
        if (array_key_exists($sId, $this->aFound)) {
            return;
        }
        $sTweetCleaned = str_replace(array("'s", "'m", "'t", '."', ',"'), array("{s}","{m}", "{t}", '"', '"') , $sTweet);
        preg_match_all("#[\"'_](.*)[_\"']#U", $sTweetCleaned, $aMatched);
        if ($aMatched) {
            reset($aMatched);
            $aMatched = next($aMatched);
            foreach ($aMatched as $sPhrase) {
                if (strlen(trim($sPhrase)) <= 2) {
                    continue;
                }
                $sPhrase = str_replace(array("{s}","{m}", "{t}"), array("'s", "'m", "'t"), $sPhrase);
                $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $sPhrase);
                $this->aFound[$sId] = null;
                $this->loadFoundBook($sId, $sUser, $sReTweet, trim($sPhrase));
            }
        }

        if (array_key_exists($sId, $this->aFound)) {
            return;
        }
        // Bracketed by a phrase or fraagment
        switch (true) {
            case preg_match("/#fridayreads\sis\"(.*)\"\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/#fridayreads\sis'(.*)'\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/#fridayreads\sis(.*)\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/#fridayreads,\s?(.*)\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/reading\s(.*)\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/read\s(.*)\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/started\s(.*)\sby/iU", $sTweet, $aResult) :
                break;
            case preg_match("/finished\s(.*)\sby/iU", $sTweet, $aResult) :
                break;
            //case preg_match("/'s\s(.*)\s+#fridayreads/iU", $sTweet, $aResult) :
            //    break;
        }
        if ($aResult) {
            $sPhrase = preg_replace("/[^A-Za-z\s0-9&]/", "", $aResult[1]);
            $this->aFound[$sId] = null;
            $this->loadFoundBook($sId, $sUser, $sReTweet, trim($sPhrase));
        }
    }

    /**
     *
     * @return PDO
     */
    public function getDb()
    {
        if (null == $this->oDb) {
            $dsn = 'mysql:host=localhost;dbname=fridayreads';
            $username = 'root';
            $password = 'Finial5$';
            $options = array(
                //PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            );

            try {
                $this->oDb = new PDO($dsn, $username, $password, $options);
            }
            catch(PDOException $e) {
                throw $e;
            }
        }
        return $this->oDb;
    }

    public function createFoundTable()
    {
        $dateIdentifier = $this->sDate;

        $drop = "DROP TABLE IF EXISTS found_$dateIdentifier ";
        $oStatement = $this->getDb()->prepare($drop);
        $oStatement->execute();

        $createTable = <<<EOS
CREATE TABLE found_$dateIdentifier (
  `tweet_id` varchar(64) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `is_rt` varchar(2) DEFAULT NULL,
  `found_text` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
EOS;

        try {
        $oStatement = $this->getDb()->prepare($createTable);
        $oStatement->execute();
        }
        catch (PDOException $e) {
            throw $e;
        }
    }

    public function loadFoundBook($id, $username, $is_rt, $found_text)
    {
        $dateIdentifier = $this->sDate;
        $query = <<<EOS
        INSERT INTO found_$dateIdentifier (tweet_id, username, is_rt, found_text)
        VALUES (:id, :username, :is_rt, :found_text)
EOS;
        $oStatement = $this->getDb()->prepare($query);
        $oStatement->execute(array(
            'id'         => $id,
            'username'   => $username,
            'is_rt'      => $is_rt,
            'found_text' => $found_text
        ));
    }

    public function cleanBookData()
    {
        $dateIdentifier = $this->sDate;
        $cleanData = <<<EOS
DELETE FROM found_$dateIdentifier where found_text in ('arc', 'viii', 'nyt','hbo','mjr','r r','random magic',
'tbr','distrib @patrickj','faith','dfw','your','nyc','the',
'and','love','loving','lol','not','great','tv','omg','evar','you','wip','fridayreads',
'iii','loved','tgif','ggbc','obl','still', 'j r r', 'jrr', 'r r', ' ', '', 'bea', 'i love', 'npr', 'grrm',
'wlt', 'WWII', 'free', 'finally');
EOS;
        $oStatement = $this->getDb()->prepare($cleanData);
        $oStatement->execute();
    }

    public function fetchSummary()
    {
        $dateIdentifier = $this->sDate;

        $summarizeBooks = <<<EOS
SELECT count(*) total, f.found_text, b.title, b.author
FROM found_$dateIdentifier f
LEFT JOIN book_unique b ON trim(f.found_text) = trim(b.title)
GROUP BY f.found_text
order by total DESC;
EOS;
        $oStatement = $this->getDb()->prepare($summarizeBooks);
        $oStatement->execute();

        return $oStatement->fetchAll();
    }

    public function outputAsCsv($aData)
    {
        $dateIdentifier = $this->sDate;
        $sFile = "fridayreads_mostRead_$dateIdentifier.csv";
        $sRow = '"%s","%s","%s","%s"';
        //header( 'Content-Type: text/csv' );
        //header( 'Content-Disposition: attachment;filename=' . $sFile);
        file_put_contents('/var/tmp/' . $sFile, '"total","found_text","title","author"' . "\n");
        foreach ($aData as $row) {
            file_put_contents('/var/tmp/' . $sFile ,sprintf($sRow, $row['total'], $row['found_text'], $row['title'], $row['author']) . "\n", FILE_APPEND);
        }
        
    }

    /**
     * Send the CSV file as an email
     * @param string $sEmail
     */
    public function emailCsv($sEmail)
    {
        $summary = $this->fetchSummary();
        $this->outputAsCsv($summary);
        
        $dateIdentifier = $this->sDate;
        $sFile = "fridayreads_mostRead_$dateIdentifier.csv";

        return $this->sendEmail($sEmail, $sFile);
    }

    public function sendEmail($to, $sFile)
    {
        //define the subject of the email
        $subject = 'Most Read list ' . $sFile;
        //create a boundary string. It must be unique
        //so we use the MD5 algorithm to generate a random hash
        $random_hash = md5(date('r', time()));
        //define the headers we want passed. Note that they are separated with \r\n
        $headers = "From: bookmeme <ian.lewis@book-meme.com>\r\nReply-To: Ian Lewis <ian.lewis.65@gmail.com>";
        //add boundary string and mime type specification
        $headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"";
        $headers .= "\r\nMIME-Version: 1.0\r\n";
        //read the atachment file contents into a string,
        //encode it with MIME base64,
        //and split it into smaller chunks
        $attachment = chunk_split(base64_encode(file_get_contents('/var/tmp/' . $sFile)));
        
        //define the body of the message.
        $message = <<<EOS
--PHP-mixed-$random_hash;
Content-Type: multipart/alternative; boundary="PHP-alt-$random_hash"

--PHP-alt-$random_hash
Content-Type: text/plain; charset="iso-8859-1"
Content-Transfer-Encoding: 7bit

Hello!

This is an automated message. Attached is the FridayReads Most Read books
for {$this->sDate}

--PHP-alt-$random_hash
Content-Type: text/html; charset="iso-8859-1"
Content-Transfer-Encoding: 7bit

<h2>Hello!</h2>
<p>This is an automated message. Attached is the FridayReads Most Read books
for {$this->sDate}</p>

--PHP-alt-$random_hash--

--PHP-mixed-$random_hash
Content-Type: application/zip; name="$sFile"
Content-Transfer-Encoding: base64
Content-Disposition: attachment

$attachment
--PHP-mixed-$random_hash--
EOS;
        //send the email
        $mail_sent = @mail( $to, $subject, $message, $headers );
        //if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
        return $mail_sent;
    }

}