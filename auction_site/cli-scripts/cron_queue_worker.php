<?php

/**
 * Description of cron_queue_worker (cron_queue_worker.php)
 *
 * This version can be called by CRON. May be more suitable for Email than bids
 *
 * @package Crowdsurge White Label Auction site
 * @category CRON
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

if ($argc < 3) {
    print "\n\n";
    print 'First argument to script must be the environment eg. production';
    print 'Second argument to script must be the id of the Queue being managed. eg. bid_queue';
    print 'Third argument is optional: Number of queue items to fetch';
    print "\n\n";
    exit;
}

$receive = 10;

if ($argc == 3) {
    list($script, $environment, $queueName) = $argv;
}
else if ($argc == 4) {
    list($script, $environment, $queueName, $receive) = $argv;
}

putenv("APPLICATION_ENV=$environment");

include_once dirname(__FILE__) . '/bootstrap.php';
//include_once dirname(dirname(__FILE__)) . '/application/Bootstrap.php';

$queueManager = new Auction_Service_QueueManager($queueName);

$options = array(
    Auction_Service_QueueManager::CONFIG_WORKER_RECEIVE => (int) $receive
);

$queueManager->worker($options);

