PHP Command line scripts folder
===============================

bootstrap.php
Use this with the scripts.
the main difference between this and the /public/index.php should be that
the Front controller is not run()

cron_queue_worker.php
Responsible for handling emails (in most cases)

bid_queue_worker_daemon.php
The bid queue workhorse.
Grabs bids from the queue and pushes them through the BidProcessor task.

run_bid_queue_worker.sh
Shell script for starting the bid_queue_worker_daemon

stop_bid_queue_worker.sh
Shell script for stopping the bid_queue_worker_daemon

