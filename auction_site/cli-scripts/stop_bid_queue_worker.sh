#!/bin/bash

 if [ -f /var/run/auction_bid_queue_worker.pid ]
  then
    kill -9 `cat /var/run/auction_bid_queue_worker.pid`

    rm -f /var/run/auction_bid_queue_worker.pid

    if [ -f /var/run/auction_bid_queue_worker.pid ]
        then
            echo "Could not remove the pid file: /var/run/auction_bid_queue_worker.pid"
            echo "Please check that the Bid Queue Worker is running"
        else
            echo "pid file has been cleaned up"
            echo
            echo "Bid Queue Worker has been terminated."
            echo
    fi

  else
    echo "No pid file was found. This may mean the Bid Queue Worker is not running."
    echo
 fi

