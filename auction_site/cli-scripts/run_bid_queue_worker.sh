#!/bin/bash
# Set environment in terminal
# required on each restart including dev!
#  APPLICATION_ENV=the_server_environment
#  export APPLICATION_ENV
# $1 is the application path /data/www/dev_auction
# $2 is environment
# $3 is frequency of loop
# $4 is the number of bids to grab from the queue

#
nohup /usr/bin/php $1/cli-scripts/bid_queue_worker_daemon.php $2 $3 $4 2>&1 &

