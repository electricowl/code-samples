<?php
$pidfile = "/var/run/auction_bid_queue_worker.pid";
$self    = __FILE__;

if (is_readable($pidfile)) {
    $pid = file_get_contents($pidfile);

    print "\n\n";
    print "Bid Queue worker is already running on this server with PID: $pid \n";
    print "You will need to stop and restart the process to continue. \n";
    print "If the PID isn't visible using $ ps ax | grep $pid \n";
    print "Remove the pid file at '$pidfile' and rerun the bash script to start this process.";
    print "\n\n";
    exit;
}

if ($argc < 4) {
    print "\n\n";
    print 'First argument to script must be the environment. eg. ianl, development, or production';
    print 'Second argument to script must loop interval in seconds, eg. 2';
    print 'Third argument to script must be the batch size of bids to process. eg. 50';
    print "\n\n";
    exit;
}

list($script, $environment, $mainLoopInterval, $bidsBatchSize) = $argv;

$queueName = "bid_queue";
putenv("APPLICATION_ENV=$environment");
/**
 * Description of auction_queue_daemon (auction_queue_worker_daemon.php)
 * The function of the Queue Daemon is to grab items from
 * the queue and then send them off for processing by the correct process.
 *
 * Data will be encoded as JSON for simplicity.
 *
 * We will be bootstrapping into the Zend Framework Application
 *
 * @package Crowdsurge White Label Auction site
 * @category background process
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
include_once dirname(__FILE__) . '/bootstrap.php';
//include_once dirname(dirname(__FILE__)) . '/application/Bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

function workerErrorHandler($errno, $errstr, $errfile = null, $errline = null, $errcontext = null)
{
    switch ($errno) {
        case E_USER_ERROR:
        case E_USER_NOTICE:
        case E_USER_WARNING:
        case E_ERROR:
        case E_NOTICE:
        case E_WARNING:
            echo "Error occurred: $errno, $errline, $errstr\n\n";
            $exec = dirname(__FILE__) . "/stop_bid_queue_worker.sh";

            exec($exec, $output, $return_var);
            var_dump($output);
            exit(1);
            break;

        default:
            echo "Error not caught: $errno, $errline, $errstr\n\n";
            break;
    }
}

set_error_handler("workerErrorHandler");

declare(ticks = 1);

// This is for grown-up servers not Apple Macs
if (function_exists('pcntl_signal')) {
    pcntl_signal(SIGTERM, "signal_handler");
    pcntl_signal(SIGINT, "signal_handler");
}

function signal_handler($signal = null) {

    if (null !== $signal) {

        $pidfile = "/var/run/auction_bid_queue_worker.pid";
        $okToKill = false;

        if (is_readable($pidfile)) {
            $pid = file_get_contents($pidfile);
            $okToKill = true;
        }

        switch($signal) {
            case SIGTERM:
                print "Caught SIGTERM\n";
                if ($okToKill) {
                    unlink($pidfile);
                }
                exit(0);
            case SIGKILL:
                print "Caught SIGKILL\n";
                if ($okToKill) {
                    unlink($pidfile);
                }
                exit(0);
            case SIGINT:
                print "Caught SIGINT\n";
                if ($okToKill) {
                    unlink($pidfile);
                }
                exit(0);
        }

    }
}

$pid = getmypid();
// Write to a lock file
file_put_contents($pidfile, $pid);

// First we dequeue from the bid queue then pass the data to the
// task which we then run()
$queue = new Auction_Service_QueueManager(
        Auction_Service_QueueManager::QUEUE_NAME_BID
    );

while (1) {

    /*** Active Code here ***/

    // Here we get the bid messages we want to process
    $messages = $queue->receive((int) $bidsBatchSize);

    // Process bids whenever we have any to work on.
    if ($messages->count()) {
        foreach ($messages as $message) {

            $data  = json_decode($message->body);
            // Store the time the bid was received on the system.
            // $message->created is a timestamp
            $data->bidData->bid_received_timestamp = $message->created;

            $processTask = new Cs_Task_ProcessBid(
                $data->auctionId,
                $data->userId,
                $data->bidData
            );

            $processTask->run();
            // immediately delete
            $queue->deleteMessage($message);

            // Destroy the object before we start again.
            $processTask = null;
        }
    }

    // file to log last loop time
    $time = new DateTime();
    $date = $time->format("Y-m-d H:i:s");
    $qMessage = "$date\t$self is Running \n";
    file_put_contents("/tmp/bid_queue", $qMessage, FILE_APPEND);

    signal_handler();
    sleep((int) $mainLoopInterval);
}
