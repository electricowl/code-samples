<?php

/**
 * Description of Orders
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Orders extends Zend_Db_Table_Abstract
{
    protected $_name    = 'orders';
    protected $_primary = 'order_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }
}

