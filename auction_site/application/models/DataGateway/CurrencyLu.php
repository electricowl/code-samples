<?php

/**
 * Description of CurrencyLu
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_CurrencyLu extends Zend_Db_Table_Abstract
{
    protected $_name    = 'currency_lu';
    protected $_primary = 'currency_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function selectCurrencyById($id)
    {
        $result = $this->find($id);

        return $result->current()->toArray();
    }
}

