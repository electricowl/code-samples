<?php
/**
 * Description of Auction_Model_DataGateway_QueueMessage (QueueMessage.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_QueueMessage extends Zend_Db_Table_Abstract
{

    protected $_schema  = 'auction_queue';
    protected $_name    = 'message';
    protected $_primary = '';

    public function useEmailQueue()
    {
        $this->_name = "email_messages";
    }

    public function useBidQueue()
    {
        $this->_name = "bid_messages";
    }

    public function useUserQueue()
    {
        $this->_name = "user_messages";
    }

    public function useDefaultQueue()
    {
        $this->_name = "message";
    }

    /**
     * Nothing sophisticated here...
     */

    public function getMessageById($id)
    {
        return $this->find($id);
    }

    public function selectAll()
    {
        return $this->fetchAll();
    }



}

