<?php
/**
 * Description of Auction_Model_DataGateway_Countries (Countries.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_Countries extends Zend_Db_Table_Abstract
{

    protected $_name    = 'countries';
    protected $_primary = 'id';

    /**
     *
     * @param int $id
     * @return Zend_Db_Table_Row
     */
    public function selectCountryById($id)
    {
        return $this->find($id);
    }

    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function selectCountryDefaultCurrency($id)
    {
        $sql = <<<SQL
SELECT
    cnt.default_currency_id,
    curr.description
FROM countries cnt
INNER JOIN currency_lu curr ON cnt.default_currency_id = curr.currency_id
WHERE cnt.id = ?
SQL;
        $result = $this->getDefaultAdapter()->query($sql, $id);
        return $result->fetchAll();
    }
}

