<?php
/**
 * Description of Auction_Model_DataGateway_AuctionItemTypesLu (AuctionItemTypesLu.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_AuctionItemTypesLu extends Zend_Db_Table_Abstract
{

    protected $_name    = 'auction_item_types_lu';
    protected $_primary = 'auction_item_type_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function fetchById($id)
    {
        $result = $this->find($id);

        return $result->toArray();
    }
}

