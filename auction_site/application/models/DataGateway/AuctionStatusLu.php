<?php
/**
 * Description of Auction_Model_DataGateway_AuctionStatusLu (AuctionStatusLu.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_AuctionStatusLu extends Zend_Db_Table_Abstract
{

    protected $_name    = 'auction_status_lu';
    protected $_primary = 'auction_status_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }
}

