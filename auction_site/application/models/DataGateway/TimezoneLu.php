<?php

/**
 * Description of TimezoneLu
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_TimezoneLu extends Zend_Db_Table_Abstract
{
    protected $_name    = 'timezone_lu';
    protected $_primary = 'timezone_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function getTimezoneById($id)
    {
        $result = $this->find($id);
        return $result->toArray();
    }

    public function getTimezoneByCode($code)
    {
        $result = $this->fetchAll(array('code' => $code));

        return $result;
    }
}
