<?php
/**
 * Description of Campaigns
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */

class Auction_Model_DataGateway_Campaigns extends Zend_Db_Table_Abstract
{
    protected $_name    = 'campaigns';
    protected $_primary = 'campaign_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }


    public function getAllActiveCampaigns($client_id)
    {
        $sql = <<<SQL
SELECT
    c.campaign_id,
    c.client_id,
    cl.client_name,
    c.campaign_custom_ref,
    c.campaign_name,
    c.campaign_description
FROM campaigns c
    INNER JOIN clients cl
        ON c.client_id = cl.client_id
    WHERE c.client_id = :client_id AND
        campaign_id IN (
        SELECT campaign_id FROM auctions
    )
SQL;
        return $this->getDefaultAdapter()->query(
                $sql,
                array(
                    'client_id' => $client_id
                ));
    }

    public function getActiveCampaignsByClientId($client_id)
    {
        $sql = <<<SQL
SELECT
    c.campaign_id,
    c.client_id,
    cl.client_name,
    c.campaign_custom_ref,
    c.campaign_name,
    c.campaign_description
FROM campaigns c
    INNER JOIN clients cl
        ON c.client_id = cl.client_id
    WHERE
        c.client_id = :client_id
        AND campaign_id IN (
        SELECT campaign_id FROM auctions
    )
SQL;
        $statement = $this->getDefaultAdapter()->prepare($sql);
        $statement->execute(array(
            'client_id' => $client_id
        ));

        return $statement->fetchAll();
    }
}

