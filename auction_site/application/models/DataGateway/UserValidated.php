<?php
/**
 * Description of Auction_Model_DataGateway_UserValidated (UserValidated.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_UserValidated extends Zend_Db_Table_Abstract
{
    const VALIDATION_TYPE = 'cc';

    protected $_name    = 'user_validated';
    protected $_primary = 'user_id';

    public function getDetails($id)
    {
        $result = $this->find($id);
        return $result->toArray();
    }

    /**
     * BY default this method will add a validated record for the user_id provided
     * @param int $userId
     * @param boolean $isValidated
     * @param string $validationMethod
     */
    public function upsertUserValidation($userId, $isValidated = true, $validationMethod = self::VALIDATION_TYPE)
    {
        $sql = <<<SQL
INSERT INTO `user_validated`
(
    `user_id`,
    `validation_method`,
    `is_validated`
)
VALUES
(
    ?,
    ?,
    ?
)
ON DUPLICATE KEY UPDATE validation_method = ?, is_validated = ?
SQL;

        return $this->getDefaultAdapter()->query($sql, array(
            $userId,
            $validationMethod,
            $isValidated ? 1 : 0,
            $validationMethod,
            $isValidated ? 1 : 0
        ));

    }
}

