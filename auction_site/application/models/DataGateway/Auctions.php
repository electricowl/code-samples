<?php

/**
 * Description of Auctions
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Auctions extends Zend_Db_Table_Abstract
{
    protected $_name    = 'auctions';
    protected $_primary = 'auction_id';

    const STATUS_DRAFT     = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_CLOSED    = 3;

    public function selectAll()
    {
        return $this->fetchAll();
    }

    /**
     *
     * @param int $id campaign_id
     * @param int $client_id
     * @return array
     */
    public function selectAllAuctionsByCampaignId($id, $client_id)
    {
        $sql = <<<SQL
SELECT
    a.auction_id,
    a.campaign_id,
    c.campaign_name,
    c.campaign_description,
    a.auction_custom_ref,
    a.auction_name,
    a.auction_description,
    a.display_order,
    a.auction_status_id
FROM auctions a
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = :client_id
WHERE c.campaign_id = :campaign_id
SQL;
        $result = $this->getDefaultAdapter()->query($sql,
                    array(
                        'campaign_id' => $id,
                        'client_id'   => $client_id
                    )
                );
        return $result->fetchAll();
    }

    public function selectAllAuctionsByClientId($client_id)
    {
        $sql = <<<SQL
SELECT
    a.auction_id,
    a.campaign_id,
    c.campaign_name,
    c.campaign_description,
    a.auction_custom_ref,
    a.auction_name,
    a.auction_description,
    a.display_order,
    a.auction_status_id
FROM auctions a
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id
WHERE c.client_id = :client_id
SQL;
        $statement = $this->getDefaultAdapter()->prepare($sql);
        $statement->execute(array(
            'client_id' => $client_id
        ));

        return $statement->fetchAll();
    }

    /**
     *
     * @param int $auction_id
     * @param int $client_id
     * @return array
     */
    public function selectAuctionDetailsByAuctionId($auction_id, $client_id)
    {
        $sql = <<<SQL
SELECT
    a.auction_id,
    a.campaign_id,
    c.campaign_custom_ref,
    c.campaign_name,
    a.auction_custom_ref,
    c.campaign_description,
    a.auction_name,
    a.auction_description,
    a.display_order,
    a.auction_status_id
FROM auctions a
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = :client_id
WHERE a.auction_id = :auction_id
SQL;
        $result = $this->getDefaultAdapter()->query(
                $sql,
                array(
                    'auction_id' => $auction_id,
                    'client_id'  => $client_id
                )
            );
        return $result->fetchAll();
    }

    /**
     *
     * @param int $campaign_id
     * @param int $client_id
     * @return array
     */
    public function selectConfirmedAuctionsByCampaignId($campaign_id, $client_id)
    {
        $sql = <<<SQL
SELECT
    a.auction_id,
    a.campaign_id,
    c.campaign_custom_ref,
    c.campaign_name,
    c.campaign_description,
    a.auction_custom_ref,
    a.auction_name,
    a.auction_description,
    a.display_order,
    a.auction_status_id
FROM auctions a
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = :client_id
WHERE c.campaign_id = :campaign_id
    AND a.auction_status_id = :status_id

SQL;
        $result = $this->getDefaultAdapter()->query(
                $sql,
                array(
                    'campaign_id' => $campaign_id,
                    'client_id'   => $client_id,
                    'status_id'   => self::STATUS_CONFIRMED
                )
            );
        return $result->fetchAll();
    }
}