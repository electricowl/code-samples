<?php

/**
 * Description of AuctionItems
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_AuctionItems extends Zend_Db_Table_Abstract
{
    protected $_name    = 'auction_items';
    protected $_primary = 'auction_item_id';

    const TYPE_STANDARD = 1;
    const TYPE_PRE_AUTH = 2;
    const TYPE_DEPOSIT  = 3;

    /**
     *
     * @param int $client_id
     * @return array
     */
    public function selectAll($client_id)
    {
        $sql = <<<SQL
SELECT
    a.campaign_id,Rowset
	ai.auction_item_id,
	ai.auction_id,
    a.auction_name,
	ai.auction_item_custom_ref,
	ai.auction_item_type_id,
	ai.auction_item_name,
	ai.auction_item_description,
	ai.start_date,
	ai.end_date,
	ai.timezone_id,
	ai.currency_id,
	ai.min_bid_increment,
	ai.max_bid_increment,
	ai.starting_bid,
	ai.reserve,
	ai.display_order
FROM auction_items ai
INNER JOIN auctions a
    ON ai.auction_id = a.auction_id
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = :client_id
SQL;
        $result = $this->getDefaultAdapter()->query(
                $sql,
                array(
                    'client_id' => $client_id
                ));
        return $result->fetchAll();
    }

    /**
     * Return the live auction lots from the db
     * Live means anything where now() is between start/end date
     * optionally filter by the campaign id
     * @param int $auctionId Optional filter by campaign
     * @return resultset
     */
    public function selectLiveAuctionItems($auctionId = null, $client_id = null)
    {
        if (null === $client_id) {
            throw new Exception("client-id must be set in " . __METHOD__);
        }

        $params = array();

        $sql = <<<SQL
SELECT
        a.campaign_id,
	ai.auction_item_id,
	ai.auction_id,
        a.auction_name,
	ai.auction_item_custom_ref,
	ai.auction_item_type_id,
	ai.auction_item_name,
	ai.auction_item_description,
	ai.start_date,
	ai.end_date,
	ai.timezone_id,
	ai.currency_id,
	ai.min_bid_increment,
	ai.max_bid_increment,
	ai.starting_bid,
	ai.reserve,
	ai.display_order
FROM auction_items ai
INNER JOIN auctions a
	ON ai.auction_id = a.auction_id
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = ?
WHERE a.auction_status_id = 2
    AND NOW() BETWEEN ai.start_date AND ai.end_date
SQL;
        // We need the client id here for filtering purposes
        $params[] = $client_id;

        if (null !== $auctionId) {
            $sql .= " AND ai.auction_id = ? ";
            $params[] = $auctionId;
        }

        $sql .= " ORDER BY ai.end_date ASC";
        $result = $this->getDefaultAdapter()->query($sql, $params);
        return $result->fetchAll();
    }

    /**
     * Return a count of the auctions that are live now.
     * @param int $campaignId optional
     * @return type
     */
    public function countLiveAuctions($campaignId = null, $client_id = null)
    {
        if (null === $client_id) {
            throw new Exception("client-id must be set in " . __METHOD__);
        }

        $params = array();

        $sql = <<<SQL
SELECT
        lots.auction_id, count(*) total_items
FROM
    auction_items lots
INNER JOIN auctions a
	ON lots.auction_id = a.auction_id
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = ?
WHERE lots.start_date < NOW() AND lots.end_date > NOW()
SQL;
        // client_id must be set
        $params[] = $client_id;

        if (null !== $campaignId) {
            $sql .= " AND a.campaign_id = ? ";
            $params[] = $campaignId;
        }

        $sql .= " GROUP BY lots.auction_id ";
        $result = $this->getDefaultAdapter()->query($sql, $params);
        return $result->fetchAll();
    }

    /**
     * Item locked to client_id - so we can only see where there is a match.
     * @param type $item_id
     * @param type $client_id
     * @return array
     */
    public function selectItem($item_id, $client_id)
    {
        $sql = <<<SQL
SELECT
    a.campaign_id,
	ai.auction_item_id,
	ai.auction_id,
    a.auction_name,
	ai.auction_item_custom_ref,
	ai.auction_item_type_id,
	ai.auction_item_name,
	ai.auction_item_description,
	ai.start_date,
	ai.end_date,
	ai.timezone_id,
	ai.currency_id,
	ai.min_bid_increment,
	ai.max_bid_increment,
	ai.starting_bid,
	ai.reserve,
	ai.display_order
FROM auction_items ai
INNER JOIN auctions a
    ON ai.auction_id = a.auction_id
INNER JOIN campaigns c
    ON a.campaign_id = c.campaign_id AND c.client_id = :client_id
WHERE ai.auction_item_id = :item_id
SQL;






        $statement = $this->getDefaultAdapter()->prepare(
                $sql
            );
        $is_valid = $statement->execute(array(
                    'item_id'   => $item_id,
                    'client_id' => $client_id
                ));

        if ($is_valid) {
            $result = $statement->fetch();

            return $result;

        }
        return null;

    }

    public function getAuctionReportByClientId($client_id = null)
    {
        if (null === $client_id) {
            throw new Exception("client-id must be set in " . __METHOD__);
        }

        $params = array();

        $sql = <<<SQL
SELECT
    c.campaign_id,
    c.campaign_custom_ref,
    c.campaign_name,
    c.campaign_description,
    a.auction_id,
    a.auction_custom_ref,
    a.auction_name,
    asl.label AS auction_status,
    ai.auction_item_id,
    ai.auction_item_custom_ref,
    ai.auction_item_name,
    ai.auction_item_description,
    ai.start_date,
    ai.end_date,
    tl.code AS timezone,
    cl.description AS currency,
    ai.min_bid_increment,
    ai.max_bid_increment,
    ai.starting_bid,
    ai.reserve,
    (SELECT COUNT(bid_id) FROM bids WHERE auction_item_id = ai.auction_item_id) AS bids,
    (SELECT current_bid FROM bids WHERE auction_item_id = ai.auction_item_id AND bid_status_id = 1) AS current_highest_bid,
    (SELECT current_bid FROM bids WHERE auction_item_id = ai.auction_item_id AND bid_status_id = 5) AS winner
FROM 
    auction_items AS ai
    INNER JOIN auctions AS a ON ai.auction_id = a.auction_id
    INNER JOIN auction_status_lu AS asl ON a.auction_status_id = asl.auction_status_id
    INNER JOIN campaigns AS c ON a.campaign_id = c.campaign_id
    INNER JOIN timezone_lu AS tl ON ai.timezone_id = tl.timezone_id
    INNER JOIN currency_lu AS cl ON ai.currency_id = cl.currency_id
WHERE
    a.auction_status_id IN(2,3)
    AND c.client_id = ?
ORDER BY
    c.campaign_name ASC, a.auction_name ASC, ai.auction_item_name ASC, c.campaign_id ASC, a.auction_id ASC, ai.auction_item_id ASC
SQL;
        // client_id must be set
        $params[] = $client_id;
        $result = $this->getDefaultAdapter()->query($sql, $params);
        return $result->fetchAll();
    }

}
