<?php

/**
 * Description of Users
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Users extends Zend_Db_Table_Abstract
{

    /** Table name */
    protected $_name    = 'users';
    protected $_primary = 'user_id';

    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function getUserById($id)
    {
        return $this->find($id);
    }

    public function resetPassByEmail($email, $new_password)
    {
        $sql = <<<SQL
UPDATE
    users
SET
    password = SHA1(:new_password)
WHERE
    email = :email
SQL;

        $statement = $this->getDefaultAdapter()->prepare($sql);

        $statement->execute(
                array(
                    'new_password' => $new_password,
                    'email' => $email
                )
        );
    }

    public function changePassByUserId($user_id, $new_password)
    {
        $sql = <<<SQL
UPDATE
    users
SET
    password = SHA1(:new_password)
WHERE
    user_id = :user_id
SQL;

        $statement = $this->getDefaultAdapter()->prepare($sql);

        $statement->execute(
                array(
                    'new_password' => $new_password,
                    'user_id' => $user_id
                )
        );
    }

    public function checkPassByUserId($user_id, $current_password)
    {
        $sql = <<<SQL
SELECT
    *
FROM
    users
WHERE
    password = SHA1(:current_password)
    AND user_id = :user_id
SQL;

        $statement = $this->getDefaultAdapter()->prepare($sql);

        $statement->execute(
                array(
                    'current_password' => $current_password,
                    'user_id' => $user_id
                )
        );

        return $statement->fetchAll();
    }

}

