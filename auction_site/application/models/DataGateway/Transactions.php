<?php

/**
 * Description of Transactions
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Transactions extends Zend_Db_Table_Abstract
{
    protected $_name    = 'transactions';
    protected $_primary = 'transaction_id';

    const TYPE_PRE_AUTH       = 1;
    const TYPE_DEPOSIT        = 2;
    const TYPE_SETTLEMENT     = 3;
    const TYPE_PRE_AUTH_VOID   = 4;
    const TYPE_DEPOSIT_REFUND = 5;
    const TYPE_REFUND         = 6;
    const TYPE_CANCELLED      = 7;

    // transaction_status_lu
    const STATUS_CONFIRMED       = 1;
    const STATUS_PAYMENT_PENDING = 2;
    const STATUS_FAILED          = 3;

    public function selectAll()
    {
        return $this->fetchAll();
    }
}

