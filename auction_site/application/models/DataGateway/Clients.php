<?php

/**
 * Description of Clients
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Clients extends Zend_Db_Table_Abstract
{
    protected $_name    = 'clients';
    protected $_primary = 'client_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function fetchClientByThemeId($theme_id)
    {
        $result = $this->fetchRow(array('client_theme_id = ?' => $theme_id));

        return $result;
    }

    public function addClient($company, $theme_id)
    {
        $new_id = $this->insert(array(
                    'client_name'     => $company,
                    'client_theme_id' => $theme_id
                )
            );

        return $new_id;
    }
}

