<?php

/**
 * Description of Emails
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Emails extends Zend_Db_Table_Abstract
{
    protected $_name    = 'emails';
    protected $_primary = 'email_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }
}

