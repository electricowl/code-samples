<?php

/**
 * Description of ClientStaff
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_ClientStaff extends Zend_Db_Table_Abstract
{
    protected $_name    = 'client_staff';
    protected $_primary = 'staff_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function addStaff($client_id, $data)
    {
        $new_id = $this->insert(
                array(
                   'client_id'   => $client_id,
                   'first_name'  => $data['client_staff_first_name'],
                   'last_name'   => $data['client_staff_last_name'],
                   'email'       => $data['client_staff_email'],
                   'username'    => $data['client_staff_username'],
                   'password'    => $data['client_staff_password']
                )
            );

        return $new_id;
    }
}
