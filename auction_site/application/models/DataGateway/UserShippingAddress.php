<?php

/**
 * Description of UserShippingAddress
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_UserShippingAddress extends Zend_Db_Table_Abstract
{
    protected $_name    = 'user_shipping_address';
    protected $_primary = 'shipping_address_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function insertAddress($address)
    {
        $id = $this->insert($address);

        return $id;
    }

    public function selectActiveAddressesByUserId($userId)
    {
        $select = $this->select($this->_name)
                ->where("user_id = ?", $userId)
                ->where("is_default = ?", 1)
                ->where("is_active = ?", 1);

        return $select->query()->fetchAll();
    }


}

