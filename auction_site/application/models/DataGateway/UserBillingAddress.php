<?php

/**
 * Description of UserBillingAddress
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_UserBillingAddress extends Zend_Db_Table_Abstract
{
    protected $_name    = 'user_billing_address';
    protected $_primary = 'billing_address_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    public function insertAddress($address)
    {
        $id = $this->insert($address);

        return $id;
    }

    public function selectActiveAddressesByUserId($userId)
    {
        $select = $this->select($this->_name)
                ->where("user_id = ?",    $userId)
                ->where("is_default = ?", 1)
                ->where("is_active = ?",  1);

        $result = $select->query()->fetchAll();

        return $result;
    }

    public function supersedePreviousAddressesForUser($userId)
    {
        $this->update(array(
            'is_default' => 0,
            'is_active'  => 0,
        ), "user_id = $userId");
    }
}

