<?php
/**
 * Description of Auction_Model_DataGateway_Queue (Queue.php)
 * @package Crowdsurge White Label Auction site
 * @category DataGateway
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Model_DataGateway_Queue extends Zend_Db_Table_Abstract
{

    protected $_schema  = 'auction_queue';
    protected $_name    = 'queue';
    protected $_primary = '';


    public function selectAll()
    {
        return $this->fetchAll();
    }
}

