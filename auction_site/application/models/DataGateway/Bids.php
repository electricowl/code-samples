<?php

/**
 * Description of Bids
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_Bids extends Zend_Db_Table_Abstract
{
    const BID_CURRENT_HIGHEST   = 1;
    const BID_OUTBID            = 2;
    const BID_OUTBID_INSTANTLY  = 3;
    const BID_MANUALLY_VOIDED   = 4;
    const BID_WINNER            = 5;
    const BID_MAXIMUM_INCREASED = 6;
    const BID_QUEUED            = 99;

    protected $_name    = 'bids';
    protected $_primary = 'bid_id';


    public function selectAll()
    {
        return $this->fetchAll();
    }

    /**
     *
     * @param int $itemId
     * @param float $bidAmount
     * @return type
     */
    public function bidRange($itemId, $bidAmount)
    {
        $sql = <<<SQL
SELECT
    b.bid_id,
    b.auction_item_id,
    i.auction_id,
    b.user_id,
    b.bid_date,
    b.current_bid,
    i.min_bid_increment,
    b.max_bid + i.min_bid_increment min_bid_permitted,
    b.max_bid + i.max_bid_increment max_bid_permitted,
    ? proposed_bid,
    CASE
        WHEN
            ? >= (b.max_bid + i.min_bid_increment)
                AND ? <= (b.max_bid + i.max_bid_increment)
        THEN 1
        ELSE 0
    END AS is_valid_bid,
    b.max_bid,
    b.transaction_id,
    b.bid_status_id
FROM
    bids b
        INNER JOIN
    auction_items i ON b.auction_item_id = i.auction_item_id
WHERE
    b.auction_item_id = ?
        AND b.bid_status_id = ?
SQL;

        $statement = $this->getDefaultAdapter()->prepare($sql);
        $statement->execute(array(
            $bidAmount,
            $bidAmount,
            $bidAmount,
            $itemId,
            Auction_Model_DataGateway_Bids::BID_CURRENT_HIGHEST,
        ));
        // Just grab the first result row
        $result = $statement->fetch();

        return $result;
    }

    /**
     * This method creates and returns a RowDataGateway from the bids table
     * The RDG can be used to apply updates to the record as we go through the
     * process
     * @param int $itemId
     * @param int $userId
     * @param float $currentBid This is the minumum permitted bid
     * @param float $maxBid This is the bid placed by the bidder
     * @param int $transactionId
     * @param int $bidStatusId
     * @return Zend_Db_Table_Row
     */
    public function createBid($itemId, $userId, $currentBid, $maxBid, $transactionId, $bidStatusId)
    {
        $data = array(
            'bid_id'          => null,
            'auction_item_id' => $itemId,
            'user_id'         => $userId,
            'bid_date'        => new Zend_Db_Expr("NOW()"),
            'current_bid'     => $currentBid,
            'max_bid'         => $maxBid,
            'transaction_id'  => $transactionId,
            'bid_status_id'   => $bidStatusId
        );

        $row = $this->createRow($data);
        $row->save();

        return $row;
    }

    public function selectBidById($id)
    {
        $row = $this->fetchRow("bid_id = $id");

        return $row;
    }
    /**
     * Update the bid record with the changed fields.
     * @param int $id
     * @param array $data
     */
    public function updateBid($id, $data)
    {
        $row = $this->fetchRow("bid_id = $id");

        $row->setFromArray($data);

        $row->save();
    }

    public function selectBidHistoryDetails($id, $bid_status_ids = array())
    {
        if (!empty($bid_status_ids)) {
            $status_ids = '';
            foreach($bid_status_ids as $bid_status_id) {
                $status_ids .= $bid_status_id.',';
            }
            $status_ids = substr($status_ids, 0, strlen($status_ids)-1);
            $bid_status_ids = "AND b.bid_status_id IN($status_ids)";
        }
        else {
            $bid_status_ids = '';
        }

        $sql = <<<SQL
SELECT
    b.bid_id,
    b.auction_item_id,
    b.user_id,
    b.bid_date,
    b.current_bid,
    b.max_bid,
    b.transaction_id,
    b.bid_status_id,
    b.locked,
    u.username,
    u.first_name,
    u.last_name,
    u.email
FROM bids b
INNER JOIN users u ON b.user_id = u.user_id
WHERE b.auction_item_id = ?
$bid_status_ids
ORDER BY b.bid_date DESC
SQL;
        $result = $this->getDefaultAdapter()->query($sql, $id);
        return $result->fetchAll();
    }

    public function selectBidHistoryByUserIdByItemId($user_id, $item_id)
    {
        $sql = <<<SQL
SELECT
    b.bid_id,
    b.auction_item_id,
    b.user_id,
    b.bid_date,
    b.current_bid,
    b.max_bid,
    b.transaction_id,
    b.bid_status_id,
    b.locked,
    u.username,
    u.first_name,
    u.last_name,
    u.email
FROM bids b
INNER JOIN users u ON b.user_id = u.user_id
WHERE b.user_id = ?
    AND b.auction_item_id = ?
ORDER BY b.bid_date DESC
SQL;

        $statement = $this->getDefaultAdapter()->prepare($sql);
        $statement->execute(array(
            $user_id,
            $item_id
        ));
        // Just grab the first result row
        $result = $statement->fetch();

        return $result;
    }

}