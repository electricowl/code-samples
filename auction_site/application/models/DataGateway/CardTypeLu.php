<?php

/**
 * Description of CardTypeLu
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Model_DataGateway_CardTypeLu extends Zend_Db_Table_Abstract
{
    protected $_name    = 'card_types_lu';
    protected $_primary = 'card_type_id';

    public function selectAllCards()
    {
        $cards = $this->fetchAll();

        return $cards->toArray();
    }

}

