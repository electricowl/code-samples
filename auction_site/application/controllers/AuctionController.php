<?php

/**
 * Description of AuctionController
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class AuctionController extends Cs_Controller_AbstractActionController
{

    public function init()
    {
        parent::init();

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext('detail-feed', 'json')
                      ->initContext();
    }

    public function indexAction()
    {
        $auction_id = (int)$this->_request->getParam('auction', 0);

        if ($auction_id > 0) {
            $this->_helper->redirector->gotoSimple(
                'list',
                'auction',
                null
            );
        }
        else {            
            $this->_redirect('/');
        }
    }

    public function listAction()
    {
        $campaign_id = $this->_request->getParam('campaign_id', null);

        $service = new Auction_Service_Auction();

        $list = array();

        $campaign_name = '';
        if (null != $campaign_id) {
            $list = $service->listAuctionsByCampaignId(
                    $campaign_id,
                    Zend_Registry::get('client_id')
                );
            $auction = reset($list);
            $campaign_name = $auction['campaign_name'];
        }

        $this->view->auctions = $list;
        $this->view->campaign_id = $campaign_id;
        $this->view->breadcrumbs = array($campaign_name => '');

        //Meta Properties
        // @todo Need a view helper for this!
        $meta_properties = array();
        $meta_properties['og:title']        = '(RED) ROWS - ' . $campaign_name;
        $meta_properties['og:type']         = 'website';
        $meta_properties['og:url']          = 'http://' . SERVER_NAME . '/campaign/' . $campaign_id;
        $meta_properties['og:image']        = 'http://' . SERVER_NAME . '/themes/red/images/' . $auction['campaign_custom_ref'] . '_sq.jpg';
        $meta_properties['og:description']  = 'Bid on tickets to see the world\'s greatest artists. Great Music Saves Lives.';
        $meta_properties['og:site_name']    = '(RED) ROWS';
        $meta_properties['fb:admins']       = '199710142';

        $this->view->meta_property = $meta_properties;

        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                'category'   => 'Auction:overview',
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $this->auth_userid,
                    'user'      => $this->auth_username,
                    'params'    => $this->_request->getParams(),
                )),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Auctions list");
    }

    /**
     * This page lists the information about a single auction and the list
     * of items (lots) that form part of the auction.
     */
    public function detailAction()
    {
        $auction_id = $this->getParam('auction_id');

        $auctionService = new Auction_Service_Auction();
        $auctionInfo = $auctionService->getAuctionDetailsById(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        if (null == $auctionInfo) {
            // we need to stop processing and provide a product not found page
            $this->renderScript('error/404.phtml');
            return;
        }

        $auctionItemsService = new Auction_Service_AuctionItem();
        // This items list us currently UNFILTERED
        // @todo provide for filtering
        $itemsList = $auctionItemsService->fetchAllItemsByAuctionId(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        //Split the auction_name to get artist/date/venue
        $crumb_name = $auctionInfo['auction_name'];
        $artist = '';
        $auction_name_parts = explode(' // ', $auctionInfo['auction_name']);
        if (count($auction_name_parts) > 1) {
            $artist = $auction_name_parts[0];
            $datetime = $auction_name_parts[1];
            $venue_name = $auction_name_parts[2];
            $venue_loc = $auction_name_parts[3];

            $event_date = new DateTime($datetime);
            $new_event_date = $event_date->format('F jS, Y');
            $crumb_name = $new_event_date . ' at ' . $venue_name;
        }

        $this->view->auction_id   = $auction_id;
        $this->view->campaign_id  = $auctionInfo['campaign_id'];
        $this->view->auction_name = $auctionInfo['auction_name'];
        $this->view->auctionInfo  = $auctionInfo;
        $this->view->itemsList    = $itemsList;
        $this->view->breadcrumbs  = array(
            $auctionInfo['campaign_name'] => '/auction/list/campaign_id/' . $auctionInfo['campaign_id'],
            $crumb_name => ''
        );

        //Meta Properties
        $meta_properties = array();
        $meta_properties['og:title']        = '(RED) ROWS - ' . $artist;
        $meta_properties['og:type']         = 'website';
        $meta_properties['og:url']          = 'http://' . SERVER_NAME . '/auction/' . $auctionInfo['auction_id'];
        $meta_properties['og:image']        = 'http://' . SERVER_NAME . '/themes/red/images/' . $auctionInfo['campaign_custom_ref'].'_sq.jpg';
        $meta_properties['og:description']  = 'Bid on tickets to see the world\'s greatest artists. Great Music Saves Lives.';
        $meta_properties['og:site_name']    = '(RED) ROWS';
        $meta_properties['fb:admins']       = '199710142';

        $this->view->meta_property = $meta_properties;
    }

    public function detailFeedAction()
    {
        $auction_id = $this->getParam('auction_id');

        $auctionService = new Auction_Service_Auction();
        $auctionInfo = $auctionService->getAuctionDetailsById(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        if (null == $auctionInfo) {
            // we need to stop processing and provide a product not found page
            $this->renderScript('error/not-found.phtml');
            return;
        }

        $auctionItemsService = new Auction_Service_AuctionItem();
        // This items list us currently UNFILTERED
        // @todo provide for filtering
        $itemsList = $auctionItemsService->fetchAllItemsByAuctionId(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        $auction_items = array();
        foreach ($itemsList as $item) {
            
            $item_data = array();
            $item_data['item_id'] = $item->getItemId();
            $item_data['custom_ref'] = $item->getCustomRef();
            $item_data['name'] = $item->getName();
            $item_data['description'] = $item->getDescription();
            $item_data['start_date'] = $item->getStartDate();
            $item_data['end_date'] = $item->getEndDate();
            $item_data['max_bid_increment'] = $item->getMaxBidIncrement();
            $item_data['min_bid_increment'] = $item->getMinBidIncrement();
            $item_data['starting_bid'] = $item->getStartingBid();
            $item_data['reserve'] = $item->getReserve();
            $item_data['highest_bidder_username'] = $item->getHighestBidderUsername();
            $item_data['currency_symbol'] = $item->getCurrencySymbol();
            $item_data['currency_iso'] = $item->getCurrencyDescription();
            $item_data['bidPublicHistory'] = $item->getBidPublicHistory();
            $item_data['bid_highest'] = $item->getCurrentBid();

            $auction_items[] = $item_data;
        }

        $this->view->server_time   = time();
        $this->view->auction_info   = $auctionInfo;
        $this->view->auction_items  = $auction_items;
    }

    public function eventAction()
    {
        $campaign_id = $this->_request->getParam('campaign_id', null);
        $auction_id  = $this->_request->getParam('auction_id', null);
        $service = new Auction_Service_Auction();
        $list = array();

        if (null != $campaign_id) {
            $list = $service->listAuctionsByCampaignId(
                    $campaign_id,
                    Zend_Registry::get('client_id')
                );
        }

        $this->view->auctions = $list;
        $this->view->campaign_id = $campaign_id;

        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                'category'   => 'Auction:overview',
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $this->auth_userid,
                    'user'      => $this->auth_username,
                    'params'    => $this->_request->getParams(),
                )),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Event Auctions list");
    }

    /**
     * Display the item information here.
     * Display the bid form
     * Display the bid history and current highest valid bid
     */
    public function showItemAction()
    {
        $campaign_id = $this->_request->getParam('campaign_id', null);
        $item_id     = $this->_request->getParam('item_id',     null);
        $auction_id  = $this->_request->getParam('auction_id',  null);
        // NB parse_str is great but remember the array name as 2nd parameter.
        // otherwise your data get dumped into the local symbol table
        // If $plan_data comes from a POST array we do this//
        parse_str($this->_request->getParam('plan_data', null), $plan_data);

        if (empty($plan_data)) {
            // we will look in the url
            $plan_data['margin_left'] = $this->_getParam('margin_left', 10);
            $plan_data['margin_top']  = $this->_getParam('margin_top' , 10);
        }

        $auctionService  = new Auction_Service_Auction();
        $bidService      = new Auction_Service_Bid();
        $currencyService = new Auction_Service_Currency();
        $itemService     = new Auction_Service_AuctionItem();

        $auctionDetails = $auctionService->getAuctionDetailsById(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        if (null == $auctionDetails) {
            // we need to stop processing and provide a product not found page
            $this->renderScript('error/404.phtml');
            return;
        }

        $this->view->auctionDetails = $auctionDetails;
        /** @var Cs_Data_AuctionItem $itemObj */
        $itemObj = $itemService->getItemById(
                $item_id,
                Zend_Registry::get('client_id')
            );

        $currencyDetails = $currencyService->getCurrencyById(
                $itemObj->getCurrencyId()
            );

        $transactionCurrencyDetails = $currencyService->getCurrencyById(
                $currencyDetails['transaction_currency']
            );

        $bidForm = new Auction_Form_NewBid();
        $bidForm->setAction("/auction/confirm-bid/campaign_id/$campaign_id/auction_id/$auction_id/item_id/$item_id");
        $bidForm->setDefaults(array(
            'auction_id'  => $auction_id,
            'campaign_id' => $campaign_id,
            'item_id'     => $item_id
        ));

        $bidHistory         = $itemObj->getBidPublicHistory();
        $bidHighest         = $itemObj->get('bid_highest');
        $isAwaitingFirstBid = $itemObj->get('is_awaiting_first_bid');

        // Valid bid range is based on the highest current bid.
        // or on the status of the bid - eg, no bids

        // Min permitted bid gets placed in the current_bid field
        // bidAmount gets placed in the max_bid field
        if ($isAwaitingFirstBid) {
            $bidAmount = $itemObj->getStartingBid();
            $minPermittedBid = $bidAmount;
            $maxPermittedBid = $itemObj->calculateMaxBid($bidAmount);
        }
        else {
            $bidAmount = $bidHighest['current_bid'];
            $bidRange = $auctionService->getBidRangeData($item_id, $bidAmount);

            $maxPermittedBid = $bidRange->max_bid_permitted;
            $minPermittedBid = $bidRange->min_bid_permitted;
        }

        // load the form with the max and min bid amounts (Will be used in the
        // confirm bid page when validating as we defer final judgement to the
        // BidProcessor
        $bidForm->getElement('maximum_bid')->setValue($maxPermittedBid);
        $bidForm->getElement('minimum_bid')->setValue($itemObj->getNextMinimumBid());

        $this->view->maxPermittedBid = $maxPermittedBid;
        $this->view->minPermittedBid = $minPermittedBid;

        $this->view->itemDetails           = $itemObj;
        $this->view->timeRemaining         = $itemObj->getTimeRemaining();
        $this->view->newBidForm            = $bidForm;
        $this->view->plan_data             = $plan_data;

        $this->view->bidHistory            = $bidHistory;
        $this->view->bidHighest            = $bidHighest;

        $this->view->currencySymbol        = $currencyDetails['symbol'];
        $this->view->currencyCode          = $currencyDetails['description'];
        $this->view->transactionCurrencyId = $currencyDetails['transaction_currency'];
        $this->view->transactionCurrencySymbol   = $transactionCurrencyDetails['symbol'];

        //Split the auction_name to get artist/date/venue
        $crumb_name = $auctionDetails['auction_name'];
        $auction_name_parts = explode(' // ', $auctionDetails['auction_name']);
        $artist = '';
        if (count($auction_name_parts) > 1) {
            $artist = $auction_name_parts[0];
            $datetime = $auction_name_parts[1];
            $venue_name = $auction_name_parts[2];
            $venue_loc = $auction_name_parts[3];

            $event_date = new DateTime($datetime);
            $new_event_date = $event_date->format('F jS, Y');
            $crumb_name = $new_event_date . ' at ' . $venue_name;
        }

        $this->view->breadcrumbs  = array(
            $auctionDetails['campaign_name'] => '/auction/list/campaign_id/' . $campaign_id,
            $crumb_name => '/auction/detail/campaign_id/'.$campaign_id.'/auction_id/' . $auction_id
        );

        //Meta Properties
        // @todo This needs to have the RED stuff parameterised
        // So we need to use the campaign owner (as it were) to here. and the theme
        $meta_properties = array();
        $meta_properties['og:title']        = '(RED) ROWS - ' . $artist;
        $meta_properties['og:type']         = 'website';
        $meta_properties['og:url']          = 'http://' . SERVER_NAME . '/auction/' . $auction_id;
        $meta_properties['og:image']        = 'http://' . SERVER_NAME . '/themes/red/images/' . $auctionDetails['campaign_custom_ref'] . '_sq.jpg';
        $meta_properties['og:description']  = 'Bid on tickets to see the world\'s greatest artists. Great Music Saves Lives.';
        $meta_properties['og:site_name']    = '(RED) ROWS';
        $meta_properties['fb:admins']       = '199710142';

        $this->view->meta_property = $meta_properties;

        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                'category'   => 'Auction:overview',
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $this->auth_userid,
                    'user'      => $this->auth_username,
                    'params'    => $this->_request->getParams(),
                )),
                // Leave off message because Logger knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Auction Item display information");
    }

    public function confirmBidAction()
    {
        $userService = new Auction_Service_User();
        // If we capture any data the it overrides anything else.
        $isStoredData = false;
        $form_data = array();

        $bidParam = $this->fetchTempStoreData(self::NS_BID_DATA);

        if (null != $bidParam) {
           if (array_key_exists('bid_form', $bidParam)) {
               $form_data = $bidParam['bid_form'];
               $isStoredData = count($form_data);
           }
        }

        $campaign_id   = $this->_request->getParam('campaign_id',   $isStoredData ? $form_data['campaign_id'] : null);
        $item_id       = $this->_request->getParam('item_id',       $isStoredData ? $form_data['item_id'] : null);
        $auction_id    = $this->_request->getParam('auction_id',    $isStoredData ? $form_data['auction_id'] : null);
        $bid_amount    = $this->_request->getParam('bid_amount',    $isStoredData ? $form_data['bid_amount'] : null);
        $place_bid_now = $this->_request->getParam('place_bid_now', $isStoredData ? $form_data['place_bid_now'] : false);

        // For now we are treating all auctions as standard.
        // @todo put in the code to handle different types of auction.
        $auctionItemService = new Auction_Service_AuctionItem();
        /** @var Cs_Data_AuctionItem $auctionItem **/
        $auctionItem = $auctionItemService->getItemById(
                $item_id,
                Zend_Registry::get('client_id')
            );
        $this->view->auctionItem = $auctionItem;

        //Min / Max bid
        $max_bid = $auctionItem->calculateMaxBid($auctionItem->getCurrentBid());
        $min_bid = $auctionItem->getNextMinimumBid();

        // Now we show the confirm bid page
        // What we show will depend on the type of auction.
        // Standard auctions will simply show the bid box.

        // There's a lot going on here so confirmBidForm has its own template
        $validate_min = new Zend_Validate_GreaterThan(array(
                'min' => $min_bid
            ));
        $validate_min->setMessage(
            'Please enter a value more than '.$min_bid,
            Zend_Validate_GreaterThan::NOT_GREATER
        );
        $validate_max = new Zend_Validate_LessThan(array(
                'max' => $max_bid
            ));
        $validate_max->setMessage(
            'Please enter a value less than '.$max_bid,
            Zend_Validate_LessThan::NOT_LESS
        );

        $validators = array($validate_min, $validate_max);

        $confirmBidForm = new Auction_Form_ConfirmBid(array(
            'auction_id'   => $auction_id,
            'campaign_id'  => $campaign_id,
            'item_id'      => $item_id,
            'bid_amount'   => $bid_amount,
            'user_id'      => $this->auth_userid,
            'auction_item_type' => $auctionItem->getTypeId(),
            'maximum_bid'       => $max_bid,
            'minimum_bid'       => $min_bid,
            'bid_amount_validator' => $validators
        ));

        $confirmBidForm->assignDataToViewscript('highest_bidder_username', $auctionItem->getHighestBidderUsername());
        $confirmBidForm->assignDataToViewscript('current_bid', $auctionItem->getCurrentBid());
        $confirmBidForm->assignDataToViewscript('currency_symbol', $auctionItem->getCurrencySymbol());
        $confirmBidForm->assignDataToViewscript('currency_iso', $auctionItem->getCurrencyDescription());
        $confirmBidForm->assignDataToViewscript('next_minimum_bid', $auctionItem->getNextMinimumBid());
        $confirmBidForm->assignDataToViewscript('campaign_id', $auctionItem->getCampaignId());
        $confirmBidForm->assignDataToViewscript('auction_id', $auctionItem->getAuctionId());
        $confirmBidForm->assignDataToViewscript('item_id', $auctionItem->getItemId());

        // Logic goes here for the post submit actions..
        // Information on what will happen next
        // When the data has been posted back here we will then decide on the routing

        if ($this->_request->isPost()) {

            $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();

            // This is where we will be checking for being logged in,
            // pre-authed and then valid bid.
            if ($confirmBidForm->isValid($this->_request->getParams())) {

                // Need to go back where we came from (store here for the next controller/action to use)
                $sessionNavData = new Zend_Session_Namespace(self::NS_NAVIGATION);
                $sessionNavData->controller = $this->_request->getControllerName();
                $sessionNavData->action     = $this->_request->getActionName();
                $sessionNavData->params     = array(
                    'campaign_id' => $campaign_id,
                    'auction_id'  => $auction_id,
                    'item_id'     => $item_id,
                    'form_params' => $confirmBidForm->getValues()
                );
                $sessionNavData->return_uri = $uri;

                // We have decisions to make here so...
                // store the data in session as we may be redirected
                // Store data as required per choice
                $values = $confirmBidForm->getValues();

                // These params are sent with the gotoSimple request.
                // as restful parameters
                $params = array(
                    'campaign_id' => $campaign_id,
                    'auction_id'  => $auction_id,
                    'item_id'     => $item_id
                );

                if (! $this->is_loggedin) {

                    $values['force_login'] = true;

                    $session = $this->saveTempStoreData(
                        self::NS_BID_DATA,
                        $values,
                        null,
                        600
                    );

                    // Confirm the bid
                    $this->setFlashMessage(
                        "You must be logged in to place a bid. Please register if necessary."
                    );

                    $this->_helper->redirector->gotoSimple(
                        'login',
                        'user',
                        null,
                        $params
                    );
                }

                // Now we are logged in..
                // Check that preauth has been done

                $userService = new Auction_Service_User();

                if (! $userService->hasPreAuthed($this->auth_userid)) {

                    $values['force_preauth'] = true;

                    $session = $this->saveTempStoreData(
                        self::NS_BID_DATA,
                        $values,
                        null,
                        600
                    );

                    // Confirm the bid
                    $this->setFlashMessage(
                        "Card pre-authorisation is required before bids can be accepted"
                    );

                    $this->_helper->redirector->gotoSimple(
                        'pre-auth',
                        'account',
                        null,
                        $params
                    );
                }

                // We actually need to show the form one more time...
                // This will depend on the place_bid_now parameter

                if ($place_bid_now) {
                    // Then if we get through all that...
                    // Confirm the bid
                    $this->setFlashMessage(
                        "Thank you. Your bid has been accepted and submitted."
                    );

                    $sessionNavData->return_uri = null;

                    // We will now add the bid to the bid_queue
                    // @todo we need to prevent resubmission of the same bid.
                    $bidData = array(
                        'item_id'             => $item_id,
                        'bid_amount'          => $bid_amount,
                        'transaction_id'      => null,
                        'auction_id'          => $auction_id,
                        'campaign_id'         => $campaign_id,
                        'user_ip'             => $_SERVER['REMOTE_ADDR'],
                        'user_agent'          => $_SERVER['HTTP_USER_AGENT'],
                    );

                    // Load our new bid into the queue
                    $task = new Cs_Task_EnQueueBid(
                        $auction_id,
                        $this->auth_userid,
                        $bidData,
                        Zend_Registry::get('client_id')
                    );
                    $task->run();

                    $this->_helper->redirector->gotoSimple(
                        'bid-accepted',
                        'auction',
                        null,
                        $params
                    );
                    exit;
                }
            }

        }

        $confirmBidForm->getSubForm('bid_form')
                ->getElement('place_bid_now')
                ->setErrorMessages(array())
                ->setValue(1);

        // If the form is invalid or we have just entered this page we show this
        $this->view->confirmBidForm = $confirmBidForm;

    }

    public function bidAcceptedAction()
    {
        //Get Auction Item ID
        $item_id = $this->_request->getParam('item_id', null);

        $auctionItemService = new Auction_Service_AuctionItem();
        $auctionItem = $auctionItemService->getItemById(
                $item_id,
                Zend_Registry::get('client_id')
            );
        $this->view->auctionItem = $auctionItem;

        $bidService = new Auction_Service_Bid();
        $this->view->lastBid = $bidService->retrieveLastBid($this->auth_userid, $item_id);

        $this->view->item_id = $item_id;
    }
}

