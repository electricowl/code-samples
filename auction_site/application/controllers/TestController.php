<?php
// Note!!!
// This is not the way to do this in ZF.
// You need to add the headers to the Request Object (check first where it
// needs to happen)
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

ini_set('display_errors', 1);
error_reporting(E_ALL);
/**
 * Description of TestController
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class TestController extends Cs_Controller_AbstractActionController
{

    public function indexAction()
    {
        //$form = new Auction_Form_ConfirmBid();
        //echo $form;

        $b = new Cs_Data_PaymentInformation();
        $b->setAddressLine1("86a Lordship Park");
        $b->type = "billing";
        echo $b->addressLine1;
        echo "<br>";
        $b->setUserId("tree");
        echo $b->getUserId();

        echo "<br>";
        $data = array(
            'first_name'     => "Ian",
            'last_name'      => "Lewis",
            'address_line_1' => "86a Lordship Park",
            'address_line_2' => "Stoke Newington",
            'town_city'      => "London",
            'county_state'   => "Gtr London",
            'post_zip_code'  => "N16 5UA",
            'country'        => "UK",
            'country_id'     => "1",

            'card_number'    => "4111111111111111",
            'end_date'       => new DateTime("20121101"),
            'cvn_number'     => "123",
            'start_date'     => new DateTime("20101001"),

        );
        //
        $b->loadArrayData($data);

        echo "<pre>";
        //var_dump($b);
        echo "</pre>";
        //$cardService = new Auction_Service_CardTypes();
        //var_dump($cardService->getCardList());
        exit;
    }

    public function goElsewhereAction()
    {
        $this->_redirector->gotoSimple('some-other',
                                       'test',
                                        null,
                                        array(
                                            'some' => 'params',
                                            'more' => 'zz9zzA'
                                        ));
    }

    public function someOtherAction()
    {
        var_dump(
                "Some other action..",
                $_SERVER['REQUEST_URI']);

        exit;
    }

    public function userDataAction()
    {
        $user = new Auction_Service_User();
        $userData = $user->getUserData($this->auth_userid);

        echo "<pre>";
        var_dump($this->auth_userid, $userData);

        exit;
    }

    public function fetchClientByThemeAction()
    {
        echo "Using arcadefire<br>";

        $db = new Auction_Model_DataGateway_Clients();
        $result = $db->fetchClientByThemeId('arcadefire');

        var_dump($result->client_id, $result->client_theme_id);
        var_dump($result);

        $result = $db->fetchClientByThemeId('red');

        var_dump($result->client_id, $result->client_theme_id);
        var_dump($result);

        $result = $db->fetchClientByThemeId('grackle');

        var_dump($result->client_id, $result->client_theme_id);
        var_dump($result);

        exit;
    }

    public function fetchClientDataAction()
    {
        $s = new Auction_Service_Client();

        $result = $s->fetchClientDataByClientId(1);

        var_dump($result);

        $result = $s->fetchClientDataByClientId(2);

        var_dump($result);

        $result = $s->fetchClientDataByClientId(3);

        var_dump($result);

        exit;
    }

    public function itemAction()
    {
        $item_id = $this->_request->getParam('item_id', 1);
        $auctionItemService = new Auction_Service_AuctionItem();
        /** @var Cs_Data_AuctionItem Cs_Data_AuctionItem **/
        $auctionItem = $auctionItemService->getItemById(
                (int) $item_id,
                Zend_Registry::get('client_id')
            );

        echo "<pre>";
        var_dump($auctionItem->getBidHistory(),
                '========== Highest bid ======', $auctionItem->getHighestBid(),
                '========== Current bid ======', $auctionItem->getCurrentBid()
            );
        echo "<br><br>";
        var_dump("=========== Auction Item ========",
                $auctionItem->toArray());

        echo "<br><br>";
        $diff = $auctionItem->getTimeRemaining();
        var_dump($diff);
        echo "</pre>";

        exit;
    }

    public function tzTestAction()
    {
        $tzId = 1;
        $t = new Auction_Model_DataGateway_AuctionItemTypesLu();
        $a = $t->fetchById($tzId);

        echo "<pre>";
        var_dump($a);
        echo "</pre>";

        exit;

    }

    public function addressAction()
    {
        $user_id = 3;

        $addressService = new Auction_Service_User();
        $shippingAddress = $addressService->fetchShippingAddressByUserId(
                    $user_id
                );

        $billingAddress = $addressService->fetchBillingAddressByUserId(
                    $user_id
                );

        echo "<pre>";
        var_dump($billingAddress, $shippingAddress);

        exit;
    }

    public function paypalSandboxAction()
    {
        echo "<pre>";
        $data = array(
            'first_name'      => 'Ian',
            'last_name'       => 'Lewis',

            'address_line_1'  => '86a Lordship Park',
            'address_line_2'  => 'Stoke Newington',
            'town_city'       => 'London',
            'county_state'    => 'Gtr London',
            'post_zip_code'   => 'N16 5UA',
            'country'         => 'United Kingdom',
            'country_id'      => 1,

            'is_default'      => 1,
            'is_active'       => 1,

            'bid_amount'      => '3.99',
            'currency'        => 'GBP',
            'card_type'       => 'Visa',

            'email'             => 'ian.lewis@electricowl.co.uk',
            'client_ip_address' => '86.188.247.250',

        );

        $paymentInformation = new Cs_Data_PaymentInformation();
        $paymentInformation->loadArrayData($data);
        // Visa card information
        $paymentInformation->setCardData(
                '4770487891656273',
                '2018-01-30',
                '123'
            );

        $transactionCurrency = "GBP";

        $gateway = new Cs_PaymentGateway_PayPalDirect();
        $result  = $gateway->runAuthorisation($paymentInformation, $transactionCurrency);

        var_dump($result);

        exit;
    }

    /**
     * test adding email to queue, displaying email, and deleting item from queue
     *
     */
    public function viewEmailHtmlAction()
    {

        $data = array(
           array(
                'template'          => "auction-closed-bid-too-late",
                'recipient_user_id' => 6,
                'item_id'           => 4,
            ),
           array(
                'template'          => "reset-password",
                'recipient_user_id' => 6,
                'recipient_password' => 'the_password',
                'recipient_username' => 'user name',
            ),
           array(
                'template'          => "auction-ended",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "auction-winner",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "bid-outbid",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "highest-bidder",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "registration-complete",
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "transaction-confirmation",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "transaction-failure",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
        );

        foreach($data as $email) {
            // Load our Email messages into the queue
            $emailTask = new Cs_Task_EnQueueEmail($email);
            $emailTask->run();
        }


        // Output Email to browser
        $queue = new Auction_Service_QueueManager(
            Auction_Service_QueueManager::QUEUE_NAME_EMAIL
        );

        $queueData = $queue->receive(10);

        foreach ($queueData as $message) {

            $messageArray = $message->toArray();

            $emailData = json_decode($messageArray['body'], true);

            // Output email headers
            echo '<hr>';
            echo '<p><b>Subject: '. $emailData['subject'] .'</b></p>';
            echo '<p>To: '. $emailData['recipient_name'] . ': ' . $emailData['recipient_email'] .'</p>';
            echo '<p>From: '. $emailData['sender_name'] . ': ' . $emailData['sender_email'] .'</p>';
            echo '<p>Reply to: '. $emailData['reply_to_name'] . ': ' . $emailData['reply_to_email'] .'</p>';

            $scriptBasePath = implode(DIRECTORY_SEPARATOR, array(
                APPLICATION_PATH,
                'views',
            ));

            $layout = new Zend_layout();
            $layout->setLayoutPath($scriptBasePath);
            $layout->setLayout('layouts/email')->disableLayout();

            $view = new Zend_View();
            $view->setBasePath($scriptBasePath);

            $template = $emailData['template'];
            $view->assign('data', $emailData);

            $layout->content = $view->render("emails/{$template}.phtml");
            $layout->site_url = $emailData['site_url'];
            $layout->subject = $emailData['subject'];
            echo $layout->render();

            $queue->deleteMessage($message);
        }

        exit;

    }


    /**
     * test adding email to queue, and sending email
     *
     */
    public function sendEmailQueueAction()
    {

        $data = array(
           array(
                'template'          => "auction-closed-bid-too-late",
                'recipient_user_id' => 6,
                'item_id'           => 4,
           ),
           array(
                'template'          => "reset-password",
                'recipient_user_id' => 6,
                'recipient_password' => 'the_password',
                'recipient_username' => 'user name',
            ),
           array(
                'template'          => "auction-ended",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "auction-winner",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "bid-outbid",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "highest-bidder",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "registration-complete",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "transaction-confirmation",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
            array(
                'template'          => "transaction-failure",
                'item_id'           => 4,
                'recipient_user_id' => 6,
            ),
        );

        foreach($data as $email) {
            // Load our Email messages into the queue
            $emailTask = new Cs_Task_EnQueueEmail($email);
            $emailTask->run();
        }


        // Send the emails
        $queue = new Auction_Service_QueueManager(
            Auction_Service_QueueManager::QUEUE_NAME_EMAIL
        );

        $options = array(
            Auction_Service_QueueManager::CONFIG_WORKER_RECEIVE => 11
        );

        $queue->worker($options);

        exit;

    }

    public function loadEmailQueueAction()
    {
        $data = array(
            // Template data
            'auction_name'    => 'Some Auction',
            'auction_item'    => 'Auction lot',

            // Email
            'template_path'   => 'emails',
            'template'        => 'auction-winner',

            'sender_name'     => "Ian Lewis",
            'sender_email'    => "ian.lewis@crowdsurge.com",
            'recipient_name'  => "Ian Lewis",
            'recipient_email' => "ian.lewis@crowdsurge.com",
            'reply_to_name'   => "Ian Lewis",
            'reply_to_email'  => "ian.lewis@crowdsurge.com",

            'subject'    => "[DEBUGGING] This is a test " . time(),
            'body'       => "Some template from somewhere" . time(),

        );

        $queue = new Auction_Service_QueueManager(
                Auction_Service_QueueManager::QUEUE_NAME_EMAIL
            );


        if (isset($_GET['noadd'])) {

        }
        else {
            // Add ten entries to the queue
            for ($i=0; $i<7; $i++) {
                $queue->addItemToQueue($data);
            }
        }
/**
        // Fetch the queue data and spit it out onto the page.
        if (isset($_GET['send'])) {
            $queue->worker(array(
                'workerReceive' => 10
            ));

        }
        else {
            echo "<pre>";
            $queueData = $queue->receive(5);

            var_dump("First block", $queueData->count());
            foreach ($queueData as $message) {
                var_dump($message->toArray());
                $queue->deleteMessage($message);
            }

            $queueData = $queue->receive(5);

            var_dump("second block", $queueData->count());
            foreach ($queueData as $message) {
                var_dump($message->toArray());
                $queue->deleteMessage($message);
            }
        }

**/
        exit;


    }

    public function populateAllQueuesAction()
    {

        $testMessage = array(
            'timestamp' => time(),
            'subject'   => "Test Message",
            'body'      => "Message Body"
        );

        $queues = array(
            Auction_Service_QueueManager::QUEUE_NAME_GENERIC,
            Auction_Service_QueueManager::QUEUE_NAME_BID,
            Auction_Service_QueueManager::QUEUE_NAME_EMAIL,
            Auction_Service_QueueManager::QUEUE_NAME_USER,
        );

        foreach ($queues as $queueName) {
            $queue = new Auction_Service_QueueManager($queueName);

            for ($m=1; $m <= 3; $m++) {
                $testMessage['ts'] = time();
                $item_data = $testMessage;
                $queue->addItemToQueue($item_data);
            }
            echo "<h1>$queueName</h1>";
            echo "<pre>";
            var_dump(
                $queue->receive($m)
            );
            echo "</pre>";
        }

        exit;
    }

    public function userMessageAction()
    {
        $start = rand(1, 10);
        $end   = rand(11, 20);

        $user_id_collection = range($start, $end);

        var_dump($user_id_collection);


        foreach ($user_id_collection as $id) {
            $queueName = "user_$id";
            echo "Queueing messages for id $user_id <br />";

            $queue = new Auction_Service_QueueManager(
                Auction_Service_QueueManager::QUEUE_NAME_USER,
                $queueName
            );
            for ($m=1; $m <= 3; $m++) {
                $testMessage = array(
                    'message_id' => "id_{$id}_msg_{$m}",
                    'timestamp' => time(),
                    'subject'   => "Test Message",
                    'body'      => "Message Body"
                );
                $item_data = $testMessage;
                $queue->addItemToQueue($item_data);
            }

            echo "<h1>$queueName</h1>";
            echo "<pre>";
            var_dump(
                $queue->receive($m)
            );
            echo "</pre>";
        }



        exit;
    }

    public function processBidQueueAction()
    {

        $debug = array();

        // First we dequeue from the bid queue then pass the data to the
        // task which we then run()
        $queue = new Auction_Service_QueueManager(
                Auction_Service_QueueManager::QUEUE_NAME_BID
            );
        $messages = $queue->receive();

        $debug[] = "Messages DeQueued: " . $messages->count();
        $debug[] = $messages;

        // Now we will process the bid..
        // @ todo looks like passing the processBid task the messages
        // will be the best way of working but first we try this...

        foreach ($messages as $n => $message) {
            $m     = $message->toArray();
            $debug[] = $m;
            $data  = json_decode($message->body);
            $debug[] = $data;

            $debug[] = "instantiate Task";
            $processTask = new Cs_Task_ProcessBid(
                $data->auctionId,
                $data->userId,
                $data->bidData
            );
            $debug[] = $processTask->status();

            $processTask->run();
            $debug[] = $processTask->status();

            $queue->deleteMessage($message);
            $debug[] = "Message Deleted";

            $processTask = null;
            $debug[] = "loop again";
        }

        echo "<pre>";

        var_dump($debug);

        exit;

    }

    public function multiBidAdderAction()
    {
        // customise as you need it
        $campaign_id = 5;
        $auction_id = 2;
        $item_id    = 6;
        $user_id    = 6; // tigger in my sandbox
        $increment  = 1.00;
        $starting   = 126.87;
        $loop       = 10;

        $bid_amount = $starting;

        for ($b=1; $b<=$loop; $b++) {

            $bidData = array(
                    'item_id'             => $item_id,
                    'bid_amount'          => $bid_amount,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => $_SERVER['REMOTE_ADDR'],
                    'user_agent'          => $_SERVER['HTTP_USER_AGENT'],
                );

                // Load our new bid into the queue
                $task = new Cs_Task_EnQueueBid(
                    $auction_id,
                    $user_id,
                    $bidData,
                    Zend_Registry::get('client_id')
                );
                $task->run();

                $task = null;

                $bid_amount += $increment;

        }

        echo "$loop Bids added to Queue";
        exit;

    }

    public function checkJsonEndpointAction()
    {
        // See the viewscript for this as it is all in AJAX.


    }

    public function getAuctionItemAction()
    {
        $id = $this->_getParam('id');

        $service = new Auction_Service_AuctionItem();

        $item = $service->getItemById(
                $id,
                Zend_Registry::get('client_id')
            );

        echo "<pre>";
        var_dump($item);
        exit;
    }

    public function timeChecksAction()
    {
        $tm = time();
        $format = 'Y-m-d H:i:sP';

        $t = "2013-05-30 11:22:32";

        $dt = new DateTime($t, new DateTimeZone("Europe/London"));

        $dtz = new DateTime($t, new DateTimeZone('Pacific/Nauru'));

        $dty = new DateTime($t, new DateTimeZone('America/Argentina/Buenos_Aires'));


        echo "<pre>";
        var_dump(
                "Europe/London", $dt->format($format),
                'Pacific/Nauru', $dtz->format($format),
                'America/Argentina/Buenos_Aires', $dty->format($format),
                "---"
                );

        var_dump(
                gmdate($format, $dt->getTimestamp()),
                gmdate($format, $dtz->getTimestamp()),
                gmdate($format, $dty->getTimestamp())
                );


        $dt1 = new DateTime(gmdate($format, $dt->getTimestamp()));
        $dtz1 = new DateTime(gmdate($format, $dtz->getTimestamp()));

        var_dump("***", $dt1, $dtz1,
                $dt1 > $dtz1,
                $dt1 < $dtz1
                );


        exit;
    }

    public function playTestBidsAction()
    {
        $campaign_id = 1;
        $auction_id  = 9;
        $item_id     = 11;

//        $bidData = array(
//            'item_id'             => $item_id,
//            'bid_amount'          => $bid_amount,
//            'transaction_id'      => null,
//            'auction_id'          => $auction_id,
//            'campaign_id'         => $campaign_id,
//            'user_ip'             => '127.0.0.1',
//            'user_agent'          => "Not FireFox",
//        );

        $bidsForQueue = array(
            1 => array(
              'userId' => 1,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 1.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),

            2 => array(
              'userId' => 2,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 10.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
            3 => array(
              'userId' => 1,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 5.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
            4 => array(
              'userId' => 3,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 7.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
            5 => array(
              'userId' => 1,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 10.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
            6 => array(
              'userId' => 3,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 15.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
            7 => array(
              'userId' => 2,
              'auctionId' => $auction_id,
              'bidData' => array(
                    'item_id'             => $item_id,
                    'bid_amount'          => 100.00,
                    'transaction_id'      => null,
                    'auction_id'          => $auction_id,
                    'campaign_id'         => $campaign_id,
                    'user_ip'             => '127.0.0.1',
                    'user_agent'          => "Not FireFox",
                )
              ),
          );

        // Load the bid queue..
        foreach ($bidsForQueue as $seq => $aBid) {

            // Load our new bid into the queue
                $task = new Cs_Task_EnQueueBid(
                    $aBid['auctionId'],
                    $aBid['userId'],
                    $aBid['bidData'],
                    Zend_Registry::get('client_id')
                );
                $task->run();

                $task = null;
        }

        // Now we need to run the bid processor...
        $debug = array();
        echo "<pre>";

        try {
            // First we dequeue from the bid queue then pass the data to the
            // task which we then run()
            $queue = new Auction_Service_QueueManager(
                    Auction_Service_QueueManager::QUEUE_NAME_BID
                );
            $messages = $queue->receive(10);

            $debug[] = "Messages DeQueued: " . $messages->count();
            //$debug[] = $messages;

            // Now we will process the bid..
            // @ todo looks like passing the processBid task the messages
            // will be the best way of working but first we try this...

            foreach ($messages as $n => $message) {
                $m     = $message->toArray();
                $debug[] = $m;
                $data  = json_decode($message->body);
                $debug[] = $data;

                $debug[] = "instantiate Task";
                $processTask = new Cs_Task_ProcessBid(
                    $data->auctionId,
                    $data->userId,
                    $data->bidData
                );
                $debug[] = $processTask->status();

                $processTask->run();
                $debug[] = $processTask->status();

                $queue->deleteMessage($message);
                $debug[] = "Message Deleted";

                $processTask = null;
                $debug[] = "loop again";
            }

            var_dump($debug);

        }
        catch(Exception $e) {
            var_dump($debug);
        }

        exit;



    }


}


