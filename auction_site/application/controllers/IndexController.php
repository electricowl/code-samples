<?php

class IndexController extends Cs_Controller_AbstractActionController
{

    public function init()
    {
        parent::init();

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext('get-server-time', 'json')
                      ->initContext();
    }

    public function indexAction()
    {
        // We'll show all the live campaigns here
        $serviceCampaigns = new Auction_Service_Campaign();
        $campaigns = $serviceCampaigns->getAllActiveCampaigns(
                Zend_Registry::get('client_id')
            );

        $this->view->campaigns = $campaigns;

        //Meta Properties
        $meta_properties = array();
        $meta_properties['og:title']        = '(RED) ROWS';
        $meta_properties['og:type']         = 'website';
        $meta_properties['og:url']          = 'http://'.$_SERVER['SERVER_NAME'];
        $meta_properties['og:image']        = 'http://'.$_SERVER['SERVER_NAME'].'/images/redrows.jpg';
        $meta_properties['og:description']  = 'Bid on tickets to see the world\'s greatest artists. Great Music Saves Lives.';
        $meta_properties['og:site_name']    = '(RED) ROWS';
        $meta_properties['fb:admins']       = '199710142';

        $this->view->meta_property = $meta_properties;
    }

    /**
     * Receive the registration data, add user and return to the required page
     * (need some session storage to keep state)
     */
    public function registerAction()
    {
        $registrationForm = new Auction_Form_Registration();
        $registrationForm->setElementsBelongTo('registration_form');
        $registrationForm->removeElement('Register');

        $billingForm = new Auction_Form_BillingAddress();
        $billingForm->removeElement('Save');
        $billingForm->setDecorators($billingForm->getFormDecoratorsTreatAsSubForm());

        $shippingForm = new Auction_Form_ShippingAddress();
        $shippingForm->removeElement('Save');
        $shippingForm->setDecorators($shippingForm->getFormDecoratorsTreatAsSubForm());;

        // We use the concept of 'sub-forms' to make use of the additional
        // forms needed. This isn't necessarily the most elegant solution but
        // each form can be dealt with discretely.
        $registrationForm->addSubForm($billingForm, 'billing_sub_form');
        $registrationForm->addSubForm($shippingForm, 'shipping_sub_form');

        $registrationForm->addSubmit('Register');

        if ($this->_request->isPost() && $registrationForm->isValid($this->_getAllParams())) {
            // Process the forms here...
            // The validation earlier should guarantee we can simply add the data
            // to the database

            $shippingValues = $registrationForm->getSubForm('shipping_sub_form')->getValues();
            $registrationForm->removeSubForm('shipping');

            $billingValues = $registrationForm->getSubForm('billing_sub_form')->getValues();
            $registrationForm->removeSubForm('billing');

            $registrationValues = $registrationForm->getValues();
            //var_dump($registrationForm->getValues(), $billingValues, $shippingValues);
            $service = new Auction_Service_User();

            $userId = $service->addUserWithAddresses(
                $registrationValues['registration_form'],
                $billingValues['billing'],
                $shippingValues['shipping']
            );

            // Need to go back where we came from
            // @todo This doesn't look complete Is this method used?
            $this->_redirect();
        }


        $this->view->registrationForm = $registrationForm;
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        // Kill Cookie
        $sessionName = Zend_Registry::get('config')->resources->session->name;
        if (isset($_COOKIE[$sessionName])) {
            Zend_Session::destroy();
            $_COOKIE[$sessionName] = null;
        }
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                'category'   => 'Logout:success',
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user'      => $this->auth_username,
                )),
                // Leave off message because Logger knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Success: User Logged Out");

        $this->_helper->redirector->gotoSimple('index', 'index', null);
    }

    public function getServerTimeAction()
    {
        $this->view->clearVars();
        $this->view->time = time();

    }

    public function maintenanceAction()
    {

    }

    public function holdingPageAction()
    {
        $this->noLayout();

//        $uri = $this->getRequest()->getRequestUri();
//        //var_dump($uri, $uri2);
//        $messages = $this->getFlashMessage();
//
//        if (strcmp('/index/holding-page', $uri) !== 0) {
//            var_dump($uri, $messages); exit;
//            // We will use this to pass a message to the page..
//            $this->setFlashMessage("REDIRECT");
//            $this->_redirect('/index/holding-page');
//        }
//
//        if (count($messages)) {
//            var_dump($messages);
//            exit;
//        }


    }
}

