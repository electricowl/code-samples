<?php
/**
 * Description of AccountController (AccountController.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, CER Ltd
 */

class AccountController extends Cs_Controller_AbstractActionController
{
    public function indexAction()
    {
        $this->_redirector->gotoSimple('payment',
                                       'account',
                                        null);
    }

    public function preAuthAction()
    {
        if (! $this->is_loggedin) {
            $this->_redirector->gotoSimple('index',
                                       'index',
                                        null);
            exit;
        }

        /**
         * Set up the form
         */
        $form = new Auction_Form_PaymentDetails();

        if ($this->_request->isPost()) {

            if ($form->isValid($this->_getAllParams())) {

                $user = new Auction_Service_User();
                $userData = $user->getUserData($this->auth_userid);

                $paymentData = $form->getValues();
                $paymentData['user_ip']    = $_SERVER['REMOTE_ADDR'];
                $paymentData['user_email'] = $userData['email'];
                $paymentData['user_id']    = $userData['user_id'];
                $paymentData['our_reference'] = "preauth_user_{$this->auth_username}_{$this->auth_userid}_t" . time();

                // Start account management service
                $service = new Auction_Service_AccountManagement();

                //Default Currency
                $bill_country = $paymentData['country_id'];
                $currency = $service->getDefaultCurrencyForCountryId($bill_country);
                $paymentData['currency'] = $currency;

                // Process the transaction
                $transactionResult = $service->cardPreAuth(
                        $paymentData
                    );

                if ($transactionResult) {

                    $user->updateUserValidation($this->auth_userid);

                    // If the preauth is valid then we need to go to the homepage
                    $this->setFlashMessage(
                            "Thank you. Your card has successfully authorised a transaction. You are clear to bid."
                        );

                    // Redirect back to the Auctions page
                    // @todo remember the previous page.
                    $params = array();
                    $controller = 'auction';
                    $action     = 'index';

                    //
                    $this->setFlashMessage('You are now logged in.');

                    // Need to go back where we came from
                    $session = new Zend_Session_Namespace(self::NS_NAVIGATION);
                    if (isset($session->controller)) {
                        $controller = $session->controller;
                        $action     = $session->action;
                        $params     = $session->params;

                        $form_params = $params['form_params'];
                        unset($params['form_params']);
                        $session->setExpirationHops(1);
                    }

                    $this->_helper->redirector->gotoSimple(
                        $action,
                        $controller,
                        null,
                        $form_params
                    );
                    return;

                }

                $this->view->flashMessage = array(
                    "There was a problem authorising the card you provided"
                );
            }
        }

        $this->view->form = $form;
    }

    public function settlementAction()
    {
        if (! $this->is_loggedin) {
            $this->_redirector->gotoSimple('index',
                                       'index',
                                        null
                    );
            exit;
        }

        // Do We list the user's unpaid items here then get them to click which
        // ones to pay.


        $form = new Auction_Form_PaymentDetails();
        $form->setAlternateViewScript('form_viewscripts/settlement.phtml');

        if ($this->_request->isPost()) {

            if ($form->isValid($this->_getAllParams())) {

                // Process the transaction
                // Hand off to a service method
                $service = new Auction_Service_AccountManagement();
                $transactionResult = $service->cardPayment(
                        $form->getValues()
                    );

                if ($transactionResult) {
                    // If the preauth is valid then we need to go to the homepage
                    $this->setFlashMessage(
                            "You have successfully paid for your auction win"
                        );
                    $this->_redirect(
                        $this->view->getBaseUrl()
                    );

                }

                $this->view->flashMessage = array(
                    "There was a problem authorising the card you provided"
                );
            }
        }

        $this->view->form = $form;
    }
}

