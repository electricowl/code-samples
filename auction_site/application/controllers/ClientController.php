<?php
/**
 * Description of ClientController (ClientController.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class ClientController extends Cs_Controller_AbstractActionController
{
    public function init()
    {
        // We only allow access to clients (or CS Admin here).
        $bounce = true;
        $session = new Zend_Session_Namespace('cs_acl');

        if (isset($session->role)) {
            $role = $session->role;
            if (get_class($role) == 'Cs_Role_Client') {
                $bounce = false;
            }
        }
        else {
            if ($this->getRequest()->getActionName() == 'login') {
                $bounce = false;
            }
        }

        if ($bounce) {
            $this->_helper->redirector->gotoSimple('index', 'index');
            return;
        }

        parent::init();
    }

    public function indexAction()
    {
        $this->_redirect('/client/login');
        $this->_redirector->gotoSimple('login',
                                       'client',
                                        null
                );
    }

    public function loginAction()
    {
        // This is specifically a login page for clients
        $loginForm = new Auction_Form_Login(array(
            'is_client_login' => 1
        ));

        if ($this->_request->isPost()) {
            if ($loginForm->isValid($this->_getAllParams())) {

                $db = Zend_Db_Table::getDefaultAdapter();

                // Todo this will need to be tweaked for MD5 password
                $authAdapter = new Zend_Auth_Adapter_DbTable(
                        $db,
                        'client_staff',
                        'username',
                        'password'
                        // ,'MD5(password)'
                );

                $authAdapter->setIdentity($loginForm->getValue('username'));
                $authAdapter->setCredential($loginForm->getValue('password'));

                $auth = Zend_Auth::getInstance();
                $authResult = $auth->authenticate($authAdapter);

                if ($authResult->isValid()) {
                    // Store user details - in session

                    $user = $authAdapter->getResultRowObject();
                    $storage = $auth->getStorage();
                    $storage->write($user);

                    $session = new Zend_Session_Namespace('cs_acl');
                    $session->role = new Cs_Role_Client();
                    $session->lock();

                    Auction_Service_Logger::log(
                        Auction_Service_Logger::LOG_APPLICATION,
                        array(
                            'event_date' => new Zend_Db_Expr("NOW()"),
                            'user_id'    => $this->auth_staff_id ? $this->auth_staff_id : 0,
                            'category'   => 'Login:success',
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                            // app_method is __METHOD__
                            'app_method' => __METHOD__,
                            'metadata'   => json_encode(array(
                                'username' => $loginForm->getValue('username'),
                                'password' => $loginForm->getValue('password'),
                                'user'      => $user,
                            )),
                            // Leave off message because Logger knows about that field
                            // It is the last parameter in the log() signature
                            //'message'    => "Log Testing"
                        ),
                        "Success: Client has Logged In");


                    $this->_helper->FlashMessenger('You have logged in successfully');
                    $this->_helper->redirector->gotoSimple('manage-campaigns', 'client', null);
                    return;
                }
                else {
                    // Login failure
                    Auction_Service_Logger::log(
                        Auction_Service_Logger::LOG_APPLICATION,
                        array(
                            'event_date' => new Zend_Db_Expr("NOW()"),
                            'user_id'    => $this->auth_staff_id ? $this->auth_staff_id : 0,
                            'category'   => 'Login:failure',
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                            // app_method is __METHOD__
                            'app_method' => __METHOD__,
                            'metadata'   => json_encode(array(
                                'username' => $loginForm->getValue('username'),
                                'password' => $loginForm->getValue('password'),
                                'user'      => null,
                            )),
                            // Leave off message because Logger knows about that field
                            // It is the last parameter in the log() signature
                            //'message'    => "Log Testing"
                        ),
                        "Failure: Client LogIn Attempt");
                }
            }
        }

        $this->view->form = $loginForm;
    }

    public function manageStaffAction()
    {
        // Empty for now
    }

    private function getCrowdSurgeAuctionItems($cs_url)
    {
        $cs_json = file_get_contents($cs_url);
        if ($cs_json === false) {
            //Throw error
            throw new Exception("Failed to get response from CrowdSurge. Check query string.");
        }
        else {
            //Convert json to array
            $cs_json = json_decode($cs_json, true);
            return $cs_json['campaign'];
        }
    }

    private function buildCrowdSurgeCall($call, $query)
    {
        $key = 'test_key';
        return 'http://api.crowdsurge.com/auction/1.0/?key='.$key.'&call='.$call.'&'.$query;
    }

    public function crowdsurgeAction()
    {

        $serviceCampaign = new Auction_Service_Campaign();
        $serviceAuction = new Auction_Service_Auction();
        $serviceAuctionItem = new Auction_Service_AuctionItem();
        $serviceCurrency = new Auction_Service_Currency();

        $form = new Auction_Form_CrowdSurgeCall();
        $tmp = '';
        $campaigns = '';
        $cs_call = '';
        $cs_query = '';

        if ($this->_request->isPost()) {

            if (isset($_POST['confirm_submitted'])) {

                //Confirm form
                $cs_url = $this->buildCrowdSurgeCall($_POST['cs_call'], $_POST['cs_query']);
                $campaigns = $this->getCrowdSurgeAuctionItems($cs_url);

                if (!empty($campaigns)) {

                    if (isset($_POST['confirms']) && !empty($_POST['confirms'])) {
                        $confirms = $_POST['confirms'];

                        //Loop through campaigns
                        foreach($campaigns as $campaign) {

                            $campaign_custom_ref = $campaign['campaign_custom_ref'];
                            $campaign_name = $campaign['campaign_name'];
                            $campaign_description = $campaign['campaign_description'];

                            //Campaign ID
                            $new_campaign_id = null;
                            $campaign_row = $serviceCampaign->getCampaignByCustomRef($campaign_custom_ref);
                            if (!empty($campaign_row)) {
                                $new_campaign_id = $campaign_row['campaign_id'];
                            }

                            if (in_array('C_'.$campaign_custom_ref, $confirms)) {

                                //Campaign Values
                                $campaign_values = array(
                                    'campaign_id' => $new_campaign_id,
                                    'client_id' => $this->auth_client_id,
                                    'campaign_custom_ref' => $campaign_custom_ref,
                                    'campaign_name' => $campaign_name,
                                    'campaign_description' => $campaign_description
                                );

                                if (!empty($new_campaign_id)) {
                                    //Update Campaign
                                    $serviceCampaign->update($campaign_values);
                                    $campaigns[$campaign_custom_ref]['db_action'] = array('updated' => $new_campaign_id);
                                }
                                else {
                                    //Insert Campaign
                                    $new_campaign_id = $serviceCampaign->save($campaign_values);
                                    $campaigns[$campaign_custom_ref]['db_action'] = array('inserted' => $new_campaign_id);
                                }
                            }

                            if (isset($campaign['auction']) && !empty($campaign['auction'])) {

                                $auctions = $campaign['auction'];

                                //Loop through auctions
                                foreach($auctions as $auction) {

                                    $auction_custom_ref = $auction['auction_custom_ref'];
                                    $auction_name = $auction['auction_name'];
                                    $auction_description = $auction['auction_description'];
                                    $display_order = $auction['display_order'];

                                    //Auction ID
                                    $new_auction_id = null;
                                    $auction_row = $serviceAuction->getAuctionByCustomRef($auction_custom_ref);
                                    if (!empty($auction_row)) {
                                        $new_auction_id = $auction_row['auction_id'];
                                    }

                                    if (in_array('A_'.$auction_custom_ref, $confirms)) {

                                        //Check campaign exists
                                        if (!empty($new_campaign_id)) {

                                            //Auction Values
                                            $auction_values = array(
                                                'auction_id' => $new_auction_id,
                                                'campaign_id' => $new_campaign_id,
                                                'auction_custom_ref' => $auction_custom_ref,
                                                'auction_name' => $auction_name,
                                                'auction_description' => $auction_description,
                                                'display_order' => $display_order,
                                                'auction_status_id' => 2
                                            );

                                            if (!empty($new_auction_id)) {
                                                //Update Auction
                                                $serviceAuction->update($auction_values);
                                                $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['db_action'] = array('updated' => $new_auction_id);
                                            }
                                            else {
                                                //Insert Auction
                                                $new_auction_id = $serviceAuction->save($auction_values);
                                                $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['db_action'] = array('inserted' => $new_auction_id);
                                            }
                                        }
                                        else {
                                            throw new Exception("You must first insert the campaign, before attempting to insert the auction");
                                        }
                                    }

                                    if (isset($auction['item']) && !empty($auction['item'])) {

                                        $items = $auction['item'];

                                        //Loop through auction items
                                        foreach($items as $item) {

                                            $auction_item_custom_ref = $item['auction_item_custom_ref'];
                                            $auction_item_name = $item['auction_item_name'];
                                            $auction_item_description = $item['auction_item_description'];
                                            $start_date = $item['start_date'];
                                            $end_date = $item['end_date'];
                                            $timezone_id = $item['timezone_id'];
                                            $currency_iso = $item['currency_iso'];
                                            $min_bid_increment = $item['min_bid_increment'];
                                            $max_bid_increment = $item['max_bid_increment'];
                                            $starting_bid = $item['starting_bid'];
                                            $display_order = $item['display_order'];

                                            //Item ID
                                            $new_auction_item_id = null;
                                            $item_row = $serviceAuctionItem->getItemByCustomRef($auction_item_custom_ref);
                                            if (!empty($item_row)) {
                                                $new_auction_item_id = $item_row['auction_item_id'];
                                            }

                                            if (in_array('I_'.$auction_item_custom_ref, $confirms)) {

                                                //Check auction exists
                                                if (!empty($new_auction_id)) {

                                                    //Get Currency ID
                                                    $currency_id = $serviceCurrency->getCurrencyIdByCode($currency_iso);

                                                    //Item Values
                                                    $item_values = array(
                                                        'auction_item_id' => $new_auction_item_id,
                                                        'auction_id' => $new_auction_id,
                                                        'auction_item_custom_ref' => $auction_item_custom_ref,
                                                        'auction_item_type_id' => 1,
                                                        'auction_item_name' => $auction_item_name,
                                                        'auction_item_description' => $auction_item_description,
                                                        'start_date' => $start_date,
                                                        'end_date' => $end_date,
                                                        'timezone_id' => $timezone_id,
                                                        'currency_id' => $currency_id,
                                                        'min_bid_increment' => $min_bid_increment,
                                                        'max_bid_increment' => $max_bid_increment,
                                                        'starting_bid' => $starting_bid,
                                                        'reserve' => 0,
                                                        'display_order' => $display_order
                                                    );

                                                    if (!empty($new_auction_item_id)) {
                                                        //Update Item
                                                        $serviceAuctionItem->update($item_values);
                                                        $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['item'][$auction_item_custom_ref]['db_action'] = array('updated' => $new_auction_item_id);

                                                    }
                                                    else {
                                                        //Insert Item
                                                        $new_auction_item_id = $serviceAuctionItem->save($item_values);
                                                        $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['item'][$auction_item_custom_ref]['db_action'] = array('inserted' => $new_auction_item_id);
                                                    }
                                                }
                                                else {
                                                    throw new Exception("You must first insert the auction, before attempting to insert the auction item");

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        //Throw error
                        throw new Exception("At least one checkbox should be ticked");
                    }
                }
            }
            else {

                //Call form

                if ($form->isValid($this->_getAllParams())) {

                    //RS: The below will relocated soon....
                    $values = $form->getValues();
                    $cs_call = $values['call'];
                    $cs_query = $values['query_string'];
                    $cs_url = $this->buildCrowdSurgeCall($cs_call, $cs_query);
                    $campaigns = $this->getCrowdSurgeAuctionItems($cs_url);

                    if (!empty($campaigns)) {

                        //Loop through campaigns
                        foreach($campaigns as $campaign) {

                            $campaign_custom_ref = $campaign['campaign_custom_ref'];

                            $campaign_row = $serviceCampaign->getCampaignByCustomRef($campaign_custom_ref);
                            if (!empty($campaign_row)) {
                                $campaigns[$campaign_custom_ref]['db_status'] = 1;
                            }

                            if (isset($campaign['auction']) && !empty($campaign['auction'])) {

                                $auctions = $campaign['auction'];

                                //Loop through auctions
                                foreach($auctions as $auction) {

                                    $auction_custom_ref = $auction['auction_custom_ref'];

                                    $auction_row = $serviceAuction->getAuctionByCustomRef($auction_custom_ref);
                                    if (!empty($auction_row)) {
                                        $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['db_status'] = 1;
                                    }

                                    if (isset($auction['item']) && !empty($auction['item'])) {

                                        $items = $auction['item'];

                                        //Loop through auction items
                                        foreach($items as $item) {

                                            $auction_item_custom_ref = $item['auction_item_custom_ref'];

                                            $item_row = $serviceAuctionItem->getItemByCustomRef($auction_item_custom_ref);
                                            if (!empty($item_row)) {
                                                $campaigns[$campaign_custom_ref]['auction'][$auction_custom_ref]['item'][$auction_item_custom_ref]['db_status'] = 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->view->form = $form;
        $this->view->cs_call = $cs_call;
        $this->view->cs_query = $cs_query;
        $this->view->campaigns = $campaigns;
        $this->view->tmp = $tmp;
    }

    public function reportsAction()
    {

        $serviceClient = new Auction_Service_Client();
        $auctions_report_data = $serviceClient->fetchAuctionsReportByClientId(
                $this->auth_client_id
            );

        $this->view->auctions_report_data = $auctions_report_data;
    }

    public function manageCampaignsAction()
    {
        $edit = $this->_request->getParam('edit', false);

        $serviceCampaign = new Auction_Service_Campaign();
        $campaigns = $serviceCampaign->getAllCampaignsForClient(
                $this->auth_client_id
            );

        $form = new Auction_Form_AddEditCampaign();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                if ($edit !== false && $form->campaign_id) {
                    $serviceCampaign->update($form->getValues());
                }
                else {
                    $serviceCampaign->save($form->getValues());
                }
                $this->_helper->redirector->gotoSimple('manage-campaigns', 'client', null);
            }
        }

        if ($edit !== false) {
            $campaign = $serviceCampaign->getCampaignById($edit);
            $form->setDefaults(
                    $campaign
                );
        }

        $form->setDefault('client_id', $this->auth_client_id);

        $this->view->form      = $form;
        $this->view->campaigns = $campaigns;
    }

    public function manageAuctionsAction()
    {
        $edit = $this->_request->getParam('edit', false);

        $campaign_id = $this->_request->getParam('campaign_id');
        $serviceCampaign = new Auction_Service_Campaign();
        $campaign = $serviceCampaign->getCampaignById($campaign_id);

        $serviceAuction = new Auction_Service_Auction();
        $auctions = $serviceAuction->getAllAuctionsByCampaign($campaign_id);

        $auctionStatuses = $serviceAuction->getStatuses();

        $form = new Auction_Form_AddEditAuction();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {

                if ($edit !== false && $form->auction_id) {
                    $serviceAuction->update($form->getValues());
                }
                else {
                    $serviceAuction->save($form->getValues());
                }
                $this->getHelper('redirector')->gotoSimple(
                        'manage-auctions',
                        'client',
                        null,
                        array(
                            'campaign_id' => $campaign_id
                        )
                    );
            }

        }

        if ($edit !== false) {
            $auction = $serviceAuction->getAuctionDetailsById(
                $edit,
                Zend_Registry::get('client_id')
            );
            $form->setDefaults(
                    $auction
                );
        }

        $form->setDefault('campaign_id', $campaign_id);

        $this->view->form     = $form;
        $this->view->campaign = $campaign;
        $this->view->auctions = $auctions->toArray();
        $this->view->auctionStatus = $auctionStatuses;
    }

    public function manageAuctionItemsAction()
    {
        $edit = $this->_request->getParam('edit', false);

        $campaign_id = $this->_request->getParam('campaign_id');
        $serviceCampaign = new Auction_Service_Campaign();
        $campaign = $serviceCampaign->getCampaignById($campaign_id);

        $auction_id = $this->_request->getParam('auction_id');

        $serviceAuction = new Auction_Service_Auction();
        $auction = $serviceAuction->getAuctionDetailsById(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        $serviceAuctionItem = new Auction_Service_AuctionItem();
        $items = $serviceAuctionItem->fetchAllItemsByAuctionId(
                $auction_id,
                Zend_Registry::get('client_id')
            );

        $form = new Auction_Form_AddEditAuctionItem();

        if ($this->_request->isPost()) {

            if ($form->isValid($this->_request->getParams())) {
                // Adjust the times here
                $now = new Datetime();
                $time = $now->format("H:i:00");

                $values = $form->getValues();
                if (preg_match("/\d\d:\d\d/", $values['start_time'])) {
                    $values['start_date'] .= " " . $values['start_time'] . ":00";
                }
                else {
                    $values['start_date'] .= " " . $time;
                }
                unset($values['start_time']);

                if (preg_match("/\d\d:\d\d/", $values['end_time'])) {
                    $values['end_date'] .= " " . $values['end_time'] . ":00";
                }
                else {
                    $values['end_date'] .= " " . $time;
                }
                unset($values['end_time']);

                if ($edit !== false && $form->auction_item_id) {
                    $serviceAuctionItem->update($values);
                }
                else {
                    $serviceAuctionItem->save($values);
                }

                $this->getHelper('redirector')->gotoSimple(
                        'manage-auction-items',
                        'client',
                        null,
                        array(
                            'campaign_id' => $campaign_id,
                            'auction_id'  => $auction_id
                        )
                    );
            }

        }

        if ($edit !== false) {
            $auctionItem = $serviceAuctionItem->getItemById(
                    $edit,
                    Zend_Registry::get('client_id')
                );
            $form->setDefaults(
                    $auctionItem->getAuctionItemRowToArray()
                );
        }

        $this->view->items       = $items;
        $this->view->campaign_id = $campaign_id;
        $this->view->campaign    = $campaign;
        $this->view->auction_id  = $auction_id;
        $this->view->auction     = $auction;
        $this->view->form        = $form;
    }
}


