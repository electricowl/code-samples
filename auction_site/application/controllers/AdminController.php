<?php
/**
 * Description of AdminController (AdminController.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class AdminController extends Cs_Controller_AbstractActionController
{

    public function init()
    {
        parent::init();

    }

    public function preDispatch() {
        parent::preDispatch();

        // We only allow access to CS Admin here
        $bounce = true;
        $session = new Zend_Session_Namespace('cs_acl');

        // @todo this is probably odd logic...
        if (isset($session->role)) {
            $role = $session->role;
            if (get_class($role) == 'Cs_Role_Admin') {
                $bounce = false;
            }
        }
        else {
            if ($this->getRequest()->getActionName() == 'login') {
                $bounce = false;
            }
        }

        if ($bounce) {
            $this->_redirector->gotoSimple('index', 'index', null);
            return;
        }
    }

    public function indexAction()
    {

    }

    public function loginAction() {

        $loginForm = new Auction_Form_Login();

        if ($this->_request->isPost() && $loginForm->isValid($this->_getAllParams())) {

            $db = Zend_Db_Table::getDefaultAdapter();

            // Todo this will need to be tweaked for MD5 password
            $authAdapter = new Zend_Auth_Adapter_DbTable(
                    $db,
                    'administrators',
                    'username',
                    'password'
                    // ,'MD5(password)'
            );

            $authAdapter->setIdentity($loginForm->getValue('username'));
            $authAdapter->setCredential($loginForm->getValue('password'));

            $auth = Zend_Auth::getInstance();
            $authResult = $auth->authenticate($authAdapter);

            if ($authResult->isValid()) {
                // Store user details - in session
                $user = $authAdapter->getResultRowObject();
                $auth->getStorage()->write($user);

                $session = new Zend_Session_Namespace('cs_acl');
                $session->role = new Cs_Role_Admin();
                $session->lock();

                Auction_Service_Logger::log(
                    Auction_Service_Logger::LOG_APPLICATION,
                    array(
                        'event_date' => new Zend_Db_Expr("NOW()"),
                        'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                        'category'   => 'Login:success',
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                        // app_method is __METHOD__
                        'app_method' => __METHOD__,
                        'metadata'   => json_encode(array(
                            'username' => $loginForm->getValue('username'),
                            'password' => $loginForm->getValue('password'),
                            'user'      => $user,
                        )),
                        // Leave off message because Logger knows about that field
                        // It is the last parameter in the log() signature
                        //'message'    => "Log Testing"
                    ),
                    "Success: Administrator Logged In");


                $this->_helper->FlashMessenger('You have logged in successfully');

                $this->_redirector->gotoSimple('index',
                                       'admin',
                                        null
                        );
                return;
            }
            else {
                // Login failure
                Auction_Service_Logger::log(
                    Auction_Service_Logger::LOG_APPLICATION,
                    array(
                        'event_date' => new Zend_Db_Expr("NOW()"),
                        'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                        'category'   => 'Login:failure',
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                        // app_method is __METHOD__
                        'app_method' => __METHOD__,
                        'metadata'   => json_encode(array(
                            'username' => $loginForm->getValue('username'),
                            'password' => $loginForm->getValue('password'),
                            'user'      => null,
                        )),
                        // Leave off message because Logger knows about that field
                        // It is the last parameter in the log() signature
                        //'message'    => "Log Testing"
                    ),
                    "Failure: Administrator LogIn Attempt");
            }
        }

        $registrationForm = new Auction_Form_Registration();

        $this->view->registrationForm = $registrationForm;
        $this->view->form = $loginForm;
    }

    public function showQueuesAction()
    {
        $queueService = new Auction_Service_Queue();
        $this->view->queues = $queueService->listAllQueues();

        $this->view->default = $queueService->listQueueContents("");
        $this->view->bid     = $queueService->listQueueContents("bid_messages");
        $this->view->email   = $queueService->listQueueContents("email_messages");
        $this->view->user    = $queueService->listQueueContents("user_messages");

    }

    public function registerClientAction()
    {
        $form = new Auction_Form_ClientRegistration();

        if ($this->_request->isPost()) {

            if ($form->isValid($this->_getAllParams())) {

                $service = new Auction_Service_Client();
                $service->addClient($form->getValues());

                

                $this->_redirector->gotoSimple('index', 'admin');
            }
        }

        $this->view->form = $form;
    }
}

