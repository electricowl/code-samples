<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                $this->render('404');
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';

                $this->emailAdministrator(
                        $errors->exception,
                        $errors->request
                    );
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }

    private function emailAdministrator($exception, $request)
    {
        $mail = new Auction_Service_Email();

        // TO MATRIX - sorted by environment
        $emailTo = array(
            'ianl' => array(
                array(
                    'to' => 'ian.lewis@crowdsurge.com',
                    'name' => 'Ian Lewis'
                )
            ),
            'development' => array(
                array(
                    'to' => 'ian.lewis@crowdsurge.com',
                    'name' => 'Ian Lewis'
                ),
                array(
                    'to' => 'ryan.simms@crowdsurge.com',
                    'name' => 'Ryan Simms'
                )
            ),
            'production' => array(
                array(
                    'to' => 'ian.lewis@crowdsurge.com',
                    'name' => 'Ian Lewis'
                ),
                array(
                    'to' => 'ryan.simms@crowdsurge.com',
                    'name' => 'Ryan Simms'
                ),
                array(
                    'to' => 'ian.venner@crowdsurge.com',
                    'name' => 'Ian Venner'
                )
            )
        );

        $recipients = $emailTo[APPLICATION_ENV];

        $from     = "auction.noreply@crowdsurge.com";
        $fromName = "Auction Site";

        $subject  = "[Exception] An exception occurred on Auction and was not handled by the application";
        $data = array(
            'template' => 'application-error',
            'server'   => $_SERVER['REQUEST_URI'],
            'reply_to_email' => $from,
            'reply_to_name'  => $fromName,
            'exception'      => $exception,
            'request'        => $request,
        );

        foreach ($recipients as $r) {
            $data['recipient_name'] = $r['name'];
            $mail->sendEmailSimple(
                $r['to'],
                $r['name'],
                $from,
                $fromName,
                $subject,
                $data
            );
        }

    }


}

