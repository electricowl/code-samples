<?php
/**
 * Description of UserController (UserController.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class UserController extends Cs_Controller_AbstractActionController
{

    public function init()
    {
        parent::init();

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext('get-my-messages', 'json')
                      ->initContext();
    }

    public function indexAction()
    {

    }

    public function loginAction()
    {
        if ($this->is_loggedin) {

            $this->_redirector->gotoSimple(null,
                                       'index',
                                        null
                    );
            exit;
        }

        $formAction = $this->_request->getParam('form_action');

        $userService = new Auction_Service_User();

        // This is a dual purpose page as we are able to both register and login
        $loginForm = new Auction_Form_Login();
        $registrationForm = new Auction_Form_Registration();

        // Form has been submitted
        if ($this->_request->isPost()) {
            // The penalties of combining actions
            $reg_username = null;
            $reg_password = null;

            $allParams = $this->_getAllParams();

            if ($formAction == "registration" && $registrationForm->isValid($allParams)) {
                // Add the user details to the database
                $userId = $userService->registerUser($registrationForm->getValues());
                $this->setFlashMessage("User Id: $userId has been created.");
                // load up the login form here... we're faking the login
                $reg_username = $registrationForm->getValue('registration_username');
                $reg_password = $registrationForm->getValue('registration_password');

                //$loginForm->setDefaults(array(
                //    'username' => $reg_username,
                //    'password' => $reg_password,
                //));
                // Now force a login
                $formAction = 'login';

                $this->setFlashMessage("You have been registered.");
                // TODO SEND EMAIL
            }

            if ($formAction == "login") {

                $login_credentials = array(
                    'username' => null == $reg_username ? $allParams['username'] : $reg_username,
                    'password' => null == $reg_password ? $allParams['password'] : $reg_password,
                );

                // Deal with the login form
                if($loginForm->isValid($login_credentials)) {

                    $db = Zend_Db_Table::getDefaultAdapter();

                    $authAdapter = new Zend_Auth_Adapter_DbTable(
                            $db,
                            'users',
                            'username',
                            'password',
                            'SHA1(?)'
                    );

                    $authAdapter->setIdentity($loginForm->getValue('username'));
                    $authAdapter->setCredential($loginForm->getValue('password'));

                    $auth = Zend_Auth::getInstance();
                    $authResult = $auth->authenticate($authAdapter);

                    if ($authResult->isValid()) {

                        // Store user details - in session
                        $user = $authAdapter->getResultRowObject();
                        $auth->getStorage()->write($user);

                        $session = new Zend_Session_Namespace('cs_acl');
                        $session->role = new Cs_Role_User();
                        $session->lock();

                        $this->is_loggedin = true;

                        Auction_Service_Logger::log(
                            Auction_Service_Logger::LOG_APPLICATION,
                            array(
                                'event_date' => new Zend_Db_Expr("NOW()"),
                                'user_id'    => $user->user_id,
                                'category'   => 'Login:success',
                                'ip_address' => $_SERVER['REMOTE_ADDR'],
                                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                                // app_method is __METHOD__
                                'app_method' => __METHOD__,
                                'metadata'   => json_encode(array(
                                    'username' => $loginForm->getValue('username'),
                                    'password' => $loginForm->getValue('password'),
                                    'user'      => $user,
                                )),
                                // Leave off message because Logger knows about that field
                                // It is the last parameter in the log() signature
                                //'message'    => "Log Testing"
                            ),
                            "Success: User Logged In");

                        $userService->updateUserLoginTime($user->user_id);

                        $params = array();
                        $controller = 'auction';
                        $action     = 'index';

                        //
                        $this->setFlashMessage('You are now logged in.');

                        // Need to go back where we came from
                        $session = new Zend_Session_Namespace(self::NS_NAVIGATION);
                        if (isset($session->controller)) {
                            $controller = $session->controller;
                            $action     = $session->action;
                            $params     = $session->params;

                            $form_params = $params['form_params'];
                            unset($params['form_params']);
                            $session->setExpirationHops(1);
                        }

                        $this->_helper->redirector->gotoSimple(
                            $action,
                            $controller,
                            null,
                            $params
                        );
                        return;
                    }
                    else {
                        // Login failure
                        Auction_Service_Logger::log(
                            Auction_Service_Logger::LOG_APPLICATION,
                            array(
                                'event_date' => new Zend_Db_Expr("NOW()"),
                                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                                'category'   => 'Login:failure',
                                'ip_address' => $_SERVER['REMOTE_ADDR'],
                                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                                // app_method is __METHOD__
                                'app_method' => __METHOD__,
                                'metadata'   => json_encode(array(
                                    'username' => $loginForm->getValue('username'),
                                    'password' => $loginForm->getValue('password'),
                                    'user'      => null,
                                )),
                                // Leave off message because Logger knows about that field
                                // It is the last parameter in the log() signature
                                //'message'    => "Log Testing"
                            ),
                            "Failure: User LogIn Attempt");

                        $this->view->flashMessage = array("Your login attempt failed. Please try again");
                    }
                }
            }
        }

        $this->view->registrationForm = $registrationForm;
        $this->view->form = $loginForm;
    }

    public function resetPassAction()
    {
        if ($this->is_loggedin) {
            $this->_redirect("/");
            exit;
        }

        $formAction = $this->_request->getParam('form_action');

        $userService = new Auction_Service_User();

        $resetForm = new Auction_Form_ResetPass();

        // Form has been submitted
        if ($this->_request->isPost()) {

            if($resetForm->isValid($this->_getAllParams())) {

                //Update password for user
                $email = $this->_request->getParam('email_address');
                $new_password = md5(uniqid(rand()));
                $new_password = substr($new_password, 0, 8);
                $userService->resetUserPass($email, $new_password);
                echo '$new_password: '.$new_password;

                //Queue email to send new password
                $user = $userService->getUserDataByEmail($email);
                $recipient_user_id = $user['user_id'];
                $recipient_username = $user['username'];

                $data = array(
                    'template' => 'reset-password',
                    'recipient_user_id' => $recipient_user_id,
                    'recipient_username' => $recipient_username,
                    'recipient_password' => $new_password
                );

                $task = new Cs_Task_EnQueueEmail($data);
                $task->run();

                $this->view->success = 1;
                $this->view->message = 'Your password has now been reset and sent to your email address';
            }
        }

        $this->view->form = $resetForm;
    }

    public function changePassAction()
    {
        if (! $this->is_loggedin) {
            $this->setFlashMessage("You are not logged in. Please log in to change your password");

            $this->_redirector->gotoSimple('login',
                                       'user',
                                        null
                    );
        }

        $formAction = $this->_request->getParam('form_action');

        $userService = new Auction_Service_User();

        $changeForm = new Auction_Form_ChangePass();

        // Form has been submitted
        if ($this->_request->isPost()) {

            if($changeForm->isValid($this->_getAllParams())) {

                //Update password for user
                $user_id = $this->auth_userid;
                $current_password = $this->_request->getParam('current_password');
                $new_password = $this->_request->getParam('new_password');

                $check_password = $userService->checkUserPassByUserId($user_id, $current_password);

                if (!empty($check_password)) {

                    $userService->changeUserPass($user_id, $new_password);

                    $this->setFlashMessage("Your password has now been changed");
                    
                    $this->_redirector->gotoSimple('my-account',
                                       'user',
                                        null
                            );
                }
                else {
                    $this->view->message = 'Your current password was incorrect, please try again';
                }
            }
        }

        $this->view->form = $changeForm;
    }


    /**
     * @todo Lock this down to cookie/logged in id. Don't release any data of there
     * is a discrepancy
     *
     * when requesting this page append ?type=json or /type/json to the url
     */
    public function getMyMessagesAction()
    {
        $message_data = array();

        $campaign_id = $this->_getParam('campaign_id');
        $auction_id = $this->_getParam('auction_id');
        $item_id = $this->_getParam('item_id');

        $queue_name_user_append = '';
        if (isset($item_id) && !empty($item_id)) {
            $queue_name_user_append = '_item_'.$item_id;
        }

        if ($this->_request->isXmlHttpRequest() && $this->auth_is_user) {

            $user_id = $this->auth_userid;
            // fetch the messages from the message queue
            $queue = new Auction_Service_QueueManager(
                    Auction_Service_QueueManager::QUEUE_NAME_USER,
                    "user_" . $user_id . $queue_name_user_append
                );
            $messages = $queue->receive(5);

            foreach ($messages as $queue_message) {
                $data = json_decode($queue_message->body, true);
                $message_data[] = $data['body'];

                $queue->deleteMessage($queue_message);
            }
            $this->view->message_count = count($message_data);
            $this->view->user_messages = $message_data;

            $this->view->campaign_id = $campaign_id;
            $this->view->auction_id = $auction_id;
            $this->view->item_id = $item_id;
        }
        else {
            $this->view->clearVars();
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            echo "Request not understood";

        }
    }

    public function myAccountAction()
    {
        if (! $this->is_loggedin) {
            $this->setFlashMessage("You are not logged in. Please log in to see your account information.");
            $this->_redirect('/');
        }

        $user = new Auction_Service_User();
        $userData = $user->getUserData($this->auth_userid);

        $validation = $user->hasPreAuthed($this->auth_userid);

        $this->view->userData   = $userData;
        $this->view->hasPreauth = $validation;
        $this->view->username   = $this->auth_username;
    }

}

