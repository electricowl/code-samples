<?php

/**
 * Description of CampaignController
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class CampaignController extends Cs_Controller_AbstractActionController
{

    public function indexAction()
    {
        // We'll display all the campaigns
        $service = new Auction_Service_Campaign();

        $this->view->campaigns = $service->getAllActiveCampaigns(
                    Zend_Registry::get('client_id')
                );


        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $this->auth_userid ? $this->auth_userid : 0,
                'category'   => 'Campaign:overview',
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $this->auth_userid,
                    'user'      => $this->auth_username,
                    'params'    => $this->_request->getParams(),
                )),
                // Leave off message because Logger knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Campaign");
    }

}

