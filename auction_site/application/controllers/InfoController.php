<?php

/**
 * Description of InfoController
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c)
 */
class InfoController extends Cs_Controller_AbstractActionController
{
    public function indexAction()
    {
        $this->_redirector->gotoSimple('about',
                                       'info',
                                        null
                );
    }


    public function contactAction()
    {
        // it all happens in the template
    }

    public function termsAndConditionsAction()
    {
        // all in the template
    }

    public function aboutAction()
    {
        // all in the template
    }



}

