<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());

        // Verify web environment (lucene, cache, session directories)
        if (php_sapi_name() != 'cli' || ! empty($_SERVER['REMOTE_ADDR'])) {

            $resources = $this->getOption('resources');
            if (! is_writeable($resources['session']['save_path'])) {
                throw new Exception(
                    'Session save_path is not writeable: ' . $resources['session']['save_path']
                );
            }
        }
        // Store the values in a registry.
        Zend_Registry::set('config', $config);

        return $config;
    }

    protected function _initMessages()
    {
        $filename = APPLICATION_PATH . '/configs/messages.ini';
        $messages = new Zend_Config_Ini($filename);

        Zend_Registry::set('messages', $messages);

        return $messages;
    }

    protected function _initView()
    {
        $this->bootstrap('FrontController');
        /** @var Zend_Controller_Front $frontController **/
        $frontController = $this->getResource('frontController');

        $this->bootstrap('layout');
        $layout = $this->getResource('layout');

        // Initialize view
        $view = new Zend_View();
        $view->doctype('XHTML5');
        //$view->headTitle('My Project');
        $view->env = APPLICATION_ENV;

        // Add it to the ViewRenderer
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
            'ViewRenderer'
        );
        $view->setBasePath(APPLICATION_PATH . "/views");

        // Themes are part of the view so we can initialise them here after the
        // basics have been set up
        // ===============================
        // So for the current theme we need to ensure that the scriptPath is set to the themes folder
        // with the fallback being the original base path.
        // The aim here is to avoid the use of a themeSwitcher() elsewhere in the code.
        // As we alreay have a fallback mechanism we should use it.
        $split = preg_split('#/#', $_SERVER['REQUEST_URI'], 2, PREG_SPLIT_NO_EMPTY);
        $app_theme = count($split) ? $split[0] : null;

        $filename = APPLICATION_PATH . '/configs/themes.ini';
        $themes = new Zend_Config_Ini($filename);

        // Now to evaluate available themes (quicker than globbing the themes folder)
        $available = $themes->available->id;

        $client_id = DEFAULT_CLIENT_ID;
        if (! in_array($app_theme, $available->toArray())) {
            $app_theme = null;
        }
        // Then we have to set up the view.
        if (null !== $app_theme) {

            $client_id = array_search($app_theme, $available->toArray());

            $layout_path = THEMES_PATH . DIRECTORY_SEPARATOR . $app_theme . '/layout';
            $script_path = THEMES_PATH . DIRECTORY_SEPARATOR . $app_theme . "/scripts";

            // Then here we can adapt the include path for the view..
            // If present it will be checked first...
            $view->addScriptPath($script_path);

            // Then we'll fix up the layout (but only if a layout is defined...
            if (file_exists($layout_path . "/layout.phtml")) {
                $layout->setLayoutPath(
                    $layout_path
                );
            }
        }

        // Save the app theme in the registry as we will be using it regularly
        Zend_Registry::set('app_theme', $app_theme);
        Zend_Registry::set('client_id', $client_id);
        Zend_Registry::set('themes',    $themes);

        $view->addHelperPath(APPLICATION_PATH . "/views/helpers", 'Auction_View_Helper');
        $viewRenderer->setView($view);

        // ===============================
        // Return it, so that it can be stored by the bootstrap
        return $view;
    }

    protected function _initQueue()
    {
        $options = $this->getOptions();

        // Create an adapter for our queue and register it.
        $queueAdapter = new Cs_Adapter_Queue_Db_Generic($options['generic_queue'] );
        Zend_Registry::getInstance()->queueAdapterGeneric = $queueAdapter;

        $queueAdapter = new Cs_Adapter_Queue_Db_Bid( $options['bid_queue'] );
        Zend_Registry::getInstance()->queueAdapterBid = $queueAdapter;

        $queueAdapter = new Cs_Adapter_Queue_Db_Email( $options['email_queue'] );
        Zend_Registry::getInstance()->queueAdapterEmail = $queueAdapter;

        $queueAdapter = new Cs_Adapter_Queue_Db_User( $options['user_queue'] );
        Zend_Registry::getInstance()->queueAdapterUser = $queueAdapter;

    }

    protected function _initCache()
    {
        // Force config to run first as a prerequisite.
        $this->bootstrap('Config');

        $config = Zend_Registry::get('config');

        $cache = NULL;
        // only attempt to init the cache if turned on
        if ($config->storage->use_cache) {
            // get the cache settings
            $cache_config = $config->storage->cache;

            try {
                    $cache = Zend_Cache::factory(
                        $cache_config->frontend->adapter,
                        $cache_config->backend->adapter,
                        $cache_config->frontend->options->toArray()
                    );
            } catch (Zend_Cache_Exception $e) {
                // ...
            }
            /** @var Zend_Cache_Core $cache **/
            Zend_Registry::set('cache', $cache);
            return $cache;
        }
    }

    protected function _initTranslator()
    {
        $logUntranslated = false;
        $logger = null;

        $params = array(
                'adapter'         => 'Zend_Translate_Adapter_Array',
                'content'         => LIBRARY_PATH . "/translations/",
                'locale'          => 'en_GB',
                //'locale'          => 'de_DE',
                'scan'            => 'filename',
                'disableNotices'  => true,
                'logUntranslated' => $logUntranslated
            );

        /**
         * TODO Enable this section during smoke testing to catch any missed translations.
         * TODO NB. This will not trap text that does not have the $this->translate()
         * function wrapping it.
        if (in_array(APPLICATION_ENV, array('ianl', 'development'))) {
            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . "/../docs/missed_translations.log");
            $format = '%message%' . PHP_EOL;
            $formatter = new Cs_FormatterSimple($format);
            $writer->setFormatter($formatter);
            $logger = new Zend_Log($writer);
            $params['log'] = $logger;
            $params['logUntranslated'] = true;
        }
        **/

        $translate = new Zend_Translate(
            $params
        );

        Zend_Registry::set('Zend_Translate', $translate);

        // For form work
        Zend_Validate_Abstract::setDefaultTranslator($translate);
        Zend_Form::setDefaultTranslator($translate);

    }

    public function _initRoutes()
    {
        $this->bootstrap('FrontController');
        /** @var Zend_Controller_Front $frontController **/
        $frontController = $this->getResource('frontController');
        $frontController->setRequest(new Zend_Controller_Request_Http);

        // returns a rewrite router by default
        $router = $frontController->getRouter();

        if (! defined('MAINTENANCE_MODE')) {
            define('MAINTENANCE_MODE', true);
        }

        //error_log(time() . " Remote: " . $_SERVER['REMOTE_ADDR'] . "\n", 3, "/tmp/remote_ip");
        // Permitted IP addresses
        $_permittedIp = array(
            '127.0.0.1',
            // UK Office
            '86.188.247.250', '81.137.245.60',
            // New York
            '24.105.128.190', '50.74.75.219',
            '24.29.104.5'
        );

        if (APPLICATION_ENV == 'production' && MAINTENANCE_MODE) {

            // Lock down unless we are Crowdsurge
            if (! empty($_SERVER['REMOTE_ADDR']) && ! in_array($_SERVER['REMOTE_ADDR'], $_permittedIp)) {

                $router->addRoute(
                        'maintenance',
                        new Zend_Controller_Router_Route(
                            '/*',
                            array(
                                'controller' => 'index',
                                'action'     => 'maintenance'
                            )
                        )
                    );
                return;
            }
        }

        if (! empty($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], $_permittedIp)) {
            Zend_Controller_Front::getInstance()->setParam('displayExceptions', 1);
        }

        // Here setting the baseUrl for the whole application
        if (null != Zend_Registry::get('app_theme')) {
            $frontController->setBaseUrl("/" . Zend_Registry::get('app_theme'));
        }

        //Auction Routes
        // @todo Switch the routes that require numeric indexes
        // to use Zend_Controller_Router_Route_Regex (from preg_match)
        $router->addRoute(
            'campaigns_route1',
            new Zend_Controller_Router_Route(
                '/campaigns',
                array(
                    'controller' => 'index',
                    'action'     => 'index',
                )
            )
        );

        $router->addRoute(
            'campaign_route1',
            new Zend_Controller_Router_Route(
                '/campaign/:campaign_id',
                array(
                    'controller' => 'auction',
                     'action'    => 'list',
                )
            )
        );

        $router->addRoute(
            'auction_route1',
            new Zend_Controller_Router_Route(
                '/campaign/:campaign_id/auction/:auction_id',
                array(
                    'controller' => 'auction',
                     'action'    => 'detail',
                )
            )
        );

        $router->addRoute(
            'auction_route2',
            new Zend_Controller_Router_Route(
                '/auction/:auction_id',
                array(
                    'controller' => 'auction',
                     'action'    => 'detail',
                )
            )
        );

        $router->addRoute(
            'auction_route3',
            new Zend_Controller_Router_Route(
                '/auction/detail/*',
                array(
                    'controller' => 'auction',
                     'action' => 'detail',
                )
            )
        );

        $router->addRoute(
            'item_route1',
            new Zend_Controller_Router_Route(
                '/campaign/:campaign_id/auction/:auction_id/item/:item_id',
                array(
                    'controller' => 'auction',
                     'action' => 'show-item',
                )
            )
        );

        $router->addRoute(
            'item_route2',
            new Zend_Controller_Router_Route(
                '/item/:item_id',
                array(
                    'controller' => 'auction',
                     'action' => 'show-item',
                )
            )
        );

        $router->addRoute(
            'userlogin_route1',
            new Zend_Controller_Router_Route(
                '/login',
                array(
                    'controller' => 'user',
                     'action' => 'login',
                )
            )
        );

        $router->addRoute(
            'userlogout_route1',
            new Zend_Controller_Router_Route(
                '/logout',
                array(
                    'controller' => 'index',
                     'action' => 'logout',
                )
            )
        );

        $router->addRoute(
            'confirm-bid_route1',
            new Zend_Controller_Router_Route(
                '/campaign/:campaign_id/auction/:auction_id/item/:item_id/confirm-bid',
                array(
                    'controller' => 'auction',
                     'action' => 'confirm-bid',
                )
            )
        );

        $router->addRoute(
            'confirm-bid_route2',
            new Zend_Controller_Router_Route(
                '/item/:item_id/confirm-bid',
                array(
                    'controller' => 'auction',
                     'action' => 'confirm-bid',
                )
            )
        );

        $router->addRoute(
            'bid-accepted_route1',
            new Zend_Controller_Router_Route(
                '/campaign/:campaign_id/auction/:auction_id/item/:item_id/bid-accepted',
                array(
                    'controller' => 'auction',
                     'action' => 'bid-accepted',
                )
            )
        );

        $router->addRoute(
            'bid-accepted_route2',
            new Zend_Controller_Router_Route(
                '//item/:item_id/bid-accepted',
                array(
                    'controller' => 'auction',
                     'action' => 'bid-accepted',
                )
            )
        );

        //Red Routes

        $router->addRoute(
            'red-artists_route1',
            new Zend_Controller_Router_Route(
                '/artists',
                array(
                    'controller' => 'index',
                     'action'    => 'index',
                )
            )
        );

        $router->addRoute(
            'red-artist_route1',
            new Zend_Controller_Router_Route(
                '/artist/:campaign_id',
                array(
                    'controller' => 'auction',
                     'action' => 'list',
                )
            )
        );

        $router->addRoute(
            'red-event_route1',
            new Zend_Controller_Router_Route(
                '/event/:auction_id',
                array(
                    'controller' => 'auction',
                     'action' => 'detail',
                )
            )
        );

        $router->addRoute(
            'red-signin_route1',
            new Zend_Controller_Router_Route(
                '/signin',
                array(
                    'controller' => 'user',
                     'action' => 'login',
                )
            )
        );

        $router->addRoute(
            'red-signout_route1',
            new Zend_Controller_Router_Route(
                '/signout',
                array(
                    'controller' => 'index',
                     'action' => 'logout',
                )
            )
        );

    }

    protected function _initZFDebug()
    {
        if (! in_array(APPLICATION_ENV, array('ianl', 'development'))) {
            return;
        }

        if (! empty($_SERVER['REMOTE_ADDR']) && ($_SERVER['REMOTE_ADDR'] === gethostbyname('jonattfield.no-ip.org'))) {
            return;
        }

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        $options = array(
            'plugins' => array('Variables',
                               'File' => array('base_path' => BASE_PATH),
                               'Memory',
                               'Time',
                               'Registry',
                               'Exception',
                               )
        );

        # Instantiate the database adapter and setup the plugin.
        # Alternatively just add the plugin like above and rely on the autodiscovery feature.
        if ($this->hasPluginResource('db')) {
            $this->bootstrap('db');
            $db = $this->getPluginResource('db')->getDbAdapter();
            $options['plugins']['Database']['adapter'] = $db;
        }

        # Setup the cache plugin
        if ($this->hasPluginResource('cache')) {
            $this->bootstrap('cache');
            $cache = $this->getPluginResource('cache')->getDbAdapter();
            $options['plugins']['Cache']['backend'] = $cache->getBackend();
        }
        $options['jquery_path'] = false; //"/js/libs/jquery-1.8.3.js";

        $debug = new ZFDebug_Controller_Plugin_Debug($options);

        // Add Custom Tab
        $message = var_export(array(
                'APPLICATION_ENV'   => APPLICATION_ENV,
                'APPLICATION_PATH'  => APPLICATION_PATH,
                'BASE_PATH'         => BASE_PATH,
            ), true);

        $debugText = new ZFDebug_Controller_Plugin_Debug_Plugin_Text();
        $debugText->setTab('Environment')->setPanel("<pre>$message</pre>");
        $debug->registerPlugin($debugText);

        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin($debug);
    }

}

