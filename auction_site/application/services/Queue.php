<?php
/**
 * Description of Queue (Queue.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Service_Queue
{

    public function listAllQueues()
    {
        $queue = new Auction_Model_DataGateway_Queue();

        return $queue->fetchAll();

    }

    public function listQueueContents($queueName)
    {
        $message = new Auction_Model_DataGateway_QueueMessage();

        switch ($queueName) {
            case "bid_messages" :

                $message->useBidQueue();

                break;

            case "email_messages" :

                $message->useEmailQueue();

                break;

            case "user_messages" :

                $message->useUserQueue();

                break;


            default:
                $message->useDefaultQueue();
                break;
        }

        return $message->fetchAll();
    }

}

