<?php

/**
 * Description of Email
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_Email
{
    private $scriptBasePath;

    public function __construct() {

        $this->scriptBasePath = implode(DIRECTORY_SEPARATOR, array(
            APPLICATION_PATH,
            'views',
        ));
    }

    /**
     * @todo This needs fixing. The implementation to integrate into message queueing
     * should have prioritised the $data array but instead the other parameters
     * have simply been ignored. Not great!!
     * @param string $to
     * @param string $toName
     * @param string $from
     * @param string $fromName
     * @param string $subject
     * @param array $data
     */
    public function sendEmail($to, $toName, $from, $fromName, $subject, $data)
    {
        $template     = $data['template'];
        $replyToEmail = $data['reply_to_email'];
        $replyToName  = $data['reply_to_name'];

        // Build html template - layout + view
        $layout = new Zend_layout();
        $layout->setLayoutPath($this->scriptBasePath);
        $layout->setLayout('layouts/email')->disableLayout();

        $view = new Zend_View();
        $view->setBasePath($this->scriptBasePath);
        $view->assign('data', $data);

        $layout->content = $view->render("emails/{$template}.phtml");
        $layout->site_url = $data['site_url'];
        $layout->subject = $data['subject'];

        $emailBodyHtml = $layout->render(); //$view->render("emails/{$template}.phtml");

        $mail = new Cs_Email();
        $mail->addHeader('X-CS-Auction', 'Complete Entertainment Resources (c) 2012-2013');
        $mail->setDate();

        $mail->setSubject($subject);
        $mail->setFrom($from, $fromName);
        $mail->addTo($to, $toName);
        $mail->setReplyTo($replyToEmail, $replyToName);
        $mail->setBodyHtml($emailBodyHtml);

        $mail->send();

        // Now we will log the work done.
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_EMAIL,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => 99999999,
                'category'   => 'Email:' . $data['template'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(
                        $data
                    ),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Email Sent : " . time()
        );
    }

    public function sendEmailSimple($to, $toName, $from, $fromName, $subject, $data)
    {
        $template     = $data['template'];
        $replyToEmail = $data['reply_to_email'];
        $replyToName  = $data['reply_to_name'];

        // Build html template - layout + view
        $layout = new Zend_layout();
        $layout->setLayoutPath($this->scriptBasePath);
        $layout->setLayout('layouts/email')->disableLayout();

        $view = new Zend_View();
        $view->setBasePath($this->scriptBasePath);
        $view->assign('data', $data);

        $layout->content = $view->render("emails/{$template}.phtml");
        if (! empty($data['site_url'])) {
            $layout->site_url = $data['site_url'];
        }

        $layout->subject = $subject;

        $emailBodyHtml = $layout->render();

        $mail = new Cs_Email();
        $mail->addHeader('X-CS-Auction', 'Complete Entertainment Resources (c) 2012-2013');
        $mail->setDate();

        $mail->setSubject($subject);
        $mail->setFrom($from, $fromName);
        $mail->addTo($to, $toName);
        $mail->setReplyTo($replyToEmail, $replyToName);
        $mail->setBodyHtml($emailBodyHtml);

        $mail->send();

        // Now we will log the work done.
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_EMAIL,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => 99999999,
                'category'   => 'Email:' . $data['template'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(
                        $data
                    ),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Email Sent : " . time()
        );
    }
}

