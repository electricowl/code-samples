<?php

/**
 * Description of Campaign
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_Campaign {

    public function getAllCampaignsForClient($clientId)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        return $campaigns->fetchAll("client_id = $clientId");
    }

    public function save($data)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        return $campaigns->insert($data);
    }

    public function update($data)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        $where = "campaign_id = " . $data['campaign_id'];
        unset($data['campaign_id']);

        return $campaigns->update($data, $where);
    }

    public function getAll()
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();

        return $campaigns->selectAll();
    }

    /**
     * An active campaign will be one where there are running auctions
     *
     */
    public function getAllActiveCampaigns($client_id)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        $auctions = new Auction_Model_DataGateway_Auctions();

        $active = $campaigns->getAllActiveCampaigns($client_id);

        $activeCampaigns = array();

        foreach ($active as $c) {
            $active_auctions = $auctions->selectConfirmedAuctionsByCampaignId(
                    $c['campaign_id'],
                    $client_id
                );
            $active_auctions_count = count($active_auctions);

            $activeCampaigns[] = array(
                'campaign_id'   => $c['campaign_id'],
                'campaign_custom_ref'   => $c['campaign_custom_ref'],
                'client_name'   => $c['client_name'],
                'campaign_name' => $c['campaign_name'],
                'description'   => $c['campaign_description'],
                'active_auctions'   => $active_auctions_count
            );
        }

        return $activeCampaigns;
    }

    public function getCampaignById($id)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        $result = $campaigns->find($id);

        $campaign = $result->current();
        return $campaign->toArray();
    }

    public function getCampaignByCustomRef($ref)
    {
        $campaigns = new Auction_Model_DataGateway_Campaigns();
        $result = $campaigns->fetchAll("campaign_custom_ref = '$ref'");

        $campaign = $result->current();
        // Check here specifically for the row class
        if ('Zend_Db_Table_Row' == get_class($campaign)) {
            return $campaign->toArray();
        }
        return $campaign;
    }


}

