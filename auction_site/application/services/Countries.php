<?php
/**
 * Description of Auction_Service_Countries (Countries.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Service_Countries
{

    public function fetchCountriesForSelect()
    {
        $countries = new Auction_Model_DataGateway_Countries();

        $result = $countries->fetchAll();

        $list = array();
        foreach ($result as $row) {
            $list[$row->id] = $row->country;
        }

        return $list;
    }

    public function getCountryById($id)
    {
        $countries = new Auction_Model_DataGateway_Countries();
        $resultSet = $countries->selectCountryById($id);

        $result = $resultSet->current();
        return $result->toArray();
    }
}

