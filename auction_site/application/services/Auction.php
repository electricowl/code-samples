<?php

/**
 * Description of Auction
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_Auction {

    public function getAllAuctionsByCampaign($id)
    {
        $dBauction = new Auction_Model_DataGateway_Auctions();
        return $dBauction->fetchAll("campaign_id = $id");
    }

    /**
     *
     * @param int $id
     * @return array
     */
    public function listAuctionsByCampaignId($id, $client_id)
    {
        $dBauction = new Auction_Model_DataGateway_Auctions();
        $list = $dBauction->selectConfirmedAuctionsByCampaignId($id, $client_id);

        $dBauctionItems = new Auction_Model_DataGateway_AuctionItems();
        $lotCount = $dBauctionItems->countLiveAuctions($id, $client_id);
        $lots = array();

        if (count($lotCount)) {
            foreach ($lotCount as $lot) {
                $liveItems = array();
                if ($lot['total_items'] > 0) {
                    $liveItems = $dBauctionItems->selectLiveAuctionItems(
                            $lot['auction_id'],
                            $client_id
                        );
                }

                $lots[$lot['auction_id']] = $liveItems;
            }
        }

        $auctionList = array();

        foreach ($list as $auction) {
            $auction['items_list'] = isset($lots[$auction['auction_id']]) ? $lots[$auction['auction_id']] : array();
            $auction['item_count'] = isset($lots[$auction['auction_id']]) ? count($lots[$auction['auction_id']]) : 0;
            $auctionList[] = $auction;
        }

        return $auctionList;
    }

    /**
     * returns: auction_id, campaign_id, auction_custom_ref, auction_name,
     * auction_description, display_order, auction_status_id
     * @param type $id
     * @return type
     */
    public function getAuctionDetailsById($id, $client_id)
    {
        $dBauction = new Auction_Model_DataGateway_Auctions();
        $result = $dBauction->selectAuctionDetailsByAuctionId(
                $id,
                $client_id
            );

        // Just to make sure we return the correct data.
        if (is_array($result)) {
            if (count($result)) {
                return reset($result);
            }
        }
        else if (is_object($result)) {
            $row = $result->current();
            if ($row) {
                return $row->toArray();
            }
        }

        return null;
    }

    /**
     *
     * @param int $item_id
     * @return Zend_Db_Table_Row
     */
    public function fetchAndLockCurrentHighBid($itemId)
    {
        $bids = new Auction_Model_DataGateway_Bids();

        $bidRow = $bids->fetchRow(
                $bids->select()
                ->where('auction_item_id = ?', $itemId)
                ->where('bid_status_id = ?', Auction_Model_DataGateway_Bids::BID_CURRENT_HIGHEST)
            );

        $bidRow->locked = true;
        //->setReadOnly(true)
        $bidRow->save();
        /** @var Zend_Db_Table_Row $bidRow **/
        return $bidRow;
    }

    /**
     *
     * @param int $itemId
     * @param float $bidAmount
     * @return stdClass
     */
    public function getBidRangeData($itemId, $bidAmount)
    {
        $bids = new Auction_Model_DataGateway_Bids();
        return (object) $bids->bidRange($itemId, $bidAmount);
    }

    private function processPayment($auctionItem, $paymentInfo, $transactionCurrency = "GBP")
    {
        throw new Exception(__METHOD__ . " Deprecated", '999');
        /** EXAMPLE DATA ***/
        $data = array(
            'first_name'      => 'Ian',
            'last_name'       => 'Lewis',

            'address_id'      => 16,
            'address_line_1'  => '86a Lordship Park',
            'address_line_2'  => 'Stoke Newington',
            'town_city'       => 'London',
            'county_state'    => 'Gtr London',
            'post_zip_code'   => 'N16 5UA',
            'country'         => 'United Kingdom',
            'country_id'      => 1,

            'is_default'      => 1,
            'is_active'       => 1,

            'bid_amount'      => '3.99',
            'currency'        => 'GBP',
            'card_type'       => 'Visa',

            'email'             => 'ian.lewis@electricowl.co.uk',
            // UK Office IP
            'client_ip_address' => '86.188.247.250',

            'transaction_currency' => 'GBP',

        );

        // This is the section where the payment is processed
        $gateway = new Cs_PaymentGateway_PayPalDirect();
        $transactionSummary  = $gateway->runAuthorisation(
                $paymentInfo,
                $transactionCurrency,
                Cs_PaymentGateway_PayPalDirect::API_DIRECT_PAYMENT_ACTION_SALE
            );

        // Select the correct tansaction type
        switch ($auctionItem->getTypeId()) {
            case Auction_Model_DataGateway_AuctionItems::TYPE_PRE_AUTH :

                /* @var $transactionTypeId int */
                $transactionTypeId = Auction_Model_DataGateway_Transactions::TYPE_PRE_AUTH;
                break;

            case Auction_Model_DataGateway_AuctionItems::TYPE_DEPOSIT :

                $transactionTypeId = Auction_Model_DataGateway_Transactions::TYPE_DEPOSIT;
                break;
        }

        $pgTransactionId = null;
        $pgAuthcode      = $transactionSummary->authorisation_code;
        // This is the paypal transaction id
        $pgPasRef        = isset($transactionSummary->pasref) ? $transactionSummary->pasref : null;

        if ($transactionSummary) {
            $serviceCurrency = new Auction_Service_Currency();
            $currencyId = $serviceCurrency->getCurrencyIdByCode($transactionCurrency);

            // Store the transaction information
            Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_TRANSACTION,
                array(
                    'transaction_type_id'   => $transactionTypeId,
                    'transaction_date'      => new Zend_Db_Expr("NOW()"),
                    'user_id'               => $userId,
                    'billing_address_id'    => $paymentInfo->getBillingAddressId(),
                    'auction_id'            => $auctionId,
                    'amount'                => $bidAmount,
                    'currency_id'           => $currencyId,
                    'transaction_status_id' => $transactionSummary->transaction_status_id,
                    //
                    'pg_transaction_id'     => $pgTransactionId,
                    'pg_auth_code'          => $pgAuthcode,
                    'pg_pas_ref'            => $pgPasRef,

                ),
                "Transaction logged: ");
        }

    }

    public function save($data)
    {
        $auctions = new Auction_Model_DataGateway_Auctions();
        return $auctions->insert($data);
    }

    public function update($data)
    {
        $auction_id = $data['auction_id'];
        unset($data['auction_id']);

        $auctions = new Auction_Model_DataGateway_Auctions();
        return $auctions->update($data, "auction_id = $auction_id");

    }

    public function getStatuses()
    {
        $stDgw = new Auction_Model_DataGateway_AuctionStatusLu();
        $statuses = $stDgw->fetchAll();

        $status = array();
        foreach ($statuses as $st) {
            $status[$st['auction_status_id']] = $st['label'];
        }

        return $status;
    }

    public function getAuctionByCustomRef($ref)
    {
        $auctions = new Auction_Model_DataGateway_Auctions();
        $result = $auctions->fetchAll("auction_custom_ref = '$ref'");

        $auction = $result->current();
        // Check here specifically for the row class
        if ('Zend_Db_Table_Row' == get_class($auction)) {
            return $auction->toArray();
        }
        return $auction;
    }

}

