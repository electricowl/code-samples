<?php

/**
 * Description of CardTypes
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_CardTypes
{

    public function getCardList()
    {
        $cards = new Auction_Model_DataGateway_CardTypeLu();
        $cardList = $cards->selectAllCards();

        return $cardList;
    }

    /**
     * Prepare array for the select object
     * @return array
     */
    static public function formatCardListForSelect()
    {
        $selfService = new self();
        $list = $selfService->getCardList();

        $formattedData = array();
        foreach ($list as $item) {
            $formattedData[$item['card_type_id']] = $item['label'];
        }

        return $formattedData;
    }

    static public function formatCardListForSelectUseLabels()
    {
        $selfService = new self();
        $list = $selfService->getCardList();

        $formattedData = array();
        foreach ($list as $item) {
            $formattedData[$item['label']] = $item['label'];
        }

        return $formattedData;
    }
}

