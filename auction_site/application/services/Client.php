<?php
/**
 * Description of Auction_Service_Client (Client.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Service_Client
{

    public function fetchClientIdFromThemeId($theme_id)
    {
        $db = new Auction_Model_DataGateway_Clients();
        $result = $db->fetchClientByThemeId($theme_id);

        return $result->client_id;
    }

    /**
     * Return list of campaigns/auctions based on the client id
     *
     * @param integer $client_id
     */
    public function fetchClientDataByClientId($client_id)
    {
        $campaignDb = new Auction_Model_DataGateway_Campaigns();
        $campaigns = $campaignDb->getActiveCampaignsByClientId($client_id);

        $clientData = array();

        $clientData['campaigns'] = $campaigns;

        $auctionDb = new Auction_Model_DataGateway_Auctions();
        $auctions = $auctionDb->selectAllAuctionsByClientId($client_id);

        $clientData['auctions'] = $auctions;

        return $clientData;
    }

    public function addClient($client_data)
    {
        $client = new Auction_Model_DataGateway_Clients();
        $client_id = $client->addClient(
                $client_data['client_name'],
                $client_data['client_theme_id']
            );

        $staff = new Auction_Model_DataGateway_ClientStaff();
        $staff_id = $staff->addStaff(
                $client_id,
                $client_data
            );

        return $staff_id;
    }

    public function fetchAuctionsReportByClientId($client_id) 
    {
        $auction_items = new Auction_Model_DataGateway_AuctionItems();
        $report_results = $auction_items->getAuctionReportByClientId($client_id);

        return $report_results;
    }
}

