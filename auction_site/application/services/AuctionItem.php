<?php

/**
 * Description of AuctionItem
 * Auction items (aka Lots) are managed here.
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_AuctionItem {

    /**
     * Fetch the Auction Item (Lot)
     * @param int $item_id
     * @param int $client_id
     * @return Cs_Data_AuctionItem
     */
    public function getItemById($id, $client_id)
    {
        $metadata = array();

        $currency = new Auction_Service_Currency();

        $item = new Auction_Model_DataGateway_AuctionItems();

        $result = $item->find($id);
        $item_row = $result->current();

        if ('Zend_Db_Table_Row' !== get_class($item_row)) {
            throw new Exception("No data found for item_id '$id' : " . __METHOD__);
        }

        $tzLu = new Auction_Model_DataGateway_TimezoneLu();
        $tzData = $tzLu->getTimezoneById(
                $item_row->timezone_id
            );
        //echo "<pre>";var_dump($row->auction_item_type_id, (get_class($tzData)), $tzData); exit;
        $metadata['timezone'] = reset($tzData);

        $itemTypeLu = new Auction_Model_DataGateway_AuctionItemTypesLu();
        $type = $itemTypeLu->fetchById($item_row->auction_item_type_id);
        $metadata['type'] = reset($type);

        $bidService = new Auction_Service_Bid();

        $metadata['history'] = $bidService->retrieveBidHistory($id);
        $user_id = $metadata['history']['user_id'];

        $metadata['public_history'] = $bidService->retrieveBidHistory($id, array(1,2,3,5));

        $auctionService = new Auction_Service_Auction();
        $metadata['auction'] = $auctionService->getAuctionDetailsById(
                $item_row->auction_id,
                $client_id
            );

        $metadata['currency'] = $currency->getCurrencyById($item_row->currency_id);

        $auction = new Auction_Model_DataGateway_Auctions();
        $result = $auction->find($item_row->auction_id);
        $auction = $result->current();

        $campaign = new Auction_Model_DataGateway_Campaigns();
        $result = $campaign->find($auction->campaign_id);
        $campaign = $result->current();

        $metadata['auction']  = $auction->toArray();
        $metadata['campaign'] = $campaign->toArray();

        $user = new Auction_Model_DataGateway_Users();

        $userService = new Auction_Service_User();
        $user_data = $userService->getUserData($user_id);

        $metadata['highest_bidder_username'] = $user_data['username'];

        return new Cs_Data_AuctionItem($item_row, $metadata);
    }

    public function save($data)
    {
        $dgw = new Auction_Model_DataGateway_AuctionItems();
        return $dgw->insert($data);
    }

    public function update($data)
    {
        $dgw = new Auction_Model_DataGateway_AuctionItems();
        $auctionItemId = $data['auction_item_id'];
        unset($data['auction_item_id']);

        $dgw->update($data, "auction_item_id = $auctionItemId");
    }

    public function getTypes()
    {
        $auctionTypesService = new Auction_Model_DataGateway_AuctionItemTypesLu();
        $auctionTypes = $auctionTypesService->fetchAll();

        $types = array();

        foreach ($auctionTypes as $type) {
            $types[$type['auction_item_type_id']] = $type['label'];
        }

        return $types;
    }

    /**
     *
     * @param type $auction_id
     * @return array Cs_Data_AuctionItem
     */
    public function fetchAllItemsByAuctionId($auction_id, $client_id)
    {
        $dgw = new Auction_Model_DataGateway_AuctionItems();
        $allItems = $dgw->fetchAll("auction_id = $auction_id");

        $data = array();
        foreach ($allItems as $item) {
            $data[] = $this->getItemById(
                    $item->auction_item_id,
                    $client_id
                );
        }
        return $data;
    }

    /**
     *
     * @return array Cs_Data_AuctionItem
     */
    public function fetchAllLiveAuctionItems($client_id)
    {
        $dgw = new Auction_Model_DataGateway_AuctionItems();
        $items = $dgw->selectLiveAuctionItems();

        $liveItems = array();
        foreach ($items as $item) {
            $liveItems[] = $this->getItemById(
                    $item['auction_item_id'],
                    $client_id
                );
        }

        return $liveItems;
    }

    public function getItemByCustomRef($ref)
    {
        $items = new Auction_Model_DataGateway_AuctionItems();
        $result = $items->fetchAll("auction_item_custom_ref = '$ref'");

        $item = $result->current();
        return $item;
    }

}

