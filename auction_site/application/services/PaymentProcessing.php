<?php

/**
 * Description of PaymentProcessing (PaymentProcessing.php)
 * This service calls other services so depends on them.
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Auction_Service_PaymentProcessing
{

    /**
     *
     * @param string $paymentGateway
     * @param int $itemId
     * @param float $bidValue
     * @param string $itemDescription
     * @param array $address
     * @param array $cardData
     */
    public function processBid($paymentGateway, $itemId, $bidValue, Cs_Data_PaymentInformation $paymentInfo)
    {
        $transactionCurrency = "GBP";

        $gw = Cs_PaymentGateway_Payment::create($paymentGateway);

        // Required data


        $success = $gw->runAuthorisation(
                $paymentInfo,
                $transactionCurrency
            );



        // Actions to perform here.
        // Process the payment
        // pass back any messages to the calling class/method


    }

}

