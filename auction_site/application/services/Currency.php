<?php

/**
 * Description of Currency
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_Currency {

    public function getCurrencyById($id)
    {
        $currency = new Auction_Model_DataGateway_CurrencyLu();
        return $currency->selectCurrencyById($id);
    }

    /**
     * Return the ID of  the 3 letter code.
     * @param type $code
     * @return int Description
     */
    public function getCurrencyIdByCode($code)
    {
        if (strlen($code) !== 3) {
            throw new Exception("Three letter currency code required: '$code' provided.");
        }
        $currency = new Auction_Model_DataGateway_CurrencyLu();

        $result = $currency->fetchRow(
                $currency->select()->where("description = ?", $code)
            );
        if ($result instanceof Zend_Db_Table_Row) {
            return $result->currency_id;
        }
        throw new Exception("Could not find ID for code: '$code'");
    }

    public function currencyDropdownList()
    {
        $currency = new Auction_Model_DataGateway_CurrencyLu();

        $result = $currency->fetchAll();

        $list = array();

        foreach ($result as $row) {
            $display = $row->description;
            if ($row->symbol != $display) {
                $display = sprintf("(%s) %s", $row->symbol, $display);
            }
            $list[$row->currency_id] = $display;
        }

        return $list;
    }

}

