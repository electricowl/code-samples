<?php
/**
 * Description of QueueManager (QueueManager.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Service_QueueManager
{
    const QUEUE_NAME_GENERIC = 'generic_queue';
    const QUEUE_NAME_BID     = 'bid_queue';
    const QUEUE_NAME_EMAIL   = 'email_queue';
    const QUEUE_NAME_USER    = 'user_queue';

    const GENERIC_WORKER = 'genericWorker';
    const BID_WORKER     = 'bidWorker';
    const EMAIL_WORKER   = 'emailWorker';
    const USER_WORKER    = 'userWorker';

    const CONFIG_LOOP_FOREVER   = 'loopForever';
    const CONFIG_SLEEP_INTERVAL = 'sleepInterval';
    const CONFIG_WORKER_RECEIVE = 'workerReceive';

    private $adapter;

    /** @var Zend_Queue $queue **/
    private $queue;
    private $queue_name;

    private $loop_forever;
    private $sleep_interval;

    private $queue_worker;
    private $worker_receive;

    private $_bidWorker = array(
        self::QUEUE_NAME_GENERIC => self::GENERIC_WORKER,
        self::QUEUE_NAME_BID     => self::BID_WORKER,
        self::QUEUE_NAME_EMAIL   => self::EMAIL_WORKER,
        self::QUEUE_NAME_USER => self::USER_WORKER,
    );

    /**
     * Queue name is optional to allow for situations where we need
     * to split out the queue ids.
     * Adapter is created then the QueueName is applied.
     * @param string $queueAdapter bid_queue, message_queue, email_queue
     * @param string $queueName This is what we will call the queue
     */
    public function __construct($queueAdapter, $queueName = null)
    {
        $this->queue_name = $queueName ? $queueName : $queueAdapter;

        if (array_key_exists($queueAdapter, $this->_bidWorker)) {
            $this->queue_worker = $this->_bidWorker[$queueAdapter];
        }
        else {
            throw new Exception("$queueAdapter not configured in " . __CLASS__);
        }

        // @todo The switch clause below is a 'sign' that we need a class per
        // adapter. This will make things super clear. Then a worker in each that
        // gets overridden
        // For now this will do.

        $adapter = 'queueAdapterGeneric';

        switch ($queueAdapter) {

            case self::QUEUE_NAME_BID :
                $adapter = 'queueAdapterBid';
                break;

            case self::QUEUE_NAME_EMAIL :
                $adapter = 'queueAdapterEmail';
                break;

            case self::QUEUE_NAME_USER :
                $adapter = 'queueAdapterUser';
                break;

            default :

                break;

        }
        //var_dump($this->queue_worker);
        // Get the required queue adapter
        $this->adapter = Zend_Registry::getInstance()->$adapter;

        $queueOptions = array(
            'name' => $this->queue_name,
        );

        $this->queue = new Zend_Queue($this->adapter, $queueOptions);

    }

    /**
     * Add an entry to the queue
     * @param array $data
     */
    public function addItemToQueue($item_data)
    {

        // Build the message here
        $message = json_encode($item_data);

        $this->queue->send($message);
    }

    /**
     * Facade for Zend_Queue::receive()
     * @param integer $maxMessages
     * @return array
     */
    public function receive($maxMessages = 1)
    {
        return $this->queue->receive($maxMessages);
    }

    public function deleteMessage($message)
    {
        $this->queue->deleteMessage($message);
    }

    /**
     * @todo check this for over-engineering.
     * Check queue and pass queue entries to the correct place when found.
     * This is destined to be our worker and will receive() queue messages.
     */
    public function worker($options)
    {
        $this->loop_forever   = array_key_exists('loopForever', $options) ? $options['loopForever'] : false;
        $this->sleep_interval = array_key_exists('sleepInterval', $options) ? $options['sleepInterval'] : 2;
        $this->worker_receive = array_key_exists('workerReceive', $options) ? $options['workerReceive'] : 1;

        // Perform the action once only unless in batch mode
        do {

            $json_messages = $this->queue->receive($this->worker_receive);

            foreach ($json_messages as $queue_message) {
                $message = $queue_message->toArray();

                $data = json_decode($message['body'], true);
                // Process the message here
                // call the bid processor and pass in the bid/email/user message.
                $this->{$this->queue_worker}($data);

                // Then delete from the queue
                $this->queue->deleteMessage($queue_message);
            }

            // We will only sleep when looping constantly.
            if ($this->loop_forever) {
                sleep($this->sleep_interval);
            }

        } while ($this->loop_forever);

    }

    /**
     * WORKER METHODS
     * @todo We can debate about refactoring into a group of Worker classes later.
     *
     */

    private function bidWorker($data)
    {
        throw new Exception("The work is done by Cs_Task_ProcessBid");
        // take the data
        // process bid
        // add emails to queue
        // add messages to queue
        //$service = new Auction_Service_Auction();

//        $service->processBid(
//            $data['auctionId'],
//            $data['userId'],
//            $data['bidData']
//        );
    }

    private function emailWorker($data)
    {
        //var_dump($data);
        $email = new Auction_Service_Email();
        $emailReturnValue = $email->sendEmail(
            $data['recipient_email'],
            $data['recipient_name'],
            $data['sender_email'],
            $data['sender_name'],
            $data['subject'],
            $data
        );

        return $emailReturnValue;
    }

    private function userWorker($data)
    {
        // Message requires an id to refer to the user.
    }

    private function genericWorker($data, $action)
    {

    }

    public function debug()
    {
        return $this->queue->debugInfo();
    }

}


