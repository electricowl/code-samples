<?php
/**
 * Description of Address (Address.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Service_Address
{

    public function fetchBillingAddressById($id)
    {
        $address = new Auction_Model_DataGateway_UserBillingAddress();
        $foundAddress = $address->find($id);

        return $foundAddress->toArray();
    }

    public function fetchShippingAddressById($id)
    {
        $address = new Auction_Model_DataGateway_UserShippingAddress();
        $foundAddress = $address->find($id);

        return $foundAddress->toArray();
    }

    public function addNewBillingAddress($data)
    {
        unset($data['billing']);

        $data['is_default'] = 1;
        $data['is_active']  = 1;

        $address = new Auction_Model_DataGateway_UserBillingAddress();

        $address->supersedePreviousAddressesForUser(
                $data['user_id']
            );

        return $address->insert($data);
    }

    public function addNewShippingAddress($data)
    {
        $address = new Auction_Model_DataGateway_UserShippingAddress();

        return $address->insert($data);
    }

    public function addNewBillingAddressFromShippingAddress($data)
    {
        // If we are repurposing a Shipping address we remove the id field
        // TODO Test this.
        if (isset($data['shipping_address_id'])) {
            unset($data['shipping_address_id']);
        }

        return $this->addNewBillingAddress($data);
    }

}

