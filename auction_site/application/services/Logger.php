<?php

/**
 * Description of Logger (Logger.php)
 * Singleton Class
 * Logger provides simple ways of interacting with the logging required in the
 * application.
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Auction_Service_Logger {

    const LOG_APPLICATION = 'application';
    const LOG_TRANSACTION = 'transaction';
    const LOG_EMAIL       = 'email';
    const LOG_BID         = 'bid';

    private static $_instance = null;

    // maintain instance map of loggers
    private static $instanceMap = array();

    private function __construct()
    {
        // setting this private prevents instantiation of the class
        // by the use of new Class();

    }

    public static function getInstance()
    {
        if( ! is_object(self::$_instance) ) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private static function create($type)
    {
        $loggerService = self::getInstance();

        $serviceName = ucfirst($type);
        $service = "create{$serviceName}Logger";

        // Create the required instance of a logger
        // All to make life simpler when getting loggers as this is effectively
        // a global method.
        $newLogger = call_user_func(array($loggerService, $service));
        return $newLogger;
    }

    private function createTransactionLogger()
    {
        if (array_key_exists(self::LOG_TRANSACTION, self::$instanceMap)) {
            return self::$instanceMap[self::LOG_TRANSACTION];
        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $writer = new Zend_Log_Writer_Db(
                $db,
                'transactions',
                array(
                    'transaction_type_id'   => 'transaction_type_id',
                    'transaction_date'      => 'transaction_date',
                    'user_id'               => 'user_id',
                    'billing_address_id'    => 'billing_address_id',
                    'auction_id'            => 'auction_id',
                    'amount'                => 'amount',
                    'currency_id'           => 'currency_id',
                    'transaction_status_id' => 'transaction_status_id',
                    'pg_transaction_id'     => 'pg_transaction_id',
                    'pg_auth_code'          => 'pg_auth_code',
                    'pg_pas_ref'            => 'pg_pas_ref'
                )
            );
        $logger = new Zend_Log($writer);

        self::$instanceMap[self::LOG_TRANSACTION] = $logger;

        return $logger;
    }

    private function createApplicationLogger()
    {
        if (array_key_exists(self::LOG_APPLICATION, self::$instanceMap)) {
            return self::$instanceMap[self::LOG_APPLICATION];
        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $writer = new Zend_Log_Writer_Db(
                $db,
                'application_log',
                array(
                    'event_date' => 'event_date',
                    'user_id'    => 'user_id',
                    'category'   => 'category',
                    'ip_address' => 'ip_address',
                    'user_agent' => 'user_agent',
                    // app_method is __METHOD__
                    'app_method' => 'app_method',
                    'metadata'   => 'metadata',
                    'message'    => 'message'
                )
            );
        $logger = new Zend_Log($writer);

        self::$instanceMap[self::LOG_APPLICATION] = $logger;
        return $logger;
    }

    private function createBidLogger()
    {
        if (array_key_exists(self::LOG_BID, self::$instanceMap)) {
            return self::$instanceMap[self::LOG_BID];
        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $writer = new Zend_Log_Writer_Db(
                $db,
                'bid_log',
                array(
                    'event_date' => 'event_date',
                    'user_id'    => 'user_id',
                    'category'   => 'category',
                    'ip_address' => 'ip_address',
                    'user_agent' => 'user_agent',
                    // app_method is __METHOD__
                    'app_method' => 'app_method',
                    'metadata'   => 'metadata',
                    'message'    => 'message'
                )
            );
        $logger = new Zend_Log($writer);

        self::$instanceMap[self::LOG_BID] = $logger;
        return $logger;
    }

    private function createEmailLogger()
    {
        if (array_key_exists(self::LOG_APPLICATION, self::$instanceMap)) {
            return self::$instanceMap[self::LOG_APPLICATION];
        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $writer = new Zend_Log_Writer_Db(
                $db,
                'email_log',
                array(
                    'event_date' => 'event_date',
                    'user_id'    => 'user_id',
                    'category'   => 'category',
                    // app_method is __METHOD__
                    'app_method' => 'app_method',
                    'metadata'   => 'metadata',
                    'message'    => 'message'
                )
            );
        $logger = new Zend_Log($writer);

        self::$instanceMap[self::LOG_APPLICATION] = $logger;
        return $logger;
    }

    /**
     * Log the mapped data to the required logger
     * @param string $loggerType
     * @param array $map
     * @param string $message
     */
    public static function log($loggerType, $map, $message) {
        $logger = self::create($loggerType);

        foreach ($map as $key => $value) {
            $logger->setEventItem($key, $value);
        }

        $logger->info($message);
    }

}

