<?php
/**
 *
 * Description of BidManagement (BidManagement.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

//header("Cache-Control: no-cache, must-revalidate");
//header("Pragma: no-cache");
//date_default_timezone_set('Europe/London');
//$date = date('Y-m-d H:i:s');

class Auction_Service_AccountManagement
{

    const TRANSACTION_SUCCESS = 'success';
    const TRANSACTION_FAILURE = 'failure';

    const CARD_TYPE_MASTERCARD = 'mastercard';
    const CARD_TYPE_VISA       = 'visa';
    const CARD_TYPE_AMEX       = 'amex';

    protected $re_merchantid = "crowdsurge";
    protected $re_secret     = "YWWyxXV4bb";
    protected $re_account    = 'amex';
    protected $re_URL        = 'https://epage.payandshop.com/epage-remote.cgi';

    protected $auth_response_packet = array();

    function cardPreAuth($form_data)
    {
        $card_type   = strtolower($form_data['card_type']);

        $auth_amount = $form_data['amount'];

        $start_month = null;
        $start_year  = null;
        $exp_month   = $form_data['expiry_month'];
        $exp_year    = $form_data['expiry_year'];
        $card_number = $form_data['card_number'];
        $cvn         = $form_data['cvv_code'];

        $first_name    = $form_data['first_name'];
        $last_name     = $form_data['last_name'];
        $address_line1 = $form_data['address_line_1'];
        $address_line2 = $form_data['address_line_2'];
        $town_city     = $form_data['town_city'];
        $county_state  = $form_data['county_state'];
        $postcode      = $form_data['post_zip_code'];
        $country       = $form_data['country_id'];

        $user_email    = $form_data['user_email'];
        $user_ip       = $form_data['user_ip'];
        $user_id       = $form_data['user_id'];
        $our_reference = $form_data['our_reference'];

        $issue_num     = "0";

        list($currency_id, $trans_currency) = explode('|', $form_data['currency']);

        // Decide here which path to take..
        $success = true;
        $result = self::TRANSACTION_FAILURE;
        
        if ($card_type == self::CARD_TYPE_AMEX) {
            $result = $this->preauthAmex($exp_month, $exp_year,
                    $card_number, $auth_amount, $issue_num,
                    $cvn, $our_reference, $trans_currency,
                    $first_name, $last_name, $postcode, $user_email, $user_ip
                );
        }
        else if ($card_type == self::CARD_TYPE_MASTERCARD || $card_type == self::CARD_TYPE_VISA) {
            $result = $this->preauthPayPal($card_type, $card_number, $start_month,
                    $start_year, $auth_amount, $trans_currency,
                    $exp_month, $exp_year, $cvn, $first_name, $last_name,
                    $address_line1, $town_city, $county_state, $postcode,
                    $country, $user_email, $user_ip
                );
        }

        $transaction = $this->getLastTransactionResult(true);

        // General Application logging.. to store the whole packet of data
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $user_id,
                'category'   => 'Auction:account preAuth',
                'ip_address' => $user_ip,
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $user_id,
                    'card_type' => $card_type,
                    'transaction' => $transaction,
                )),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Account: PreAuth");

        // Store the transaction information
            Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_TRANSACTION,
                array(
                    'transaction_type_id'   => Auction_Model_DataGateway_Transactions::TYPE_PRE_AUTH,
                    'transaction_date'      => new Zend_Db_Expr("NOW()"),
                    'user_id'               => $user_id,
                    'billing_address_id'    => 0, //$billing_address_id,
                    'auction_id'            => 0, //$auction_id,
                    'amount'                => $auth_amount,
                    'currency_id'           => $currency_id,
                    'transaction_status_id' => 1, //$transaction['auth_successful'],
                    //
                    'pg_transaction_id'     => isset($transaction['transaction_id']) ? $transaction['transaction_id'] : null,
                    'pg_auth_code'          => isset($transaction['auth_code']) ? $transaction['auth_code'] : '',
                    'pg_pas_ref'            => isset($transaction['transaction_pasref']) ? $transaction['transaction_pasref'] : null,

                ),
                "Transaction logged: PreAuth");
        // Then

        if ($result == self::TRANSACTION_SUCCESS) {

            $comment = "Crowdsurge Auction preAuth refund";
            if ($card_type == self::CARD_TYPE_AMEX) {
                $this->voidAmex(
                        $transaction['our_reference'],
                        $transaction['transaction_pasref'],
                        $transaction['transaction_id'],
                        $comment
                    );
            }
            else if ($card_type == self::CARD_TYPE_MASTERCARD || $card_type == self::CARD_TYPE_VISA) {
                $this->voidPayPal(
                        $transaction['transaction_id'],
                        $comment
                    );
            }


        }
        else {
            $success = false;
        }

        $transaction = $this->getLastTransactionResult(true);

        // General Application logging.. to store the whole packet of data
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_APPLICATION,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $user_id,
                'category'   => 'Auction:account preAuth VOID',
                'ip_address' => $user_ip,
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'   => $user_id,
                    'card_type' => $card_type,
                    'transaction' => $transaction,
                )),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Account: VOID PreAuth");

        // Logging
        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_TRANSACTION,
                array(
                    'transaction_type_id'   => Auction_Model_DataGateway_Transactions::TYPE_PRE_AUTH_VOID,
                    'transaction_date'      => new Zend_Db_Expr("NOW()"),
                    'user_id'               => $user_id,
                    'billing_address_id'    => 0, //$billing_address_id,
                    'auction_id'            => 0, //$auction_id,
                    'amount'                => $auth_amount,
                    'currency_id'           => $currency_id,
                    'transaction_status_id' => 1, //$transaction['auth_successful'],
                    //
                    'pg_transaction_id'     => isset($transaction['transaction_id']) ? $transaction['transaction_id'] : null,
                    'pg_auth_code'          => isset($transaction['auth_code']) ? $transaction['auth_code'] : '',
                    'pg_pas_ref'            => isset($transaction['transaction_pasref']) ? $transaction['transaction_pasref'] : null,

                ),
                "Transaction logged: VOID PreAuth");

        return $success;

    }

    /**
     *
     * @param string $exp_month eg. 03, 12
     * @param string $exp_year eg. 13, 16
     * @param string $cardnum 00000000000000000
     * @param string $auth_amount 1.00
     * @param string $issue_num 0 ??
     * @param string $cvn 123
     * @param string $our_reference auction_preauth
     * @param string $trans_currency USD/GBP
     * @param string $firstnameb Ian
     * @param string $lastname Lewis
     * @param string $postcode N16 5UA
     * @param string $user_email ian.lewis@crowdsurge.com
     * @param string $user_ip 86.188.247.250
     * @return string Authorisation response (Success/Fail)
     */
    public function preauthAmex($exp_month, $exp_year, $cardnum, $auth_amount,
            $issue_num, $cvn, $our_reference, $trans_currency,
            $firstname, $lastname, $postcode, $user_email, $user_ip
            )
    {
        $merchantid = "crowdsurge";
        $secret     = "YWWyxXV4bb";
        $account    = 'amex';
        $re_URL        = 'https://epage.payandshop.com/epage-remote.cgi';

        //Send Auth request to RealEx
        $expdate = $exp_month . substr($exp_year, 2, 2); //Realex only requires 2 digits for year
        $amount = $auth_amount * 100;

        //Hash
        $timestamp = strftime('%Y%m%d%H%M%S');
        $p_orderid = $our_reference;
        $tmp = "$timestamp.$merchantid.$p_orderid.$amount.$trans_currency.$cardnum";
        $md5hash = md5($tmp);

        $tmp = "$md5hash.$secret";
        $md5hash = md5($tmp);

        //Request
        $xml = "<request type='auth' timestamp='$timestamp'>
                <merchantid>$merchantid</merchantid>
                <account>$account</account>
                <orderid>$p_orderid</orderid>
                <amount currency='$trans_currency'>$amount</amount>
                <card><number>$cardnum</number>
                <expdate>$expdate</expdate>
                <type>$cardtype</type>
                <chname>$firstname $lastname</chname>
                <issueno>$issue_num</issueno>
                <cvn><number>$cvn</number>
                <presind>1</presind></cvn></card>
                <autosettle flag='0'/><comments>
                <comment id='1'>$postcode</comment></comments>
                <tssinfo><varref>$user_email</varref>
                <custipaddress>$user_ip</custipaddress></tssinfo>
                <md5hash>$md5hash</md5hash>
            </request>";

        //Send the request array to Realex Payments
        exec("/usr/bin/curl -s -m 120 -d \"$xml\" $re_URL -L", $return_message_array, $return_number) or die('Sorry, we are unable to contact our payment gateway at the moment.');

        $xmlObj = simplexml_load_string($return_message_array[1]);
        $arrXml = $this->objectsIntoArray($xmlObj);

        $auth_response = $arrXml['result'];

        if ($auth_response == '00') {
            $success = true;
            $auth_transaction_id = $arrXml['authcode'];
            $auth_transaction_pasref = $arrXml['pasref'];
            $auth_amount = $auth_amount;
            $auth_currency = $trans_currency;
            $auth_successful = 1;
            $auth_response = self::TRANSACTION_SUCCESS;
        }
        else {
            $success = false;
            $auth_transaction_id = null;
            $auth_transaction_pasref = null;
            $auth_amount = $auth_amount;
            $auth_currency = $trans_currency;
            $auth_successful = null;
            $auth_response = self::TRANSACTION_FAILURE;
        }

        // Save to the Auth Response Packet
        $this->auth_response_packet = $arrXml + array(
            'success'            => $success,
            'our_reference'      => $our_reference,
            'type'               => 'amex_preauth',
            'cs_method'          => __METHOD__,
            'transaction_id'     => $auth_transaction_id,
            'transaction_pasref' => $auth_transaction_pasref,
            'amount'             => $amount,
            'currency'           => $auth_currency,
            'auth_successful'    => $auth_successful,
            'auth_response'      => $auth_response,
            'auth_code'          => '',
            'response' => '00',
        );

        return $auth_response;
    }

    /**
     *
     * @param string $our_reference Our own reference to the transaction
     * @param string $transaction_pasref
     * @param string $transaction_id
     * @param string $comment Reason for voiding transaction
     * @return string
     */
    public function voidAmex($our_reference, $transaction_pasref, $transaction_id, $comment)
    {
        $merchantid = "crowdsurge";
        $secret     = "YWWyxXV4bb";
        $account    = 'amex';
        $re_URL     = 'https://epage.payandshop.com/epage-remote.cgi';

        //
        $curl_return = '';
        $curl_response = '';

        //Hash
        $timestamp = strftime('%Y%m%d%H%M%S');
        $p_orderid = $our_reference;
        $tmp = "$timestamp.$merchantid.$p_orderid...";
        $md5hash = md5($tmp);

        $tmp = "$md5hash.$secret";
        $md5hash = md5($tmp);

        $xml = "<request timestamp='$timestamp' type='void'><merchantid>$merchantid</merchantid><account>$account</account><orderid>$p_orderid</orderid><pasref>$transaction_pasref</pasref><authcode>$transaction_id</authcode><comments><comment id='1'>$comment</comment></comments><md5hash>$md5hash</md5hash></request>";

        //Send the request array to Realex Payments
        exec("/usr/bin/curl -s -m 120 -d \"$xml\" $re_URL -L", $curl_return, $curl_response) or die('Sorry, we are unable to contact our payment gateway at the moment.');

        $xmlObj = simplexml_load_string($curl_return[1]);

        $arrXml = $this->objectsIntoArray($xmlObj);
        $void_response = $arrXml['result'];

        // Set the auth_response_packet to the
        $this->auth_response_packet = $void_response;

        return $void_response;
    }

    /**
     *
     * @param string $cardtype delta/mastercard etc
     * @param string $card_num 0000000000000000
     * @param string $start_month 12
     * @param string $start_year 12
     * @param string $amount 3.99
     * @param string $trans_currency GBP/USD/EUR
     * @param string $end_month 12
     * @param string $end_year 16
     * @param string $cvn 123
     * @param string $firstname Ian
     * @param string $lastname  Lewis
     * @param string $addr_line1 86a Lordship Park
     * @param string $towncity London
     * @param string $countystate Greater London
     * @param string $postcode N16 5UA
     * @param string $country UK
     * @param string $user_email ian.lewis@crowdsurge.com
     * @param string $user_ip 86.188.247.250
     * @return string Authorisation success/fail
     */
    public function preauthPayPal($cardtype, $card_num, $start_month, $start_year, $amount,
            $trans_currency, $end_month, $end_year, $cvn, $firstname, $lastname, $addr_line1,
            $towncity, $countystate, $postcode, $country, $user_email, $user_ip)
    {
        //Send Auth request to PayPal
        $METHOD = 'DoDirectPayment';

        // This means PreAuthorisation
        $PAYMENTACTION = 'Authorization';
        // Create the URL String for the POST
        $nvpstr = '&AMT='.$amount.'&CURRENCYCODE='.$trans_currency.'&PAYMENTACTION='.$PAYMENTACTION.'&CREDITCARDTYPE='.$cardtype.'&ACCT='.$card_num.'&STARTDATE='.$start_month.$start_year.'&EXPDATE='.$end_month.$end_year.'&CVV2='.$cvn.'&FIRSTNAME='.$firstname.'&LASTNAME='.$lastname.'&STREET='.$addr_line1.'&CITY='.$towncity.'&STATE='.$countystate.'&ZIP='.$postcode.'&COUNTRYCODE='.$country.'&EMAIL='.$user_email.'&IPADDRESS='.$user_ip;

        $resArray = $this->hash_call($METHOD, $nvpstr, $trans_currency);
        $auth_response = strtolower($resArray['ACK']);

        if ($auth_response == 'success' || $auth_response == 'successwithwarning') {
            $success = true;
            $auth_transaction_id = $resArray['TRANSACTIONID'];
            $auth_transaction_pasref = $resArray['TRANSACTIONID'];
            $auth_amount = $resArray['AMT'];
            $auth_currency = $resArray['CURRENCYCODE'];
            $auth_code     = $resArray['CORRELATIONID'];
            $auth_successful = 1;
        }
        else {
            $success = false;
            $auth_transaction_id = $resArray['TRANSACTIONID'];
            $auth_transaction_pasref = $resArray['TRANSACTIONID'];
            $auth_amount = $resArray['AMT'];
            $auth_currency = $resArray['CURRENCYCODE'];
            $auth_code     = $resArray['CORRELATIONID'];
            $auth_successful = 0;
        }

        // Save to the Auth Response Packet
        $this->auth_response_packet = $resArray + array(
            'success'            => $success,
            'type'               => 'paypal_preauth',
            'cs_method'          => __METHOD__,
            'transaction_id'     => $auth_transaction_id,
            'transaction_pasref' => $auth_transaction_pasref,
            'amount'             => $auth_amount,
            'currency'           => $auth_currency,
            'auth_successful'    => $auth_successful,
            'auth_response'      => $auth_response,
            'auth_code'          => $auth_code,
            'response' => '00',
        );

        return $auth_response;
    }

    /**
     *
     * @param string $cardtype delta/mastercard etc
     * @param string $card_num 0000000000000000
     * @param string $start_month 12
     * @param string $start_year 12
     * @param string $amount 3.99
     * @param string $trans_currency GBP/USD/EUR
     * @param string $end_month 12
     * @param string $end_year 16
     * @param string $cvn 123
     * @param string $firstname Ian
     * @param string $lastname  Lewis
     * @param string $addr_line1 86a Lordship Park
     * @param string $towncity London
     * @param string $countystate Greater London
     * @param string $postcode N16 5UA
     * @param string $country UK
     * @param string $user_email ian.lewis@crowdsurge.com
     * @param string $user_ip 86.188.247.250
     * @return string Authorisation success/fail
     */
    public function paymentPayPal($cardtype, $card_num, $start_month, $start_year, $amount,
            $trans_currency, $end_month, $end_year, $cvn, $firstname, $lastname, $addr_line1,
            $towncity, $countystate, $postcode, $country, $user_email, $user_ip)
    {
        //Send request to PayPal
        $METHOD = 'DoDirectPayment';

        // This means an immediate request for payment
        $PAYMENTACTION = 'Sale';
        // Create the URL String for the POST
        $nvpstr = '&AMT='.$amount.'&CURRENCYCODE='.$trans_currency.'&PAYMENTACTION='.$PAYMENTACTION.'&CREDITCARDTYPE='.$cardtype.'&ACCT='.$card_num.'&STARTDATE='.$start_month.$start_year.'&EXPDATE='.$end_month.$end_year.'&CVV2='.$cvn.'&FIRSTNAME='.$firstname.'&LASTNAME='.$lastname.'&STREET='.$addr_line1.'&CITY='.$towncity.'&STATE='.$countystate.'&ZIP='.$postcode.'&COUNTRYCODE='.$country.'&EMAIL='.$user_email.'&IPADDRESS='.$user_ip;

        $resArray = $this->hash_call($METHOD, $nvpstr, $trans_currency);
        $auth_response = strtolower($resArray['ACK']);

        if ($auth_response == 'success') {
            $success = true;
            $auth_transaction_id = $resArray['TRANSACTIONID'];
            $auth_transaction_pasref = $resArray['TRANSACTIONID'];
            $auth_amount = $resArray['AMT'];
            $auth_currency = $resArray['CURRENCYCODE'];
            $auth_code     = $resArray['CORRELATIONID'];
            $auth_successful = 1;
        }
        else {
            $success = false;
            $auth_transaction_id     = $resArray['TRANSACTIONID'];
            $auth_transaction_pasref = $resArray['TRANSACTIONID'];
            $auth_amount             = $resArray['AMT'];
            $auth_currency           = $resArray['CURRENCYCODE'];
            $auth_code               = $resArray['CORRELATIONID'];
            $auth_successful         = 0;
        }

        // Save to the Auth Response Packet
        $this->auth_response_packet = $resArray + array(
            'success'            => $success,
            'type'               => 'paypal_preauth',
            'cs_method'          => __METHOD__,
            'transaction_id'     => $auth_transaction_id,
            'transaction_pasref' => $auth_transaction_pasref,
            'amount'             => $auth_amount,
            'currency'           => $auth_currency,
            'auth_successful'    => $auth_successful,
            'auth_response'      => $auth_response,
            'auth_code'          => $auth_code,
            'response' => '00',
        );

        return $auth_response;
    }


    /**
     *
     * @param string $transaction_id
     * @param string $comment
     * @return string
     */
    public function voidPayPal($transaction_id, $comment)
    {
        $METHOD = 'DoVoid';

        $nvpstr = '&AUTHORIZATIONID='.$transaction_id.'&NOTE='.$comment;

        $resultArray = $this->hash_call($METHOD, $nvpstr, $auction_trans_currency);
        $void_response = $resultArray['ACK'];

        $this->auth_response_packet = $resultArray;

        return $void_response;
    }

    /**
     *
     * @param boolean $clear Set true if you will be using the instance multiple times
     */
    public function getLastTransactionResult($clear = false)
    {
        $packet = $this->auth_response_packet;

        if ($clear) {
            $this->auth_response_packet = array();
        }

        return $packet;
    }

    public function isSSL(){
        if ($_SERVER['SERVER_PORT'] == 443) {
            return true;
        }
        else {
            return false;
        }
    }

    private function luhn_check($number) {
        $number=preg_replace('/\D/', '', $number);
        $number_length=strlen($number);
        $parity=$number_length % 2;
        $total=0;
        for ($i=0; $i<$number_length; $i++) {
            $digit=$number[$i];
            if ($i % 2 == $parity) {
                $digit*=2;
                if ($digit > 9) { $digit-=9; }
            }
            $total+=$digit;
        }
        return ($total % 10 == 0) ? TRUE : FALSE;
    }

    private function deformatNVP($nvpstr) {
        $intial = 0;
        $nvpArray = array();
        while(strlen($nvpstr)){
            $keypos= strpos($nvpstr,'=');
            $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);
            $keyval=substr($nvpstr,$intial,$keypos);
            $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
            $nvpArray[urldecode($keyval)] =urldecode( $valval);
            $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
         }
        return $nvpArray;
    }

    /**
     * Refers to PayPal transactions
     * @param type $methodName
     * @param type $nvpStr
     * @param type $trans_curr
     * @return type
     */
    private function hash_call($methodName, $nvpStr, $trans_curr) {

        //Account details
        if ($trans_curr == 'USD') {
            $API_UserName = 'paypal.us_api1.crowdsurge.com';
            $API_Password = 'XN78XGT5V8PB2BD5';
            $API_Signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AIJTFUwVig4yf7xFIysju1x1BWVu';
            $API_Endpoint = 'https://api-3t.paypal.com/nvp';
        }
        else {
            $API_UserName = 'ashley.dexter_api1.crowdsurge.com';
            $API_Password = '5XPCFZSDTZQZRDNX';
            $API_Signature = 'AS6ZFys9FZC6uMrGRn.4fma1tTNlAImjvJLoRVvy7RlDAFckrm.toZ3Y';
            $API_Endpoint = 'https://api-3t.paypal.com/nvp';
        }

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);

        //NVPRequest for submitting to server
        $nvpreq = 'METHOD='.urlencode($methodName).'&VERSION=64&PWD='.urlencode($API_Password).'&USER='.urlencode($API_UserName).'&SIGNATURE='.urlencode($API_Signature).$nvpStr.'&BUTTONSOURCE=PP-ECWizard';

        //setting the nvpreq as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
        $response = curl_exec($ch);

        //$response = 'TIMESTAMP=2012%2d05%2d15T09%3a36%3a25Z&CORRELATIONID=40dc99e2ce61&ACK=Success&VERSION=64&BUILD=2929894&AMT=0%2e01&CURRENCYCODE=GBP&AVSCODE=F&CVV2MATCH=M&TRANSACTIONID=2DD17561WW170020D';

        //convrting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);
        $nvpReqArray = $this->deformatNVP($nvpreq);

        if (curl_errno($ch)) {
            // moving to display page to display curl errors
            $_SESSION['curl_error_no'] = curl_errno($ch) ;
            $_SESSION['curl_error_msg'] = curl_error($ch);
        }
        else {
            curl_close($ch);
        }

        return $nvpResArray;
    }

    private function objectsIntoArray($arrObjData, $arrSkipIndices = array()){
        $arrData = array();
        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }
        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }

    /**
     * This follows on from cardPreAuth() contains a lotof logic we may not need
     * here though.
     * @param type $auth_successful
     */
    function checkAuthorisationSuccess($auth_successful)
    {
        throw new Exception("Do not use");
        //Check Auth Success
        if ($auth_successful == 1) {

            //MONGO Update bid_request
            $col->update(array('_id' => new MongoId($new_bid_id)), array('$set' => array('auth_response' => $auth_response,'auth_transaction_id' => $auth_transaction_id,'auth_transaction_pasref' => $auth_transaction_pasref,'auth_amount' => $auth_amount,'auth_currency' => $auth_currency)));

            //Tmp max bid split
            $tmp_p_max_bid = number_format($p_max_bid,2);
            $tmp_p_max_bid_pieces = explode('.',$tmp_p_max_bid);
            $tmp_p_max_bid_int = (int)$tmp_p_max_bid_pieces[0];
            $tmp_p_max_bid_dec = (int)$tmp_p_max_bid_pieces[1];
            $tmp_p_max_bid_pence = $tmp_p_max_bid_int.$tmp_p_max_bid_dec;

            //$old_bid_date
            $d = new DateTime($date);
            $d->modify('-2 minutes');
            $old_bid_date = $d->format('Y-m-d H:i:s');

            //Double check this user hasn't been outbid/matched bid during script run
            $still_highest_bidder = 0;
            $sh_col = $db->selectCollection('bid_history');
            //$sh_curs = $sh_col->find(array('auction_id' => $p_auction_id,'bid_max_pence' => array('$gte' => $tmp_p_max_bid_pence),'email' => array('$ne' => $p_u_email),'process' => 'noerror'));
            $sh_curs = $sh_col->find(array('auction_id' => $p_auction_id,'bid_date' => array('$gt' => $old_bid_date),'email' => array('$ne' => $p_u_email),'process' => 'noerror'));
            if ($sh_curs->count() == 0) {
                $still_highest_bidder = 1;
            }

            if ($still_highest_bidder == 1) {
                //Void previous bid
                if (!empty($auction_bid_id)) {

                    // If we have a bid id for the auction
                    // This is how to void for Amex followed by how to void for
                    // PayPal
                    if ($auction_bid_auth_cardtype == 'Amex') {
                        //Reset array
                        $return_message_array = '';
                        $return_number = '';

                        //Hash
                        $timestamp = strftime('%Y%m%d%H%M%S');
                        $p_orderid = $auction_bid_id->{'$id'};
                        $tmp = "$timestamp.$re_merchantid.$p_orderid...";
                        $md5hash = md5($tmp);
                        $tmp = "$md5hash.$re_secret";
                        $md5hash = md5($tmp);

                        $xml = "<request timestamp='$timestamp' type='void'><merchantid>$re_merchantid</merchantid><account>$re_account</account><orderid>$p_orderid</orderid><pasref>$auction_bid_auth_pasref</pasref><authcode>$auction_bid_auth_transaction_id</authcode><comments><comment id='1'>RED Auction Outbid Notice</comment></comments><md5hash>$md5hash</md5hash></request>";

                        //Send the request array to Realex Payments
                        exec("/usr/bin/curl -s -m 120 -d \"$xml\" $re_URL -L", $return_message_array, $return_number) or die('Sorry, we are unable to contact our payment gateway at the moment.');

                        $xmlObj = simplexml_load_string($return_message_array[1]);
                        $arrXml = $this->objectsIntoArray($xmlObj);
                        $void_response = $arrXml['result'];
                    }
                    else {
                        $METHOD = 'DoVoid';
                        $note = 'RED Auction Outbid Notice';
                        $nvpstr = '&AUTHORIZATIONID='.$auction_bid_auth_transaction_id.'&NOTE='.$note;
                        $v_resArray = $this->hash_call($METHOD, $nvpstr, $auction_trans_currency);
                        $void_response = $v_resArray['ACK'];
                    }
                    //print_r($v_resArray);
                    $col->update(array('_id' => $auction_bid_id), array('$set' => array('void_response' => $void_response,'void_date' => $date,'process' => 'voided')));
                }

                //Update auction current bid
                if ($p_u_email == $auction_bid_email) {
                    $new_bid_current = $auction_bid_current;
                }
                else if ($auction_bid_current_max + $p_bid_increment >= $p_max_bid) {
                    $new_bid_current = $p_max_bid;
                }
                else {
                    $new_bid_current = $auction_bid_current_max + $p_bid_increment;
                }

                //Tmp bid current split
                $tmp_new_bid_current = number_format($new_bid_current,2);
                $tmp_new_bid_current_pieces = explode('.',$tmp_new_bid_current);
                $tmp_new_bid_current_int = (int)$tmp_new_bid_current_pieces[0];
                $tmp_new_bid_current_dec = (int)$tmp_new_bid_current_pieces[1];

                $col = $db->selectCollection('auctions');
                $col->update(array('_id' => $auction_id), array('$set' => array('bid_current' => $new_bid_current,'bid_current_int' => $tmp_new_bid_current_int, 'bid_current_dec' => $tmp_new_bid_current_dec,'bid_current_max' => $p_max_bid,'bid_current_max_int' => $tmp_p_max_bid_int,'bid_current_max_dec' => $tmp_p_max_bid_dec,'bid_name' => $p_display_name,'bid_city' => $p_ub_towncity,'bid_email' => $p_u_email,'bid_id' => $new_bid_id,'bid_modified' => $date,'bid_auth_transaction_id' => $auth_transaction_id,'bid_auth_cardtype' => $p_up_cardtype,'bid_auth_pasref' => $auth_transaction_pasref)));
                $update_static_content = 1;

                $col = $db->selectCollection('bid_history');
                $col->update(array('_id' => new MongoId($new_bid_id)), array('$set' => array('process' => 'complete')));

                //Format for emails
                $auction_bid_current = number_format($auction_bid_current,2);
                $auction_bid_current_max = number_format($auction_bid_current_max,2);
                $new_bid_current = number_format($new_bid_current,2);
                $p_max_bid = number_format($p_max_bid,2);

                //Log Emails
                $col = $db->selectCollection('bid_emails');

                //Outbid notification
                if ($p_u_email != $auction_bid_email) {
                    //Email: $auction_bid_email // Name: $auction_bid_name
                    $subject = 'You\'ve been outbid!';
                    $body = 'Hi '.$auction_bid_name.', You have been outbid for '.$auction_bid_title.' <br /> Please try again.';
                    include('email_outbid.php');
                    $data = array('date' => $date,'email' => $auction_bid_email,'name' => $auction_bid_name,'type' => 'OUTBID','auction_id' => $auction_id,'subject' => $subject,'body' => $body,'status' => 'pending');
                    $col->insert($data);
                }

                //Bid accepted notification
                //Email: $p_u_email // Name: $display_name
                $subject = 'Your bid has been accepted!';
                $body = 'Hi '.$p_display_name.', Your bid has been accepted for '.$auction_bid_title.' <br /> Thank you.';
                include('email_highest_bidder.php');

                $data = array('date' => $date,'email' => $p_u_email,'name' => $p_display_name,'type' => 'ACCEPTED','auction_id' => $auction_id,'subject' => $subject,'body' => $body,'status' => 'pending');
                $col->insert($data);

                //Update Static Content
                if ($update_static_content == 1) {
                    //Update the static file for this auction and save...
                }

                //Display order_confirm
                $p_u_email = stripslashes(htmlspecialchars($p_u_email));
                include('bid_confirm.php');
                exit;

            }
            else {
                //Void posted bid

                // VOIDING AMEX
                if ($p_up_cardtype == 'Amex') {
                    //Reset array
                    $return_message_array = '';
                    $return_number = '';

                    //Hash
                    $timestamp = strftime('%Y%m%d%H%M%S');
                    $p_orderid = $new_bid_id_str;
                    $tmp = "$timestamp.$re_merchantid.$p_orderid...";
                    $md5hash = md5($tmp);
                    $tmp = "$md5hash.$re_secret";
                    $md5hash = md5($tmp);

                    $xml = "<request timestamp='$timestamp' type='void'><merchantid>$re_merchantid</merchantid><account>$re_account</account><orderid>$p_orderid</orderid><pasref>$auth_transaction_pasref</pasref><authcode>$auth_transaction_id</authcode><comments><comment id='1'>RED Auction Outbid Notice</comment></comments><md5hash>$md5hash</md5hash></request>";

                    //Send the request array to Realex Payments
                    exec("/usr/bin/curl -s -m 120 -d \"$xml\" $re_URL -L", $return_message_array, $return_number) or die('Sorry, we are unable to contact our payment gateway at the moment.');

                    $xmlObj = simplexml_load_string($return_message_array[1]);
                    $arrXml = objectsIntoArray($xmlObj);
                    $void_response = $arrXml['result'];
                }
                else {
                    $METHOD = 'DoVoid';
                    $note = 'RED Auction Outbid Notice';
                    $nvpstr = '&AUTHORIZATIONID='.$auth_transaction_id.'&NOTE='.$note;
                    $v_resArray = $this->hash_call($METHOD, $nvpstr, $auction_trans_currency);
                    $void_response = $v_resArray['ACK'];
                }
                //print_r($v_resArray);
                $col->update(array('_id' => $new_bid_id), array('$set' => array('void_response' => $void_response,'void_date' => $date,'fatal_error' => 1,'process' => 'instantvoid')));

                //Fatal Error
                $fatalerror = 'Sorry, it seems an issue has occurred while processing your bid. Please check that your payment details are correct.
                               If the problem continues, please contact your bank or try a different card. Alternately, another user may have simultaneously
                               submitted an identical bid, preventing us from finalizing your submission.<br /><br />
                               <a href="bid.php?p='.$p_auction_id.'&int='.$p_bid_increment.'" >Please click here to try again.</a></div><br />
                               <div style="margin-top: 300px; text-align:center;"><img src="images/scrollup.gif" alt="Scroll up" />';
            }
        }
        else {
            //Update bid request
            $col->update(array('_id' => new MongoId($new_bid_id)), array('$set' => array('auth_response' => $auth_response,'process' => 'noauth')));

            $error .= '<li>We were unable to authorize your maximum bid amount using the payment details provided. Please contact your bank or try a different card.</li>';
        }
    }

    function getDefaultCurrencyForCountryId($country_id)
    {
        //Set default
        $currency = '1|GBP';

        $countries = new Auction_Model_DataGateway_Countries();
        $resultSet = $countries->selectCountryDefaultCurrency($country_id);
        if ($resultSet) {
            $currency = $resultSet[0]['default_currency_id'].'|'.$resultSet[0]['description'];
        }

        return $currency;
    }

}
