<?php

/**
 * Description of User
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Auction_Service_User {

    public function getUserData($id)
    {
        $table = new Auction_Model_DataGateway_Users();

        $result = $table->find($id);
        $user = $result->current();
        if (get_class($user) == 'Zend_Db_Table_Row') {
            return $user->toArray();
        }
        return $user;
    }

    public function registerUser($data)
    {
        $table = new Auction_Model_DataGateway_Users();
        $user = array(
            'username' => $data['registration_username'],
            'password' => sha1($data['registration_password']),
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'],
            'email'      => $data['email_address'],
            'phone'      => isset($data['phone']) ? $data['phone'] : null,
            'create_date' => new Zend_Db_Expr(" NOW() "),
        );

        return $table->insert($user);
    }

    /**
     * Successful login resets a few of the timers/counters
     * @param int $userId
     */
    public function updateUserLoginTime($userId)
    {
        $table = new Auction_Model_DataGateway_Users();
        $table->update(
                array(
                    'last_login' => new Zend_Db_Expr(" NOW() "),
                    'failed_attempts'   => null,
                    'locked_until'      => null,
                    'reset_pass_hash'   => null,
                    'reset_pass_expire' => null,
                ),
                "user_id = $userId"
            );
    }

    public function updateMailerOption($userId, $choice = 0)
    {
        $table = new Auction_Model_DataGateway_Users();
        $table->update(
                array(
                    'mailer_optin' => $choice,
                ),
                "user_id = $userId"
            );
    }

    public function addUserWithAddresses($userData, $billingAddress, $shippingAddress)
    {
        $table = new Auction_Model_DataGateway_Users();

        $user = array(
            'username'   => $userData['username'],
            'password'   => sha1($userData['password']),
            'first_name' => $userData['first_name'],
            'last_name'  => $userData['last_name'],
            'email'      => $userData['email_address'],

        );
        $userId = $table->insert($user);

        $billing = new Auction_Model_DataGateway_UserBillingAddress();
        $address = $billingAddress;
        $address['user_id'] = $userId;

        $billing->insertAddress($address);
        unset($address);

        $shipping = new Auction_Model_DataGateway_UserShippingAddress();
        $address = $shippingAddress;
        $address['user_id'] = $userId;

        $shipping->insertAddress($address);

        return $userId;
    }

    public function fetchShippingAddressByUserId($userId)
    {
        if (empty($userId)) {
            throw new Exception("Cannot retrieve shipping address with userId: '$userId'");
        }
        $shipping = new Auction_Model_DataGateway_UserShippingAddress();

        $result = $shipping->selectActiveAddressesByUserId($userId);

        return $result;
    }

    public function fetchBillingAddressByUserId($userId)
    {
        if (empty($userId)) {
            throw new Exception("Cannot retrieve billing address with userId: '$userId'");
        }
        $billing = new Auction_Model_DataGateway_UserBillingAddress();

        $result = $billing->selectActiveAddressesByUserId($userId);

        return $result;
    }

    /**
     * Update the user validation for
     * @param type $userId
     * @param type $isValidated
     * @param type $validationMethod
     */
    public function updateUserValidation(
                $userId,
                $isValidated = true,
                $validationMethod = Auction_Model_DataGateway_UserValidated::VALIDATION_TYPE
            )
    {
        $userValid = new Auction_Model_DataGateway_UserValidated();
        $userValid->upsertUserValidation($userId, $isValidated, $validationMethod);
    }

    /**
     *
     * @param int $user_id
     * @return boolean
     */
    public function hasPreAuthed($user_id)
    {
        $db = new Auction_Model_DataGateway_UserValidated();
        $result = $db->find($user_id);

        $result = $result->toArray();
        if (count($result)) {
            $user = reset($result);
            if (array_key_exists('is_validated', $user)) {
                return (bool) intval($user['is_validated']);
            }
        }

        return false;
    }


    public function resetUserPass($email, $new_password)
    {
        $table = new Auction_Model_DataGateway_Users();

        $table->resetPassByEmail($email, $new_password);
    }

    public function changeUserPass($user_id, $new_password)
    {
        $table = new Auction_Model_DataGateway_Users();

        $table->changePassByUserId($user_id, $new_password);
    }

    public function checkUserPassByUserId($user_id, $current_password)
    {
        $table = new Auction_Model_DataGateway_Users();

        $result = $table->checkPassByUserId($user_id, $current_password);

        return $result;
    }

    public function getUserDataByEmail($email)
    {
        $table = new Auction_Model_DataGateway_Users();
        $result = $table->fetchAll("email = '$email'");

        $user = $result->current();
        // Check here specifically for the row class
        if ('Zend_Db_Table_Row' == get_class($user)) {
            return $user->toArray();
        }
        return $user;
    }

}

