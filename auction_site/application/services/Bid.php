<?php
/**
 * Description of Bid
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Service_Bid {

    /**
     * Fetch the bid history for the given auction item
     * @param int $auction_item_id
     * @return array (multidimensional)
     */
    public function  retrieveBidHistory($auction_item_id, $bid_status_ids = array())
    {
        $db = new Auction_Model_DataGateway_Bids();
        $result = $db->selectBidHistoryDetails($auction_item_id, $bid_status_ids);

        $bids = array();
        $highestBid = null;
        foreach ($result as $row) {
            $bids[] = $row;

            if ((int) $row['bid_status_id'] === (int) Auction_Model_DataGateway_Bids::BID_CURRENT_HIGHEST) {
                $highestBid = $row;
            }
        }
        // If we don't have a highest bid we are awaiting first bid.
        $is_awaiting_first_bid = null === $highestBid;

        return array(
            'user_id'               => $highestBid['user_id'],
            'history'               => $bids,
            'highest'               => $highestBid,
            'is_awaiting_first_bid' => $is_awaiting_first_bid,
        );
    }

    public function retrieveLastBid($user_id, $item_id)
    {
        $db = new Auction_Model_DataGateway_Bids();
        $result = $db->selectBidHistoryByUserIdByItemId($user_id, $item_id);
        
        return $result;
    }

}

