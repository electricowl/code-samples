<?php

/**
 * Description of Auction_View_Helper_MergeForMinify (GetStaticContent.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Helper)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Auction_View_Helper_MergeForMinify extends Zend_View_Helper_Abstract
{
    const CUSTOM_CONTENT_DIR    = 'themes';
    const OUTPUT_DIRECTORY      = "{BASE_PATH}/public";
    const CSS_EXTENSION         = '.css';
    const JS_EXTENSION          = '.js';


    public function mergeForMinify($app_theme, $extension, $output_dir = null) {

                $folder = implode(DIRECTORY_SEPARATOR, array(
                        BASE_PATH,
                        self::CUSTOM_CONTENT_DIR,
                        $app_theme,
                        $extension
                    )
                );

                if (is_dir($folder) && is_writable($folder)) {
                    $output_file = implode(DIRECTORY_SEPARATOR, array(
                        self::OUTPUT_DIRECTORY,
                        $extension,
                        $app_theme . "-combined." . $extension
                    ));

                    $tmp_time = time();

                    foreach (glob($folder . "/*.{$extension}") as $file) {
                        file_put_contents(
                            $output_file . $tmp_time,
                            file_get_contents($file),
                            FILE_APPEND
                        );
                    }
                    rename($output_file . $tmp_time, $output_file);
                    unlink($output_file . $tmp_time);
                }


        return false;
    }

}

