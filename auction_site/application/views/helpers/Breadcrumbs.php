<?php

/**
 * Description of Auction_View_Helper_Breadcrumbs (Breadcrumbs.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Auction_View_Helper_Breadcrumbs extends Zend_View_Helper_Abstract
{

    /**
     * Pass in
     * array(
     *   array(
     *      'url' => '/my/page/id',
     *      'desc' => 'My Title',
     *   ),
     *   array(
     *      'url' => '/my/other-page/id',
     *      'desc' => 'My Other Title',
     *   )
     * )
     * @param array $data
     * @return string
     */
    public function breadcrumbs($data)
    {
        $crumbs = array();
        // Let's assume the following
        $template = '<a href="%url">%desc</a>';
        $separator = ">>";

        foreach($data as $href) {

            $item = str_replace("%url", $href['url'], $template);
            $item = str_replace("%href", $href['href'], $item);
            $crumbs[] = $item;
            unset($item);
        }

        return implode($separator, $crumbs);
    }

}

