<?php

/**
 * Description of GetStaticContent (GetStaticContent.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Helper)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Auction_View_Helper_GetCustomContent extends Zend_View_Helper_Abstract
{
    const CUSTOM_CONTENT_DIR = 'views/custom_content';
    const TYPE_STATIC_CONTENT     = 'static';
    const TYPE_DYNAMIC_CONTENT    = 'dynamic';
    const STATIC_EXTENSION   = '.html';
    const DYNAMIC_EXTENSION  = '.phtml';


    public function getCustomContent($content_filename, $type = self::TYPE_STATIC_CONTENT, $data = array()) {

        $fc = Zend_Controller_Front::getInstance();
        $request = $fc->getRequest();
        $controllerName = $request->getControllerName();

        $content_filename .= self::TYPE_STATIC_CONTENT == $type ? self::STATIC_EXTENSION : self::DYNAMIC_EXTENSION;

            if (self::TYPE_STATIC_CONTENT == $type) {
                $filename = implode(DIRECTORY_SEPARATOR, array(
                    APPLICATION_PATH,
                    self::CUSTOM_CONTENT_DIR,
                    $controllerName,
                    $type,
                    $content_filename
                )
                );
                if (is_readable($filename)) {
                    return file_get_contents($filename);
                }

            }
            else if (self::TYPE_DYNAMIC_CONTENT == $type){
                $filename = implode(DIRECTORY_SEPARATOR, array(
                        $controllerName,
                        $type,
                        $content_filename
                    ));
                $customPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . self::CUSTOM_CONTENT_DIR;
                if (is_readable($customPath . DIRECTORY_SEPARATOR . $filename)) {
                    $content = $this->view->partial(
                        $filename,
                        $data
                    );

                    return $content;
                }

            }

        return false;
    }

}

