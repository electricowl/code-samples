<?php

/**
 * Description of Auction_View_Helper_MergeThemeJs (GetStaticContent.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Helper)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Auction_View_Helper_MergeThemeJs extends Zend_View_Helper_Abstract
{
    const EXTENSION         = 'js';

    public function mergeThemeJs($app_theme) {
                $extension = self::EXTENSION;

                $folder = implode(DIRECTORY_SEPARATOR, array(
                        THEMES_PATH,
                        $app_theme,
                        $extension
                    )
                );

                if (! is_dir($folder) || ! is_readable($folder)) {
                    throw new Exception("Can't read $folder");
                }

        /** @var Zend_Cache_Core $cache **/
        $cache = Zend_Registry::get('cache');

        $cache_id = "{$app_theme}_{$extension}_cached";
        //$merge_file = $app_theme . "-merged." . $extension;

        if (($data = $cache->load($cache_id)) === false ) {

            ob_start();
            foreach (glob($folder . "/*.{$extension}") as $file) {
                echo file_get_contents($file);
                echo "\n";
            }
            $data = ob_get_contents();
            ob_end_clean();

            $cache->save($data);
        }

        return $data;
    }

}

