<?php
/**
 * Description of Auction_Form_AddEditAuctionItem (AddEditAuctionItem.php)
 * @package Crowdsurge White Label Auction site
 * @category Forms
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Form_AddEditAuctionItem extends Cs_Form_Abstract
{
    public function init()
    {
        $this->addElement('hidden',
                'auction_item_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),

                )
            );

        $this->addElement('hidden',
                'auction_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required' => true,
                )
            );
        $service = new Auction_Service_AuctionItem();

        $this->addElement('text',
                'auction_item_name',
                array(
                    'label' => "Auction Item Name",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 40
                    )
                )
            );

        $this->addElement('select',
                'auction_item_type_id',
                array(
                    'label'        => "Auction Type",
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => $service->getTypes(),
                )
            );

        $this->addElement('text',
                'auction_item_custom_ref',
                array(
                    'label' => "Auction Custom Reference",
                    'required' => false,
                    'allowEmpty' => true,
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 40
                    )
                )
            );

        $this->addElement('textarea',
                'auction_item_description',
                array(
                    'label' => "Auction Item Description",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 40
                    )
                )
            );

        $this->addElement('text',
                'start_date',
                array(
                    'label' => "Start Date",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'validators' => array(
                        new Zend_Validate_Date()
                    ),
                    'attribs' => array(
                        'size' => 10
                    )
                )
            );

        $this->addElement('text',
                'start_time',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'attribs' => array(
                        'size' => 5
                    ),
                )
            );

        $this->addElement('text',
                'end_date',
                array(
                    'label' => "End Date",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'validators' => array(
                        new Zend_Validate_Date()
                    ),
                    'attribs' => array(
                        'size' => 10
                    )
                )
            );

        $this->addElement('text',
                'end_time',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'attribs' => array(
                        'size' => 5
                    ),
                )
            );

        $tzLu = new Auction_Model_DataGateway_TimezoneLu();
        $timezones = $tzLu->fetchAll();
        $options = array();
        foreach ($timezones as $tz) {
            $options[$tz['timezone_id']] = $tz['timezone'] . " ({$tz['gmt_offset']})";
        }

        $this->addElement('select',
                'timezone_id',
                array(
                    'label' => "Time Zone",
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => $options,
                )
            );
        unset($options);

        $currLu = new Auction_Model_DataGateway_CurrencyLu();
        $currencies = $currLu->fetchAll();

        $options = array();
        foreach ($currencies as $cu) {
            $options[$cu['currency_id']] = $cu['description'] . " ({$cu['symbol']})";
        }

        $this->addElement('select',
                'currency_id',
                array(
                    'label' => "Currency",
                    'decorators'   => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => $options,
                )
            );

        $this->addElement('text',
                'min_bid_increment',
                array(
                    'label' => "Minimum Bid Increment",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 20
                    )
                )
            );

        $this->addElement('text',
                'max_bid_increment',
                array(
                    'label' => "Maximum Bid Increment",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 20
                    )
                )
            );

        $this->addElement('text',
                'starting_bid',
                array(
                    'label' => "Starting Bid",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 20
                    ),
                    'default' => "0.99"
                )
            );

        $this->addElement('text',
                'reserve',
                array(
                    'label' => "Reserve Price",
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 20
                    )
                )
            );

        $this->addElement('text',
                'display_order',
                array(
                    'label' => "Display Order",
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 5
                    )
                )
            );

        $this->addSubmit("Save");

    }

    /**
     * Override setDefaults as we need to do a bit of manipulation of the datetime
     * @param type $defaults
     */
    public function setDefaults(array $defaults)
    {
        parent::setDefaults($defaults);

        $date_format = "Y-m-d H:i:s";
        // Now fix the time/date thing
        $start = DateTime::createFromFormat(
                $date_format,
                $this->getElement('start_date')->getValue()
            );
        $this->getElement('start_date')->setValue($start->format("Y-m-d"));
        $this->getElement('start_time')->setValue($start->format("H:i"));

        $end = DateTime::createFromFormat(
                $date_format,
                $this->getElement('end_date')->getValue()
            );
        $this->getElement('end_date')->setValue($end->format("Y-m-d"));
        $this->getElement('end_time')->setValue($end->format("H:i"));
    }
}

