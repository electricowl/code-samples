<?php
/**
 * Description of AddEditAuction (AddEditAuction.php)
 * @package Crowdsurge White Label Auction site
 * @category Forms
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Form_AddEditAuction extends Cs_Form_Abstract
{

    public function init()
    {
        $this->addElement('hidden',
                'auction_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                )
            );

        $this->addElement('hidden',
                'campaign_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required' => true
                )
            );

        $this->addElement('text',
                'auction_name',
                array(
                    'label' => "Auction Name",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 40
                    )
                )
            );

        $this->addElement('text',
                'auction_custom_ref',
                array(
                    'label' => "Custom Reference",
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attribs' => array(
                        'size' => 40
                    )
                )
            );

        $this->addElement('textarea',
                'auction_description',
                array(
                    'label' => "Auction Description",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                )
            );

        $this->addElement('text',
                'display_order',
                array(
                    'label' => "Display Order",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'default' => 0
                )
            );

        $this->addElement('select',
                'auction_status_id',
                array(
                    'label' => "Auction Status",
                    'required' => true,
                    'multiOptions' => array(
                        Auction_Model_DataGateway_Auctions::STATUS_DRAFT => "Draft",
                        Auction_Model_DataGateway_Auctions::STATUS_CONFIRMED => "Confirmed",
                        Auction_Model_DataGateway_Auctions::STATUS_CLOSED => "Closed"
                    )
                )
            );

        $this->addSubmit("Save");

    }
}

