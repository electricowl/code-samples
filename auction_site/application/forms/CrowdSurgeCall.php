<?php

/**
 * Description of CrowdSurge Call
 *
 * @author Ryan Simms <ryan.simms@crowdsurge.com>
 */
class Auction_Form_CrowdSurgeCall extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setName('crowdsurge_call');

        $this->setAttrib('autocomplete', 'off');

        $this->addElement('select',
                'call',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "api.crowdsurge.com/auction/?key=[hidden]&call=",
                    'multioptions' => array("auction_items" => 'auction_items'),
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    )
                ));

        $this->addElement('text',
                'query_string',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "&",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    )
                ));

        $this->addSubmit('Request');

        // $this->setDecorators(
        //         $this->getFormDecorators()
        //     );

    }

}

