<?php

/**
 * Description of Address
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
abstract class Auction_Form_Address extends Cs_Form_Abstract
{
    public function init()
    {

        $this->addElement('hidden',
                'user_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement('text',
                'first_name',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "First Name",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                    'errorMessages' => array(
                        'Please provide your first name'
                    )
                ));

        $this->addElement('text',
                'last_name',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Last Name",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                ));

        $this->addElement('text',
                'address_line_1',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Address Line 1",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                    'errorMessages' => array(
                        'Please provide house number and street information'
                    )
                ));

        $this->addElement('text',
                'address_line_2',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Address Line 2",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                ));

        $this->addElement('text',
                'town_city',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Town/City",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Please enter the Town/City here'
                    )
                ));

        $this->addElement('text',
                'county_state',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "County/State",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Please enter the County/State here'
                    )
                ));

        $this->addElement('text',
                'post_zip_code',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Post/Zip Code",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Postcode/Zipcode is required'
                    )
                ));

        $countries = new Auction_Service_Countries();
        $selectCountries = $countries->fetchCountriesForSelect();

        // Country friendly name
        $this->addElement('select',
                'country_id',
                array(
                    'decorators' => $this->getElementDecorators(),
                    'label'      => "Country",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Country is required'
                    ),
                    'multiOptions' => $selectCountries,
                    'value' => 224,
                ));

        // holds the country text (via a bit of JS)
        $this->addElement('hidden',
                'country',
                array(
                    'decorators' => $this->getHiddenDecorators()

                ));

        $this->addElement('hidden',
                'is_default',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement('hidden',
                'is_active',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));
    }

}

