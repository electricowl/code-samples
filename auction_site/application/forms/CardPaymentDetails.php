<?php

/**
 * Description of Auction_Form_CardPaymentDetails
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Auction_Form_CardPaymentDetails extends Cs_Form_Abstract
{

    public function init()
    {
        $this->addElement('radio',
                'payment_gateway',
                array(
                    'label' => 'Payment Method',
                    'multiOptions' => array(
                        Cs_PaymentGateway_Payment::PAYPAL_DIRECT => 'Paypal Direct',
                        Cs_PaymentGateway_Payment::GATEWAY_PAYPAL_EXPRESS => 'Paypal Express',
                        Cs_PaymentGateway_Payment::GATEWAY_REALEX => 'RealEx'
                    ),
                    'value' => Cs_PaymentGateway_Payment::PAYPAL_DIRECT,
                    'decorators' => $this->getElementDecorators(),
                    'separator'  => '&nbsp',
                    'required'   => true
                ));

        $this->addElement('select',
                'card_type',
                array(
                    'label' => 'Card Type',
                    'description' => 'Select the type of card you will be using',
                    'required'   => true,
                    'decorators' => $this->getElementDecorators(),
                    'multiOptions' => array(
                        '' => '-- ')
                        + Auction_Service_CardTypes::formatCardListForSelectUseLabels(),
                    'errorMessages' => array(
                        'Select the type of card you are using'
                    )
                ));

        $this->addElement('text',
                'card_number',
                array(
                    'label' => 'Card Number',
                    'description' => 'Enter the long card number here',
                    'required'   => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecorators(),
                    'filters' => array(
                        'StringTrim',
                    ),
                    'validators' => array(
                        new Zend_Validate_CreditCard(array(
                            // These are the types we allow
                            Zend_Validate_CreditCard::AMERICAN_EXPRESS,
                            Zend_Validate_CreditCard::MASTERCARD,
                            Zend_Validate_CreditCard::VISA
                        )),
                    ),
                    'errorMessages' => array(
                        'A valid card number is required'
                    ),
                    'attributes' => array(
                        'size'      => 20,
                        'maxlength' => 16
                    ),
                ));

        $months = array();
        for($m = 1;$m <= 12; $m++){
            $month =  date("F", mktime(0, 0, 0, $m, 1));
            $months[$m] = $month;
        }

        $this->addElement('select',
                'expiry_month',
                array(
                    'label' => 'Expiry Month',
                    'required' => true,
                    'decorators' => $this->getElementDecorators(),
                    'multiOptions' => $months
                ));

        $years = array();
        $startYear = date('Y');
        $endYear   = date('Y', strtotime('+5 year'));
        for ($yr = $startYear; $yr <= $endYear; $yr++) {
            $years[$yr] = $yr;
        }

        $this->addElement('select',
                'expiry_year',
                array(
                    'label' => 'Expiry Year',
                    'required' => true,
                    'decorators' => $this->getElementDecorators(),
                    'multiOptions' => $years
                ));

        $this->addDisplayGroup(array(
                    'expiry_month',
                    'expiry_year'
                ),
                'expiry_group',
                array(
                    'legend' => 'Card Expiry Date'
                ));

        $this->addElement('text',
                'cvv_code',
                array(
                    'label' => 'CVV Code',
                    'description' => 'The last three digits on the strip on the back of the card',
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecorators(),
                    'attributes' => array(
                        'size'      => 4,
                        'maxlength' => 4
                    ),
                    'errorMessages' => array(
                        'Enter the 3-digit code from the strip on the back of your card'
                    )
                ));

    }

}

