<?php

/**
 * Description of Registration
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_ClientRegistration extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setName('client_registration');

        $this->setAttrib('autocomplete', 'off');

        $this->addElement('hidden',
                'form_action',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'value'      => 'client_registration'
                ));

        $this->addElement('text',
                'client_name',
                array(
                    'label' => 'Client Name',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters' => array(
                        'StringTrim'
                    ),
                ));

        $this->addElement('text',
                'client_theme_id',
                array(
                    'label' => 'Theme Identifier',
                    'description' => 'Theme id, no spaces! Lowercase',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters' => array(
                        'StringTrim',
                        new Zend_Filter_StringToLower(),
                        new Zend_Filter_PregReplace(
                                array(
                                    'match' => '#(\s|\t)#',
                                    'replace' => ''
                                ))
                    ),
                ));

        /**
         * @Zend_Validate_Db_NoRecordExists
         * Provides basic configuration for use with Zend_Validate_Db Validators
         * Setting $exclude allows a single record to be excluded from matching.
         *  Exclude can either be a String containing a where clause, or an
         * array with `field` and `value` keys to define the where clause added
         * to the sql. A database adapter may optionally be supplied to avoid
         * using the registered default adapter.  The following option keys are
         * supported: 'table' => The database table to validate against
         * @param 'schema' => The schema keys
         * @param 'field' => The field to check for a match
         * @param 'exclude' => An optional where clause or field/value pair to exclude
         * from the query
         * @param 'adapter' => An optional database adapter to use
         */
        $this->addElement('text',
                'client_staff_username',
                array(
                    'label' => 'Username',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'validators' => array(
                        new Zend_Validate_Db_NoRecordExists(array(
                            'adapter' => $this->getDbDefaultAdapter(),
                            'schema' => AUCTION_SCHEMA,
                            'table'  => 'client_staff',
                            'field'  => 'username',
                            'messages' => array(
                                Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'This username is not available'
                            )
                        )),
                    )
                ));

        $this->addElement('text',
                'client_staff_first_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "First Name",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),

                ));

        $this->addElement('text',
                'client_staff_last_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Last Name",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    )
                ));

        /**
         * 'hostname' => A hostname validator, see Zend_Validate_Hostname
         * 'allow' => Options for the hostname validator,
         * see Zend_Validate_Hostname::ALLOW_*
         * 'mx' => If MX check should be enabled, boolean
         * 'deep' => If a deep MX check should be done, boolean
         */
        $this->addElement('text',
                'client_staff_email',
                array(
                    'label' => 'Email Address',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim',
                        'StringToLower'
                    ),
                    'validators' => array(
                        new Zend_Validate_EmailAddress(array(
                            'allow' => Zend_Validate_Hostname::ALLOW_DNS,
                            'mx'    => false,
                            'deep'  => false
                        )),
                        new Zend_Validate_Db_NoRecordExists(array(
                            'adapter' => $this->getDbDefaultAdapter(),
                            'schema' => AUCTION_SCHEMA,
                            'table'  => 'client_staff',
                            'field'  => 'email',
                            'messages' => array(
                            Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'This email address is already taken'
                            )
                        )),
                    ),
                ));


        $this->addElement('password',
                'client_staff_password',
                array(
                    'label' => 'Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                ));

        $this->addElement('password',
                'client_staff_confirm_password',
                array(
                    'label' => 'Confirm Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'validators' => array(
                        new Zend_Validate_Identical('client_staff_password')
                    ),
                    'messages' => array(
                        'Passwords do not match :('
                    )
                ));

        $this->addSubmit('Register New Client');

        $this->setDecorators(
                $this->getFormDecorators()
            );

    }



}

