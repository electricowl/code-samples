<?php

/**
 * Description of ConfirmBid - metaform
 * This is a complex form so we are building it here and using a viewscript to
 * manage the display of the fields.
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Auction_Form_ConfirmBid extends Cs_Form_Abstract
{
    private $_auction_id  = null;
    private $_campaign_id = null;
    private $_item_id     = null;
    private $_bid_amount  = null;
    private $_bid_amount_validator = null;
    private $_maximum_bid;
    private $_minimum_bid;
    private $_user_id     = null;
    private $_type_id     = null;

    const BILLING_ADDRESS_SUBFORM = 'billing_address';
    const CARD_DETAILS_SUBFORM    = 'card_form';
    const BID_SUBFORM             = 'bid_form';
    const BILLING                 = 'billing';
    const BID                     = 'bid';

    public function __construct($options) {
        $requiredKeys = array(
            'auction_id', 'campaign_id',
            'bid_amount', 'user_id'
        );
        if (! array_intersect_key(array_flip($requiredKeys), $options)) {
            throw new Exception(__METHOD__ . " requires IDs auction_id, campaign_id, bid_amount & user_id");
        }

        if (isset($options['auction_id']) && !empty($options['auction_id'])) {
            $this->_auction_id = $options['auction_id'];
            unset($options['auction_id']);
        }

        if (isset($options['campaign_id']) && !empty($options['campaign_id'])) {
            $this->_campaign_id = $options['campaign_id'];
            unset($options['campaign_id']);
        }

        if (isset($options['item_id']) && !empty($options['item_id'])) {
            $this->_item_id = $options['item_id'];
            unset($options['item_id']);
        }

        if (isset($options['bid_amount']) && !empty($options['bid_amount'])) {
            $this->_bid_amount = $options['bid_amount'];
            unset($options['bid_amount']);
        }

        // Add the validator
        if (isset($options['bid_amount_validator']) && ! empty($options['bid_amount_validator'])) {
            $this->_bid_amount_validator = $options['bid_amount_validator'];
            unset($options['bid_amount_validator']);
        }

        if (isset($options['maximum_bid']) && !empty($options['maximum_bid'])) {
            $this->_maximum_bid = $options['maximum_bid'];
            unset($options['maximum_bid']);
        }

        if (isset($options['minimum_bid']) && !empty($options['minimum_bid'])) {
            $this->_minimum_bid = $options['minimum_bid'];
            unset($options['minimum_bid']);
        }

        if (isset($options['user_id']) && !empty($options['user_id'])) {
            $this->_user_id = $options['user_id'];
            unset($options['user_id']);
        }

        if (isset($options['auction_item_type']) && !empty($options['auction_item_type'])) {
            $this->_type_id = $options['auction_item_type'];
            unset($options['auction_item_type']);
        }

        parent::__construct($options);
    }

    public function init() {

        $this->setAlternateViewScript('form_viewscripts/confirm-bid.phtml');

        // Bring in the required forms
        $this->setMethod('post');
        $this->setName('confirm_bid');

        $bidForm = new Auction_Form_NewBid();

        $bidForm->setDefaults(array(
            'auction_id'  => $this->_auction_id,
            'campaign_id' => $this->_campaign_id,
            'item_id'     => $this->_item_id,
            'bid_amount'  => $this->_bid_amount,
            'user_id'     => $this->_user_id,
            'maximum_bid' => $this->_maximum_bid,
            'minimum_bid' => $this->_minimum_bid,
        ));

        if (null != $this->_bid_amount_validator) {
            $bidForm->getElement('bid_amount')->addValidators(
                $this->_bid_amount_validator
            );
        }

        $bidForm->removeDecorator('Form');
        $bidForm->removeElement('Bid Now');
        $bidForm->addSubmit('Submit Bid');

        //$bidForm->setElementsBelongTo(self::BID);
        $this->addSubForm($bidForm, self::BID_SUBFORM);

        $this->assignDataToViewscript('type_id', $this->_type_id);
        $this->assignDataToViewscript('is_standard_item',
                ((int)$this->_type_id === (int)Auction_Model_DataGateway_AuctionItems::TYPE_STANDARD));

        $this->addElement('submit', 'Confirm Bid');

        $form_action = "/auction/confirm-bid/campaign_id/{$this->_campaign_id}/auction_id/{$this->_auction_id}/item_id/{$this->_item_id}";
        if (Zend_Registry::get('app_theme') != null) {
            $form_action = '/'.Zend_Registry::get('app_theme').$form_action;
        }

        $this->setAction($form_action);
    }

    private function setUpBillingForms()
    {
        // Set up the Card form
        $cardForm = new Auction_Form_CardPaymentDetails();
        $cardForm->removeDecorator('Form');

        $cardForm->setElementsBelongTo(self::CARD_DETAILS_SUBFORM);

        // Set up the Billing Address Form
        $newBillingAddressForm = new Auction_Form_BillingAddress();
        $newBillingAddressForm->removeDecorator('Form');
        $newBillingAddressForm->setElementsBelongTo(self::BILLING);

        // Fetch the
        $addressService = new Auction_Service_User();
        $shippingAddress = $addressService->fetchShippingAddressByUserId(
                    $this->_user_id
                );

        $billingAddress = $addressService->fetchBillingAddressByUserId(
                    $this->_user_id
                );

        $this->assignDataToViewscript('shipping_address', $shippingAddress);
        $this->assignDataToViewscript('billing_address',  $billingAddress);

        // Add the forms as sub forms here
        $this->addSubForm($newBillingAddressForm, self::BILLING_ADDRESS_SUBFORM);
        $this->addSubForm($cardForm,           self::CARD_DETAILS_SUBFORM);

        // We need an address selector
        $this->addElement('select',
            'select_address',
            array(
                'label' => 'Choose Address',
                'multiOptions'    => array(
                    'billing'     => 'Billing',
                    'shipping'    => 'Shipping',
                    'new_billing' => 'Create New Billing Address'
                )
            ));
    }

    public function toArray()
    {
        // forms
        $bidForm            = $this->getSubForm('bid_form');
        $billingAddressForm = $this->getSubForm('billing_form');
        $cardForm           = $this->getSubForm('card_form');

        $values = array(
            // identification information
            'user_id'     => $bidForm->getValue('user_id'),
            'auction_id'  => $bidForm->getValue('auction_id'),
            'campaign_id' => $bidForm->getValue('campaign_id'),
            'item_id'     => $bidForm->getValue('item_id'),

            // Bid form
            'bid_amount' => $bidForm->getValue('bid_amount'),

            // Card Form
            'card_type'    => $cardForm->getValue('card_type'),
            'card_number'  => $cardForm->getValue('card_number'),
            'expiry_month' => $cardForm->getValue('expiry_month'),
            'expiry_year'  => $cardForm->getValue('expiry_month'),
            'cvv_code'     => $cardForm->getValue('cvv_code'),

            // Selected Åddress field
            'selected_address' => $this->getValue('select_address'),

            // Billing Address
            // This has to be the correct address selected by the user or
            // entered in the new form field.
            'first_name'     => $billingAddressForm->getValue('first_name'),
            'last_name'      => $billingAddressForm->getValue('last_name'),
            'address_line_1' => $billingAddressForm->getValue('address_line_1'),
            'address_line_2' => $billingAddressForm->getValue('address_line_2'),
            'town_city'      => $billingAddressForm->getValue('town_city'),
            'county_state'   => $billingAddressForm->getValue('county_state'),
            'post_zip_code'  => $billingAddressForm->getValue('post_zip_code'),
            'country'        => $billingAddressForm->getValue('country'),
            'country_id'     => $billingAddressForm->getValue('country_id'),

        );

        return $values;
    }

}

