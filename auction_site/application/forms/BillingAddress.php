<?php

/**
 * Description of BillingAddress
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_BillingAddress extends Auction_Form_Address
{

    public $billing_address_id;

    public function init()
    {
        $this->addElement('hidden',
                'billing_address_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));
        
        parent::init();

        $this->setLegend('Billing Details');
        $this->setElementsBelongTo('billing');

        $this->setDecorators(
            $this->getFormDecorators()
        );

        $this->addSubmit('Save');

    }

}

