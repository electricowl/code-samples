<?php
/**
 * Description of NewBid
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_NewBid extends Cs_Form_Abstract
{
    public function init()
    {
        $this->setMethod('post');

        $this->addElement('hidden',
                'auction_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => true,
                    'allowEmpty' => false
                ));

        /**
         * This is used as a flag in the confirm bid process
         */
        $this->addElement('hidden',
                'place_bid_now',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => false,
                    'allowEmpty' => false,
                    'value'    => 0,
                ));

        $this->addElement('hidden',
                'maximum_bid',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => false,
                    'allowEmpty' => false,
                    'value'    => 0,
                ));

        $this->addElement('hidden',
                'minimum_bid',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => false,
                    'allowEmpty' => false,
                    'value'    => 0,
                ));

        $this->addElement('hidden',
                'item_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => true,
                    'allowEmpty' => false
                ));

        $this->addElement('hidden',
                'campaign_id',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'required'   => true,
                    'allowEmpty' => false
                ));


        $this->addElement('text', 'bid_amount', array(
            'label'      => 'Your Bid',
            'decorators' => $this->getElementDecorators(),
            'required' => true,
            'allowEmpty' => false,
            'attributes' => array(
                'size'  => 20,
                'class' => 'new_bid'
            ),
            'filters' => array(
                'StringTrim',
                new Zend_Filter_LocalizedToNormalized(array('precision' => 2))
            ),
            //'errorMessages' => array(
            //    'A bid amount higher than the minimum amount is required'
            //),
            //'validators' => array(
            //    new Zend_Validate_GreaterThan(0)
            //)
        ));

        $this->addSubmit("Bid Now");

        $this->setDecorators(
            $this->getFormDecorators()
        );

    }

}

