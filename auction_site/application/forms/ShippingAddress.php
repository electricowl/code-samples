<?php

/**
 * Description of ShippingAddress
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_ShippingAddress extends Auction_Form_Address
{

    public $shipping_address_id;

    public function init()
    {
        $this->addElement('hidden',
                'shipping_address_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));
        
        parent::init();

        $this->setLegend('Shipping Details');
        $this->setElementsBelongTo('shipping');

        $this->setDecorators(
            $this->getFormDecorators()
        );

        $this->addSubmit('Save');


    }

}

