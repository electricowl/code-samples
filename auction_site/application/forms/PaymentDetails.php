<?php
/**
 * Description of PaymentDetails (PaymentDetails.php)
 * @package Crowdsurge White Label Auction site
 * @category Forms
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Form_PaymentDetails extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setAlternateViewScript('form_viewscripts/payment-details.phtml');

        $this->addElement('text',
                'amount',
                array(
                    'label' => 'Payment Amount',
                    'description' => 'You may increase this value if you wish',
                    'required'   => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'filters' => array(
                        'StringTrim',
                    ),
                    //'validators' => array(
                    //
                    //),
                    // @todo Validation to ensure the amount is not less than the Auction settlement
                    // required
                    'errorMessages' => array(
                        'Please enter a payment amount into this box'
                    ),
                    'attributes' => array(
                        'size'      => 20,
                        'maxlength' => 16
                    ),
                ));

        $this->addElement('select',
                'card_type',
                array(
                    'label' => 'Card Type',
                    'description' => 'Select the type of card you will be using',
                    'required'   => true,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => array(
                        '' => '-- ')
                        + Auction_Service_CardTypes::formatCardListForSelectUseLabels(),
                    'errorMessages' => array(
                        'Select the type of card you are using'
                    )
                ));

        $this->addElement('text',
                'card_number',
                array(
                    'label' => 'Card Number',
                    'description' => 'Enter the long card number here',
                    'required'   => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'filters' => array(
                        'StringTrim',
                    ),
                    'validators' => array(
                        new Zend_Validate_CreditCard(array(
                            // These are the types we allow
                            Zend_Validate_CreditCard::AMERICAN_EXPRESS,
                            Zend_Validate_CreditCard::MASTERCARD,
                            Zend_Validate_CreditCard::VISA
                        )),
                    ),
                    'errorMessages' => array(
                        'A valid card number is required'
                    ),
                    'attributes' => array(
                        'size'      => 20,
                        'maxlength' => 16
                    ),
                ));

        $months = array();
        for($m = 1;$m <= 12; $m++){
            $month =  date("F", mktime(0, 0, 0, $m, 1));
            $months[$m] = $month;
        }

        $this->addElement('select',
                'expiry_month',
                array(
                    'label' => 'Expiry Month',
                    'required' => true,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => $months
                ));

        $years = array();
        $startYear = date('Y');
        $endYear   = date('Y', strtotime('+5 year'));
        for ($yr = $startYear; $yr <= $endYear; $yr++) {
            $years[$yr] = $yr;
        }

        $this->addElement('select',
                'expiry_year',
                array(
                    'label' => 'Expiry Year',
                    'required' => true,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'multiOptions' => $years
                ));

        $this->addDisplayGroup(array(
                    'expiry_month',
                    'expiry_year'
                ),
                'expiry_group',
                array(
                    'legend' => 'Card Expiry Date'
                ));

        $this->addElement('text',
                'cvv_code',
                array(
                    'label' => 'CVV Code',
                    'description' => 'The last three digits on the strip on the back of the card',
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'attributes' => array(
                        'size'      => 4,
                        'maxlength' => 4
                    ),
                    'errorMessages' => array(
                        'Enter the 3-digit code from the strip on the back of your card'
                    )
                ));


        // Address Information
        $this->addElement('hidden',
                'user_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement('text',
                'first_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "First Name",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                    'errorMessages' => array(
                        'Please provide your first name'
                    )
                ));

        $this->addElement('text',
                'last_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Last Name",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                ));

        $this->addElement('text',
                'address_line_1',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Address Line 1",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                    'errorMessages' => array(
                        'Please provide house number and street information'
                    )
                ));

        $this->addElement('text',
                'address_line_2',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Address Line 2",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'attributes' => array(
                        'size' => 50
                    ),
                ));

        $this->addElement('text',
                'town_city',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Town/City",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Please enter the Town/City here'
                    )
                ));

        $this->addElement('text',
                'county_state',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "County/State",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Please enter the County/State here'
                    )
                ));

        $this->addElement('text',
                'post_zip_code',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Post/Zip Code",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Postcode/Zipcode is required'
                    )
                ));

        $countries = new Auction_Service_Countries();
        $selectCountries = $countries->fetchCountriesForSelect();

        // Country friendly name
        $this->addElement('select',
                'country_id',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Country",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'errorMessages' => array(
                        'Country is required'
                    ),
                    'multiOptions' => $selectCountries,
                    'value' => 224,
                ));

        $tclink = Zend_Registry::get('config')->links->terms_conditions;

        $this->addElement('checkbox',
                'accept_terms_and_conditions',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'description' => 'Yes, I have read the <a href="' . $tclink . '">Terms &amp; Conditions</a>',
                    'value' => 1,
                    'checked' => false,
                    'required' => true,
                    'allowEmpty' => false,
                    'messages' => array(
                        "You must accept the Terms and Conditions to proceed"
                    ),
                )
            );

        $this->addSubmit('Submit');

    }
}

