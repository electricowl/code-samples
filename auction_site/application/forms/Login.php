<?php

/**
 * Description of Login
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_Login extends Cs_Form_Abstract
{

    public function init() {

        $this->setName('login_form');

        $this->setMethod('post');

        //$this->setAttrib('autocomplete', 'off');

        $this->addElement('hidden',
                'form_action',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'value'      => 'login'
                ));

        $this->addElement('hidden',
                'is_client_login',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement(
                'text',
                'username',
                array(
                    'label' => 'Username',
                    //'description' => 'Please enter your username',
                    'attributes' => array(
                        'size' => 30
                    ),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters' => array(
                        'stringToLower'
                    ),
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'errorMessages' => array(
                        'Username is required'
                    )
                )
            );

        $this->addElement(
                'password',
                'password',
                array(
                   'label' => 'Password',
                    //'description' => 'Please provide your password',
                    'attributes' => array(
                        'size' => 30
                    ),
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'errorMessages' => array(
                        'Password is required'
                    )
                )
            );

        $this->addSubmit("Enter");

        $this->setDecorators(
            $this->getFormDecorators()
        );
    }
}

