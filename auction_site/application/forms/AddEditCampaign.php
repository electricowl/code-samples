<?php
/**
 * Description of Auction_Form_AddEditCampaign (AddEditCampaign.php)
 * @package Crowdsurge White Label Auction site
 * @category Forms
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Auction_Form_AddEditCampaign extends Cs_Form_Abstract
{
    public function init()
    {
        $this->addElement('hidden',
                'campaign_id',
                array(
                   'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement('hidden',
                'client_id',
                array(
                    'decorators' => $this->getHiddenDecorators()
                ));

        $this->addElement('text',
                'campaign_name',
                array(
                    'label' => "Campaign Name",
                    //'description' => "Campaign Name",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel()
                )
            );

        $this->addElement('text',
                'campaign_custom_ref',
                array(
                    'label' => "Custom Reference",
                    //'description' => "Custom Reference",
                    'required'   => false,
                    'allowEmpty' => true,
                    'decorators' => $this->getElementDecoratorsWithLabel()
                )
            );

        $this->addElement('textarea',
                'campaign_description',
                array(
                    'label' => "Description",
                    //'description' => "Description",
                    'required' => true,
                    'allowEmpty' => false,
                    'decorators' => $this->getElementDecoratorsWithLabel()
                )
            );

        $this->addSubmit("Save");
    }
}

