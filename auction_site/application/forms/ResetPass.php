<?php

/**
 * Description of Reset Password
 *
 * @author Ryan Simms
 */
class Auction_Form_ResetPass extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setName('reset_pass');

        $this->setAttrib('autocomplete', 'off');

        $this->addElement('hidden',
                'form_action',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'value'      => 'resetpass'
                ));

        /**
         * 'hostname' => A hostname validator, see Zend_Validate_Hostname
         * 'allow' => Options for the hostname validator,
         * see Zend_Validate_Hostname::ALLOW_*
         * 'mx' => If MX check should be enabled, boolean
         * 'deep' => If a deep MX check should be done, boolean
         */
        $this->addElement('text',
                'email_address',
                array(
                    'label' => 'Email Address',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim',
                        'StringToLower'
                    ),
                    'validators' => array(
                        new Zend_Validate_EmailAddress(array(
                            'allow' => Zend_Validate_Hostname::ALLOW_DNS,
                            'mx'    => false,
                            'deep'  => false
                        )),
                        new Zend_Validate_Db_RecordExists(array(
                            'adapter' => $this->getDbDefaultAdapter(),
                            'schema' => AUCTION_SCHEMA,
                            'table'  => 'users',
                            'field'  => 'email',
                            'messages' => array(
                            Zend_Validate_Db_RecordExists::ERROR_RECORD_FOUND => 'We cannot find an account for this email address'
                            )
                        )),
                    ),
                ));

        $this->addSubmit('Reset Password');

        $this->setDecorators(
                $this->getFormDecorators()
            );

    }



}

