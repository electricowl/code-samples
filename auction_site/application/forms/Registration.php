<?php

/**
 * Description of Registration
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Auction_Form_Registration extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setName('auction_registration');

        $this->setAttrib('autocomplete', 'off');

        $this->addElement('hidden',
                'form_action',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'value'      => 'registration'
                ));


        /**
         * @Zend_Validate_Db_NoRecordExists
         * Provides basic configuration for use with Zend_Validate_Db Validators
         * Setting $exclude allows a single record to be excluded from matching.
         *  Exclude can either be a String containing a where clause, or an
         * array with `field` and `value` keys to define the where clause added
         * to the sql. A database adapter may optionally be supplied to avoid
         * using the registered default adapter.  The following option keys are
         * supported: 'table' => The database table to validate against
         * @param 'schema' => The schema keys
         * @param 'field' => The field to check for a match
         * @param 'exclude' => An optional where clause or field/value pair to exclude
         * from the query
         * @param 'adapter' => An optional database adapter to use
         */
        $this->addElement('text',
                'registration_username',
                array(
                    'label' => 'Username',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'validators' => array(
                        new Zend_Validate_Db_NoRecordExists(array(
                            'adapter' => $this->getDbDefaultAdapter(),
                            'schema' => AUCTION_SCHEMA,
                            'table'  => 'users',
                            'field'  => 'username',
                            'messages' => array(
                                Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'This username is not available'
                            )
                        )),
                    )
                ));

        $this->addElement('text',
                'first_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "First Name",
                    'required'   => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),

                ));

        $this->addElement('text',
                'last_name',
                array(
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'label'      => "Last Name",
                    'required'   => false,
                    'allowEmpty' => true,
                    'filters'    => array(
                        'StringTrim'
                    )
                ));

        /**
         * 'hostname' => A hostname validator, see Zend_Validate_Hostname
         * 'allow' => Options for the hostname validator,
         * see Zend_Validate_Hostname::ALLOW_*
         * 'mx' => If MX check should be enabled, boolean
         * 'deep' => If a deep MX check should be done, boolean
         */
        $this->addElement('text',
                'email_address',
                array(
                    'label' => 'Email Address',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim',
                        'StringToLower'
                    ),
                    'validators' => array(
                        new Zend_Validate_EmailAddress(array(
                            'allow' => Zend_Validate_Hostname::ALLOW_DNS,
                            'mx'    => false,
                            'deep'  => false
                        )),
                        new Zend_Validate_Db_NoRecordExists(array(
                            'adapter' => $this->getDbDefaultAdapter(),
                            'schema' => AUCTION_SCHEMA,
                            'table'  => 'users',
                            'field'  => 'email',
                            'messages' => array(
                            Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'This email address is already taken'
                            )
                        )),
                    ),
                ));

        $this->addElement('text',
                'phone',
                array(
                    'label' => 'Phone Number',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => false,
                    'allowEmpty' => true,
                    'filters' => array(
                        'StringTrim'
                    ),
                ));

        $this->addElement('password',
                'registration_password',
                array(
                    'label' => 'Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                ));

        $this->addElement('password',
                'confirm_password',
                array(
                    'label' => 'Confirm Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'validators' => array(
                        new Zend_Validate_Identical('registration_password')
                    ),
                    'messages' => array(
                        'Passwords do not match'
                    )
                ));

        $this->addSubmit('Register');

        $this->setDecorators(
                $this->getFormDecorators()
            );

    }



}

