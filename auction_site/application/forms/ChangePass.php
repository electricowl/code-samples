<?php

/**
 * Description of Reset Password
 *
 * @author Ryan Simms
 */
class Auction_Form_ChangePass extends Cs_Form_Abstract
{

    public function init()
    {
        $this->setName('change_pass');

        $this->setAttrib('autocomplete', 'off');

        $this->addElement('hidden',
                'form_action',
                array(
                    'decorators' => $this->getHiddenDecorators(),
                    'value'      => 'changepass'
                ));

        $this->addElement('password',
                'current_password',
                array(
                    'label' => 'Current Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim',
                    ),
                ));

        $this->addElement('password',
                'new_password',
                array(
                    'label' => 'New Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                ));

        $this->addElement('password',
                'confirm_new_password',
                array(
                    'label' => 'Confirm New Password',
                    'decorators' => $this->getElementDecoratorsWithLabel(),
                    'required' => true,
                    'allowEmpty' => false,
                    'filters'    => array(
                        'StringTrim'
                    ),
                    'validators' => array(
                        new Zend_Validate_Identical('new_password')
                    ),
                    'messages' => array(
                        'Passwords do not match'
                    )
                ));

        $this->addSubmit('Change Password');

        $this->setDecorators(
                $this->getFormDecorators()
            );

    }



}

