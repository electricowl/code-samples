Auction Site
============

Developed based on ZF1

Designed to improve on an existing system that was written procedurally and 
failed to handle bid contention.

Initial design created the bid queue in MySql but then this was moved over to
RabbitMQ which provided a far better approach (in terms of capacity and 
scalability) to managing bids.

Features:
* OO PHP5 design
* Sane project layout
* Environment 'aware'
* Initial design for theme-able UI with default and custom CSS/HTML possible
* Bids handled in queues to ensure fairness
* RabbitMQ handles bid queues - scalable/HA

I designed and developed this product, as a lone developer, whilst at Crowdsurge
 
