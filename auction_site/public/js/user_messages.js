/**
 *
 * Fetch user messages by using AJAX Call
 */


// bid_failed              = "1||Your bid was unsuccessful"
// bid_success             = "2||Your bid succeeded"
// bid_highest             = "3||Congratulations. Your bid is currently the highest bid"
// outbid                  = "4||You have been outbid"
// bid_again               = "5||Bid again for another opportunity"
// starting_bid            = "6||The starting bid is %s"
// bid_late_auction_closed = "7||Your bid arrived too late to be accepted"
// auction_closed          = "8||The auction has now closed, no more bids are being accepted"

(function(){

    var do_call = 1;
    var calls = 0;

    //bid_title
    //bid_values
    //bid_text

    var default_title   = 'Your Bid Has Been Accepted.<br/>Good Luck!';
    var default_values  = '';
    var default_text    = '<h6>What Happens Next?</h6><p>An email has been sent confirming that your bid has been accepted, but you could still be outbid!</p><p>If you are outbid you will be notified by email. If you are the highest bidder when the auction ends, Congratulations. You will receive an email confirmation of your winning bid and details of payment.</p><div class="cf bid-return"><a class="button white" id="cancel" href="AUCTION_HREF">Return To Auction</a></div>';

    var pending_title   = 'Your Bid Has Been Accepted.<br/>Good Luck!';
    var pending_values  = '<span class="bid-sending">You bid is being placed, this may take a few seconds &hellip;</span>';
    var pending_text    = '<div class="bid-loader cf"><div id="loader"></div></div>';

    var outbid_title   = 'You Have Been Outbid!';
    var outbid_values  = '<span class="bid-sending">Sorry, you\'ve been outbid. Please try again.</span>';
    var outbid_text    = '<div class="cf bid-return"><a class="button white" id="cancel" href="AUCTION_HREF">Place New Bid</a></div>';

    var highest_title   = 'You Are The Highest Bidder.<br/>Good Luck!';
    // var highest_values  = '<div class="bid-row">
    //                          <div class="bid-info">
    //                              <span class="bid-info-title">Current Bid</span>
    //                              <span class="bid-info-placed">Placed by [Username]</span>
    //                          </div>
    //                          <div class="bid-ammount">
    //                              [$][CURRENT BID]
    //                          </div>
    //                         </div>
    //                         <div class="bid-row">
    //                          <div class="bid-info">
    //                              <span class="bid-info-title bid-info-max">Your Maximum Bid</span>
    //                          </div>
    //                          <div class="bid-ammount">
    //                              [$][HIGH BID]
    //                          </div>
    //                         </div>';
    var highest_values  = '';
    var highest_text    = '<h6>What Happens Next?</h6><p>An email has been sent confirming that you are the highest bidder, but you could still be outbid!</p><p>If you are outbid you will be notified by email. If you are still the highest bidder when the auction ends, Congratulations. You will receive an email confirmation of your winning bid and details of payment.</p><div class="cf bid-return"><a class="button white" id="cancel" href="AUCTION_HREF">Return To Auction</a></div>';

    function fetchMessages() {

        var campaign_id = jQuery(document).data('campaign_id');
        var auction_id = jQuery(document).data('auction_id');
        var item_id = jQuery(document).data('item_id');

        jQuery.getJSON('/user/get-my-messages/campaign_id/' + campaign_id + '/auction_id/' + auction_id + '/item_id/' + item_id + '/format/json', function(data) {
            var items = [];
            var xd = data.user_messages;
            var auction_href = '/auction/detail/campaign_id/'+data.campaign_id+'/auction_id/'+data.auction_id;
            var auction_item_href = '/auction/confirm-bid/campaign_id/'+data.campaign_id+'/auction_id/'+data.auction_id+'/item_id/'+data.auction_item_id;

            jQuery.each(xd, function(key, val) {

                if (val != '') {
                    msg_vars = val.split('||');
                    msg_status_id = msg_vars[0];
                }
                else {
                    msg_status_id = '999';
                }

                //Highest Bidder
                if (msg_status_id == '3') {
                    do_call = 0;

                    $('#bid_title').html(highest_title);
                    $('#bid_values').html(highest_values);
                    highest_text = highest_text.replace('AUCTION_HREF', auction_href);
                    $('#bid_text').html(highest_text);
                }
                //Outbid
                else if (msg_status_id == '4') {
                    do_call = 0;

                    $('#bid_title').html(outbid_title);
                    $('#bid_values').html(outbid_values);
                    outbid_text = outbid_text.replace('AUCTION_HREF', auction_item_href);
                    $('#bid_text').html(outbid_text);
                }
                else {
                    //Revert to bid accepted after 5 call attempts
                    if (calls > 5) {
                        do_call = 0;

                        $('#bid_title').html(default_title);
                        $('#bid_values').html(default_values);
                        default_text = default_text.replace('AUCTION_HREF', default_text);
                        $('#bid_text').html(default_text);
                    }
                }

                calls = calls + 1;

            });
        });

        if (do_call != 1) {
            if (caller != 'undefined') {
                clearInterval(caller);
            }
        }
    }

    jQuery(document).ready(fetchMessages);

    $('#bid_title').html(pending_title);
    $('#bid_values').html(pending_values);
    $('#bid_text').html(pending_text);

    var caller = setInterval(fetchMessages, 2000);


})();


