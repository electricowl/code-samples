/* AUCTION.JS */

jQuery(document).ready(function() {	//this is jquery

// define some variables
    var auctionitem_id = jQuery('#auctioncontainer').data("auctionid");
    var time = get_server_time();  // console.log('time: '+ time);
    var historyarray = "";


    // Create the notebox
    jQuery('body').prepend('<div id="notebox"></div><div id="overlay"></div>');
    jQuery('#overlay, #notebox').hide();



    // Create the notebox
// jQuery('body').prepend('<div id="notebox"></div><div id="overlay"></div>');jQuery('#overlay, #notebox').hide();

    // Activate the seatplan
    jQuery('#auctionitems').seatplan();



    /* loop through all auction items in list */
    jQuery('#auctionitemlist li').not('#auctionitemlist li:first-child').each(function() {
        setthecountdown(this, time);
        historycount(this);
        var current = jQuery(this).find('.itm-current');
        current.addClass('bidchecker').attr('data-itemid', jQuery(this).data('itemid'));
//if ()     item.find('.starting_bid').html();

        if (parseFloat(current.html().replace(/[^0-9.-]+/g, '')) < 0.01) {
            current.html(jQuery(this).find('.starting_bid').html());
        }


// jQuery(this).attr('rel',jQuery(this).data('itemref'));

    });

    // highjack the listings bid link
    jQuery('#auctionitemlist li .action a').click(function(e) {
        e.preventDefault();
    });
    jQuery('#auctionitemlist li').click(function(e) {
        var itemid = jQuery(this).attr('data-itemid');

        gotoauction(itemid);
    });




//  CHECK - FOR DEEP LINKING TO SEATS
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var auction = "";
    var auction = getUrlVars()["auction"];
    if (auction != "") {
        if (jQuery('#auctionitemlist li[data-itemid="' + auction + '"]').length) {
            gotoauction(auction);
        }
    }


    // Hidden Silo for new data. update every 10 seconds
    $('body').append('<div id="newdata" style="display:none;"></div>');
    setInterval("reloadpage(); console.log('data reloaded');", 10000);



}); // jquery.ready FIN ///////////////////////////// do not delete


// @todo This needs to be moved into its own phtml template or even detail.phtml
function gotoauction(itemid) {

    jQuery('#seatdetails').remove();

    // Create the bid details (this should really be placed in the markup not here)
    var biddetails = ['<div id="seatdetails">'
            , '<div id="seatnumbers"><span> </span><a href="#" class="viewall">View All</a></div><!--/div seatnumbers-->'
            , '<div id="currentbid"><span class="label"><span class="pretext">STARTING</span> BID:<span id="currentbidder"></span></span><span id="currentbidvalue"> <span class="currency"></span> <span id="v"></span></span><a href="#" id="bidhistory">Bid History: <span>0</span></a></div><!--/div currentbid-->'
            , '<div id="timeleft"><span class="label">TIME LEFT:</span> <span id="countdown" class="cdown"> </span></div><!--/div timeleft-->'
            , '<div id="yourbid"><span class="label">YOUR MAXIMUM BID: <small>Enter <span class="currency"></span><span class="minbid"></span> or more</small></span> <span class="currency"></span>'
            , '<form id="bidform" action="" method="POST"><input type="text" name="bid_amount" maxlength="7" /><a href="#" class="button" >BID NOW</a>'
            , '<input type="hidden" name="maximum_bid" id="maximum_bid" value="" /> '
            , '<input type="hidden" name="minimum_bid" id="minimum_bid" value="" /> '
            , '</form>'
            , '<br class="clear"/>'
            , '</div><!--/div yourbid--></div><!--/div seatdetails-->'];
    jQuery('#auctionitemlist').after(biddetails.join(''));

    // reset the seatdetails values
    jQuery('#seatdetails').hide();
    jQuery('#seatdetails').attr('data-itemid', itemid);
    jQuery('#seatdetails #bidhistory').hide();
    console.log("go to auction - itemid: " + itemid);
    jQuery('#auctionitemlist').hide();
    jQuery('.selected').removeClass('selected');
    jQuery('#yourbid').show();
    jQuery('input[name="bid_amount"]').val('');
    jQuery('#timeleft').removeClass('ended');


    // set all the variables and populate the content holders
    // Need to populate maximum/minimum bids (IL) for trasmission to confirm-bid
    var maxMinBids = jQuery(document).data('maxMinBids');

    if (maxMinBids) {
        if (itemid in maxMinBids) {
             jQuery('#maximum_bid').val(maxMinBids[itemid].maximum_bid);
             jQuery('#minimum_bid').val(maxMinBids[itemid].minimum_bid);
        }
    }

    var item = jQuery('#auctionitems li.auction_item[data-itemid="' + itemid + '"]');
    var itemref = item.attr('rel');
    var historyarray = item.attr('data-bidhistory');

    var currency = item.data('currency');
    jQuery('#seatdetails .currency').text(currency);
    var currentbid = item.find('.itm-current').html();
    if (parseFloat(currentbid.replace(/[^0-9.-]+/g, '')) < 0.01) {
        var currentbid = item.find('.starting_bid').html();
    }
    jQuery('#seatdetails #currentbidvalue').html(currentbid).addClass('bidchecker').attr('data-itemid', jQuery('#seatdetails').data('itemid'));
    var itemname = item.find('.auctionitem').html();
    jQuery('#seatdetails #seatnumbers>span').html(itemname);
    var timeleft = item.find('.cdown').data('time');
    jQuery('#seatdetails #countdown').attr('data-time', timeleft);
    setthecountdown('#seatdetails #timeleft', get_server_time());
//     var minbid = parseFloat(currentbid.replace(/[^0-9.-]+/g, '')) + (parseFloat(item.find('.min_bid_increment').text().replace(/[^0-9.-]+/g, ''))); jQuery('#seatdetails .minbid').html(minbid);
    //    var minbid = parseFloat(item.find('.next_min_bid').text().replace(/[^0-9.-]+/g, '')); jQuery('#seatdetails .minbid').html(minbid);
    var minbid = item.find('.next_min_bid').text().replace(/[^\d\.]/g, '');
    jQuery('#seatdetails .minbid').html(minbid);


    // if closed remove form
    if (jQuery('#seatdetails #timeleft').hasClass('ended')) {
        jQuery('#yourbid').hide();
    }
    else {
        jQuery('#yourbid').show();
    }
    // make the chosen seat selected
    jQuery('* [data-itemid="' + itemid + '"]').addClass('selected');

    jQuery('#seatdetails').fadeIn();
    jQuery('#seatdetails .viewall').click(function() {
        jQuery('.selected').removeClass('selected');
        jQuery('#seatdetails').remove();
        jQuery('#selectyourseats').hide();
        jQuery('#yourbid #redrows').hide();
        jQuery('#auctionitemlist').fadeIn('fast');
        return false;
    });

    if (historyarray) {
        jQuery('#bidhistory').show();
        historyarray = jQuery.parseJSON(historyarray);
        jQuery('#seatdetails').find('#bidhistory span').html(historyarray.length);
        if (historyarray.length > 0) {
            jQuery('#seatdetails #currentbidder').text('Placed by ' + historyarray[0].bidder);
            jQuery('#seatdetails .pretext').text('CURRENT');
        }
        else {
            jQuery('#bidhistory').hide();
        }
        if (jQuery('#countdown').parent().hasClass('ended')) {
            jQuery('#seatdetails .pretext').text('WINNING');
        }
        //else { jQuery('#seatdetails .pretext').text('CURRENT');}



        /* bid history popup */
        jQuery('#bidhistory').click(function() {

            //var historyarray = eval("new Array(" + jQuery(item).data('bidhistory') + ")");
            //var historycount = historyarray.length;
            jQuery('#overlay, #notebox').remove();
            jQuery(window).scrollTop(0);
            jQuery('body').prepend('<div id="overlay"> </div>');
            var thebidders = '';
            jQuery('body').prepend('<div id="notebox"><h2>Bid History (' + historyarray.length + ')</h2>   <table width="100%" id="historybidders" cellspacing="10" style="border-bottom:1px solid #db293f; display:block;margin-bottom:15px; width:100%;"><thead><tr><th width="40%">BIDDER</th><th style="padding-right:30px;">BID&nbsp;AMOUNT</th><th>BID TIME</th></tr></thead><tbody>' + thebidders + '</tbody></table>  <span class="littlemessagebuttons"><a href="#" class="closebutton buttonminor button white">Close</a></span></div>');

            $.each(historyarray, function(i, data) {
                var bidtime = new Date(data.time * 1000);
                var thebidders = "<tr><td>" + data.bidder + "</td><td><span class='currency'>" + currency + "</span> " + data.bid + "</td><td nowrap>" + bidtime.toLocaleString() + "</td></tr>";

                jQuery(thebidders).appendTo("#historybidders");
                //jQuery('#historybidders .currency').changecurrency(currencysymbol);
            });


            jQuery('#overlay, .closebutton').click(function(e) {
                e.preventDefault();
                jQuery('#overlay, #notebox').remove();
                return false;
            });

            return false;

        });

    }   // end if historyarray



// Submit the form open up crowdsurge bid process
    jQuery('#yourbid a.button').click(function() {
        // var newbid = jQuery('input[name="bid_amount"]').val();
        //jQuery('input[name="bid_amount"]').attr('value',newbid);
        //alert(itemid);
        var minbid = parseFloat(jQuery('#yourbid .minbid').text());
        var bidval = parseFloat(jQuery('input[name="bid_amount"]').val());

        if (bidval < (minbid - 0.009) || bidval == "")
        {
            var message = '<h2>Your Bid Is Less Than The Minimum Bid Required.</h2>Enter an amount that is equal to or greater than the minimum bid required. This can be found next to the bid entry box';
            $('body').prepend('<div id="overlay"> </div>');
            jQuery(window).scrollTop(0);
            $('body').prepend('<div id="notebox"><span class="littlemessage">' + message + '</span><br class="clear"/><span class="littlemessagebuttons"><a href="#" class="closebutton buttonminor">Close</a></span></div>');
            $('#overlay, .closebutton').click(function() {
                $('#overlay, #notebox').remove();
                return false;
            });
        }
        else {
            var formaction = jQuery('#auctioncontainer').data('formaction') + itemid;
            jQuery('#bidform').attr('action', formaction).submit();
        }
        return false;


    });

    /* ////////////////////////////// RED ONLY ///
     /* //////////////////////////////add the stories under the bid input */
    jQuery('#yourbid').append("<div id='redrows'><span>YOUR BID COULD HELP PROVIDE<b></b> <span class='people'>PEOPLE</span> LIFE-SAVING HIV/AIDS MEDICINE FOR 1 YEAR.</span><ul id='stories'></ul><span><small>This medicine costs around 40¢ a day. It helps keep people living with HIV healthy, and can help HIV-positive pregnant women deliver HIV free babies.</small></span></div>");
    jQuery('input[name="bid_amount"]').keydown(function(event) {
        if ((!event.shiftKey && !event.ctrlKey && !event.altKey) && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105))) // 0-9 or numpad 0-9, disallow shift, ctrl, and alt
        {// check textbox value now and tab over if necessary
        }
        else if (event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 110 && event.keyCode != 9 && event.keyCode != 190) // not esc, del, left or right
        {
            event.preventDefault();
        }
// else the key should be handled normally
    });
    oldval = "";
    jQuery('input[name="bid_amount"]').keyup(function() {
        var newval = jQuery(this).val();
        if (newval != oldval) {
            stories(newval);
            oldval = newval;
        }
    });
    /* ////////////////////////////// RED ONLY /// */


} // GoTobid ends here.


// GET THE TIME FOR COUNTDOWN FROM THE SERVER
function get_server_time() {
    var server_time = null;
    //var server_time = 1369340360000; // HARDCODED Because i need the time in the past to demo *****

    var url = '/index/get-server-time/format/json'
    jQuery.getJSON(
            url,
            function(data) {
                if (time in data) {
                    server_time = data.time;
                }
                else {
                    server_time = new Date();
                    //    server_time = time.getTime();
                }

            }
    );

    return server_time;
}

var time = get_server_time();

function setthecountdown(c, time) {
    // set the countdowns
    var countdowncell = jQuery(c).find(".cdown");
    var settimer = new Date(countdowncell.attr("data-time") * 1000).toString();
    countdowncell.countdown({
        "date": settimer,
        "servertime": time,
        "onComplete": function(event) {
            countdowncell.html("<span style='opacity:0.33;filter: alpha(opacity = 33);'>Closed</span>");
            jQuery(c).addClass('ended').find('.action a').text('View');
            reloadpage();
        }

    });
}


function historycount(item) {
// get the bid history counts
    historyarray = jQuery(item).attr('data-bidhistory');
    if (historyarray) {
        historyarray = jQuery.parseJSON(historyarray);
        jQuery(item).find('.itm-bids').html(historyarray.length);
    }


}


// load the new data into page
function reloadpage() {
    $('#newdata').load(window.location.pathname + ' #auctionitemlist', function() {
        checker_currentbid();
    });
}

function checker_currentbid() {

    $('.bidchecker').each(function() {
        var oldbid = jQuery(this).text();
        //var oldbid = oldbid.replace(/[^\d\.\-\ ]/g, '');

        var itemid = jQuery(this).data('itemid');
        var newdata = jQuery('#newdata li.auction_item[data-itemid="' + itemid + '"]');
        var target = jQuery('#auctionitems li.auction_item[data-itemid="' + itemid + '"]');
        var newbid = newdata.find('.itm-current').html();
        if (parseFloat(newbid.replace(/[^0-9.-]+/g, '')) < 0.01)
        {
            newbid = newdata.find('.starting_bid').html();
        }



        if (newbid != oldbid) {
            var historyarray = ""; // empty this array
            $(this).text(newbid).hide().fadeIn();
            var oldbid = newbid;
            target.attr('data-bidhistory', newdata.attr('data-bidhistory'));
            target.find('.next_min_bid').html(newdata.find('.next_min_bid').text().replace(/[^\d\.]/g, ''));

            historycount(target);
            historyarray = target.attr('data-bidhistory');

            if (jQuery('#seatdetails').data('itemid') == itemid) {
                if (historyarray) {
                    jQuery('#bidhistory').show();
                    historyarray = jQuery.parseJSON(historyarray);
                    jQuery('#seatdetails').find('#bidhistory span').html(historyarray.length);
                    if (historyarray.length > 0) {
                        jQuery('#seatdetails #currentbidder').text('Placed by ' + historyarray[0].bidder);
                        jQuery('#seatdetails .pretext').text('CURRENT');
                        var minbid = target.find('.next_min_bid').text().replace(/[^\d\.]/g, '');
                        jQuery('#seatdetails .minbid').html(minbid);
                    }
                    else {
                        jQuery('#bidhistory').hide();
                    }

                }
            }
        }


        //var bidincrement = seatdata.attr("data-increment");
        //$('#seatdetails #bidhistory[rel="'+rel+'"] .historycount').html(historycount);
        //$('#seatdetails .minbid[rel="'+rel+'"]').html(numberWithCommas((parseFloat(newbid)+ (parseFloat(bidincrement))).toFixed(2)));
        // var historyarrayeval = eval("new Array(" + $(seatdata).data('bidhistory') + ")");
        //    var historycount = historyarrayeval.length;
        //    if(historycount != 0) {$('#seatdetails[rel="'+rel+'"] #currentbidder, #seatdetails[rel="'+rel+'"] #bidhistory').show();  $('#seatdetails[rel="'+rel+'"] .pretext').text('CURRENT');
        //    var historyarray =  $(seatdata).data('bidhistory');
        //    var historyarray =  historyarray.replace(/'/g, '"');
        //    var object = $.parseJSON('['+historyarray+']');
        //   $('#seatdetails[rel="'+rel+'"] #currentbidder').text('Placed by ' + object[0].bidder);
        //    $('#seatdetails[rel="'+rel+'"] #bidhistory').attr('rel',rel).html('Bid History: <span class="historycount">'+historycount+'</span>');




    });

}





// (RED STORIES )

function stories(bidvalue) {
    /*
     this function displays the amount of stories displayed when a
     customer bids and controls the sizes as the quantity gets bigger.
     */
    var howmanysaved = bidvalue / 150;
    howmanysaved = parseInt(howmanysaved);
    var storyarray = new Array("Charles", "Beatrice", "Patience_", "Ann", "Doris", "Joyce_", "Deborah", "Delali", "Michael_", "Desmond", "Michael", "Dorcas", "Jason", "Catherine", "Esther", "Soloman", "Christina", "Bani", "Daniel", "Florence", "Happy", "Gladys", "Joyce", "Benjamin", "Marvis", "Grace", "Motselisi", "Patience", "Emilia", "Rita", "Simona", "Ruth", "Benedicta", "Stella", "Davis");
    if (isNaN(howmanysaved)) {
        jQuery('#yourbid #redrows').hide();
    }
    if (howmanysaved == 0) {
        jQuery('#yourbid #redrows').hide();
    }
    if (howmanysaved != 0) {
        jQuery('#yourbid #redrows').fadeIn();
        jQuery('#yourbid #redrows > span > b').html(howmanysaved);
        jQuery('ul#stories').html("");

        // Shuffle the names
        function randomit(arr) {
            for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x)
                ;
            return arr;
        }

        // put stories on page depending on bid value
        var getstories = $.each(randomit(storyarray), function(i, name) {
            if (i < howmanysaved) {
                namewspace = name.replace(/_/g, " ");

                jQuery('ul#stories').append('<li><a href="#" id="' + name + '"><img src="/themes/red/images/stories/black.jpg" class="' + name + '"/><span class="name">' + namewspace + '</span></a></li>');
                //	jQuery('ul#stories').append('<li><a href="#" id="'+ name +'"><img src="../../../images/stories/black.jpg" class="'+ name +'"/><span class="name">'+ namewspace +'</span></a></li>');

                jQuery('ul#stories a img.' + name).attr('src', '/themes/red/images/stories/' + name + '.jpg');
                var storywidth = 90;
                var storyheight = 90;
                if (howmanysaved == 1) {
                    jQuery('#redrows .people').text('PERSON');
                }
                if (howmanysaved > 1) {
                    jQuery('#redrows .people').text('PEOPLE');
                }
                if ((howmanysaved > 3) && (howmanysaved <= 5)) {
                    storywidth = 60;
                    storyheight = 60;
                }
                if ((howmanysaved > 5) && (howmanysaved <= 13)) {
                    storywidth = 40;
                    storyheight = 40;
                }
                if ((howmanysaved > 13) && (howmanysaved <= 25)) {
                    storywidth = 35;
                    storyheight = 34;
                    jQuery('#stories .name').hide();
                    jQuery('ul#stories li').css('margin', '2px');
                }
                if ((howmanysaved > 25) && (howmanysaved <= 99999)) {
                    storywidth = 20;
                    storyheight = 19;
                    jQuery('#stories .name').hide();
                    jQuery('ul#stories li').css('margin', '2px');
                }
                jQuery('ul#stories a img').css({'width': storywidth, 'height': storyheight});

            }
        });
    }
    if (isNaN(howmanysaved)) {
        jQuery('#yourbid #redrows').hide();
    }

    jQuery('ul#stories li a').click(function() {
        var name = jQuery(this).attr('id');
        storybox(name);
        return false;
    });


}

// little message story details
function storybox(name) {
    jQuery(window).scrollTop(0);
    jQuery('#overlay, #notebox').remove();

// STORIES CONTENT
    var nameList = {
        "Beatrice": "<img src='/themes/red/images/stories/Beatrice_and_Charles_large.jpg'   width='470'/><span class='littlemessage'><h2>Beatrice and Charles</h2><b class='country'>Ghana</b>Beatrice and Charles, both HIV-positive, now know they and their sons can live long and healthy lives.  Life-saving ARV treatment helps keep them and their eldest son, Bright, healthy and helped prevent their youngest son, Herbert, from contracting the disease.<br /></span>",
        "Charles": "<img src='/themes/red/images/stories/Beatrice_and_Charles_large.jpg'  width='470' /><span class='littlemessage'><h2>Beatrice and Charles</h2><b class='country'>Ghana</b>Beatrice and Charles, both HIV-positive, now know they and their sons can live long and healthy lives.  Life-saving ARV treatment helps keep them and their eldest son, Bright, healthy and helped prevent their youngest son, Herbert, from contracting the disease.<br /></span>",
        "Doris": "<img src='/themes/red/images/stories/Doris_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Doris and Michael</h2><b class='country'>Ghana</b>Almost 2 years ago, (RED) met Doris and her son Michael, who was born HIV-negative thanks to treatment that prevented the transmission of HIV during pregnancy.  Today, Doris is more vibrant than ever.  \"Life is good to me.  Now I\'m healthy...  taking my drugs...  I\'m taking good care of myself and my boy,\" she says, beaming with pride.  At the top of his class at school, it seems Michael\'s future can be as bright as any mother can hope.<br /></span>",
        "Michael": "<img src='/themes/red/images/stories/Doris_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Doris and Michael</h2><b class='country'>Ghana</b>Almost 2 years ago, (RED) met Doris and her son Michael, who was born HIV-negative thanks to treatment that prevented the transmission of HIV during pregnancy.  Today, Doris is more vibrant than ever.  \"Life is good to me.  Now I\'m healthy...  taking my drugs...  I\'m taking good care of myself and my boy,\" she says, beaming with pride.  At the top of his class at school, it seems Michael\'s future can be as bright as any mother can hope.<br /></span>",
        "Dorcas": "<img src='/themes/red/images/stories/Dorcas_and_Jason_large.jpg'   width='470'/><span class='littlemessage'><h2>Dorcas and Jason</h2><b class='country'>Ghana</b>Dorcas says of her ARV treatment:  \"ARVs help you live long...  you become strong, you can do your work, you can go anywhere you want to go.\"  ARVs, as part of prevention of mother-to-child transmission treatment, helped ensure her son Jason could be born healthy and HIV-free.<br /></span>",
        "Jason": "<img src='/themes/red/images/stories/Dorcas_and_Jason_large.jpg'   width='470'/><span class='littlemessage'><h2>Dorcas and Jason</h2><b class='country'>Ghana</b>Dorcas says of her ARV treatment:  \"ARVs help you live long...  you become strong, you can do your work, you can go anywhere you want to go.\"  ARVs, as part of prevention of mother-to-child transmission treatment, helped ensure her son Jason could be born healthy and HIV-free.<br /></span>",
        "Catherine": "<img src='/themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>",
        "Esther": "<img src='/themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>",
        "Soloman": "<img src='/themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>",
        "Christina": "<img src='/themes/red/images/stories/Christina_and_Bani_large.jpg'   width='470'/><span class='littlemessage'><h2>Christina and Bani</h2><b class='country'>Ghana</b>Christina is HIV-positive, but her daughter Bani is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Christina receives treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants.<br /></span>",
        "Bani": "<img src='/themes/red/images/stories/Christina_and_Bani_large.jpg'   width='470'/><span class='littlemessage'><h2>Christina and Bani</h2><b class='country'>Ghana</b>Christina is HIV-positive, but her daughter Bani is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Christina receives treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants.<br /></span>",
        "Daniel": "<img src='/themes/red/images/stories/Daniel_large.jpg'   width='470'/><span class='littlemessage'><h2>Daniel</h2><b class='country'>Ghana</b>Daniel is HIV-positive and receives treatment at Tema General Hospital, funded by (RED)-financed Global Fund grants.<br /></span>",
        "Deborah": "<img src='/themes/red/images/stories/Deborah_Ashley_and_Desmond_large.jpg'   width='470'/><span class='littlemessage'><h2>Deborah Ashley and Desmond</h2><b class='country'>Ghana</b>Like many women living in the world’s poorest regions, it wasn’t until Deborah Ashley became pregnant with her son, Desmond, that she learned she was HIV-positive. Luckily for both of them, Deborah was diagnosed early and was carefully coached through her treatment.  Months later, she gave birth to a healthy, HIV-negative baby.  \"When I heard he was negative, I laughed, I cried, and I sang praises...  with the help of my medication, Desmond was negative.\"<br /></span>",
        "Desmond": "<img src='/themes/red/images/stories/Deborah_Ashley_and_Desmond_large.jpg'   width='470'/><span class='littlemessage'><h2>Deborah Ashley and Desmond</h2><b class='country'>Ghana</b>Like many women living in the world’s poorest regions, it wasn’t until Deborah Ashley became pregnant with her son, Desmond, that she learned she was HIV-positive. Luckily for both of them, Deborah was diagnosed early and was carefully coached through her treatment.  Months later, she gave birth to a healthy, HIV-negative baby.  \"When I heard he was negative, I laughed, I cried, and I sang praises...  with the help of my medication, Desmond was negative.\"<br /></span>",
        "Florence": "<img src='/themes/red/images/stories/Florence_and_Happy_large.jpg'   width='470'/><span class='littlemessage'><h2>Florence and Happy</h2><b class='country'>Ghana</b>As a proud mother of two HIV-negative sons, Florence didn\'t think she could get any happier.  Yet when Florence became pregnant with her third child, all of her dreams came true - a healthy, HIV-free baby girl.  Florence named her Happy, after her own elation, and spoke triumphantly when she said, \"Happy is negative.\"<br /></span>",
        "Happy": "<img src='/themes/red/images/stories/Florence_and_Happy_large.jpg'   width='470'/><span class='littlemessage'><h2>Florence and Happy</h2><b class='country'>Ghana</b>As a proud mother of two HIV-negative sons, Florence didn\'t think she could get any happier.  Yet when Florence became pregnant with her third child, all of her dreams came true - a healthy, HIV-free baby girl.  Florence named her Happy, after her own elation, and spoke triumphantly when she said, \"Happy is negative.\"<br /></span>",
        "Gladys": "<img src='/themes/red/images/stories/Gladys_large.jpg'   width='470'/><span class='littlemessage'><h2>Gladys</h2><b class='country'>Ghana</b>Gladys is a role model for men and women living with HIV.  Through the Models of Hope program at Korle Bu Clinic, funded by (RED)-financed Global Fund grants, Gladys counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.<br /></span>",
        "Joyce": "<img src='/themes/red/images/stories/Joyce_and_Benjamin_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Benjamin</h2><b class='country'>Ghana</b>Joyce is HIV-positive, but her son Benjamin is not, thanks to treatment that prevents HIV transmission from moms to their babies.  \"I didn\'t know how to express my joy,\" Joyce said, after hearing Benjamin tested negative, \"but I knew it was because of the drugs.\" <br /></span>",
        "Benjamin": "<img src='/themes/red/images/stories/Joyce_and_Benjamin_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Benjamin</h2><b class='country'>Ghana</b>Joyce is HIV-positive, but her son Benjamin is not, thanks to treatment that prevents HIV transmission from moms to their babies.  \"I didn\'t know how to express my joy,\" Joyce said, after hearing Benjamin tested negative, \"but I knew it was because of the drugs.\" <br /></span>",
        "Joyce_": "<img src='/themes/red/images/stories/Joyce_and_Delali_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Delali</h2><b class='country'>Ghana</b>Joyce is a beautiful and vibrant mother of two healthy children, Mawenia and Delali, who were both born HIV-free.  \"Before I start my story,\" Joyce says, \"people don’t believe I have HIV.\"  Thanks to Joyce’s role as a public HIV/AIDS Ambassador for the Heart to Heart organization, she is able to share her story with community groups, schools, and through the media in order to spread awareness of HIV prevention and treatment and to combat stigma through changing public perception of what it means to be HIV-positive.  As Joyce puts it, \"I\'d rather come out and say it myself... to help reduce stigma... this is how HIV is.\" <br /></span>",
        "Delali": "<img src='/themes/red/images/stories/Joyce_and_Delali_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Delali</h2><b class='country'>Ghana</b>Joyce is a beautiful and vibrant mother of two healthy children, Mawenia and Delali, who were both born HIV-free.  \"Before I start my story,\" Joyce says, \"people don’t believe I have HIV.\"  Thanks to Joyce’s role as a public HIV/AIDS Ambassador for the Heart to Heart organization, she is able to share her story with community groups, schools, and through the media in order to spread awareness of HIV prevention and treatment and to combat stigma through changing public perception of what it means to be HIV-positive.  As Joyce puts it, \"I\'d rather come out and say it myself... to help reduce stigma... this is how HIV is.\"<br /></span>",
        "Marvis": "<img src='/themes/red/images/stories/Marvis_and_Grace_large.jpg'   width='470'/><span class='littlemessage'><h2>Marvis and Grace</h2><b class='country'>Ghana</b>Marvis is HIV-positive, but her daughter Grace is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Marvis received this treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants. <br /></span>",
        "Grace": "<img src='/themes/red/images/stories/Marvis_and_Grace_large.jpg'   width='470'/><span class='littlemessage'><h2>Marvis and Grace</h2><b class='country'>Ghana</b>Marvis is HIV-positive, but her daughter Grace is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Marvis received this treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants. <br /></span>",
        "Motselisi": "<img src='/themes/red/images/stories/Motselisi_large.jpg'   width='470'/><span class='littlemessage'><h2>Motselisi</h2><b class='country'>Lesotho</b>Before beginning ARV treatment, Motselisi, at eleven months-old, weighed almost the same as the day she was born.  Within only 90 days of treatment, we saw what is called the Lazarus Effect, as ARV drugs brought Motselisi back to life.  Today, nearly four years later, Motselisi continues her treatment and is a happy, healthy, and playful young girl. <br /></span>",
        "Patience": "<img src='/themes/red/images/stories/Patience_and_Emilia_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience and Emilia</h2><b class='country'>Ghana</b>Patience and Emilia have a lot to smile about.  Thanks to treatment that helps prevent mother-to-child transmission of HIV, Emilia was born HIV-free. <br /></span>",
        "Emilia": "<img src='/themes/red/images/stories/Patience_and_Emilia_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience and Emilia</h2><b class='country'>Ghana</b>Patience and Emilia have a lot to smile about.  Thanks to treatment that helps prevent mother-to-child transmission of HIV, Emilia was born HIV-free. <br /></span>",
        "Patience_": "<img src='/themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>",
        "Simon": "<img src='/themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>",
        "Ann": "<img src='/themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>",
        "Rita": "<img src='/themes/red/images/stories/Rita_Simon_and_Simona_large.jpg'   width='470'/><span class='littlemessage'><h2>Rita, Simon and Simona</h2><b class='country'>Ghana</b>Rita\'s HIV-positive, but her children Simon and Simona are not, thanks to treatment that prevents mother-to-child transmission of HIV.   Today, she\'s as strong as ever and able to support her healthy family.  \"I continue to take the drugs. It\'s been three years now and I\'ve never felt ill.\" <br /></span>",
        "Simona": "<img src='/themes/red/images/stories/Rita_Simon_and_Simona_large.jpg'   width='470'/><span class='littlemessage'><h2>Rita, Simon and Simona</h2><b class='country'>Ghana</b>Rita\'s HIV-positive, but her children Simon and Simona are not, thanks to treatment that prevents mother-to-child transmission of HIV.   Today, she\'s as strong as ever and able to support her healthy family.  \"I continue to take the drugs. It\'s been three years now and I\'ve never felt ill.\" <br /></span>",
        "Ruth": "<img src='/themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>",
        "Benedicta": "<img src='/themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>",
        "Michael_": "<img src='/themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>",
        "Stella": "<img src='/themes/red/images/stories/Stella_and_Davis_large.jpg'   width='470'/><span class='littlemessage'><h2>Stella and Davis</h2><b class='country'>Ghana</b>As a role model in the Models for Hope program at Korle Bu clinic, Stella counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.   As she had done for her son Davis, Stella helps to ensure that future generations of children are born HIV-free. <br /></span>",
        "Davis": "<img src='/themes/red/images/stories/Stella_and_Davis_large.jpg'  width='470'/><span class='littlemessage'><h2>Stella and Davis</h2><b class='country'>Ghana</b>As a role model in the Models for Hope program at Korle Bu clinic, Stella counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.   As she had done for her son Davis, Stella helps to ensure that future generations of children are born HIV-free. <br /></span>"
    };

    var message = nameList[name];

    jQuery('body').prepend('<div id="overlay"> </div>');
    jQuery('body').prepend('<div id="notebox" class="storybox">' + message + '<br class="clear"/> <span class="littlemessagebuttons" style="position:relative;"><img src="/themes/red/images/redresults_small.png" style="box-shadow: 0 0 0px #333333; position:absolute; top:0px; left:5px;"/><a href="#" class="closebutton buttonminor button white">Close</a></span></div>');
    jQuery('#overlay, .closebutton').click(function() {
        jQuery('#overlay, #notebox').remove();
        return false;
    });

}

