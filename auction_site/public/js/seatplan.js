/* seatplan.JS */
(function($){
    $.fn.extend({ 
        seatplan: function() {
        // define some variables
        var auctionitems = $(this);
		var seatplanswitch = $('#auctioncontainer').data("seatplanswitch"); // this is default and gets overridden below. It is here in case we want to extend some control to the DB/Admin
		var seatplanimage = $('#auctioncontainer').data("seatplanimage");
		var seatplanscript = $('#auctioncontainer').data("seatplanscript");
		var seatplansvg = $('#auctioncontainer').data("seatplansvg");
 
		//console.log('seatplanimage = '+seatplanimage); console.log('seatplanscript = '+seatplanscript); console.log('seatplansvg = '+ seatplansvg);
 
		  /*/////////////////////// seatplan generator */
			/* the seatplanswitch dictates the seatplan functionality in seatplan.js
			it could be set by the data-seatplanswitch attribute on the #auctioncontainer but is also overriden by JS searching for available files. 
			Concept is to deploy what is required based on content but also re-generate the seatplan if window size changes
			0 = no seatplan. just list the auctionitems
			1 = seatplan is a static image
			2 = fully interactive seatplan with list 
			3 = full width seatplan - no list (think NIN visual)    
			4 = svg upgrade */
       
		if (seatplanscript == "" && seatplanimage != "") {seatplanswitch = 1;}
		if (seatplanscript != "" && seatplanimage != "") {seatplanswitch = 2;}
		if (seatplansvg != "") {seatplanswitch = 4;}    
		
        // console.log('seatplanswitch = '+seatplanswitch);
 
     if (seatplanswitch > 0) {
          if (seatplanswitch == 1) { // seatplan is static image - seatplan1   //////////////////////////////////
          // prepare the area
          $(this).prepend('<div id="seatmap"><img src="/'+$('#auctioncontainer').data("seatplanimage")+'" /></div>');
          $('#auctionitems').addClass('seatplan1');
          }  // end 1
   
   
            if (seatplanswitch == 2) { // interactive seating plan - seatplan2 //////////////////////////////////
          // prepare the area
          $('#auctionitems').addClass('seatplan2');
          $(this).prepend('<div id="seatmap"></div>');
          $('#seatmap').append('<div id="floorplanbackground" style="width:1600px; top:-708px; left:-902px;  display:none;margin-left:50%;"  class="unselectable"><div id="seatcontainer" style="pointer-events: none;"><img src="/'+$("#auctioncontainer").data("seatplanimage")+'" style="width:1600px; pointer-events: none;"/></div></div>');
          $('#seatmap').append('<div id="foreground"></div>');  // removed in CSS for IE
          $('#seatmap').append('<div id="zoomin" class="ready"></div><div id="zoomout" class="ready"></div>');
          if ($('#floorplanbackground').length != 0) {$('#floorplanbackground').draggable();}



           $.getJSON('/'+$('#auctioncontainer').data("seatplanscript"),function(result){
           $('#floorplanbackground').css({'top': result.data.starttop , 'left': result.data.startleft }).fadeIn();
  		    var currentbidammount = "";
  		    var itemid = "";
  		     $.each(result.seatlayout, function(i,seat){  
  		          currentbidammount = $('li[rel="'+seat.seatid+'"]').find('.itm-current').html();
				if (parseFloat(currentbidammount.replace(/[^0-9.-]+/g, '')) < 0.01) 
				{   currentbidammount =   $('li[rel="'+seat.seatid+'"]').find('.starting_bid').html();  } 
  		          itemid = $('li[rel="'+seat.seatid+'"]').attr('data-itemid');
                  if (itemid) {$("#seatcontainer").append('<div id="'+seat.seatid+'" rel="'+seat.seatid+'" class="seaticon" style="top:'+seat.top+'; left:'+seat.left+';width:'+seat.width+';height:'+seat.width+';" data-top="'+seat.top+'" data-left="'+seat.left+'" data-width="'+seat.width+'" data-height="'+seat.width+'" data-itemid="'+itemid+'">	<div class="tooltip"><span><span class="pretext">Current Bid</span>:&nbsp;<b class="bidchecker" data-itemid="'+itemid+'">'+currentbidammount+'</b></span><span class="littlearrow"></span></div></div><div id="'+seat.droneseatid+'" rel="'+seat.seatid+'" class="seaticon seat'+seat.seatid+'" rel="'+seat.seatid+'" style="top:'+seat.droneseattop+';left:'+seat.droneseatleft+';width:'+seat.droneseatwidth+';height:'+seat.droneseatheight+';" data-top="'+seat.droneseattop+'" data-left="'+seat.droneseatleft+'" data-width="'+seat.droneseatwidth+'" data-height="'+seat.droneseatwidth+'" data-itemid="'+itemid+'"></div>');    }
      
             
             }); // each

              if ($('#seatdetails').data('itemid')){   $('.seaticon[data-itemid="'+$('#seatdetails').data('itemid')+'"]').addClass('selected');}
	   
				// get values from seating dropdown and apply them to seats
				seatArray =[];
				seatArray = $.makeArray($('#auctionitemlist li').not('#auctionitemlist li:first-child').map(function() {return $(this).attr("data-itemref");}));
			    $.each(seatArray, function() {
 				var x = this.toString();
			  
				// show the available seats
				if ($('#auctionitemlist li[rel="'+x+'"]').hasClass('ended')) { }
				else {
 				$('#' + x).addClass('available');
				$('#drone' + x).addClass('available');
                }
                });
                
				// Seat hover functionality
				$('.seaticon, #auctionitemlist li').hover(function(){  
					var seatid = $(this).attr('rel'); $('[rel="'+seatid+'"]').addClass('hover'); $('.seaticon.available[rel="'+seatid+'"]').find('.tooltip').show();
					}, function(){ var seatid = $(this).attr('rel'); $('[rel="'+seatid+'"]').removeClass('hover').find('.tooltip').hide();  
				});


               
               
               
				 // Seat click functionality
				  $('.seaticon').click(function(){
				  itemid = $(this).attr('data-itemid');
				  gotoauction(itemid);
				  });

       
       
				/* Zoom Functionality */
				// set globals from zoom level 1
				var zoomlevel = 0;
				var floorplan = $('#seatmap #floorplanbackground');
				var floorplanimg = $('#seatmap #floorplanbackground #seatcontainer>img');

 
				// ZOOM OUT
				  $('#zoomout.ready').click(function(){ 

				  $('#seatmap').css('background-image','none');
					var leftval = parseFloat(floorplan.css('left')) / 1.6;
					var widthval = parseFloat(floorplan.css('width')) / 1.6;
					var topval = parseFloat(floorplan.css('top')) / 1.6;
				  if (widthval > 500) {    $('#floorplanbackground').draggable("disable");
					  zoomlevel = zoomlevel -= 1;
	 
						$('#zoomin').addClass('ready').css('pointer-events','auto');
					   $('#zoomout').removeClass('ready').css('pointer-events','none');

					$(floorplan).animate({left: leftval,   width: widthval,top: topval}, 1000, function() {   $('#floorplanbackground').draggable("enable");});
					$(floorplanimg).animate({width: widthval}, 1000, function() {  if (floorplan.width() > 600)  {$('#zoomout').addClass('ready').css('pointer-events','auto')}  });
  


				  $('#seatmap .seaticon').each(function(){
						if(zoomlevel == 0) { 
						var leftval = $(this).data('left');
						var widthval = $(this).data('width');
						var topval = $(this).data('top');
						var heightval = $(this).data('height');
						}
						else {		
						var leftval = (parseFloat($(this).css('left')) / 1.6);
						var widthval = (parseFloat($(this).css('width')) / 1.6);
						var topval = (parseFloat($(this).css('top')) / 1.6);
						var heightval = (parseFloat($(this).css('height')) / 1.6);
						}
					   $(this).animate({left: leftval,   width: widthval,height: heightval,top: topval}, 1000, function() {});
				   }); 

				  } 
				 });


				// ZOOM IN
				  $('#zoomin.ready').click(function(){ 
					var leftval = parseFloat(floorplan.css('left')) * 1.6;
					var widthval = parseFloat(floorplan.css('width')) * 1.6;
					var topval = parseFloat(floorplan.css('top')) * 1.6;
				  if (widthval < 3500) {      $('#floorplanbackground').draggable("disable");
						zoomlevel = zoomlevel += 1;
		 
						$('#zoomin').removeClass('ready').css('pointer-events','none');
						$('#zoomout').addClass('ready').css('pointer-events','auto');
						$(floorplan).animate({ left: leftval, width: widthval, top: topval }, 1000, function() {$('#floorplanbackground').draggable("enable");});
						$(floorplanimg).animate({ width: widthval }, 1000, function() {  if (floorplan.width() < 3500)  {$('#zoomin').addClass('ready').css('pointer-events','auto')}  });
	
					  $('#seatmap .seaticon').each(function(){
						if(zoomlevel == 0) { 
						var leftval = $(this).data('left');
						var widthval = $(this).data('width');
						var topval = $(this).data('top');
						var heightval = $(this).data('height');
						}
						else {		
						var leftval = Math.ceil((parseFloat($(this).css('left')) * 1.6));
						var widthval = Math.ceil((parseFloat($(this).css('width')) * 1.6));
						var topval = Math.ceil((parseFloat($(this).css('top')) * 1.6));
						var heightval = Math.ceil((parseFloat($(this).css('height')) * 1.6));
						}
						$(this).animate({left: leftval,   width: widthval,height: heightval,top: topval}, 1000, function() {  });
					 });}	
	 
				  }); // end zoom functionality
	   
       
       
       
       
       
        
           });  //  END getJSON Floorplan generator ///////////////////////  
 
 
 
 
 
 
 
          } // end 2
 
                if (seatplanswitch == 4) { // SVG seatplan   ////////////////////////////////////////////////////////////////////
          // prepare the area
          $(this).prepend('<div id="seatmap"> SVG seatmap next phase 2.0 </div>');
          $('#auctionitems').addClass('seatplan4');
          } // end 4
 
     
     
     
     
      } // end seatplanswitch
 

	 }
    });
})(jQuery);

 