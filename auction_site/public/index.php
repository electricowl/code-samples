<?php
ini_set('date.timezone', 'UTC');

define('AUCTION_SCHEMA', 'auction');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

defined('LIBRARY_PATH')
    || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../library'));

// We define all paths in relation to this one.
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/../'));

// Themes path
defined('THEMES_PATH')
    || define('THEMES_PATH', realpath(dirname(__FILE__) . '/../themes'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

defined('SERVER_NAME')
    || define('SERVER_NAME', $_SERVER['SERVER_NAME']);

define("TITLE_SEPARATOR", " | ");

define("PAYPAL_DEBUG", false);

// Use this to trigger a lockdown of the site
define('MAINTENANCE_MODE', true);

// This equates to crowdsurge and the null baseUrl();
define('DEFAULT_CLIENT_ID', 1);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();