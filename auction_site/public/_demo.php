<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
   <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <title>Auction Site | Auction Details</title>
    <link href="css/smoothness/jquery-ui-1.9.2.custom.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/normalise.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/cal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/cal-red.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/libs/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/libs/jquery.jcountdown1.4.js"></script>
<script type="text/javascript" src="js/seatplan.js"></script>
<script type="text/javascript" src="js/auction.js?cachebust=1369967972"></script>
<script type="text/javascript" src="js/libs/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="js/master.js"></script>
    </head>
<body>

<section id="outer">
    <header>
        <h1>Ticket Auction</h1>
        <nav>
            <ul class="nav cf">

                <li class="nav-left"><a href="/">All Artists</a></li>

                <li class="nav-left divider">/</li><li class="nav-left"><a href="/auction/list/campaign_id/11">RSdesign - Ryan's Auction Test Campaign</a></li><li class="nav-left divider">/</li><li class="nav-left">May 9th, 2015 at DEMO Venue</li>
                
                    <li class="nav-right"><a href="/signin">Sign In</a></li>

                
            </ul>
        </nav>
    </header>

    
    
<div id="auctioncontainer" data-auctionID="19" data-seatplanimage="images/seatplans/19.jpg" data-seatplanscript="js/seatplans/19.js" data-seatplansvg="" data-seatplanswitch="0" data-time="1369967972" data-formaction="/auction/confirm-bid/campaign_id/11/auction_id/19/item_id/">
    <main role="main">
        <section id="items" class="cf">

            <div class="itm-top cf" >
                <div class="itm-left">
                    <img src="/themes/red/images/781_wide.jpg" class="itm-pic" alt="RSdesign" />
                </div>
                <div class="itm-right itm-details">
                    <span class="itm-campaign">RSdesign</span>
                    <span class="itm-date">May 9th, 2015</span>
                    <span class="itm-venue">DEMO Venue</span>
                    <span class="itm-loc">V_Town, V_County, United Kingdom</span>

                    <div class="itm-about">
                        <p><span>This is some dummy auction description text from 32195.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span><span>This is some dummy auction description text.&nbsp;</span></p>
                    </div>

                    <div class="itm-share">
                        <div class="fb-like" data-href="http://dev.auction.crowdsurge.com/auction/19" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false" data-font="arial"></div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-hashtags="REDrows" data-related="joinRED‎" data-text="Bid on tickets to see RSdesign. Great Music Saves Lives." data-url="http://dev.auction.crowdsurge.com/auction/19">Tweet</a>
                    </div>

                </div>
            </div>
            <div class="itm-auction cf">
           

                <div class="itm-auctions" id="auctionitems">
                    <div id="auctionitemlist">
                        <ul class="itm-list">
                            <li class="itm-row itm-header itm-alt">
                                <div class="itm-name">Auctions</div>
                                <div class="itm-time">Time Left</div>
                                <div class="itm-bids">Bids</div>
                                <div class="itm-current">Current Bid</div>
                                <div class="itm-action">&nbsp;</div>
                            </li>
                                                        
                                
                                <li class="itm-row  auction_item" data-itemref="block_1_row_a_seats_1_and_2" rel="block_1_row_a_seats_1_and_2" data-itemid="33" data-bidhistory='[{"bidder":"ryan","bid":"10.00","time":"1369756488"}]' data-currency="&pound;">
                                    <div class="itm-name">Block 1, Row A, Seats 1 & 2</div>
                                      <div class="itm-time timeleft">
                                        <b><span data-time="1400021100" class="cdown"></span></b>
                                    </div>
                                    <div class="itm-bids">1</div>
                                    <div class="itm-current">&pound;5.00</div>
                                    <div class="itm-action action"><a href="/auction/show-item/campaign_id/11/auction_id/19/item_id/33" data-itemid="33" class="button">BID</a></div>

                                    <span class="auctionitem">Block 1, Row A, Seats 1 & 2</span>
                                    <span class="customref">1538618_1538619</span>
                                    <span class="description">Reserved Seating</span>
                                  
                                    <span class="duration">
                                        <span class="start_date">2013-05-13 09:00:00+01:00</span>
                                        <span class="end_date">2014-05-13 23:45:00+01:00</span>
                                    </span>
                                    <span class="starting_bid">
                                        &pound;10.00                                    </span>
                                    <span class="next_min_bid">
                                        &pound;5.00                                    </span>
                                    <span class="min_bid_increment">
                                        &pound;0.00                                    </span>
                                    <span class="max_bid_increment">
                                        &pound;0.00                                    </span>
                                    <span class="bidhistory">
                                        1                                    </span>
                                    <span class="currentbid">
                                        &pound;16.00                                    </span>
                                 </li>

                            
                                
                                <li class="itm-row itm-alt auction_item" data-itemref="block_1_row_a_seats_3_and_4" rel="block_1_row_a_seats_3_and_4" data-itemid="34" data-bidhistory='[{"bidder":"Susan","bid":"200.00","time":"1369845699"},{"bidder":"dave","bid":"185.00","time":"1369845699"},{"bidder":"ryan","bid":"105.00","time":"1369845699"},{"bidder":"ryan","bid":"10.00","time":"1369756488"}]' data-currency="&pound;">
                                    <div class="itm-name">Block 1, Row A, Seats 3 & 4</div>
                                      <div class="itm-time timeleft">
                                        <b><span data-time="1369973030" class="cdown"></span></b>
                                    </div>
                                    <div class="itm-bids">2</div>
                                    <div class="itm-current">&pound;200.00</div>
                                    <div class="itm-action action"><a href="/auction/show-item/campaign_id/11/auction_id/19/item_id/34" data-itemid="34" class="button">BID</a></div>

                                    <span class="auctionitem">Block 1, Row A, Seats 3 & 4</span>
                                    <span class="customref">1538620_1538621</span>
                                    <span class="description">Reserved Seating</span>
                                  
                                    <span class="duration">
                                        <span class="start_date">2013-05-13 09:00:00+01:00</span>
                                        <span class="end_date">2014-05-13 23:45:00+01:00</span>
                                    </span>
                                    <span class="starting_bid">
                                        &pound;10.00                                    </span>
                                    <span class="next_min_bid">
                                        &pound;110.00                                    </span>
                                    <span class="min_bid_increment">
                                        &pound;5.00                                    </span>
                                    <span class="max_bid_increment">
                                        &pound;20.00                                    </span>
                                    <span class="bidhistory">
                                        2                                    </span>
                                    <span class="currentbid">
                                        &pound;200.00                                    </span>
                                 </li>

                            
                                
                                <li class="itm-row  auction_item" data-itemref="block_1_row_a_seats_5" rel="block_1_row_a_seats_5" data-itemid="35" data-bidhistory='[{"bidder":"jonattfield","bid":"10.00","time":"1369926236"}]' data-currency="&pound;">
                                    <div class="itm-name">Block 1, Row A, Seats 5</div>
                                      <div class="itm-time timeleft">
                                        <b><span data-time="1400021100" class="cdown"></span></b>
                                    </div>
                                    <div class="itm-bids">1</div>
                                    <div class="itm-current">&pound;15.00</div>
                                    <div class="itm-action action"><a href="/auction/show-item/campaign_id/11/auction_id/19/item_id/35" data-itemid="35" class="button">BID</a></div>

                                    <span class="auctionitem">Block 1, Row A, Seats 5</span>
                                    <span class="customref">1538622</span>
                                    <span class="description">Reserved Seating</span>
                                  
                                    <span class="duration">
                                        <span class="start_date">2013-05-13 09:00:00+01:00</span>
                                        <span class="end_date">2014-05-13 23:45:00+01:00</span>
                                    </span>
                                    <span class="starting_bid">
                                        &pound;10.00                                    </span>
                                    <span class="next_min_bid">
                                        &pound;100.50                              </span>
                                    <span class="min_bid_increment">
                                        &pound;0.00                                    </span>
                                    <span class="max_bid_increment">
                                        &pound;0.00                                    </span>
                                    <span class="bidhistory">
                                        1                                    </span>
                                    <span class="currentbid">
                                        &pound;10.00                                    </span>
                                 </li>

                            

                        </ul>
                            
                    </div>
                 
                </div>
            </div>

            <div class="itm-bottom cf">
                <div class="itm-left">
                    <h6>Important Information</h6>
                    <p>Many events contain multiple auctions - frequently for different pairs of seats - for which you may choose to place a bid. When placing a bid, please make sure to enter an amount that is equal to or greater than the minimum bid amount required, which may be found next to the bid entry box. You may bid as frequently as you would like, but we suggest bidding the maximum amount you are willing and having the system automatically raise your bid incrementally up to your maximum if you are outbid. Your bid will never be increased beyond the minimum bid amount required to maintain your place as the top bidder. Your credit card will only be charged if you are the highest bidder at the close of the auction.</p>
                    <h6>If You Win</h6>
                    <p>If you are the winning bidder, your tickets and any other outlined items included in the lot will be available for you to collect at the venue on the evening of the event upon presenting valid photo identification and the credit card you used to make your purchase.</p>
                    <h6>Contribution To The Global Fund</h6>
                    <p>All proceeds from this auction will be contributed to The Global Fund, the world's leading financier of programs to fight AIDS, TB and malaria, and the recipient of (RED) funds. The Global Fund invests 100% of (RED) dollars in HIV/AIDS programs in Africa, including interventions targeting women and children. (RED)-supported Global Fund HIV/AIDS grants are selected based on a track record of consistently good performance and opportunity for impact.</p>

                    <p>Complete Entertainment Resources LLC dba CrowdSurge will serve as agent on behalf of The Global Fund. All contributed funds will be remitted to Complete Entertainment Resources LLC and subsequently disbursed to The Global Fund. The processing time for remittance of funds to The Global Fund will be 30-180 days to allow for possible returns or exchanges.</p>

                    <p>Proceeds from this auction consist of the total winning bid amount collected, less the face value (or standard price) of the tickets, applicable facility and service fees, and any local taxes, and credit card fees.</p>

                    <p>Please Note: The Global Fund is a Geneva based organization and as such contributions are not tax deductible. No letters of donation will be provided by (RED) and (RED) makes no assurances, guarantees, or other claims to a bidders ability to claim a charitable deduction as part of this auction. If you are interested in the tax implications of participation, please consult a tax professional.</p>
                </div>


                <div class="itm-right">
                    <div class="itm-before">
                        <h6>Before You Bid</h6>
                        <p>Your bid is a legal contract. All bids placed on (RED) auctions are legally binding. If you are the successful bidder, you will enter into a legal contract to purchase the item and will be considered the buyer of the lot. All lots are non-transferable and can only be used by the winning bidder without exception.</p>
                    </div>

                    <div class="itm-donate">
                        <h6>Want To Do More?</h6>
                        <p>Like (RED) on Facebook and get involved.<br />Donate directly to The Global Fund, the recipient of (RED) funds.</p>
                        <a href="https://secure.globalproblems-globalsolutions.org/site/Donation2?df_id=1862&1862.donation=form1&__utma=1.3307738938720983500.1233685014.1240944250.1247422614.4&__utmb=1.1.10.1247422614&__utmc=1&__utmx=-&__utmz=1.1247422614.4.4.utmcsr=theglobalfund.org|utmccn=%28referral%29|utmcmd=referral" class="button">Donate</a>
                    </div>

                </div>


            </div>


        </section>
    </main>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=466608106741768";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <footer>
        <ul class="nav cf">
            <li class="nav-left"><a href="/info/about">Privacy</a></li>
            <li class="nav-left"><a href="/info/terms-and-conditions">Terms</a></li>
            <li class="nav-left ft-support"><a href="/info/contact">Support</a></li>
            <li class="nav-right nav-logo">powered by CrowdSurge</li>
        </ul>
    </footer>
</section>

<script type="text/javascript">
(function(){
    jQuery(document).ready(function(){
        jQuery("#flash_message").effect("highlight", {}, 5000).fadeOut(2000);
    });
})()
</script>
</body>
</html>