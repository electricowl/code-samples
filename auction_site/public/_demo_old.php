<!DOCTYPE html><!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
 <!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
       
   
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Auction Site</title>
  <link href="css/smoothness/jquery-ui-1.9.2.custom.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/red.css" media="screen" rel="stylesheet" type="text/css" />
   
  <script type="text/javascript" src="js/libs/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/libs/jquery.jcountdown1.4.js"></script>
<script type="text/javascript" src="js/seatplan.js"></script>
<script type="text/javascript" src="js/auction.js"></script>
<script type="text/javascript" src="js/libs/jquery-ui-1.9.2.custom.js"></script></head>
<body> 


<div id="container">
        <div>
        <span><a href="/">Home</a></span>
        <span style="float: right;">You are not logged in&nbsp; &nbsp;<a href="/user/login">login</a></span>
    </div>
    
        <div class="content_area">
 
       <h1>Event 5 - Static Content</h1>


<div id="auctioncontainer" data-time="1368959683" data-seatplanswitch="0" data-seatplansvg="" data-seatplanscript="js/seatplans/8.js" data-seatplanimage="images/seatplans/8.jpg" data-auctionid="8" > 
		<h2>Justin Timberlake<!-- artist name --></h2>
		<span id="date">December 1st, 2013</span>
		<span id="venue">MGM grand theater, Las Vegas, NV</span>
		<div id="sharelinks">[twitter][facebook]</div>
		<span id="auction_description">Experience Justin Timberlake live from the best seats in the house as he performs his most recent <em>The 20/20 Experience</em> album. Your bid helps support the fight against HIV/AIDS. <br><br> <strong>You Are Bidding On:</strong> One (1) pair of premium seats.</span>
	</div><!--/div auctiondetails-->

	<div id="auctionitems">

 <div id="auctionitemlist">
      	<ul>
      	<li class="headerrow"><span class="itemname">Auctions</span><span class="timelefts">Time&nbsp;Left</span><span class="bidhistory">Bids</span><span class="currentbids">Current&nbsp;Bid</span></li>

            
            
            <li data-itemref="floor_a_row_4_seats_1_and_2" rel="floor_a_row_4_seats_1_and_2" data-itemid="8"  class="auction_item">
                <span class="auctionitem">Floor A, Row 4, Seats 1 and 2</span>
                <span class="customref">this is an auction lot</span>
                <span class="description">this is an auction lot description</span><span class="timeleft"><b><span data-time="1368997200" class="cdown"></span></b></span><span class="duration">
                    <span class="start_date">2013-05-01 10:00:00</span>
                    <span class="end_date">2013-05-19 21:00:00</span>
                </span><span class="starting_bid">$150.00</span><span class="min_bid_increment">$10.00</span><span class="max_bid_increment">$1000.00</span><span class="bidhistory">0</span><span class="currentbid">$0.00</span><span class="action"><a class="button" href="/auction/show-item/campaign_id/6/auction_id/8/item_id/8" data-itemid="8">BID</a></span>
                    <!--<form action="/auction/show-item/campaign_id/6/auction_id/8/item_id/8" method="post">
                        <div><input type="hidden" name="plan_data" value="margin-left=20&margin-top=40" /></div>
                        <span><input type="submit" name="View" id="View" value="View" /></span>
                    </form>-->
               

                <span class="item_form">
                    <form enctype="application/x-www-form-urlencoded" method="post" action="/auction/confirm-bid/campaign_id/6/auction_id/8/item_id/8">
                        <div class="cs_form">
                            <fieldset>
                                <div class=""><input type="text" name="bid_amount" value="" attributes="20 new_bid" /></div>
                                <span><input type="submit" name="BidNow" value="Bid Now" /></span>
                            </fieldset>
                        </div>
                    </form>
                </span>


                            </li>

            
            
            <li data-itemref="floor_a_row_4_seats_3_and_4" rel="floor_a_row_4_seats_3_and_4" data-itemid="9"  class="auction_item">
                <span class="auctionitem">Floor A, Row 4, Seats 3 and 4</span>
                <span class="customref">this is an auction lot</span>
                <span class="description">this is an auction lot description</span><span class="timeleft"><b><span data-time="1368918360" class="cdown"></span></b></span><span class="duration">
                    <span class="start_date">2013-05-01 23:06:00</span>
                    <span class="end_date">2013-05-18 23:06:00</span>
                </span><span class="starting_bid">&pound;130.00</span><span class="min_bid_increment">&pound;10.00</span><span class="max_bid_increment">&pound;1000.00</span><span class="bidhistory">0</span><span class="currentbid">&pound;0.00</span><span class="action"><a class="button" href="/auction/show-item/campaign_id/6/auction_id/8/item_id/9" data-itemid="9">BID</a></span>
                    <!--<form action="/auction/show-item/campaign_id/6/auction_id/8/item_id/9" method="post">
                        <div><input type="hidden" name="plan_data" value="margin-left=20&margin-top=40" /></div>
                        <span><input type="submit" name="View" id="View" value="View" /></span>
                    </form>-->
               

                <span class="item_form">
                    <form enctype="application/x-www-form-urlencoded" method="post" action="/auction/confirm-bid/campaign_id/6/auction_id/8/item_id/9">
                        <div class="cs_form">
                            <fieldset>
                                <div class=""><input type="text" name="bid_amount" value="" attributes="20 new_bid" /></div>
                                <span><input type="submit" name="BidNow" value="Bid Now" /></span>
                            </fieldset>
                        </div>
                    </form>
                </span>


                            </li>

                        	
            	
            	<!-- test li <li data-itemref="floor_a_row_4_seats_1_and_2" rel="floor_a_row_4_seats_1_and_2"><span class="auctionitem">Floor A, Row 4, Seats 1 & 2</span><span class="timeleft"><b><span data-time="1368795660" class="cdown"></span></b></span><span class="bidhistory">6</span><span class="currentbid">$1,150.00</span><span class="action"><a class="button" href="#">View</a></span></li>-->

        </ul>
    </div><!--/div auctionitemlist-->
    
    
	<br class="clear"/>
	</div><!--/div auctionitems-->
</div><!--/div auctioncontainer-->




    </div>
  
  
  
    <div class="footer">
        <span style="float: right;">&nbsp;<a href="/admin/login" >Admin Login</a></span>
        <span style="float: right;">&nbsp;<a href="/client/login" >Client Login</a></span>
        <span><a href="/info/about">About</a>&nbsp;&nbsp;<a href="/info/contact">Contact</a>&nbsp;&nbsp;<a href="/info/terms-and-conditions">Terms and Conditions</a></span>
        <span class="copyright">&copy; 2012-2013 CER Ltd</span>
    </div>



</div> <!--/div container-->
</body>
</html>

