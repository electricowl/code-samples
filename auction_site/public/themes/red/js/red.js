/* RED.JS */
 



 jQuery(document).ready(function(){	//this is jquery 


   
/* //////////////////////////////
  add the stories under the bid input */  
jQuery('#yourbid').append("<div id='redrows'><span>YOUR BID COULD HELP PROVIDE<b></b> <span class='people'>PEOPLE</span> LIFE-SAVING HIV/AIDS MEDICINE FOR 1 YEAR.</span><ul id='stories'></ul><span><small>This medicine costs around 40¢ a day. It helps keep people living with HIV healthy, and can help HIV-positive pregnant women deliver HIV free babies.</small></span></div>");
jQuery('input[name="bid"]').keydown(function(event){
if ((!event.shiftKey && !event.ctrlKey && !event.altKey) && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105))) // 0-9 or numpad 0-9, disallow shift, ctrl, and alt
{// check textbox value now and tab over if necessary
}
else if (event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 110 && event.keyCode != 9 && event.keyCode !=  190 ) // not esc, del, left or right
{ event.preventDefault();}
// else the key should be handled normally
});
oldval = ""; 
jQuery('input[name="bid"]').keyup(function(){ var newval = jQuery(this).val(); if  (newval != oldval) { stories(newval);  oldval = newval;}     });

   }); // jquery.ready FIN ///////////////////////////// do not delete
 






// (RED STORIES )

function stories(bidvalue) {
	/* 
	this function displays the amount of stories displayed when a 
	customer bids and controls the sizes as the quantity gets bigger.
	*/
	var howmanysaved = bidvalue / 150;
	howmanysaved = parseInt(howmanysaved);
var storyarray = new Array("Charles","Beatrice","Patience_", "Ann", "Doris","Joyce_", "Deborah","Delali", "Michael_","Desmond", "Michael", "Dorcas", "Jason", "Catherine", "Esther", "Soloman", "Christina", "Bani", "Daniel", "Florence", "Happy", "Gladys", "Joyce", "Benjamin", "Marvis", "Grace", "Motselisi", "Patience", "Emilia", "Rita", "Simona", "Ruth", "Benedicta", "Stella", "Davis" );
	if (isNaN(howmanysaved)) {jQuery('#yourbid #redrows').hide();}	
	if (howmanysaved == 0) {jQuery('#yourbid #redrows').hide();}
		if (howmanysaved != 0)  {
		jQuery('#yourbid #redrows').fadeIn();
		jQuery('#yourbid #redrows > span > b').html(howmanysaved);
		jQuery('ul#stories').html("");    
		   
		   // Shuffle the names
			function randomit(arr){ for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);return arr;}
		  
			// put stories on page depending on bid value
			var getstories =   $.each(randomit(storyarray),function(i, name){ 
			if (i < howmanysaved) {
			namewspace = name.replace(/_/g," ");
			jQuery('ul#stories').append('<li><a href="#" id="'+ name +'"><img src="themes/red/images/stories/black.jpg" class="'+ name +'"/><span class="name">'+ namewspace +'</span></a></li>');   
			 
			jQuery('ul#stories a img.'+ name).attr('src','themes/red/images/stories/'+ name +'.jpg');
			var storywidth = 90;
			var storyheight = 90;
			if (howmanysaved == 1) {jQuery('#redrows .people').text('PERSON');}
			if (howmanysaved > 1) {jQuery('#redrows .people').text('PEOPLE'); }
			if ((howmanysaved > 3) && (howmanysaved <= 5)) {storywidth = 60;storyheight = 60;}
			if ((howmanysaved > 5) && (howmanysaved <= 13)) {storywidth = 40;storyheight = 40;}
			if ((howmanysaved > 13) && (howmanysaved <= 25)) {storywidth = 35;storyheight = 34; jQuery('#stories .name').hide(); jQuery('ul#stories li').css('margin','2px');}
			if ((howmanysaved > 25) && (howmanysaved <= 99999)) {storywidth = 20;storyheight = 19; jQuery('#stories .name').hide(); jQuery('ul#stories li').css('margin','2px');}
			jQuery('ul#stories a img').css({'width':storywidth,'height':storyheight});
			
			}
		});
 	}
	if (isNaN(howmanysaved)) {jQuery('#yourbid #redrows').hide();}

	jQuery('ul#stories li a').click(function(){ 
	var name = jQuery(this).attr('id');  storybox(name); return false;
	});


}

  // little message story details
function storybox(name) {
jQuery('window').scrollTop();  
jQuery('document').parent().find('window').scrollTop();  
jQuery('#overlay, #notebox').remove();

var message = "";



// STORIES CONTENT
if (name == "Beatrice") {var message = "<img src='themes/red/images/stories/Beatrice_and_Charles_large.jpg'   width='470'/><span class='littlemessage'><h2>Beatrice and Charles</h2><b class='country'>Ghana</b>Beatrice and Charles, both HIV-positive, now know they and their sons can live long and healthy lives.  Life-saving ARV treatment helps keep them and their eldest son, Bright, healthy and helped prevent their youngest son, Herbert, from contracting the disease.<br /></span>";}
if (name == "Charles") {var message = "<img src='themes/red/images/stories/Beatrice_and_Charles_large.jpg'  width='470' /><span class='littlemessage'><h2>Beatrice and Charles</h2><b class='country'>Ghana</b>Beatrice and Charles, both HIV-positive, now know they and their sons can live long and healthy lives.  Life-saving ARV treatment helps keep them and their eldest son, Bright, healthy and helped prevent their youngest son, Herbert, from contracting the disease.<br /></span>";}
if (name == "Doris") {var message = "<img src='themes/red/images/stories/Doris_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Doris and Michael</h2><b class='country'>Ghana</b>Almost 2 years ago, (RED) met Doris and her son Michael, who was born HIV-negative thanks to treatment that prevented the transmission of HIV during pregnancy.  Today, Doris is more vibrant than ever.  \"Life is good to me.  Now I\'m healthy...  taking my drugs...  I\'m taking good care of myself and my boy,\" she says, beaming with pride.  At the top of his class at school, it seems Michael\'s future can be as bright as any mother can hope.<br /></span>";}
if (name == "Michael") {var message = "<img src='themes/red/images/stories/Doris_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Doris and Michael</h2><b class='country'>Ghana</b>Almost 2 years ago, (RED) met Doris and her son Michael, who was born HIV-negative thanks to treatment that prevented the transmission of HIV during pregnancy.  Today, Doris is more vibrant than ever.  \"Life is good to me.  Now I\'m healthy...  taking my drugs...  I\'m taking good care of myself and my boy,\" she says, beaming with pride.  At the top of his class at school, it seems Michael\'s future can be as bright as any mother can hope.<br /></span>";}
if (name == "Dorcas") {var message = "<img src='themes/red/images/stories/Dorcas_and_Jason_large.jpg'   width='470'/><span class='littlemessage'><h2>Dorcas and Jason</h2><b class='country'>Ghana</b>Dorcas says of her ARV treatment:  \"ARVs help you live long...  you become strong, you can do your work, you can go anywhere you want to go.\"  ARVs, as part of prevention of mother-to-child transmission treatment, helped ensure her son Jason could be born healthy and HIV-free.<br /></span>";}
if (name == "Jason") {var message = "<img src='themes/red/images/stories/Dorcas_and_Jason_large.jpg'   width='470'/><span class='littlemessage'><h2>Dorcas and Jason</h2><b class='country'>Ghana</b>Dorcas says of her ARV treatment:  \"ARVs help you live long...  you become strong, you can do your work, you can go anywhere you want to go.\"  ARVs, as part of prevention of mother-to-child transmission treatment, helped ensure her son Jason could be born healthy and HIV-free.<br /></span>";}
if (name == "Catherine") {var message = "<img src='themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>";}
if (name == "Esther") {var message = "<img src='themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>";}
if (name == "Soloman") {var message = "<img src='themes/red/images/stories/Catherine_Esther_and_Soloman_large.jpg'   width='470'/><span class='littlemessage'><h2>Catherine, Esther and Soloman</h2><b class='country'>Ghana</b>Catherine is a proud mother of three beautiful children.  Her eldest child is HIV-positive, however thanks to treatment that prevents the transmission of HIV from moms to their babies, Esther and Soloman, her two youngest children (pictured here) are HIV-negative.<br /></span>";}
if (name == "Christina") {var message = "<img src='themes/red/images/stories/Christina_and_Bani_large.jpg'   width='470'/><span class='littlemessage'><h2>Christina and Bani</h2><b class='country'>Ghana</b>Christina is HIV-positive, but her daughter Bani is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Christina receives treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants.<br /></span>";}
if (name == "Bani") {var message = "<img src='themes/red/images/stories/Christina_and_Bani_large.jpg'   width='470'/><span class='littlemessage'><h2>Christina and Bani</h2><b class='country'>Ghana</b>Christina is HIV-positive, but her daughter Bani is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Christina receives treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants.<br /></span>";}
if (name == "Daniel") {var message = "<img src='themes/red/images/stories/Daniel_large.jpg'   width='470'/><span class='littlemessage'><h2>Daniel</h2><b class='country'>Ghana</b>Daniel is HIV-positive and receives treatment at Tema General Hospital, funded by (RED)-financed Global Fund grants.<br /></span>";}
if (name == "Deborah") {var message = "<img src='themes/red/images/stories/Deborah_Ashley_and_Desmond_large.jpg'   width='470'/><span class='littlemessage'><h2>Deborah Ashley and Desmond</h2><b class='country'>Ghana</b>Like many women living in the world’s poorest regions, it wasn’t until Deborah Ashley became pregnant with her son, Desmond, that she learned she was HIV-positive. Luckily for both of them, Deborah was diagnosed early and was carefully coached through her treatment.  Months later, she gave birth to a healthy, HIV-negative baby.  \"When I heard he was negative, I laughed, I cried, and I sang praises...  with the help of my medication, Desmond was negative.\"<br /></span>";}
if (name == "Desmond") {var message = "<img src='themes/red/images/stories/Deborah_Ashley_and_Desmond_large.jpg'   width='470'/><span class='littlemessage'><h2>Deborah Ashley and Desmond</h2><b class='country'>Ghana</b>Like many women living in the world’s poorest regions, it wasn’t until Deborah Ashley became pregnant with her son, Desmond, that she learned she was HIV-positive. Luckily for both of them, Deborah was diagnosed early and was carefully coached through her treatment.  Months later, she gave birth to a healthy, HIV-negative baby.  \"When I heard he was negative, I laughed, I cried, and I sang praises...  with the help of my medication, Desmond was negative.\"<br /></span>";}
if (name == "Florence") {var message = "<img src='themes/red/images/stories/Florence_and_Happy_large.jpg'   width='470'/><span class='littlemessage'><h2>Florence and Happy</h2><b class='country'>Ghana</b>As a proud mother of two HIV-negative sons, Florence didn\'t think she could get any happier.  Yet when Florence became pregnant with her third child, all of her dreams came true - a healthy, HIV-free baby girl.  Florence named her Happy, after her own elation, and spoke triumphantly when she said, \"Happy is negative.\"<br /></span>";}
if (name == "Happy") {var message = "<img src='themes/red/images/stories/Florence_and_Happy_large.jpg'   width='470'/><span class='littlemessage'><h2>Florence and Happy</h2><b class='country'>Ghana</b>As a proud mother of two HIV-negative sons, Florence didn\'t think she could get any happier.  Yet when Florence became pregnant with her third child, all of her dreams came true - a healthy, HIV-free baby girl.  Florence named her Happy, after her own elation, and spoke triumphantly when she said, \"Happy is negative.\"<br /></span>";}
if (name == "Gladys") {var message = "<img src='themes/red/images/stories/Gladys_large.jpg'   width='470'/><span class='littlemessage'><h2>Gladys</h2><b class='country'>Ghana</b>Gladys is a role model for men and women living with HIV.  Through the Models of Hope program at Korle Bu Clinic, funded by (RED)-financed Global Fund grants, Gladys counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.<br /></span>";}
if (name == "Joyce") {var message = "<img src='themes/red/images/stories/Joyce_and_Benjamin_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Benjamin</h2><b class='country'>Ghana</b>Joyce is HIV-positive, but her son Benjamin is not, thanks to treatment that prevents HIV transmission from moms to their babies.  \"I didn\'t know how to express my joy,\" Joyce said, after hearing Benjamin tested negative, \"but I knew it was because of the drugs.\" <br /></span>";}
if (name == "Benjamin") {var message = "<img src='themes/red/images/stories/Joyce_and_Benjamin_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Benjamin</h2><b class='country'>Ghana</b>Joyce is HIV-positive, but her son Benjamin is not, thanks to treatment that prevents HIV transmission from moms to their babies.  \"I didn\'t know how to express my joy,\" Joyce said, after hearing Benjamin tested negative, \"but I knew it was because of the drugs.\" <br /></span>";}
if (name == "Joyce_") {var message = "<img src='themes/red/images/stories/Joyce_and_Delali_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Delali</h2><b class='country'>Ghana</b>Joyce is a beautiful and vibrant mother of two healthy children, Mawenia and Delali, who were both born HIV-free.  \"Before I start my story,\" Joyce says, \"people don’t believe I have HIV.\"  Thanks to Joyce’s role as a public HIV/AIDS Ambassador for the Heart to Heart organization, she is able to share her story with community groups, schools, and through the media in order to spread awareness of HIV prevention and treatment and to combat stigma through changing public perception of what it means to be HIV-positive.  As Joyce puts it, \"I\'d rather come out and say it myself... to help reduce stigma... this is how HIV is.\" <br /></span>";}
if (name == "Delali") {var message = "<img src='themes/red/images/stories/Joyce_and_Delali_large.jpg'   width='470'/><span class='littlemessage'><h2>Joyce and Delali</h2><b class='country'>Ghana</b>Joyce is a beautiful and vibrant mother of two healthy children, Mawenia and Delali, who were both born HIV-free.  \"Before I start my story,\" Joyce says, \"people don’t believe I have HIV.\"  Thanks to Joyce’s role as a public HIV/AIDS Ambassador for the Heart to Heart organization, she is able to share her story with community groups, schools, and through the media in order to spread awareness of HIV prevention and treatment and to combat stigma through changing public perception of what it means to be HIV-positive.  As Joyce puts it, \"I\'d rather come out and say it myself... to help reduce stigma... this is how HIV is.\"<br /></span>";}
if (name == "Marvis") {var message = "<img src='themes/red/images/stories/Marvis_and_Grace_large.jpg'   width='470'/><span class='littlemessage'><h2>Marvis and Grace</h2><b class='country'>Ghana</b>Marvis is HIV-positive, but her daughter Grace is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Marvis received this treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants. <br /></span>";}
if (name == "Grace") {var message = "<img src='themes/red/images/stories/Marvis_and_Grace_large.jpg'   width='470'/><span class='littlemessage'><h2>Marvis and Grace</h2><b class='country'>Ghana</b>Marvis is HIV-positive, but her daughter Grace is not, thanks to treatment that prevents HIV transmission from moms to their babies.  Marvis received this treatment at Tema General Hospital, which is funded by (RED)-financed Global Fund grants. <br /></span>";}
if (name == "Motselisi") {var message = "<img src='themes/red/images/stories/Motselisi_large.jpg'   width='470'/><span class='littlemessage'><h2>Motselisi</h2><b class='country'>Lesotho</b>Before beginning ARV treatment, Motselisi, at eleven months-old, weighed almost the same as the day she was born.  Within only 90 days of treatment, we saw what is called the Lazarus Effect, as ARV drugs brought Motselisi back to life.  Today, nearly four years later, Motselisi continues her treatment and is a happy, healthy, and playful young girl. <br /></span>";}
if (name == "Patience") {var message = "<img src='themes/red/images/stories/Patience_and_Emilia_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience and Emilia</h2><b class='country'>Ghana</b>Patience and Emilia have a lot to smile about.  Thanks to treatment that helps prevent mother-to-child transmission of HIV, Emilia was born HIV-free. <br /></span>";}
if (name == "Emilia") {var message = "<img src='themes/red/images/stories/Patience_and_Emilia_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience and Emilia</h2><b class='country'>Ghana</b>Patience and Emilia have a lot to smile about.  Thanks to treatment that helps prevent mother-to-child transmission of HIV, Emilia was born HIV-free. <br /></span>";}
if (name == "Patience_") {var message = "<img src='themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>";}
if (name == "Simon") {var message = "<img src='themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>";}
if (name == "Ann") {var message = "<img src='themes/red/images/stories/Patience_Simon_and_Ann_large.jpg'   width='470'/><span class='littlemessage'><h2>Patience, Simon and Ann</h2><b class='country'>Ghana</b>\"I want everything in life,\" says Patience, and thanks to ARV treatment, her dreams are becoming reality.  Patience feels strong, her dress shop is thriving, and best of all, her children, Simon and Ann, were born HIV-free. <br /></span>";}
if (name == "Rita") {var message = "<img src='themes/red/images/stories/Rita_Simon_and_Simona_large.jpg'   width='470'/><span class='littlemessage'><h2>Rita, Simon and Simona</h2><b class='country'>Ghana</b>Rita\'s HIV-positive, but her children Simon and Simona are not, thanks to treatment that prevents mother-to-child transmission of HIV.   Today, she\'s as strong as ever and able to support her healthy family.  \"I continue to take the drugs. It\'s been three years now and I\'ve never felt ill.\" <br /></span>";}
if (name == "Simona") {var message = "<img src='themes/red/images/stories/Rita_Simon_and_Simona_large.jpg'   width='470'/><span class='littlemessage'><h2>Rita, Simon and Simona</h2><b class='country'>Ghana</b>Rita\'s HIV-positive, but her children Simon and Simona are not, thanks to treatment that prevents mother-to-child transmission of HIV.   Today, she\'s as strong as ever and able to support her healthy family.  \"I continue to take the drugs. It\'s been three years now and I\'ve never felt ill.\" <br /></span>";}
if (name == "Ruth") {var message = "<img src='themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>";}
if (name == "Benedicta") {var message = "<img src='themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>";}
if (name == "Michael_") {var message = "<img src='themes/red/images/stories/Ruth_Sam_Benedicta_and_Michael_large.jpg'   width='470'/><span class='littlemessage'><h2>Ruth, Benedicta and Michael</h2><b class='country'>Ghana</b>When we met Ruth nearly 2 years ago, she was a proud mother to Benedicta, born HIV-free, and was hoping to expand her family.  Today, thanks to treatment that prevents the transmission of HIV from moms to their babies, Ruth has a beautiful and healthy baby boy, Michael.  Ruth beams, \"When he tested negative I was happy. I was very happy.\" <br /></span>";}
if (name == "Stella") {var message = "<img src='themes/red/images/stories/Stella_and_Davis_large.jpg'   width='470'/><span class='littlemessage'><h2>Stella and Davis</h2><b class='country'>Ghana</b>As a role model in the Models for Hope program at Korle Bu clinic, Stella counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.   As she had done for her son Davis, Stella helps to ensure that future generations of children are born HIV-free. <br /></span>";}
if (name == "Davis") {var message = "<img src='themes/red/images/stories/Stella_and_Davis_large.jpg'  width='470'/><span class='littlemessage'><h2>Stella and Davis</h2><b class='country'>Ghana</b>As a role model in the Models for Hope program at Korle Bu clinic, Stella counsels those diagnosed with HIV and helps them to understand HIV/AIDS and the treatments available.   As she had done for her son Davis, Stella helps to ensure that future generations of children are born HIV-free. <br /></span>";}




jQuery('body').prepend('<div id="overlay"> </div>');  
jQuery('body').prepend('<div id="notebox" class="storybox">'+ message +'<br class="clear"/> <span class="littlemessagebuttons" style="position:relative;"><img src="../../../images/redresults_small.png" style="box-shadow: 0 0 0px #333333; position:absolute; top:0px; left:5px;"/><a href="#" class="closebutton buttonminor">Close</a></span></div>');
jQuery('#overlay, .closebutton').click( function(){jQuery('#overlay, #notebox').remove(); return false;});

}







