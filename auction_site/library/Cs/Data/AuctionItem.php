<?php
/**
 * Description of Cs_Data_AuctionItem (AuctionItem.php)
 * @package Crowdsurge White Label Auction site
 * @category Data
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Cs_Data_AuctionItem
{
    private $auction_item_row_data;

    private $auction_name;
    private $auction_custom_ref;
    private $auction_description;
    private $auction_status_id;
    private $campaign_id;
    private $campaign_name;

    private $item_id;
    private $auction_id;
    private $custom_ref;
    private $type_id;
    private $type;
    private $name;
    private $description;
    /** @var DateTime $start_date **/
    private $start_date;
    /** @var DateTime $end_date **/
    private $end_date;
    private $timezone_id;
    private $timezone;
    private $currency_id;
    private $max_bid_increment;
    private $min_bid_increment;
    private $starting_bid;
    private $reserve;
    private $display_order;

    private $end_date_gmt_equiv;

    private $highest_bidder_username;

    // currency
    private $currency;

    // Maintain the bid history
    private $bidHistory;
    private $bidPublicHistory;
    private $bid_highest;
    private $bid_highest_bid;
    private $is_awaiting_first_bid;

    // Both set at 1 unit
    const DEFAULT_STARTING_BID            = 1.00;
    const DEFAULT_MINIMUM_BID_INCREMENT   = 1.00;
    const DEFAULT_MAXIMUM_BID_INCREMENT = 100.00;

    /**
     *
     * @param Zend_Db_Table_Row $data
     * @param array $metadata
     */
    public function __construct(Zend_Db_Table_Row $data, $metadata)
    {
        if ('auctions' !== $data->getTable()) {
            //throw new Exception(__CLASS__ . ' requires row data from auctions table only.');
        }
        $this->auction_item_row_data = $data->toArray();

        $this->load($data, $metadata);
    }

    private function load(Zend_Db_Table_Row $data, $metadata)
    {
        $this->item_id           = $data->auction_item_id;
        $this->auction_id        = $data->auction_id;
        $this->custom_ref        = $data->auction_item_custom_ref;
        $this->type_id           = $data->auction_item_type_id;
        $this->type              = $metadata['type']['key'];
        $this->name              = $data->auction_item_name;
        $this->description       = $data->auction_item_description;

        $timezone = new DateTimeZone($metadata['timezone']['timezone']);

        $this->timezone_id       = $data->timezone_id;
        $this->timezone          = $timezone;

        // We store the start and end dates for the auction item as date objects
        // The timezone is included here.
        $this->start_date        = new DateTime($data->start_date, $timezone);
        $this->end_date          = new DateTime($data->end_date,   $timezone);

        // auction End Date GMT equivalent
        $this->end_date_gmt_equiv = '';


        $this->currency_id       = $data->currency_id;

        if (0.00 == $data->max_bid_increment) {
            $this->max_bid_increment = (float) self::DEFAULT_MAXIMUM_BID_INCREMENT;
        }
        else {
            $this->max_bid_increment = (float) $data->max_bid_increment;
        }

        if (0.00 == $data->min_bid_increment) {
            $this->min_bid_increment = (float) self::DEFAULT_MINIMUM_BID_INCREMENT;
        }
        else {
            $this->min_bid_increment = (float) $data->min_bid_increment;
        }

        if (0.00 == $data->starting_bid) {
            $this->starting_bid = (float) self::DEFAULT_STARTING_BID;
        }
        else {
            $this->starting_bid = (float) $data->starting_bid;
        }

        $this->reserve           = $data->reserve;
        $this->display_order     = $data->display_order;

        $this->bidHistory        = $metadata['history']['history'];
        $this->bidPublicHistory  = $metadata['public_history']['history'];
        $this->bid_highest       = $metadata['history']['highest'];
        $this->bid_highest_bid   = $metadata['history']['highest']['current_bid'];
        $this->is_awaiting_first_bid = $metadata['history']['is_awaiting_first_bid'];

        $auction = $metadata['auction'];
        $this->auction_name        = $auction['auction_name'];
        $this->auction_custom_ref  = $auction['auction_custom_ref'];
        $this->auction_description = $auction['auction_description'];
        $this->auction_status_id   = $auction['auction_status_id'];
        $this->campaign_id         = $auction['campaign_id'];
        $this->campaign_name       = $metadata['campaign']['campaign_name'];

        $this->highest_bidder_username = $metadata['highest_bidder_username'];

        // We'll do this in a slightly different way
        // rather than creating instance variables for everything.
        $this->currency = (object) $metadata['currency'];

    }

    public function getAuctionName()
    {
        return $this->auction_name;
    }

    public function getHighestBidderUsername()
    {
        return $this->highest_bidder_username;
    }

    public function getCampaignName()
    {
        return $this->campaign_name;
    }

    public function getCurrentBid()
    {
        $highest = $this->getHighestBid();
        return $highest['current_bid'];
    }

    public function getNextMinimumBid()
    {
        //If bid highest is zero, set to starting_bid
        if (empty($this->bid_highest_bid)) {
            if (!empty($this->starting_bid)) {
                return $this->starting_bid;
            }
        }
        return (double) $this->min_bid_increment + (double) $this->bid_highest_bid;
    }

    public function getItemId()
    {
        return $this->item_id;
    }

    public function getAuctionId()
    {
        return $this->auction_id;
    }

    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    public function getHighestBid()
    {
        return $this->bid_highest;
    }

    public function getCustomRef()
    {
        return $this->custom_ref;
    }

    public function getTypeId()
    {
        return $this->type_id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getStartDate($format = "Y-m-d H:i:sP")
    {
        return $this->start_date->format($format);
    }

    public function getStartDateObject()
    {
        return $this->start_date;
    }

    public function getStartDateTimestamp()
    {
        return $this->start_date->getTimestamp();
    }

    public function getEndDate($format = "Y-m-d H:i:sP")
    {
        return $this->end_date->format($format);
    }

    public function getEndDateObject()
    {
        return $this->end_date;
    }

    public function getEndDateTimestamp()
    {
        return $this->end_date->getTimestamp();
    }

    public function getTimezoneId()
    {
        return $this->timezone_id;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public function getCurrencyId()
    {
        return $this->currency_id;
    }

    public function getMaxBidIncrement()
    {
        return $this->max_bid_increment;
    }

    public function getMinBidIncrement()
    {
        return $this->min_bid_increment;
    }

    public function getStartingBid()
    {
        return $this->starting_bid;
    }

    public function getReserve()
    {
        return $this->reserve;
    }

    public function getDisplayOrder()
    {
        return $this->display_order;
    }


    /**
     *
     * @param array $history
     */
    public function setBidHistory($history)
    {
        $this->bidHistory = $history;
    }

    public function getBidHistory()
    {
        return $this->bidHistory;
    }

    public function getBidPublicHistory()
    {
        return $this->bidPublicHistory;
    }

    /**
     * UTILITY METHODS
     */

    public function calculateMinBid($currentValue = 0)
    {
        //If currentValue is zero, set to starting_bid
        if (empty($currentValue)) {
            if (!empty($this->starting_bid)) {
                return $this->starting_bid;
            }
        }
        return $this->min_bid_increment + $currentValue;
    }

    public function calculateMaxBid($currentValue)
    {
        return $this->max_bid_increment + $currentValue;
    }

    public function getTimeRemaining()
    {
        // TODO Must include TimeZone!!!
        $now = new Datetime();
        $now->setTimezone($this->timezone);

        $interval = date_diff($this->end_date, $now, false);

        return $this->getFormattedInterval($interval);
    }

    /**
     *
     * @param DateInterval $interval
     * @return boolean|string
     */
    private function getFormattedInterval(DateInterval $interval)
    {
         // We'll use a
         $parts = array(
             "y" => "Years",
             "m" => "Months",
             "d" => "Days",
             "h" => "Hours",
             "i" => "Minutes",
             "s" => "Secs",

            // "days" is also available
         );

         $format = "";
         $intervalSign = $interval->format("%R");
         // When interval sign is + the auction is over.
         if ($intervalSign === "+") {
             return false;
         }

         foreach ($parts as $intervalPart => $label) {
             if ($interval->{$intervalPart}) {
                 $format .= " %{$intervalPart} {$label}";
             }
         }
         return $interval->format($format);
    }

    public function getAuctionItemRowToArray()
    {
        return $this->auction_item_row_data;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    // Currency
    public function getCurrencySymbol()
    {
        return $this->currency->symbol;
    }

    public function getCurrencyDescription()
    {
        return $this->currency->description;
    }

    public function getTransactionCurrency()
    {
        return $this->currency->transaction_currency;
    }

    public function getCurrencyFlag()
    {
        return $this->currency->flag;
    }

    /**
     *
     * @param string $prop
     * @return mixed
     */
    public function get($prop)
    {
        return $this->$prop;
    }
}

