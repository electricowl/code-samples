<?php
/**
 * Description of Cs_Data_PaymentInformation (PaymentInformation.php)
 * Payment information provides a consistent (non-array based) means of storing
 * the data required when performing a payment.
 *
 * Defined as 'final' to prevent overriding of the class.
 *
 * @package Crowdsurge White Label Auction site
 * @category Data Object
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

final class Cs_Data_PaymentInformation
{
    const ADDRESS_BILLING  = 'billing';
    const ADDRESS_SHIPPING = 'shipping';

    // Payment requires at least the following properties
    private $_type;
    private $_userId;

    private $_billingAddressId;

    private $_firstName;
    private $_lastName;

    private $_addressLine1;
    private $_addressLine2;
    private $_townCity;
    private $_countyState;
    private $_postZipCode;
    private $_country;
    private $_countryId;

    private $_isDefault;
    private $_isActive;

    private $_cardType;
    private $_cardNumber;
    private $_startDate;
    private $_endDate;
    private $_cvnNumber;

    private $_value;

    private $_bidAmount;
    private $_currency;

    private $_email;
    private $_clientIpAddress;

    public function loadFormData(Zend_Form $form)
    {
        $values = $form->getValues();
        $this->load($values);
    }

    public function loadArrayData($data)
    {
        $this->load($data);
    }

    public function wipeCardData()
    {
        unset($this->_cardNumber);
        unset($this->_startDate);
        unset($this->_endDate);
        unset($this->_cvnNumber);
    }

    public function setCardData($type, $number, $endDate, $cvn, $startDate = null)
    {
        $this->_cardType   = $type;
        $this->_cardNumber = $number;
        $this->_endDate    = new DateTime($endDate);
        $this->_cvnNumber  = $cvn;
        $this->_startDate  = null != $startDate ? new DateTime($startDate) : $startDate;
    }

    public function getFormattedStartDate($format = "mdy")
    {
        return $this->_startDate->format($format);
    }

    public function getStartMonth()
    {
        if (null != $this->_startDate) {
            return $this->_startDate->format("m");
        }
        return null;
    }

    public function getStartYear()
    {
        if (null != $this->_startDate) {
            return $this->_startDate->format("y");
        }
        return null;
    }

    public function getFormattedEndDate($format = "mdy")
    {
        return $this->_endDate->format($format);
    }

    public function getEndMonth()
    {
        if (null != $this->_endDate) {
            return $this->_endDate->format("m");
        }
        return null;
    }

    public function getEndYear()
    {
        if (null != $this->endDate) {
            return $this->_endDate->format("y");
        }
        return null;
    }

    public function getClientIpAddress()
    {
        if (! isset($this->_clientIpAddress)) {
            $this->_clientIpAddress = $_SERVER['REMOTE_ADDR'];
        }
        return $this->_clientIpAddress;
    }

    /**
     * A bunch of magic stuff here.
     * This is a Data Transfer object with a few helper methods.
     */

    public function __get($name)
    {
        if (! empty($name)) {
            $property = "_" . $name;
            return $this->{$property};
        }
        throw new Exception(__METHOD__ . " trying to get nothing");
    }

    public function __set($name, $value)
    {
        $property = "_" . $name;
        $this->{$property} = $value;
    }

    public function __call($method, $args)
    {
        preg_match("/^(set|get)/", $method, $matched);

        if (! empty($matched)) {
            $action = $matched[1];
        }
        else {
            throw new Exception("Method does not exist. $method");
            //trigger_error("Method does not exist", E_USER_NOTICE);
            return;
        }

        $property = "_" . lcfirst(str_replace($action, '', $method));

        if (!property_exists($this, $property)) {

            throw new Exception("Method does not exist. $property");
            // trigger_error("Method does not exist", E_USER_NOTICE);
            return;
        }
        if ("set" == $action) {
            $this->{$property} = reset($args);
        }
        else if ("get" == $action) {
            return $this->{$property};
        }
    }

    private function load($data)
    {
        foreach ($data as $key => $value) {
            $prop = $this->propertify($key);
            if (property_exists($this, $prop)) {
                $this->{$prop} = $value;
            }
        }
    }

    /**
     * TODO refactor propertify() to have a better name..
     * @param type $prop eg. address_line_1
     * @return string eg _addressLine1
     */
    private function propertify($prop)
    {
        $propSplit = explode("_", $prop);
        $propRevised = array_map(
                function($var) {
                    return ucfirst($var);
                }, $propSplit);
        return "_" . lcfirst(implode('', $propRevised));
    }

}

