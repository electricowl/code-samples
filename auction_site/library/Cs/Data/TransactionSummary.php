<?php
/**
 * Description of Cs_Data_TransactionSummary (TransactionSummary.php)
 * This object carries the data required to summarise a transaction.
 * Ensures a consistent format and interface with other objects in the system
 * rather than the 'flexible' array notation.
 *
 * Defined as 'final' to prevent overriding of the class.
 *
 * @package Crowdsurge White Label Auction site
 * @category Data
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

final class Cs_Data_TransactionSummary
{

    const PAYPAL_ = '';

    private $is_success;
    private $is_error;
    private $authorisation_code;
    private $error_code;
    private $error_message;
    private $pasref;
    private $account;
    private $currency;

    public function setSuccess($success = true)
    {
        $this->is_success = $success;
    }

    public function setError($error = true)
    {
        $this->is_error = $error;
    }

    public function setAuthorisationCode($code)
    {
        $this->authorisation_code = $code;
    }

    public function setErrorCode($code)
    {
        $this->error_code = $code;
    }

    public function setErrorMessage($message)
    {
        $this->error_message = $message;
    }

    public function setPasRef($pasref)
    {
        $this->pasref = $pasref;
    }

    public function setAccount($account)
    {
        $this->account = $account;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }


    public function getSuccess()
    {
        return $this->is_success;
    }

    public function getError()
    {
        return $this->is_error;
    }

    public function getAuthorisationCode()
    {
        return $this->authorisation_code;
    }

    public function getErrorCode()
    {
        return $this->error_code;
    }

    public function getErrorMessage()
    {
        return $this->error_message;
    }

    public function getPasRef()
    {
        return $this->pasref;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function getCurrency()
    {
        return $this->currency;
    }



    public function f() {


        $transactionSummary = new stdClass();

        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            $transactionSummary->is_error           = false;
            $transactionSummary->is_success         = true;
            $transactionSummary->authorisation_code = $result["CORRELATIONID"];
            $transactionSummary->pasref             = $result["TRANSACTIONID"];

            if ($transactionCurrency == 'USD') {
                $transactionSummary->account = 'PayPal_US';
            }
        }
        else {
            $transactionSummary->is_error      = true;
            $transactionSummary->error_code    = $result["L_ERRORCODE0"];
            $transactionSummary->error_message = $result['L_LONGMESSAGE0'];
            // Note below substitutions are for: code - message
            $transactionSummary->error_text    = "Your payment has failed, please check your card details and try again (Error ID: PPD_'%s' - '%s')";
        }
    }



}

