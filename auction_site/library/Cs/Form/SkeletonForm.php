<?php

/**
 * Description of SkeletonForm
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Cs_Form_SkeletonForm extends Cs_Form_Abstract
{
    public function __construct() {
        $this->setDecorators(
                $this->getFormDecorators()
            );
    }

}

