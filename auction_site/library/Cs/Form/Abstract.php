<?php

/**
 * Description of Abstract
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
abstract class Cs_Form_Abstract extends Zend_Form
{
    protected $alternateViewScript = null;

    public function __construct($options = null) {
        parent::__construct($options);

    }

    public function getFormDecorators()
    {
        return array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div', 'class' => 'cs_form')),
            'Form'
        );
    }

    public function getFormDecoratorsTreatAsSubForm()
    {
        return array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div', 'class' => 'cs_form')),
            array('Form', array('tag' => null))
        );
    }

    public function getHiddenDecorators()
    {
        return array(
            'ViewHelper',
            //array('Description', array('tag' => 'div')),
            //'Errors',
            array('HtmlTag', array()),
            array('Label', array('tag' => null)),
        );
    }

    public function getElementDecorators($class = '')
    {
        return array(
            'ViewHelper',
            array('Description', array('tag' => 'div')),
            'Errors',
            array(
                'HtmlTag', array('tag' => 'div', 'class' => $class)),
            //array('Label', array('tag' => 'span')),
        );
    }

    public function getElementDecoratorsInline($class = '')
    {
        return array(
            'ViewHelper',
            array('Description', array('tag' => 'div')),
            'Errors',
            array(
                'HtmlTag', array('tag' => 'span', 'class' => $class)),
            //array('Label', array('tag' => 'span')),
        );
    }

    public function getElementDecoratorsWithLabel($class = 'cf')
    {
        return array(
            'ViewHelper',
            array(
                'Description', array('tag' => 'div')),
            'Errors',
            array(
                'HtmlTag', array('tag' => 'div', 'class' => $class)),
            array(
                'Label', array('tag' => 'span')),
        );
    }

    public function getElementDecoratorsInlineWithLabel($class = '')
    {
        return array(
            'ViewHelper',
            array(
                'Description', array('tag' => 'div')),
            'Errors',
            array(
                'HtmlTag', array('tag' => 'span', 'class' => $class)),
            array(
                'Label', array('tag' => 'span')),
        );
    }

    public function getSubmitDecorators()
    {
        return array(
            'ViewHelper',
            //'Description',
            //'Errors',
//            array(
//                'HtmlTag', array('tag' => 'span', 'class' => 'button')
//                )
            );

    }

    public function addSubmit($label)
    {
        $name = strtolower(str_replace(" ", "_", $label));

        $this->addElement(
            'submit',
            $name,
            array(
                'ignore' => true,
                'label'  => $label,
                'decorators' => $this->getSubmitDecorators(),
                'attribs' => array(
                    'class' => 'button'
                )
            )
        );
        $element = $this->getElement($name);
        $element->removeDecorator('Label');
    }

    /**
     * Convenience method actually wrapping a global reference.
     * This is, in theory, easier to remember.
     * @return type
     */
    protected function getDbDefaultAdapter()
    {
        return Zend_Db_Table::getDefaultAdapter();
    }

    /**
     * Set an alternate viewscript for the
     * @param string $path
     */
    public function setAlternateViewScript($path)
    {
        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => $path)),
            'Form'
        ));
    }

    public function assignDataToViewscript($key, $value = null)
    {
        $viewscript = $this->getDecorator('ViewScript');

        if (is_array($key)) {
            foreach ($key as $key1 => $value) {
                $viewscript->setOption($key1, $value);
            }
        }
        else {
            $viewscript->setOption($key, $value);
        }
    }

}

