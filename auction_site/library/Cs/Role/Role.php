<?php

/**
 * Description of Cs_Role (Role.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
abstract class Cs_Role_Role {

    const ROLE_ADMIN  = 'admin';
    const ROLE_CLIENT = 'client';
    const ROLE_USER   = 'user';

    protected $role;

    public function getRole()
    {
        return $this->role;
    }

}

