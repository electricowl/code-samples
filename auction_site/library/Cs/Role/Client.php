<?php

/**
 * Description of Client (Client.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Cs_Role_Client extends Cs_Role_Role {

    protected $role = self::ROLE_CLIENT;

}

