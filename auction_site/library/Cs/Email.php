<?php

/**
 * Description of Cs_Email
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
class Cs_Email extends Zend_Mail
{

    public function __construct($charset = 'utf8') {
        parent::__construct($charset);
    }
}

