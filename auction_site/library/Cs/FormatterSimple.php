<?php
/**
 * Description of FormatterSimple (FormatterSimple.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Cs_FormatterSimple extends Zend_Log_Formatter_Simple
{
    /**
     * This has been modified so we can generate a list of items to drop in to a translation file.
     * @param type $event
     * @return boolean
     */
    public function format($event)
    {
        $output = $this->_format;

        foreach ($event as $name => $value) {
            if ((is_object($value) && !method_exists($value,'__toString'))
                || is_array($value)
            ) {
                $value = gettype($value);
            }

            $value = str_replace("Untranslated message within 'en':", '', $value);
            if ('' == trim($value)) {
                continue;
            }
            $value = trim($value);
            $translateString = "\"$value\" => \"$value\",";

            $output = str_replace("%$name%", $translateString, $output);
        }
        if (trim($output) === '%message%') {
            return false;
        }
        return $output;
    }
}

