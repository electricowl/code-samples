<?php

/**
 * Description of AbstractActionController
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 */
abstract class Cs_Controller_AbstractActionController extends Zend_Controller_Action
{
    protected $is_loggedin    = null;
    protected $auth_username  = null;
    protected $auth_userid    = null;
    // client information
    protected $auth_is_client = null;
    protected $auth_staff_id  = null;
    protected $auth_client_id = null;
    protected $_redirector    = null;

    const NS_BID_DATA          = 'bid_data_temp_storage';
    const NS_LOGIN             = 'login_temp_storage';
    const NS_PREAUTH           = 'preauth_temp_storage';
    const NS_NAVIGATION        = 'navigation_storage';
    const NS_CS_ACCESS_CONTROL = 'access_control_storage';

    public function init()
    {
        parent::init();

        $auth = Zend_Auth::getInstance();
        $identity = $auth->hasIdentity();
        $this->is_loggedin       = $identity;
        $this->view->is_loggedin = $identity;
        $identity = null;

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();

            $this->auth_username = $identity->username;
            $this->view->auth_username = $identity->username;

            $this->auth_is_client = false;
            $this->auth_is_user   = false;
            $this->auth_is_admin  = false;

            if (isset($identity->staff_id)) {
                $this->auth_is_client = true;
                $this->auth_staff_id  = $identity->staff_id;
                $this->auth_client_id = $identity->client_id;

                $this->view->auth_is_client = true;
                $this->view->auth_staff_id  = $identity->staff_id;
                $this->view->auth_client_id = $identity->client_id;

                $this->auth_userid       = null;
                $this->view->auth_userid = null;
            }
            else if (isset($identity->user_id)) {
                $this->auth_is_user      = true;
                $this->auth_userid       = isset($identity->user_id) ? $identity->user_id : null;
                $this->view->auth_userid = isset($identity->user_id) ? $identity->user_id : null;
            }
            else if (isset($identity->admin_id)) {
                $this->auth_is_admin      = true;
                $this->auth_adminid       = $identity->admin_id;
                $this->view->auth_adminid = $identity->admin_id;
            }
        }

        $this->_redirector = $this->_helper->getHelper('Redirector');

    }

    public function preDispatch() {
        parent::preDispatch();

        // This forces the page to go to the holding page if
        // app_theme is null
        $controller = $this->_request->getControllerName();
        if ('admin' !== $controller) {
            if (null == Zend_Registry::get('app_theme')) {
                $uri = $this->getRequest()->getRequestUri();
                if ('/index/holding-page' !== $uri) {
                    $this->_redirector->gotoSimple(
                        'holding-page',
                        'index'
                    );
                }
            }
        }
    }

    public function postDispatch()
    {
        // Flash message - ensure it turns up on the page if it has been set
        // during the action menthod.
        $message = $this->getFlashMessage();
        if (count($message)) {
            $this->view->flashMessage = $message;
        }

        // This is essential for the themes to work.
        //$this->themeSwitcher();

        parent::postDispatch();
    }

    /**
     * Flash Messages are a simple (abstracted) way of sending messages to the
     * next page requested.
     * @param string $message
     */
    public function setFlashMessage($message)
    {
        $this->_helper->flashMessenger->addMessage($message);
    }

    /**
     * Fetch the flash message stored in the queue
     * @return array
     */
    public function getFlashMessage()
    {
        return $this->_helper->flashMessenger->getMessages();
    }

    protected function fetchNamespace($ns)
    {
        return new Zend_Session_Namespace($ns);
    }

    protected function saveTempStoreData($ns, $data, $hops = null, $seconds = null)
    {
        $session = $this->fetchNamespace($ns);
        $session->data = $data;

        if (null != $hops) {
            $session->setExpirationHops((int) $hops);
        }

        if (null != $seconds) {
            $session->setExpirationSeconds($seconds);
        }

        return $session;
    }

    protected function fetchTempStoreData($ns)
    {
        $session = $this->fetchNamespace($ns);
        if ($session->data) {
            return $session->data;
        }

        return null;
    }

    protected function redirectToPrevious()
    {
        $params = array();
        $controller = 'campaigns';
        $action     = 'index';

        // Need to go back where we came from
        $session = new Zend_Session_Namespace(self::NS_NAVIGATION);
        if (isset($session->controller)) {
            $controller = $session->controller;
            $action     = $session->action;
            $params     = $session->params;

            $form_params = isset($params['form_params']) ? $params['form_params'] : null;
            if (isset($params['form_params'])) {
                unset($params['form_params']);
            }
            $session->setExpirationHops(1);
        }

        $this->_helper->redirector->gotoSimple(
            $action,
            $controller,
            null,
            $params
        );
    }

    public function noLayout()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function noRender()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function useAlternateLayout($alternateLayout)
    {
        $this->_helper->layout->setLayout($alternateLayout);
    }
}

