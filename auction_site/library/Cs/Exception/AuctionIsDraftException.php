<?php

/**
 * Description of AuctionIsDraftException (AuctionIsDraftException.php)
 * @package Crowdsurge White Label Auction site
 * @category Services
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Cs_Exception_AuctionIsDraftException extends Exception
{

}

