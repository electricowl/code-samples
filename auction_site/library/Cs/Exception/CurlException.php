<?php

/**
 * Description of CurlException
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Cs_Exception_CurlException extends Zend_Http_Client_Exception
{
    public function __construct($msg, $code = '0', Exception $previous = null)
    {
        parent::__construct($msg, $code, $previous);

    }


}

