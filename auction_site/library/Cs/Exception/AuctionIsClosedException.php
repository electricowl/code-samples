<?php

/**
 * Description of Cs_Exception_AuctionIsClosedException (AuctionIsClosedException.php)
 * @package Crowdsurge White Label Auction site
 * @category Exceptions
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
class Cs_Exception_AuctionIsClosedException extends Exception
{

}

