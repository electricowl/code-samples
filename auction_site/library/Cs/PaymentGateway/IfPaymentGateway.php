<?php

/**
 * Description of IfPaymentGateway
 * Interface to create the contact for the runAuthorisation() method
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
interface Cs_PaymentGateway_IfPaymentGateway {

    /**
     *
     * @param array $data
     * @param string $transactionCurrency
     * @return \stdClass
     */
    public function runAuthorisation(Cs_Data_PaymentInformation $data, $transactionCurrency);
}

