<?php

/**
 * Description of PayPalDirect
 * 'NVP' is PayPal's abbreviation for Name Value Pair
 * originally based on the pay-paypal_direct.php file found in the
 * /live/presale/includes folder in the legacy codebase.
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012 Crowdsurge Ltd
 */
class Cs_PaymentGateway_PayPalDirect extends Cs_PaymentGateway_Abstract implements Cs_PaymentGateway_IfPaymentGateway
{

    const API_VERSION = '64';

    const API_BUTTONSOURCE_ECWIZARD = 'PP-ECWizard';

    const API_DIRECT_PAYMENT_ACTION_SALE = 'Sale';
    const API_METHOD_DODIRECTPAYMENT     = 'DoDirectPayment';
    const API_METHOD_PAYMENT_ACTION_ORDER = 'Order';

    // Values for use in the US transactions
    const API_USERNAME_US   = 'paypal.us_api1.crowdsurge.com';
    const API_PASSWORD_US   = 'XN78XGT5V8PB2BD5';
    const API_SIGNATURE_US  = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AIJTFUwVig4yf7xFIysju1x1BWVu';

    // Rest of World (ROW)
    const API_USERNAME_ROW  = 'ashley.dexter_api1.crowdsurge.com';
    const API_PASSWORD_ROW  = '5XPCFZSDTZQZRDNX';
    const API_SIGNATURE_ROW = 'AS6ZFys9FZC6uMrGRn.4fma1tTNlAImjvJLoRVvy7RlDAFckrm.toZ3Y';

    const API_ENDPOINT = 'https://api-3t.paypal.com/nvp';

    // Debug settings
//  //Values for use in the US transactions
    const API_USERNAME_US_DEBUG   = 'USMerc_1358348261_biz_api1.crowdsurge.com';
    const API_PASSWORD_US_DEBUG   = '1358348297';
    const API_SIGNATURE_US_DEBUG  = 'AF9xzpFaarvL2kMJNCfe24XtFyIHAi-koOu.jQHC8QT0sqSHuKgwiT.K';

    // Rest of World (ROW)
    const API_USERNAME_ROW_DEBUG  = 'UKmerc_1358348028_biz_api1.crowdsurge.com';
    const API_PASSWORD_ROW_DEBUG  = '1358348059';
    const API_SIGNATURE_ROW_DEBUG = 'AGOpXBdkGf80e0XYB.iVOqSigMRvAeupjjuREajAxmnUQ7S7CnGmGfiC';

    const API_ENDPOINT_DEBUG = 'https://api-3t.sandbox.paypal.com/nvp';

    /**
     * This is where we will run the authorisation/payment procedure.
     * Data is passed in from the controller.
     * Any exceptions will be captured and rethrown or dealt with as application
     * errors.
     *
     * @param Cs_Data_PaymentInformation $data
     * @param string $transactionCurrency
     * @return \stdClass
     */
    public function runAuthorisation(Cs_Data_PaymentInformation $data, $transactionCurrency, $paymentAction = self::API_DIRECT_PAYMENT_ACTION_SALE)
    {
        // grab data
        // 1. Perform a few string substitutions for the
        $street = str_replace('&', 'and', $data->getAddressLine1() . ' ' . $data->getAddressLine2());
        $city   = str_replace('&', 'and', $data->getTownCity());
        $state  = str_replace('&', 'and', $data->getCountyState());
        $zip    = str_replace('&', 'and', $data->getPostZipCode());

        // 2. Build the Name Value Pair request
        $requestData = $this->populateQueryData(
                $paymentAction,
                $data->getBidAmount(),
                $data->getCurrency(),
                $data->getCardType(),
                $data->getCardNumber(),
                $data->getStartMonth(),
                $data->getStartYear(),
                $data->getEndMonth(),
                $data->getEndYear(),
                $data->getCvnNumber(),
                $data->getFirstName(),
                $data->getLastName(),
                $street,
                $city,
                $state,
                $zip,
                $data->getCountry(),
                $data->getEmail(),
                $data->getClientIpAddress()
            );

        $bError = false;
        try {
            // Direct payment call
            $result = $this->executeCallToPayPal(
                self::API_METHOD_DODIRECTPAYMENT,
                $requestData,
                $transactionCurrency
            );
        }
        catch(Exception $e) {
            if (get_class($e) == 'Cs_Exception_CurlException') {
                $bError = true;
                error_log($e->getCode() . " : " . $e->getMessage());
            }
        }

        // Now we have our transaction result to process
        $ack = strtoupper($result["ACK"]);

        // We'll summarise the transaction here.
        $transactionSummary = new stdClass();

        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            $transactionSummary->ack                = $ack;
            $transactionSummary->is_error           = false;
            $transactionSummary->is_success         = true;
            $transactionSummary->authorisation_code = $result["CORRELATIONID"];
            $transactionSummary->pasref             = $result["TRANSACTIONID"];
            $transactionSummary->transaction_status_id = Auction_Model_DataGateway_Transactions::STATUS_CONFIRMED;

            if ($transactionCurrency == 'USD') {
                $transactionSummary->account = 'PayPal_US';
            }

            $transactionSummary->error_code    = null;
            $transactionSummary->error_message = null;
        }
        else {
            // Transaction failure
            $transactionSummary->is_error      = true;
            $transactionSummary->is_success    = false;
            $transactionSummary->error_code    = $result["L_ERRORCODE0"];
            $transactionSummary->error_message = $result['L_LONGMESSAGE0'];
            $transactionSummary->transaction_status_id = Auction_Model_DataGateway_Transactions::STATUS_FAILED;

            // Note below substitutions are for: code - message
            $transactionSummary->error_text    =
                    sprintf("Your payment has failed, please check your card details and try again (Error ID: PPD_'%s' - '%s')",
                            $result["L_ERRORCODE0"],
                            $result['L_LONGMESSAGE0']
                        );

            $transactionSummary->authorisation_code = null;
        }

        return $transactionSummary;
    }


    /**
     *
     * @param type $methodName
     * @param type $nvpRequest
     * @param type $transactionCurrency
     * @return array
     * @throws Cs_Exception_CurlException
     */
    private function executeCallToPayPal($methodName, $nvpRequest, $transactionCurrency)
    {
        if ($transactionCurrency === 'USD') {
		$apiUsername  = PAYPAL_DEBUG ? self::API_USERNAME_US_DEBUG : self::API_USERNAME_US;
		$apiPassword  = PAYPAL_DEBUG ? self::API_PASSWORD_US_DEBUG : self::API_PASSWORD_US;
		$apiSignature = PAYPAL_DEBUG ? self::API_SIGNATURE_US_DEBUG : self::API_SIGNATURE_US;
	}
	else {
		$apiUsername  = PAYPAL_DEBUG ? self::API_USERNAME_ROW_DEBUG : self::API_USERNAME_ROW;
		$apiPassword  = PAYPAL_DEBUG ? self::API_PASSWORD_ROW_DEBUG : self::API_PASSWORD_ROW;
		$apiSignature = PAYPAL_DEBUG ? self::API_SIGNATURE_ROW_DEBUG : self::API_SIGNATURE_ROW;
	}

        $parameters = $this->populateCredentials(
                $methodName,
                $apiPassword,
                $apiUsername,
                $apiSignature
            );

        $parameters = array_merge($parameters, $nvpRequest);

        // This is the bit that does the work for us
        // The adapter/client is preconfigured for PayPal Direct
        // As long as the correct parameters are fed in then we will have a
        // Client that we can simply call request() on.
        // The response contains our data for success/failure
        $httpClient = Cs_Adapter_HttpCurl::factory(
                Cs_Adapter_HttpCurl::PAYPAL_DIRECT,
                PAYPAL_DEBUG ? self::API_ENDPOINT_DEBUG : self::API_ENDPOINT,
                array(
                    'parameters' => $parameters
                )
            );

        $response = $httpClient->request();

        //convrting NVPResponse to an Associative Array
	$nvpResultArray = $this->nvpStringToArray($response->asString());
	$nvpRequestArray = $this->nvpStringToArray($nvpRequest);

        /** @var resource $curlHandle **/
        $curlHandle = $httpClient->getAdapter()->getHandle();

        if (curl_errno($curlHandle)) {
            // We'll throw the exception here and handle it in the Exception
            // Handler/error page.
            throw new Cs_Exception_CurlException(
                    curl_error($curlHandle),
                    curl_errno($curlHandle)
                );
	}
	else {
            // Behave responsibly and close the cUrl connection
            $httpClient->getAdapter()->close();
	}

	return $nvpResultArray;
    }

    /**
     * //This method was provided by PayPal
     * Decomposes a request string into an array.
     * This may have been provided because the PHP parse_str() function has a
     * bad habit of substituting '.' for '_' which potentially causes issues.
     * @param type $nvpstr
     * @return array $nvp
     */
    private function nvpStringToArray($nvpstr)
    {
        if (is_string($nvpstr)) {
            parse_str($nvpstr, $result);
        }
        else {
            return $nvpstr;
        }

        return $result;
    }

    /**
     * The signature a bit excessive but pretty straightforward
     * Returned is a query string for use in the cUrl request to the PayPal
     * endpoint.
     * @param string $transTotal
     * @param string $transCurrency
     * @param string $cardType
     * @param string $cardNumber
     * @param string $startMonth
     * @param string $startYear
     * @param string $expireMonth
     * @param string $expireYear
     * @param string $cvn
     * @param string $firstName
     * @param string $lastName
     * @param string $street
     * @param string $city
     * @param string $state
     * @param string $zip
     * @param string $countryCode
     * @param string $custEmail
     * @param string $custIp
     * @return string
     */
    private function populateQueryData($paymentAction, $transTotal, $transCurrency, $cardType, $cardNumber, $startMonth, $startYear, $expireMonth, $expireYear, $cvn,
            $firstName, $lastName, $street, $city, $state, $zip, $countryCode, $custEmail, $custIp)
    {
        $queryData = array(
            'AMT'            => $transTotal,
            'CURRENCYCODE'   => $transCurrency,
            'PAYMENTACTION'  => $paymentAction,
            'CREDITCARDTYPE' => $cardType,
            'ACCT'           => $cardNumber,
            //'STARTDATE'      => $startMonth . $startYear,
            'EXPDATE'        => $expireMonth . "20" . $expireYear,
            'CVV2'           => $cvn,
            'FIRSTNAME'      => $firstName,
            'LASTNAME'       => $lastName,
            'STREET'         => $street,
            'CITY'           => $city,
            'STATE'          => $state,
            'ZIP'            => $zip,
            'COUNTRYCODE'    => $countryCode,
            'EMAIL'          => $custEmail,
            'IPADDRESS'      => $custIp,
        );

        return $queryData;
    }

    public function buildNvpData($queryData)
    {
        return http_build_query($queryData);
    }

    /**
     * This builds the basic authentication for the PayPal API
     * @param string $methodName
     * @param string $apiPassword
     * @param string $apiUsername
     * @param string $apiSignature
     * @return type
     */
    private function populateCredentials($methodName, $apiPassword, $apiUsername, $apiSignature)
    {
        // These are the fields for POST which have been decomposed into an array
        $queryData = array(
            'METHOD'       => urlencode($methodName),
            'VERSION'      => self::API_VERSION,
            'PWD'          => urlencode($apiPassword),
            'USER'         => urlencode($apiUsername),
            'SIGNATURE'    => urlencode($apiSignature),
            'BUTTONSOURCE' => self::API_BUTTONSOURCE_ECWIZARD
        );

        return $queryData;
    }
}

