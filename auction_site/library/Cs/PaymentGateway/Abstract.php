<?php
/**
 * Description of Cs_PaymentGateway_Abstract (Abstract.php)
 * @package Crowdsurge White Label Auction site
 * @category PaymentGateways
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

abstract class Cs_PaymentGateway_Abstract implements Cs_PaymentGateway_IfPaymentGateway
{

    abstract public function runAuthorisation(Cs_Data_PaymentInformation $data, $transactionCurrency);
}

