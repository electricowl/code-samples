<?php
/**
 * Description of Cs_PaymentGateway_Payment (Payment.php)
 * @package Crowdsurge White Label Auction site
 * @category PaymetGateways
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */

class Cs_PaymentGateway_Payment
{
    const PAYPAL_DIRECT          = 'paypal_direct';
    const PAYPAL_EXPRESS         = 'paypal_express';
    const REALEX                 = 'realex';

    const GATEWAY_PAYPAL_DIRECT  = 'Cs_PaymentGateway_PayPalDirect';
    const GATEWAY_PAYPAL_EXPRESS = 'Cs_PaymentGateway_PayPalExpress';
    const GATEWAY_REALEX         = 'Cs_PaymentGateway_RealEx';

    private static $gateways = array(
        self::PAYPAL_DIRECT  => self::GATEWAY_PAYPAL_DIRECT,
        self::PAYPAL_EXPRESS => self::GATEWAY_PAYPAL_EXPRESS,
        self::REALEX         => self::GATEWAY_REALEX
    );

    /**
     *
     * @param string $gateway
     * @return Cs_PaymentGateway_Abstract
     */
    public static function create($gateway)
    {
        $selectedGw = self::$gateways[$gateway];

        return new $selectedGw;
    }

    private function __construct()
    {
        // prevent instantiation of the class
    }
}

