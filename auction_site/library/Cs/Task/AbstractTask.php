<?php
/**
 * Description of AbstractTask (AbstractTask.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

abstract class Cs_Task_AbstractTask implements Cs_Task_ITask
{
    const DO_LOGGING = true;

    const STATUS_READY    = 'ready';
    const STATUS_RUNNING  = 'running';
    const STATUS_COMPLETE = 'complete';
    const STATUS_ERROR    = 'error';

    const MESSAGE_ACTION      = 'action';
    const MESSAGE_INFORMATION = 'info';
    const MESSAGE_EXCEPTION   = 'exception';

    protected $_messages = array();

    protected $data = array();

    protected $logfile;

    /**
     * Constructor can be overridden if required
     * @param type $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Implement this and place all your command logic in here.
     * When Task::run() is called everything in commands() will be executed.
     */
    abstract protected function commands();

    public function run()
    {
        $this->status = self::STATUS_RUNNING;
        // We have the bid data so we need to process it...
        try {

            $this->commands();

        }
        catch (Exception $e) {
            $this->status = self::STATUS_ERROR;
            $this->_messages['exception'] = $e->getMessage();

        }

        if (self::STATUS_ERROR !== $this->status) {
            // Processing completed
            $this->status = self::STATUS_COMPLETE;
        }

        return $this->_messages;

    }

    public function status()
    {
        return $this->status;
    }

    protected function addMessage($text, $action = self::MESSAGE_ACTION)
    {
        $this->_messages[$action][] = $text;
    }

    protected function getMessages()
    {
        return $this->_messages;
    }

    public function log($message, $destination = null)
    {
        if (self::DO_LOGGING) {
            error_log($message . "\n", 3, $this->logfile);
        }
    }

    public function sendEmail($data)
    {
        $outbid_email_task = new Cs_Task_EnQueueEmail($data);
        $outbid_email_task->run();
        unset($outbid_email_task);
    }

    public function sendMessage($user_id, $message_text, $extra = array(), $template = null)
    {

        //Append auction item ID to filter to just messages relating to that item for the user
        $queue_name_append = '';
        if (!empty($extra)) {
            if (isset($extra['auction_item_id'])) {
                $queue_name_append = '_item_'.$extra['auction_item_id'];
            }
        }

        $queue_name = "user_{$user_id}".$queue_name_append;

        $queue = new Auction_Service_QueueManager(
            Auction_Service_QueueManager::QUEUE_NAME_USER,
            $queue_name
        );

        $item_data = array(
            'recipient_id' => $user_id,
            'message_id'   => "bid_message_{$user_id}",
            'timestamp'    => microtime(true),
            'subject'      => "User Message",
            'body'         => $message_text,
            'user_id'      => $user_id,
        ) + $extra;



        // @todo Fill this message HTML out.
        if (null !== $template) {
            // Get some HTML

            $item_data['html'] = '';
        }

        $queue->addItemToQueue($item_data);
    }

}

