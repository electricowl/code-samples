<?php
/**
 * Description of Cs_Task_EnQueueEmail (EnQueueEmail.php)
 * @package Crowdsurge White Label Auction site
 * @category Tasks
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, CER Ltd (Crowdsurge)
 */

class Cs_Task_EnQueueEmail extends Cs_Task_AbstractTask
{

    /**
     * @var array Default options for Email
     */
    public $emailDefaults = array();

    /**
     * @var array Options for email template views
     */
    public $emailTypes = array(
        'auction-ended' => array(
            'subject' => 'An auction lot has ended',
            'template' => 'auction-ended',
            'template_path'   => 'emails',
            'user_bid' => true,
            'highest_bid' => false,
            'auction' => true,
        ),
        'auction-closed-bid-too-late' => array(
            'subject' => 'Unable to process bid: bid arrived after auction closed',
            'template' => 'auction-closed-bid-too-late',
            'template_path'   => 'emails',
            'user_bid' => true,
            'highest_bid' => false,
            'auction' => true,
        ),
        'auction-winner' => array(
            'subject' => 'Congratulations on winning an auction',
            'template' => 'auction-winner',
            'template_path'   => 'emails',
            'user_bid' => true,
            'highest_bid' => false,
            'auction' => true,
        ),
        'bid-outbid' => array(
            'subject' => 'You have been outbid',
            'template' => 'bid-outbid',
            'template_path'   => 'emails',
            'user_bid' => true,
            'highest_bid' => true,
            'auction' => true,
        ),
        'highest-bidder' => array(
            'subject' => 'You are the highest bidder. Good Luck!',
            'template' => 'highest-bidder',
            'template_path'   => 'emails',
            'user_bid' => true,
            'highest_bid' => false,
            'auction' => true,
        ),
        'registration-complete' => array(
            'subject' => 'Registration complete',
            'template' => 'registration-complete',
            'template_path'   => 'emails',
            'user_bid' => false,
            'highest_bid' => false,
            'auction' => false,
        ),
        'transaction-confirmation' => array(
            'subject' => 'Your payment has been confirmed',
            'template' => 'transaction-confirmation',
            'template_path'   => 'emails',
            'user_bid' => false,
            'highest_bid' => false,
            'auction' => false,
        ),
        'transaction-failure' => array(
            'subject' => 'Notice: payment failure for auction',
            'template' => 'transaction-failure',
            'template_path'   => 'emails',
            'user_bid' => false,
            'highest_bid' => false,
            'auction' => false,
        ),
        'reset-password' => array(
            'subject' => 'Your new login in information for Red Auction',
            'template' => 'reset-password',
            'template_path'   => 'emails',
            'user_bid' => false,
            'highest_bid' => false,
            'auction' => false,
        ),
    );

    /**
     * Builds Data array for object
     *
     * @param array $data
     */
    public function __construct($data) {

        // get items from config
        $this->emailDefaults = array(
            'site_url' => Zend_Registry::get('config')->email->siteUrl,
            'sender_name' => Zend_Registry::get('config')->email->senderName,
            'sender_email' => Zend_Registry::get('config')->email->senderEmail,
            'reply_to_name' => Zend_Registry::get('config')->email->replyToName,
            'reply_to_email' => Zend_Registry::get('config')->email->replyToEmail,
        );

        // merge custom options
        $this->data = array_merge($this->emailDefaults, $data);

        // get Specifics for template
        $this->data['subject'] = $this->emailTypes[$data['template']]['subject'];
        $this->data['template_path'] = $this->emailTypes[$data['template']]['template_path'];
        $this->data['template'] = $this->emailTypes[$data['template']]['template'];

        // get user details
        $user = new Auction_Model_DataGateway_Users();
        $userLookup = $user->getUserById($data['recipient_user_id']);
        $this->data['recipient_name'] = $userLookup->current()->first_name . ' ' . $userLookup->current()->last_name;
        $this->data['recipient_email'] = $userLookup->current()->email;

        // get auction lot data
        if ($this->emailTypes[$data['template']]['auction']) {
            $itemService = new Auction_Service_AuctionItem();
            $itemObj = $itemService->getItemById($data['item_id'])->toArray();

            // does require the user's bid
            if ($this->emailTypes[$data['template']]['user_bid']) {
                foreach ($itemObj['bidHistory'] as $bid) {
                    // match on user ID
                    if ((int) $bid['user_id'] === (int) $data['recipient_user_id']) {
                        $this->data['user_bid'] = $bid;
                        break;
                    }
                }
            }

            // does require the highest bid
            if ($this->emailTypes[$data['template']]['highest_bid']) {
                 $this->data['highest_bid'] = $itemObj['bid_highest'];
            }

            unset($itemObj['bidHistory']);
            unset($itemObj['bid_highest']);

            $this->data['auction'] = $itemObj;
        }

    }

    protected function commands()
    {
        $queue = new Auction_Service_QueueManager(
            Auction_Service_QueueManager::QUEUE_NAME_EMAIL
        );

        $queue->addItemToQueue($this->data);
    }

}
