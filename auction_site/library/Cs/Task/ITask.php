<?php
/**
 * Description of Task (Task.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

interface Cs_Task_ITask
{
    public function run();

    //protected function commands();

}

