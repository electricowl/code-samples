<?php
/**
 * Description of ProcessBid (ProcessBid.php)
 * @package Crowdsurge White Label Auction site
 * @category Controllers
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

class Cs_Task_ProcessBid extends Cs_Task_AbstractTask
{
    protected $status;

    protected $userId;
    protected $bidData;
    protected $auctionId;

    /** @var Auction_Model_DataGateway_Bids $bids **/
    protected $bids;
    protected $auctionService;
    protected $itemService;

    protected $logfile = '/var/opt/debugging/process_bid_log';

    // This is the server time of the DB Record.
    // all other timestamps are ignored.

    protected $bidReceivedDate;

    protected $_currentHighBid = null;

    public function __construct($auctionId, $userId, $bidData)
    {
        $this->status = self::STATUS_READY;
        // We are creating this from a timestamp. Note the @
        $messageDate = new DateTime("@" . (int) $bidData->bid_received_timestamp,
                new DateTimeZone(date_default_timezone_get())
            );
        $this->bidReceivedDate = $messageDate;

        $this->userId     = $userId;
        $this->bidData    = $bidData;
        $this->auctionId  = $auctionId;
        $this->log(__METHOD__);
    }

    private function getCurrentHighBid()
    {
        if (null == $this->_currentHighBid) {
            $this->_currentHighBid = $this->auctionService->fetchAndLockCurrentHighBid(
                (int) $this->bidData->item_id
            );
        }
        return $this->_currentHighBid;
    }

    protected function commands()
    {
        $this->log(__METHOD__);
        $format = 'Y-m-d H:i:sP';

        // Set up our objects
        $this->auctionService = new Auction_Service_Auction();
        $this->itemService = new Auction_Service_AuctionItem();

        // @todo This breaks the model (should be a service call)
        $this->bids = new Auction_Model_DataGateway_Bids();

        // Fetch the information we need
        $auctionId       = $this->auctionId;
        $userId          = $this->userId;
        $itemId          = $this->bidData->item_id;
        $client_id       = $this->bidData->client_id;

        // Defines the (secret) upper limit of the bid
        $incomingMaxBid     = $this->bidData->bid_amount;
        $transactionId   = $this->bidData->transaction_id;

        // Convert array to stdClass for convenience.
        $auction = (object) $this->auctionService->getAuctionDetailsById(
                $this->auctionId,
                $client_id
            );
        $this->auction = $auction;

        // Get the data we need
        /** @var Cs_Data_AuctionItem $auctionItem **/
        $auctionItem = $this->itemService->getItemById(
                $itemId,
                $client_id
            );

        // check the item is valid and that the auction has been confirmed.
        if (! ($this->checkItemInTime($auctionItem) && $this->isAuctionConfirmed($auction))) {
            // LOG and exit early (the methods take care of messaging)
            $this->logBidToApplication($userId, $this->bidData, $this->_messages);
            $this->log("Item is out of time. Auction is NOT confirmed. Exit from BidProcessor.");
            return;
        }

        // So now we begin to process the bids according to the rules.
        // Is this a first bid?
        $isAwaitingFirstBid = $auctionItem->get('is_awaiting_first_bid');

        // All of this is transactional so...
        try {

            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            $currentBid = $auctionItem->getNextMinimumBid();

            if ($isAwaitingFirstBid) {
                // Create a new bid line and add to the DB immediately
                $this->log("Apply first bid to $itemId");
                $newBid = $this->createNewHighBid(
                    $itemId,
                    $userId,
                    $currentBid,
                    $incomingMaxBid
                );

            }
            else {
                $currentHighBid = $this->getCurrentHighBid();

                $this->log("Follow on bid processing");
                $this->log("({$currentHighBid->user_id} == $userId) && ( (float) {$currentHighBid->max_bid} < (float) $incomingMaxBid)");
                // sanity check -> does this bid belong to the user?
                // we will increase the maximum bid if it does.
                if (($currentHighBid->user_id == $userId) && ( (float) $currentHighBid->max_bid < (float) $incomingMaxBid)) {

                    $this->log("Increasing user max bid for userid $userId");

                    $newBid = $this->increaseUserMaximumBid(
                        $itemId,
                        $userId,
                        $auctionItem,
                        $incomingMaxBid
                    );
                }
                else {
                    // We are processing follow on bids
                    $currentBid = $auctionItem->getCurrentBid();

                    $this->log("Processing the follow on bid");

                    $newBid = $this->createAutoBid(
                        $itemId,
                        $userId,
                        $auctionItem,
                        $currentBid,
                        $incomingMaxBid
                    );
                }

            }

            $this->log("New bid line has been created: bid_id " . $newBid->bid_id);

            Zend_Db_Table::getDefaultAdapter()->commit();
        }
        catch(Exception $e) {
            $this->log("EXCEPTION:: Rolling back ; " . $e->getMessage());
            Zend_Db_Table::getDefaultAdapter()->rollBack();

            $this->addMessage(
                "Exception: " . $e->getMessage(),
                self::MESSAGE_EXCEPTION
            );

            $this->logBidToApplication($userId, $this->bidData, $this->_messages);

            throw $e;
        }

        // Finally.
        // log the bid
        $this->log("========= Item {$itemId} Processing Complete ===========");
        $this->logBidToApplication($userId, $this->bidData, $this->_messages);
    }

    public function increaseUserMaximumBid($itemId, $userId, $auctionItem, $incomingMaxBid)
    {
        $this->log(__METHOD__);
        // ## Step 1 ##
        /** @var Zend_Db_Table_Row $currentHighBid **/
        $currentHighBid = $this->getCurrentHighBid();
        $currentHighBid->locked        = null;
        $currentHighBid->bid_status_id = Auction_Model_DataGateway_Bids::BID_MAXIMUM_INCREASED;
        $currentHighBid->save();

        // Copy down
        $newBid = $this->createNewHighBid(
                $itemId,
                $userId,
                $currentHighBid->current_bid,
                $incomingMaxBid
            );

        return $newBid;
    }

    /**
     * This is the routine for creating the autobid when the incoming bid is lower
     * than the current max bid.
     * @param int $itemId
     * @param int $userId
     * @param Cs_Data_AuctionItem $auctionItem
     * @param float $currentBid
     * @param float $incomingMaxBid
     */
    public function createAutoBid($itemId, $userId, $auctionItem, $currentBid, $incomingMaxBid)
    {
        $this->log(__METHOD__);
        // ## Step 1 ##
        /** @var Zend_Db_Table_Row $currentHighBid **/
        $currentHighBid = $this->getCurrentHighBid();
        $currentHighBid->locked        = null;
        $currentHighBid->bid_status_id = Auction_Model_DataGateway_Bids::BID_OUTBID;
        $currentHighBid->save();

        if ((float) $incomingMaxBid <= (float) $currentHighBid->max_bid) {

            $newCurrentBid = (float) $incomingMaxBid + (float) $auctionItem->getMinBidIncrement();

            // ## Step 2 ##
            // Create row that has been instantly outbid
            // We copy down the data from the
            $outbidRow = $this->createInstantOutbidEntry(
                $itemId,
                $userId,
                $currentHighBid->current_bid,
                $incomingMaxBid
            );

            // However..we cannot exceed the original maximum bid in these circumstances
            if ((float) $incomingMaxBid == (float) $currentHighBid->max_bid) {
                $newCurrentBid = (float) $currentHighBid->max_bid;
            }

            // ## Step 3 ##
            // Create the new high bid row. (From the original)
            $newHighBidRow = $this->createNewHighBid(
                    $itemId,
                    $currentHighBid->user_id,
                    $newCurrentBid,
                    $currentHighBid->max_bid
                );
        }
        else {
            // Getting here implies that $incomingMaxBid is > $currentHighBid->max_bid
            // So we simply create a new Row with the incoming bidder's data
            $newCurrentBid = (float) $currentHighBid->max_bid + (float) $auctionItem->getMinBidIncrement();

            $newHighBidRow = $this->createNewHighBid(
                     $itemId,
                    $userId,
                    $newCurrentBid,
                    $incomingMaxBid
                );
        }

        return $newHighBidRow;
    }

    public function createInstantOutbidEntry($itemId, $userId, $currentBid, $incomingMaxBid, $transactionId = null)
    {
        $this->log(__METHOD__);
        // Create and store the failed bid
        $outbidRow = $this->bids->createBid(
            $itemId,
            $userId,
            $currentBid,
            $incomingMaxBid,
            $transactionId,
            Auction_Model_DataGateway_Bids::BID_OUTBID_INSTANTLY
        );

        // Need to enqueue email here..
        // @todo refactor this to a method in the Abstract class
        $outbid_email_data = array(
            'template'          => "bid-outbid",
            'auction_id'        => $this->auctionId,
            'item_id'           => $itemId,
            'recipient_user_id' => $userId,
        );
        $this->sendEmail($outbid_email_data);

        $this->sendMessage(
            $userId,
            Zend_Registry::get('messages')->bid_system->outbid,
            array(
                'auction_item_id' => $itemId,
                'auction_id'      => $this->auctionId,
                'campaign_id'     => $this->auction->campaign_id,
            )
        );

        $this->addMessage("Auction_Model_DataGateway_Bids::BID_OUTBID_INSTANTLY");

        return $outbidRow;
    }

    /**
     *
     * @param int $itemId Lot Id
     * @param int $userId obv
     * @param float $currentBid Current bid to appear as user's current bid amount
     * @param float $incomingMaxBid Bid entered by user
     * @param string $transactionId (not currently used)
     * @return type
     */
    public function createNewHighBid($itemId, $userId, $currentBid, $incomingMaxBid, $transactionId = null)
    {
        $this->log(__METHOD__);
        // Create the bid
        $newBidRow = $this->bids->createBid(
            $itemId,
            $userId,
            $currentBid, // current bid (the one which is being superseded)
            $incomingMaxBid, // bid being placed.
            $transactionId,
            Auction_Model_DataGateway_Bids::BID_CURRENT_HIGHEST
        );
        // Need to enqueue email here..
        // @todo refactor this to a method in the Abstract class
        $top_bid_email_data = array(
            'template'           => "highest-bidder",
            'auction_id'         => $this->auction->auction_id,
            'item_id'            => $itemId,
            'recipient_user_id'  => $userId,
        );
        $this->sendEmail($top_bid_email_data);

        $this->sendMessage(
            $userId,
            Zend_Registry::get('messages')->bid_system->bid_highest,
            array(
                'auction_item_id' => $itemId,
                'auction_id'      => $this->auction->auction_id,
                'campaign_id'     => $this->auction->campaign_id,
            )
        );

        $this->log("New bid record has been added to db..");
        $this->addMessage("Auction_Model_DataGateway_Bids::BID_CURRENT_HIGHEST");

        return $newBidRow;
    }

    private function isAuctionConfirmed($auction)
    {
        $this->log(__METHOD__);
        if ($auction->auction_status_id == Auction_Model_DataGateway_Auctions::STATUS_CONFIRMED) {
            return true;
        }
        else {
            if ($auction->auction_status_id === Auction_Model_DataGateway_Auctions::STATUS_DRAFT) {
                $this->addMessage(
                    "Trying to add bid to Draft Auction",
                    self::MESSAGE_EXCEPTION
                );
                $this->log("This is a Draft Auction!");

                //throw new Cs_Exception_AuctionIsDraftException("Trying to add bid to Draft Auction");
            }
            else {
                $this->addMessage(
                    "Cannot bid against an auction that is closed",
                    self::MESSAGE_EXCEPTION
                );
                $this->log("This is a closed auction!");
                //throw new Cs_Exception_AuctionIsClosedException("Cannot bid against an auction that is closed");
            }
        }
        return false;

    }

    private function checkItemInTime($auctionItem)
    {
        $this->log(__METHOD__);
        $format = 'Y-m-d H:i:sP';
        // Messy bit here but we must establish the timings to decide on
        // what to do with the bids.
        // We will get the GMT Timestamp for both dates and as long as
        // the following: bidReceived_timestamp < auctionClose_timestamp
        // then we will submit the bid.
        $itemEndDate = gmdate($format, $auctionItem->getEndDateTimestamp());
        $bidDate     = gmdate($format, $this->bidReceivedDate->getTimestamp());

        // Now convert to date time objects...
        $oEnd = new DateTime($itemEndDate);
        $oBid = new DateTime($bidDate);

        if ($oEnd < $oBid) {
            // Bid fails as it is out of time

            // Need to enqueue email here..
                // @todo refactor this to a method in the Abstract class
                $auction_closed = array(
                    'template'           => "auction-closed-bid-too-late",
                    'auction_id'         => $this->auction->auction_id,
                    'item_id'            => $itemId,
                    'recipient_user_id'  => $userId,
                );
                $this->sendEmail($auction_closed);
                $this->sendMessage(
                    $userId,
                    Zend_Registry::get('messages')->bid_system->bid_late_auction_closed,
                    array(
                        'auction_item_id' => $itemId,
                        'auction_id'      => $this->auction->auction_id,
                        'campaign_id'     => $this->auction->campaign_id,
                    )
                );

                $this->addMessage("Out of time..");
                return false;
        }
        return true;
    }

    private function logBidToApplication($user_id, $bid_data, $messages)
    {
        $ip = $bid_data->user_ip;
        $ua = $bid_data->user_agent;

        unset($bid_data->user_ip);
        unset($bid_data->user_agent);

        Auction_Service_Logger::log(
            Auction_Service_Logger::LOG_BID,
            array(
                'event_date' => new Zend_Db_Expr("NOW()"),
                'user_id'    => $user_id,
                'category'   => 'ProcessBid',
                'ip_address' => $ip,
                'user_agent' => $ua,
                // app_method is __METHOD__
                'app_method' => __METHOD__,
                'metadata'   => json_encode(array(
                    'user_id'         => $user_id,
                    'bid_data'        => $bid_data,
                    'status_messages' => $messages,
                )),
                // Leave off 'message' because Zend_Log knows about that field
                // It is the last parameter in the log() signature
                //'message'    => "Log Testing"
            ),
            "Raw Bid Processing Record");
    }
}

