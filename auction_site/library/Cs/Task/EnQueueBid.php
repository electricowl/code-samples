<?php
/**
 * Description of Cs_Task_EnQueueBid (EnQueueBid.php)
 * @package Crowdsurge White Label Auction site
 * @category Tasks
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, CER Ltd (Crowdsurge)
 */

class Cs_Task_EnQueueBid extends Cs_Task_AbstractTask
{
    private $user;

    public function __construct($auctionId, $userId, $bidData, $client_id)
    {

        $this->data['auctionId'] = $auctionId;
        $this->data['userId']    = $userId;
        
        $bidData['client_id']   = $client_id;

        $this->data['bidData']   = $bidData;

    }

    protected function commands()
    {
        $queue = new Auction_Service_QueueManager(
                Auction_Service_QueueManager::QUEUE_NAME_BID
            );

        // Add a timestamp as late as possible.
        $this->data['bidData']['queued_at_timestamp'] = microtime(true);
        $queue->addItemToQueue($this->data);
    }

}

