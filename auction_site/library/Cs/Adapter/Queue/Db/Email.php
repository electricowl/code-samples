<?php

/**
 * Description of Cs_Adapter_Queue_Email (Email.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Cs_Adapter_Queue_Db_Email extends Zend_Queue_Adapter_Db
{

    /**
     * Constructor
     *
     * @param  array|Zend_Config $options
     * @param  Zend_Queue|null $queue
     * @return void
     */
    public function __construct($options, Zend_Queue $queue = null)
    {
        Zend_Queue_Adapter_AdapterAbstract::__construct($options, $queue);

        if (!isset($this->_options['options'][Zend_Db_Select::FOR_UPDATE])) {
            // turn off auto update by default
            $this->_options['options'][Zend_Db_Select::FOR_UPDATE] = false;
        }

        if (!is_bool($this->_options['options'][Zend_Db_Select::FOR_UPDATE])) {
            require_once 'Zend/Queue/Exception.php';
            throw new Zend_Queue_Exception('Options array item: Zend_Db_Select::FOR_UPDATE must be boolean');
        }

        if (isset($this->_options['dbAdapter'])
            && $this->_options['dbAdapter'] instanceof Zend_Db_Adapter_Abstract) {
            $db = $this->_options['dbAdapter'];
        } else {
            $db = $this->_initDbAdapter();
        }

        $this->_queueTable = new Zend_Queue_Adapter_Db_Queue(array(
            'db' => $db,
        ));

        $this->_messageTable = new Cs_Adapter_Queue_Db_Message_Email(array(
            'db' => $db,
        ));

    }


    public function setMessageTableAdapter(Zend_Queue_Adapter_Db_Message $adapter) {
        $this->_messageTable = $adapter;
    }

}

