<?php

/**
 * Description of Bid (Bid.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Cs_Adapter_Queue_Db_Message_Bid extends Zend_Queue_Adapter_Db_Message
{

    /**
     * @var string
     */
    protected $_name = 'bid_messages';

}

