<?php

/**
 * Description of Generic (Generic.php)
 * @package Crowdsurge White Label Auction site
 * @category Class (Unassigned)
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */
class Cs_Adapter_Queue_Db_Message_Generic extends Zend_Queue_Adapter_Db_Message
{

    // Consider this a facade to the original class.

}

