<?php

/**
 * Description of HttpCurl
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) Expression year is undefined on line 6, column 21 in Templates/Scripting/PHPClass.php., Crowdsurge Ltd
 */
class Cs_Adapter_HttpCurl {

    const ZEND_ADAPTER = 'Zend_Http_Client_Adapter_Curl';

    const STANDARD = 'standard';

    const REALEX = 'realex';
    const PAYPAL_DIRECT   = 'paypal_direct';
    const PAYPAL_EXPRESS = 'paypal_express';

    private function __construct() {
        // prevent instantiation of the class by setting this private
    }

    /**
     *
     * @param string $type
     * @param string $uri
     * @param array $options
     * @return \Zend_Http_Client
     */
    public static function factory($type = self::STANDARD, $uri, $options = array())
    {
        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client();

        $client->setAdapter($adapter);
        $adapter->setConfig(array(
                    'curloptions' => self::getDefaultOptions($type)
                )
            );
        // Add in the options
        if (isset($options['curl']) && count($options['curl'])) {
            foreach($options['curl'] as $option => $value) {
                $adapter->setCurlOption($option, $value);
            }
            unset($value);
            unset($option);
        }

        if (isset($options['parameters']) && count($options['parameters'])) {
            foreach($options['parameters'] as $param => $value) {
                $client->setParameterPost($param, $value);
            }
        }

        $client->setUri($uri);
        $client->setMethod('POST');

        return $client;
    }

    /**
     *
     * @param string $type
     * @return array cUrl options
     */
    private static function getDefaultOptions($type)
    {
        $defaults = array(

            self::STANDARD => array(
                CURLOPT_FOLLOWLOCATION => true,
            ),

            self::PAYPAL_DIRECT => array(
                //CURLOPT_URL             => null,
                CURLOPT_VERBOSE         => 0,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_RETURNTRANSFER  => 1,
                CURLOPT_POST            => 1,
                //CURLOPT_POSTFIELDS      => $nvpreq
            )
        );

        return $defaults[$type];
    }

}

