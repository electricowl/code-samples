<?php
/**
 * Recommended absolute maximum for PHP translation array is 5000!!
 * Note all of the translations are loaded so huge arrays will make this
 * rather cumbersome.
 * If it gets anywhere near that big we need to be using
 * gettext (.pot/.po/.mo files)
 *
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2012, Crowdsurge Ltd
 */
return array(
    'Welcome' => 'Welcome',
    'Auction Site' => 'Auction Site',
    'Confirm Bid'  => 'Confirm Bid',
    'A bid amount is required' => 'A bid amount is required',

    //
    'Username is required' => 'Username is required',
    'Password is required' => 'Password is required',
    'Logged in as' => 'Logged in as',
    'You are not logged in' => 'You are not logged in',
    'login' => 'login',
    'logout' => 'logout',

    // Forms
    'Card Type' => 'Card Type',
    'Select the type of card you will be using' => 'Select the type of card you will be using',
    'Card Number' => 'Card Number',
    'Enter the long card number here' => 'Enter the long card number here',
    'Expiry Month' => 'Expiry Month',
    'Expiry Year'  => 'Expiry Year',
    'Card Expiry Date' => 'Card Expiry Date',
    'CVV Code' => 'CVV Code',
    'The last three digits on the strip on the back of the card' => 'The last three digits on the strip on the back of the card',
    'Send To' => 'Send To',
    'A valid card number is required' => 'A valid card number is required',
    'Enter the 3-digit code from the strip on the back of your card' => 'Enter the 3-digit code from the strip on the back of your card',
    'Select the type of card you are using' => 'Select the type of card you are using',

    'Payment Method' => 'Payment Method',

    // Months
    'January' => 'January',
    'February' => 'February',
    'March'    => 'March',
    'April'    => 'April',
    'May'      => 'May',
    'June'     => 'June',
    'July'     => 'July',
    'August'   => 'August',
    'September' => 'September',
    'October'   => 'October',
    'November'  => 'November',
    'December'  => 'December',

    'Years'     => 'Years',
    'Months'    => 'Months',
    'Days'      => 'Days',
    'Hours'     => 'Hours',
    'Minutes'   => 'Minutes',
    'Seconds'   => 'Seconds',

    //
    'Authorisation Required' => 'Authorisation Required',
    'Place Bid' => 'Place Bid',
    'Add New' => 'Add New',
    'First Name' => 'First Name',
    'Last Name'  => 'Last Name',
    'House No./Street' => 'House No./Street',
    'Town/City' => 'Town/City',
    'County/State' => 'County/State',
    'Postcode/Zipcode' => 'Postcode',
    'Country' => 'Country',

    //
    'You may place a bid between %s%.2f and %s%.2f' => 'You may place a bid between %s%.2f and %s%.2f',
    'Time Remaining' => 'Time left',

    //
    'Please provide your first name' => 'Please provide your first name',
    'Please provide house number and street information' => 'Please provide house number and street information',
    'Please enter the Town/City here' => 'Please enter the Town/City here',
    'Please enter the County/State here' => 'Please enter the County/State here',
    'Postcode/Zipcode is required' => 'Postcode is required',
    'Country is required' => 'Country is required',

    'Placing this bid requires the amount to be reserved on your credit/debit card' => 'Placing this bid requires the amount to be reserved on your credit/debit card',

    'Please check the amount then click Submit to place your bid' => 'Please check the amount then click Submit to place your bid',
    //
    'About' => 'About',
    'Contact' => 'Contact',
    'Terms and Conditions' => 'Terms and Conditions',
    "Add New Shipping Address" => "Add New Shipping Address",

    //
    "Your bid has been placed" => "Your bid has been placed",
    "This address will be used for the card transaction" => "This address will be used for the card transaction",

    //
    "Manage Staff"          => "Manage Staff",
    "Manage Campaigns"      => "Manage Campaigns",
    "Manage Auctions"       => "Manage Auctions",
    "Manage Auction Items"  => "Manage Auction Items",

    "Custom Reference"      => "Custom Reference",
    "Campaign Name"         => "Name",
    "Campaign Description"  => "Description",

    "Auction Name"          => "Auction Name",
    "Auction Description"   => "Auction Description",

    "Client Login" => "Client Login",

    "Register" => "Register",
    "Login" => "Login",
    "Bid Now" => "Bid Now",
    "Login" => "Login",
    "Name" => "Name",
    "Description" => "Description",
    "Save" => "Save",
    "Draft" => "Draft",
    "Confirmed" => "Confirmed",
    "Closed" => "Closed",
    "Display Order" => "Display Order",
    "Auction Status" => "Auction Status",
    "Standard" => "Standard",
    "Pre Authorised" => "Pre Authorised",
    "Deposit" => "Deposit",

    "Time Zone" => "Time Zone",

    "Currency" => "Currency",
    "Maximum Bid Increment" => "Maximum Bid Increment",
    "Minimum Bid Increment" => "Minimum Bid Increment",
    "Starting Bid" => "Starting Bid",
    "Reserve Price" => "Reserve Price",

    'Be the first to bid!' => 'Be the first to bid!',

    // Using translate via tags
    'copyright_notice' => "&copy; 2012-2013 CER Ltd",
    "errors_on_form_generic" => "There were errors on the form, please correct them and submit again",

);