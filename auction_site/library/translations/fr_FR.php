<?php

return array(
    'Welcome' => 'Bienvenue',
    'Auction Site' => "site d'enchères",
    'Confirm Bid'  => 'confirment offre',
    'Logged in as' => "Connecté au site avec l'identité",
    'You are not logged in' => "Vous n'êtes pas connecté",
    'login' => 'Connectez-vous',
    'logout' => "Vous déconnecter"
);