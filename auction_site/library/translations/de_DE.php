<?php

return array(
    'Welcome'      => 'Wilkommen',
    'Auction Site' => 'Auktions Website',
    'Confirm Bid'  => 'Gebot bestätigen',
    'Logged in as' => 'angemeldet mit Benutzername',
    'You are not logged in' => 'Sie sind nicht angemeldet auf dieser Seite',
    'login' => 'einloggen',
    'logout' => 'ausloggen',

    // Forms
    'Card Type' => 'Card Type',
    'Select the type of card you will be using' => 'Wählen Sie die Art der Karte, die Sie verwenden werden',
    'Card Number' => 'Kartennummer',
    'Enter the long card number here' => 'Geben Sie die lange Kartennummer hier',
    'Expiry Month' => 'Verfallsmonat',
    'Expiry Year'  => 'Ablauf Jahre',
    'Card Expiry Date' => 'Karte Ablaufdatum',
    'CVV Code' => 'Validierungscode',
    // Months
    'January' => 'Januar',
    'February' => 'Februar',
    'March'    => 'März',
    'April'    => 'April',
    'May'      => 'Mai',
    'June'     => 'Juni',
    'July'     => 'Juli',
    'August'   => 'August',
    'September' => 'September',
    'October'   => 'Oktober',
    'November'  => 'November',
    'December'  => 'Dezember',
);