-- Add Clients
INSERT INTO `auction`.`clients` (`client_name`) VALUES ('Z Promotions');
INSERT INTO `auction`.`clients` (`client_name`) VALUES ('Y Promoters');

-- Client Staff
INSERT INTO `auction`.`client_staff` (`client_id`, `first_name`, `last_name`, `email`, `username`, `password`) VALUES ('1', 'Kid', 'Rock', 'kid.rock@crowdsurge.com', 'kidrock', 'password');
INSERT INTO `auction`.`client_staff` (`client_id`, `first_name`, `last_name`, `email`, `username`, `password`) VALUES ('1', 'Jason', 'Statham', 'jason.statham@crowdsurge.com', 'jasons', 'password');
INSERT INTO `auction`.`client_staff` (`client_id`, `first_name`, `last_name`, `email`, `username`, `password`) VALUES ('2', 'Sarah', 'Brightman', 'sb@crowdsurge.com', 'sarahb', 'password');
INSERT INTO `auction`.`client_staff` (`client_id`, `first_name`, `last_name`, `email`, `username`, `password`) VALUES ('2', 'Luc', 'Besson', 'lb@crowdsurge.com', 'lucb', 'password');

-- Campaigns
INSERT INTO `auction`.`campaigns` (`client_id`, `campaign_custom_ref`, `campaign_name`, `campaign_description`) VALUES ('1', 'customRef1', 'Turning Point', 'Get more people engaged through social media');
INSERT INTO `auction`.`campaigns` (`client_id`, `campaign_custom_ref`, `campaign_name`, `campaign_description`) VALUES ('1', 'customRef2', 'Denouement', 'The bit of the story before the climax');
INSERT INTO `auction`.`campaigns` (`client_id`, `campaign_custom_ref`, `campaign_name`, `campaign_description`) VALUES ('2', 'customRef3', 'Sleeping Beauty', 'Princess gets sleepy, wakes up, gets married');
INSERT INTO `auction`.`campaigns` (`client_id`, `campaign_custom_ref`, `campaign_name`, `campaign_description`) VALUES ('2', 'customRef4', 'Concert Tour', 'Tour the world, see the fans');

-- Auctions
INSERT INTO `auction`.`auctions` (`campaign_id`, `auction_custom_ref`, `auction_name`, `auction_description`, `auction_status_id`) VALUES ('1', 'CustomRef1', 'First Auction', 'This is the first ever auction', '1');
INSERT INTO `auction`.`auctions` (`campaign_id`, `auction_custom_ref`, `auction_name`, `auction_description`, `auction_status_id`) VALUES ('1', 'CustomRef1-2', 'Second Auction', 'This is the second auction', '1');
INSERT INTO `auction`.`auctions` (`campaign_id`, `auction_custom_ref`, `auction_name`, `auction_description`, `auction_status_id`) VALUES ('3', 'sb', 'Sleeping Beauty', 'Sleeping Beauty auction', '2');
INSERT INTO `auction`.`auctions` (`campaign_id`, `auction_custom_ref`, `auction_name`, `auction_description`, `auction_status_id`) VALUES ('4', 'Concert Tour', 'Concert Tour auction', 'This has loads of tickets ', '2');

-- Auction Items (Lot)
INSERT INTO `auction`.`auction_items` (`auction_id`, `auction_item_custom_ref`, `auction_item_type_id`, `auction_item_name`, `auction_item_description`, `start_date`, `end_date`, `timezone_id`, `currency_id`, `min_bid_increment`, `max_bid_increment`, `starting_bid`) VALUES ('1', 'auction item 1-1', '1', 'On Stage Tickets', 'You get to be on stage during the performance', '2012-12-12', '2012-12-15', '325', '1', '1.01', '5.00', '0.99');
INSERT INTO `auction`.`auction_items` (`auction_id`, `auction_item_custom_ref`, `auction_item_type_id`, `auction_item_name`, `auction_item_description`, `start_date`, `end_date`, `timezone_id`, `currency_id`, `min_bid_increment`, `max_bid_increment`, `starting_bid`) VALUES ('1', 'auction item 1-2', '1', 'On Stage Tickets', 'Be on stage with the band', '2012-12-15', '2012-12-18', '325', '1', '1.01', '5.00', '0.99');
INSERT INTO `auction`.`auction_items` (`auction_id`, `auction_item_custom_ref`, `auction_item_type_id`, `auction_item_name`, `auction_item_description`, `start_date`, `end_date`, `timezone_id`, `currency_id`, `min_bid_increment`, `max_bid_increment`, `starting_bid`) VALUES ('2', 'auction item 2-1', '2', 'Green Room tickets', 'Spend time with the band in the Green Room', '2012-11-23 15:00:00', '2012-11-30 15:00:00', '314', '2', '2.01', '10.00', '1.99');
INSERT INTO `auction`.`auction_items` (`auction_id`, `auction_item_custom_ref`, `auction_item_type_id`, `auction_item_name`, `auction_item_description`, `start_date`, `end_date`, `timezone_id`, `currency_id`, `min_bid_increment`, `max_bid_increment`) VALUES ('3', 'sleeping beauty', '1', 'Dance with the ballet company', 'A full half day session dancing on stage at Sadler\'s Wells with the company', '2012-12-05 16:40:00', '2012-12-20 09:00:00', '325', '1', '5.01', '20.00');

-- Users
INSERT INTO `auction`.`users` (`username`, `password`, `first_name`, `last_name`, `email`, `mailer_optin`) VALUES ('user1', 'password', 'Ian', 'Lewis', 'ian.lewis@crowdsurge.com', '0');
INSERT INTO `auction`.`users` (`username`, `password`, `first_name`, `last_name`, `email`, `mailer_optin`) VALUES ('ian.lewis.cs', 'password', 'Ian', 'Lewis2', 'ian.lewis@electricowl.co.uk', '0');

-- Bids (with no transaction links)
INSERT INTO `auction`.`bids` (`auction_item_id`, `user_id`, `bid_date`, `current_bid`, `max_bid`, `bid_status_id`) VALUES ('4', '1', '2012-12-10 14:03:23', '1.22', '10.00', '1');
INSERT INTO `auction`.`bids` (`auction_item_id`, `user_id`, `bid_date`, `current_bid`, `max_bid`, `bid_status_id`) VALUES ('4', '2', '2012-12-10 14:07:01', '3.22', '10.00', '2');
INSERT INTO `auction`.`bids` (`auction_item_id`, `user_id`, `bid_date`, `current_bid`, `max_bid`, `bid_status_id`) VALUES ('4', '1', '2012-12-10 14:13:32', '5.22', '10.00', '1');

-- Admin
INSERT INTO `auction`.`administrators` (`username`, `password`, `first_name`, `last_name`, `email`, `create_date`, `last_login`, `active`, `is_superadmin`) VALUES ('admin', 'Admin1', 'Administrator', '-', 'auction.admin@crowdsurge.com', '2013-01-09', '2013-01-09', '1', '1');


-- Some data changes to make the increments more reasonable.
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.05', `max_bid_increment`='0.15' WHERE `auction_item_id`='1';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.05', `max_bid_increment`='0.15' WHERE `auction_item_id`='2';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.07', `max_bid_increment`='0.17' WHERE `auction_item_id`='3';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.07', `max_bid_increment`='0.17' WHERE `auction_item_id`='4';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.09', `max_bid_increment`='0.19' WHERE `auction_item_id`='5';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.11', `max_bid_increment`='0.21' WHERE `auction_item_id`='6';
UPDATE `auction`.`auction_items` SET `min_bid_increment`='0.05', `max_bid_increment`='0.15' WHERE `auction_item_id`='7';
