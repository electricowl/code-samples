-- Ensure the DB tables are running as InnoDB and not myISAM
-- Especially important on live server
use auction;
ALTER TABLE administrators ENGINE=InnoDB;
ALTER TABLE application_log ENGINE=InnoDB;
ALTER TABLE auction_items ENGINE=InnoDB;
ALTER TABLE auction_item_types_lu ENGINE=InnoDB;
ALTER TABLE auction_status_lu ENGINE=InnoDB;
ALTER TABLE auctions ENGINE=InnoDB;
ALTER TABLE bid_status_lu ENGINE=InnoDB;
ALTER TABLE bids ENGINE=InnoDB;
ALTER TABLE campaigns ENGINE=InnoDB;
ALTER TABLE card_types_lu ENGINE=InnoDB;
ALTER TABLE client_staff ENGINE=InnoDB;
ALTER TABLE clients ENGINE=InnoDB;
ALTER TABLE countries ENGINE=InnoDB;
ALTER TABLE currency_lu ENGINE=InnoDB;
ALTER TABLE email_log ENGINE=InnoDB;
ALTER TABLE email_type_lu ENGINE=InnoDB;
ALTER TABLE emails ENGINE=InnoDB;
ALTER TABLE order_status_lu ENGINE=InnoDB;
ALTER TABLE orders ENGINE=InnoDB;
ALTER TABLE timezone_lu ENGINE=InnoDB;
ALTER TABLE transaction_status_lu ENGINE=InnoDB;
ALTER TABLE transaction_types_lu ENGINE=InnoDB;
ALTER TABLE transactions ENGINE=InnoDB;
ALTER TABLE user_billing_address ENGINE=InnoDB;
ALTER TABLE user_shipping_address ENGINE=InnoDB;
ALTER TABLE user_validated ENGINE=InnoDB;
ALTER TABLE users ENGINE=InnoDB;
