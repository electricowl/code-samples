-- Ideally this would have a UNIQUE key
ALTER TABLE `auction`.`clients`
    ADD COLUMN `client_theme_id` VARCHAR(100) NOT NULL  AFTER `client_name` ;

-- Then add the clients test data
UPDATE `auction`.`clients` SET `client_theme_id`='zpromotions' WHERE `client_id`='1';
UPDATE `auction`.`clients` SET `client_theme_id`='ypromoters' WHERE `client_id`='2';
INSERT INTO `auction`.`clients` (`client_name`, `client_theme_id`) VALUES ('RED', 'red');
INSERT INTO `auction`.`clients` (`client_name`, `client_theme_id`) VALUES ('Blue Floyd', 'bluefloyd');
INSERT INTO `auction`.`clients` (`client_name`, `client_theme_id`) VALUES ('Arcade Fire', 'arcadefire');



