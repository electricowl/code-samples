USE auction;

DROP TABLE IF EXISTS `email_log`;

delimiter $$

CREATE TABLE auction.`email_log` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `event_date` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` varchar(512) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `app_method` varchar(128) NOT NULL,
  `metadata` varchar(1024) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='maintain email log for Auctions'$$


CREATE  TABLE `auction`.`user_validated` (
  `user_id` INT NOT NULL ,
  `validation_method` VARCHAR(45) NULL ,
  `is_validated` TINYINT NULL ,
  PRIMARY KEY (`user_id`) );

