<?php

/**
 * Description of queue_item (queue_item.php)
 * @package Crowdsurge White Label Auction site
 * @category
 * @author Ian Lewis <ian.lewis@crowdsurge.com>
 * @copyright (c) 2013, Crowdsurge Ltd
 */

return array(
    "name"       => "Ian Lewis <ian.lewis@crowdsurge.com>",

    "action"     => "make_bid",
    "auction_id" => "123",
    "lot_id"     => "456",
    "currency"   => "sterling",
    "amount"     => "3.99",

    "bidder"     => "9999999",
    "ip_address" => "192.168.1.0",
);

