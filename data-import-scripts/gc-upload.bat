SET client=dom

call gsutil cp -m %client%*streams.csv gs://deezer-daily-files/

SET date1=20170315
SET date2=2017-03-15

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz


SET date1=20170317
SET date2=2017-03-17

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170318
SET date2=2017-03-18

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170319
SET date2=2017-03-19

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170320
SET date2=2017-03-20

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170321
SET date2=2017-03-21

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170322
SET date2=2017-03-22

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz

SET date1=20170323
SET date2=2017-03-23

call bq load --source_format=CSV --skip_leading_rows=1 robotic-abode-88310:deezer.%client%_streams_%date1% gs://deezer-daily-files/%client%_%date1%_streams.csv schema.json

call bq query --destination_table=robotic-abode-88310:deezer.%client%_elastic_%date1% "SELECT s.StartReport as date, s.UPC as upc, s.ISRC as isrc, s.Artist as track_artists, s.Title as track_title, s.Album as album_name, s.UPC as parent_identifier, s.Country as territory, s.Nbofplays as units FROM [robotic-abode-88310:deezer.%client%_streams_%date1%] as s"

call bq extract --destination_format=NEWLINE_DELIMITED_JSON --compression=GZIP robotic-abode-88310:deezer.%client%_elastic_%date1% gs://deezer-elastic-files/%client%_deezer_%date2%.json.gz
