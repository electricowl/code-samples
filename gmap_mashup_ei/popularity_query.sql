-- select distinct isrc, artist, title, count(*) popularity 
-- from public.sec_shazam_201708
-- group by isrc, artist, title
-- order by popularity desc
select distinct artist, count(*) popularity 
from public.sec_shazam_201708
group by artist
order by popularity desc
