<?php

include './data.php';

// Calculate MA and +/- 4% bands
$output = array();
$ranges = array();
$averages = array();
$dataout = array();
$stack = array(); // Has to be limited to length 20 push - check length - if 21 pop oldest value
foreach ($data as $index => $point) {
    // throw a value on top of the
    array_unshift($stack, $point);
    if (count($stack) > 20) {
        // drop last value
        array_pop($stack);
    }

    $average = array_sum($stack) / count($stack);
    $upper_band = round($average * 1.04, 2);
    $lower_band = round($average * 0.96, 2);

    $ranges[] = array($index, $lower_band, $upper_band);
    $averages[] = array($index, round($average));
    $dataout[] = array($index, $point);
    /*
        $output[] = array(
            'datapoint' => $point,
            'upper_band'     => $upper_band,
            'average'       => $average,
            'lower_band'     => $lower_band
        );
    */

}

//header("Content-type: application/json");
//echo json_encode($output, );
