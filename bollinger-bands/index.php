<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bollinger Bands</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="highcharts/js/highcharts.js"></script>
        <script src="highcharts/js/highcharts-more.js"></script>
        <script src="highcharts/js/modules/exporting.js"></script>
    </head>
    <body>
        <h1>Bollinger Bands</h1>
        <div id="graph" style="min-width: 310px; height: 400px; margin: 0 auto">Graph goes here</div>

        <script>
            <?php include './generate_bb.php'; ?>
            // 1. Fetch Data
            var ranges = <?php echo json_encode($ranges) ?>;
            var averages = <?php echo json_encode($averages) ?>;
            var data = <?php echo json_encode($dataout); ?>
            // 2. render

            $(function () {



    $('#graph').highcharts({

        title: {
            text: 'Bollinger Bands'
        },

        xAxis: {
            type: 'linear'
        },

        yAxis: {
            min: 7000,
            max: 10000,
            title: {
                text: null
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: ''
        },

        legend: {
        },

        series: [{
            name: 'Data points',
            data: data, // or averages but we're looking for line crossing
            zIndex: 0,
            lineColor: 'aqua',
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        },
        {
            name: 'Average',
            data: averages, // or averages but we're looking for line crossing
            zIndex: 1,
            type: 'spline',
            marker: {
                fillColor: 'pink',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        },
          {
            name: 'Range',
            data: ranges,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: 'black', //Highcharts.getOptions().colors[0],
            fillOpacity: 0.3,
            zIndex: 2
        }]
    });
});

        </script>
    </body>
</html>
